		$(document).ready(function() {
			$(".plus_image").click(function(){
				tabEventPress(this);
			});		
			$(".num_tag").click(function(){
				var tabId = $(this).attr("tab-id");
				tabEventPress('#'+tabId);
			});
		});	
		function tabEventPress(obj){
			var liId = $(obj).attr("id");
			$(obj).siblings().closest("li").hide();
			$(obj).closest("li").toggle();
			$("[tab-id="+liId+"]").closest("li").siblings().removeClass("on");
			$(obj).closest("li").siblings().removeClass('active');
			$("li.tab_value>div.campaign").css("display","none");
			$(".plus_image").css("background-image",'url("img/blue_plus.png")');
			$(obj).closest("li").toggleClass("active");
			$("li.active>div.campaign").slideDown("slow").css("display","block");
			$("li.active .plus_image").css("background-image",'url("img/minus_image.png")');
			$("[tab-id="+liId+"]").closest("li").toggleClass("on");
		}