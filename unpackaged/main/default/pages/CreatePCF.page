<apex:page controller="CreatePCFController">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pluralize/4.0.0/pluralize.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.css" />
    
    <style type="text/css">
        #processStatus .green {
        color: #360;
        }
        #processStatus .yellow {
        color: #f90;
        }
        #processStatus .gray {
        color: #ddd;
        }
        .jquery-modal.blocker {
        z-index: 9999;
        }
        
        .btn.warning {
        background: #ff6d6d;
        }
        
    </style>
    
    <apex:sectionHeader title="Professional Certification Form Approval">
        
        <apex:pageBlock title="Customer {!objContact.Name}" rendered="{!objContact != null}">
            <apex:outputPanel layout="block">Audiologist Name: <a href="/{!objContact.Health_Care_Professional__c}" target="{!objContact.Health_Care_Professional__c}">{!objContact.Health_Care_Professional__r.Name}</a></apex:outputPanel>
            <apex:outputPanel layout="block">Audiologist Email: {!objContact.Health_Care_Professional__r.Email}</apex:outputPanel>
            <apex:outputPanel layout="block">Audiologist Fax: {!objContact.Health_Care_Professional__r.Fax}</apex:outputPanel>
        </apex:pageBlock>
        
        <apex:outputPanel rendered="{!status.isSigned}">
            <apex:pageBlock title="Professional Certification Form Approved">
                <i class="fa fa-fw fa-thumbs-o-up green"></i> This Professional Certification Form has been signed.<br/><br/>
                <apex:outputPanel rendered="{!objPcfApproval.PCF_Attachment_Id__c != null}">
                    <i class="fa fa-fw fa-file-pdf-o"></i> 
                    <a href="/servlet/servlet.FileDownload?file={!objPcfApproval.PCF_Attachment_Id__c}" target="_blank">Professional Certification Form</a><br/>
                </apex:outputPanel>
                <br/>
                <apex:form >
                    <apex:commandButton value="Return to Contact" action="{!backToContactDetail}"/>
                </apex:form>
            </apex:pageBlock>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!status.isSigned == false}">
            <apex:form >
                <apex:actionFunction name="updateContactMetadata" action="{!updateContactMetadata}" reRender="out,step2" oncomplete="updateContactMetadataComplete();"/>
                <apex:actionFunction name="mergePcfDocument" action="{!mergePcfDocument}" reRender="out,step2" oncomplete="mergePcfDocumentComplete();"/>
                <apex:actionFunction name="saveMergedPcfAttachment" action="{!saveMergedPcfAttachment}" reRender="out,step2" oncomplete="saveMergedPcfAttachmentComplete();"/>
                <apex:actionFunction name="createPcfApproval" action="{!createPcfApproval}" reRender="out,step2" oncomplete="createPcfApprovalComplete();"/>
                <apex:actionFunction name="setIsPrepared" action="{!setIsPrepared}" reRender="out,step2" oncomplete="setIsPreparedComplete();"/>
                <apex:actionFunction name="createEslPackage" action="{!createEslPackage}" reRender="out,actions" oncomplete="createEslPackageComplete();"/>
                <apex:actionFunction name="goToEmailForm" action="{!goToEmailForm}"/>
                <apex:actionFunction name="sendFax" reRender="out,actions" action="{!sendFax}" oncomplete="updateFaxesSent();"/>
                
                <apex:pageBlock title="Process PCF Approval">
                    <apex:pageMessages id="showmsg"></apex:pageMessages>
                    
                    <div id="pcf" class="container" style="display:none;">
                        <div id="processStatus" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12" style="display:none;">
                            <div id="updateContactMetadata"></div>
                            <div id="mergePcfDocument"></div>
                            <div id="pcfApproval"></div>
                            <div id="createEslPackage"></div>
                            <div id="sentFax"></div>
                            <br/>
                            <div id="emailsSent"></div>
                            <div id="faxesSent"></div>
                        </div>
                        <div class="errors">
                            <div id="eslMessages" class="message errorM4 error alert alert-danger"></div>
                            <div class="commandButtons" id="isContactAlreadyAssigned">
                                <apex:commandButton value="Continue" 
                                                    action="{!updateContactMetadata}" 
                                                    reRender="out,step2" 
                                                    onclick="updateContactMetadataProcessing();" 
                                                    oncomplete="updateContactMetadataComplete();"
                                                    />
                                <apex:commandButton value="Return to Queue" 
                                                    action="{!backToListView}"
                                                    />
                            </div>
                            <div class="commandButtons" id="isRequiredFieldsMissing">
                                <apex:commandButton value="Return to Contact" 
                                                    action="{!backToContactDetail}"
                                                    />
                            </div>
                            <div class="commandButtons" id="isWarningFields">
                                <apex:commandButton value="Continue" 
                                                    action="{!updateContactMetadata}" 
                                                    reRender="out,step2" 
                                                    onclick="updateContactMetadataProcessing();" 
                                                    oncomplete="updateContactMetadataComplete();"
                                                    />
                                <apex:commandButton value="Return to Contact"
                                                    action="{!backToContactDetail}"
                                                    />
                            </div>
                        </div>
                    </div>
                </apex:pageBlock>
                
                <apex:outputPanel id="step2">
                    <apex:outputPanel >
                        <apex:outputPanel rendered="{!status.isPrepared}">
                            <apex:pageBlock title="Review Documents">
                                
                                <apex:outputPanel layout="block" rendered="{!objPcfApproval.Instructions_Attachment_Id__c != null && status.hasMrfAttachment && status.hasRemoteEslPackage}">
                                    <i class="fa fa-fw fa-file-pdf-o"></i> 
                                    <a href="/servlet/servlet.FileDownload?file={!objPcfApproval.Instructions_Attachment_Id__c}" target="_blank">Instructions Document</a> <small>(email only)</small>
                                </apex:outputPanel>
                                <apex:outputPanel layout="block" rendered="{!objPcfApproval.MRF_Attachment_Id__c != null}">
                                    
                                    <apex:outputPanel rendered="{!status.hasMrfAttachment}">
                                        <apex:outputPanel rendered="{! !IsFile}">
                                            <i class="fa fa-fw fa-file-pdf-o"></i>
                                            <a href="/servlet/servlet.FileDownload?file={!objPcfApproval.MRF_Attachment_Id__c}"  target="!objPcfApproval.MRF_Attachment_Id__c">Medical Release Form</a>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{! IsFile}">
                                            <i class="fa fa-fw fa-file-pdf-o"></i>
                                            <a href="/lightning/r/ContentDocument/{!objPcfApproval.MRF_Attachment_Id__c}/view"  target="!objPcfApproval.MRF_Attachment_Id__c">Medical Release Form</a>
                                        </apex:outputPanel>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!status.isMrfTooBig}" styleClass="message errorM4 error alert alert-danger">
                                        <p>An <a href="/{!objPcfApproval.MRF_Attachment_Id__c}" target="{!objPcfApproval.MRF_Attachment_Id__c}">MRF Attachment</a> exists but is too large to upload.</p>
                                        <p>Please add a new attachment less than {!status.maxMrfFileSize}mb in size to the <strong><a href="/{!objContact.Medical_Release_Form__c}" target="{!objContact.Medical_Release_Form__c}">Medical Release Form</a></strong>.</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!status.isMrfInvalidContentType}">
                                        <span style="color:red; font-weight: bold;">An <a href="/{!objPcfApproval.MRF_Attachment_Id__c}" target="{!objPcfApproval.MRF_Attachment_Id__c}">MRF Attachment</a> exists but is not a supported file type ({!status.mrfContentType}).</span>
                                        <p>Please add a new attachment as a PDF or image, less than {!status.maxMrfFileSize}mb in size to the <strong><a href="/{!objContact.Medical_Release_Form__c}" target="{!objContact.Medical_Release_Form__c}">Medical Release Form</a></strong>. Supported content types: {!status.validMrfContentTypes}.</p>
                                    </apex:outputPanel>
                                    <br/>
                                </apex:outputPanel>
                                <apex:outputPanel layout="block" rendered="{!objPcfApproval.PCF_Attachment_Id__c != null && status.isPendingReview == false && status.isSigned == false}">
                                    <i class="fa fa-fw fa-file-pdf-o"></i> 
                                    <a href="/servlet/servlet.FileDownload?file={!objPcfApproval.PCF_Attachment_Id__c}" target="{!objPcfApproval.PCF_Attachment_Id__c}">Professional Certification Form</a>
                                </apex:outputPanel>
                                
                                <apex:outputPanel layout="block" rendered="{!objPcfApproval.PCF_Signed_Attachment_Id__c != null && (status.isPendingReview || status.isSigned)}">
                                    <i class="fa fa-fw fa-file-pdf-o"></i> 
                                    <strong><a href="/servlet/servlet.FileDownload?file={!objPcfApproval.PCF_Signed_Attachment_Id__c}" target="{!objPcfApproval.PCF_Signed_Attachment_Id__c}">Signed Professional Certification Form</a></strong>
                                </apex:outputPanel>
                                
                            </apex:pageBlock>
                        </apex:outputPanel>
                        
                        <apex:outputPanel rendered="{!status.isRequiredFieldsMissing == false && status.isWarningFields == false && status.isContactAssignedToCurrentUser == true}">
                            <apex:pageBlock title="Actions" id="actions">
                                
                                <apex:outputPanel layout="block" rendered="{!status.isPendingReview}">
                                    <i class="fa fa-fw fa-thumbs-o-up green"></i> This approval is pending review.
                                </apex:outputPanel>
                                <apex:outputPanel layout="block" rendered="{!status.isSigned}">
                                    <i class="fa fa-fw fa-thumbs-o-up green"></i> This approval has been signed.
                                </apex:outputPanel>
                                
                                <apex:outputPanel layout="block" rendered="{!status.collectFaxCoverRemarks == false}">
                                    <apex:outputPanel rendered="{!status.isSigned == false && status.isPendingReview == false}">
                                        <apex:commandButton value="Generate Email Package" 
                                                            action="{!createEslPackage}" 
                                                            rendered="{!status.isPrepared && status.isEmailApprovalEnabled && status.isSigned == false && status.hasRemoteEslPackage == false && status.isMrfTooBig == false && status.isMrfInvalidContentType == false}" 
                                                            onclick="createEslPackageProcessing();"
                                                            oncomplete="createEslPackageComplete();" 
                                                            reRender="out,actions" 
                                                            />
                                        <apex:commandButton value="Review & {!IF(objPcfApproval.Emails_Sent__c == 0, 'Send', 'Resend')} Signing Email" 
                                                            action="{!goToEmailForm}" 
                                                            rendered="{!status.isPrepared && status.isEmailApprovalEnabled && status.isSigned == false && status.hasRemoteEslPackage}"
                                                            />
                                        <apex:commandButton value="{!IF(objPcfApproval.Faxes_Sent__c == 0, 'Send', 'Resend')} via Fax" 
                                                            reRender="out,actions,step2"
                                                            action="{!sendFax}" 
                                                            rendered="{!status.isFaxApprovalEnabled && status.isPrepared && status.isSigned == false && status.isMrfTooBig == false && status.isMrfInvalidContentType == false}" id="btnSendFax"
                                                            />
                                        <apex:commandButton value="Regenerate PCF"
                                                            action="{!allowRegeneratePcfApproval}"
                                                            rendered="{!status.isRegenerateAllowed == false && status.isPrepared && status.isSigned == false}"
                                                            reRender="step2"
                                                            />
                                        <apex:commandButton value="Confirm Regenerate PCF" 
                                                            action="{!regeneratePcfApproval}" 
                                                            styleClass="warning" 
                                                            rendered="{!status.isRegenerateAllowed && status.isPrepared && status.isSigned == false}"
                                                            onclick="resetUI(); setContactUpdated();"
                                                            oncomplete="mergePcfDocumentProcessing(); mergePcfDocument();" 
                                                            reRender="out, step2"
                                                            />
                                    </apex:outputPanel>
                                    <apex:commandButton value="Return to Contact"
                                                        action="{!backToContactDetail}"
                                                        rendered="{!status.isPrepared}"
                                                        />
                                </apex:outputPanel>
                                
                                <!-- Fax Cover Remarks Edit -->
                                <apex:outputPanel layout="block" rendered="{!status.collectFaxCoverRemarks}">
                                    <apex:outputPanel layout="block">
                                        <h5>Cover Page Remarks:</h5><br/>
                                        <apex:inputTextarea id="faxCoverRemarks" value="{!faxCoverRemarks}" cols="120" rows="10"/>
                                    </apex:outputPanel>
                                    <apex:outputPanel layout="block">
                                        <apex:commandButton value="{!IF(objPcfApproval.Faxes_Sent__c == 0, 'Send', 'Resend')} via Fax" 
                                                            reRender="out,actions,step2"
                                                            action="{!sendFax}" 
                                                            oncomplete="sendFaxComplete();" 
                                                            onclick="sendFaxProcessing();"
                                                            rendered="{!status.isFaxApprovalEnabled && status.isPrepared && status.isSigned == false && status.isMrfTooBig == false && status.isMrfInvalidContentType == false}" id="btnSendFax2"
                                                            />
                                        <apex:commandButton value="Cancel" 
                                                            reRender="out,actions,step2"
                                                            action="{!cancelFax}" 
                                                            oncomplete="sendFaxComplete();" 
                                                            id="btnCancelFax"
                                                            />
                                    </apex:outputPanel>
                                </apex:outputPanel>
                                
                                <apex:outputPanel rendered="{!status.isPrepared == false}">
                                    Please wait...
                                </apex:outputPanel>
                                
                            </apex:pageBlock>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:outputPanel>
                
            </apex:form>
            
            <script language="JavaScript">
            var pcf = {};
            
            function hideAll() {
                jQuery('#eslMessages').empty().hide();
                jQuery('.commandButtons').hide();
            }
            
            function setContactNotUpdated() {
                jQuery('#updateContactMetadata').empty().append('<i class="fa fa-fw fa-times-circle gray"></i> Customer contact not ready.');
            }
            function setContactUpdated() {
                jQuery('#updateContactMetadata').empty().append('<i class="fa fa-fw fa-check-circle green"></i> Customer contact ready.');
            }
            function setPcfNotCreated() {
                jQuery('#mergePcfDocument').empty().append('<i class="fa fa-fw fa-times-circle gray"></i> Professional Certification Form does not exist.');
            }
            function setPcfCreated() {
                jQuery('#mergePcfDocument').empty().append('<i class="fa fa-fw fa-check-circle green"></i> Created Professional Certification Form.');
            }
            
            function setPcfApprovalNotCreated() {
                jQuery('#pcfApproval').empty().append('<i class="fa fa-fw fa-times-circle gray"></i> PCF Approval object not created.');
            }
            function setPcfApprovalCreated() {
                jQuery('#pcfApproval').empty().append('<i class="fa fa-fw fa-check-circle green"></i> PCF Approval object created.');
            }
            
            function setEslPackageNotCreated() {
                jQuery('#createEslPackage').empty().append('<i class="fa fa-fw fa-times-circle gray"></i> Email approval package not created.');
            }
            function setEslPackageCreated() {
                jQuery('#createEslPackage').empty().append('<i class="fa fa-fw fa-check-circle green"></i> Email approval package created.');
            }
            
            function setFaxNotSent() {
                jQuery('#sentFax').empty().append('<i class="fa fa-fw fa-times-circle gray"></i> Fax approval not sent.');
            }
            function setFaxDisabled() {
                jQuery('#sentFax').empty().append('<i class="fa fa-fw fa-ban gray"></i> Healthcare provider does not have a fax number.');
            }
            function setFaxSent() {
                jQuery('#sentFax').empty().append('<i class="fa fa-fw fa-check-circle green"></i> Fax approval sent.');
            }
            
            function sendFaxProcessing() {
                jQuery('#sentFax').empty().append('<i class="fa fa-fw fa-circle-o-notch fa-spin yellow"></i> Sending fax approval.');
            }
            
            function sendFaxComplete() {
                if (!pcf.lastError) {
                    updateFaxesSent();
                } else {
                    jQuery('#sentFax').empty().append('<i class="fa fa-fw fa-exclamation-circle errorMsg"></i> Error sending fax approval. ' + pcf.lastError );
                }
            }
            
            function updateEmailsSent() {
                console.log('updateEmailsSent');
                if (pcf.emailsSent > 0) {
                    jQuery('#emailsSent').empty().append(
                        '<i class="fa fa-fw fa-envelope"></i> Sent ' + 
                        pluralize('email', pcf.emailsSent, true) + '. ' + 
                        'Last emailed on ' + pcf.lastEmailed + '.'
                    );
                }
            }
            
            function updateFaxesSent() {
                console.log('updateFaxesSent');
                if (pcf.faxesSent > 0) {
                    jQuery('#faxesSent').empty().append(
                        '<i class="fa fa-fw fa-fax"></i> Sent ' + 
                        pluralize('fax', pcf.faxesSent, true) + '. ' + 
                        'Last faxed on ' + pcf.lastFaxed + '.' +
                        (pcf.faxStatus ? ('<br/><i class="fa fa-fw fa-info"></i> Fax was ' + (!pcf.faxSuccess ? 'un' : '') + 'successful. The status is "' + pcf.faxStatus + '".') : '')
                    );
                    setFaxSent();
                } else if (!pcf.isFaxApprovalEnabled) {
                    setFaxDisabled();
                }
            }
            
            // Apex Action Handling
            
            function updateContactMetadataProcessing() {
                console.log('updateContactMetadataProcessing')
                hideAll();
                jQuery('#processStatus').show();
                jQuery('#updateContactMetadata').empty().append('<i class="fa fa-fw fa-circle-o-notch fa-spin yellow"></i> Saving customer contact updates.');
            }
            function updateContactMetadataComplete() {
                console.log('updateContactMetadataComplete')
                if (pcf.isContactAlreadyAssigned && pcf.isContactAssignedToCurrentUser) {
                    setContactUpdated();
                    if (!pcf.hasPcfAttachment) {
                        mergePcfDocumentProcessing();
                        mergePcfDocument();
                    } else {
                        saveMergedPcfAttachmentComplete();
                    }
                } else {
                    jQuery('#updateContactMetadata').empty().append('<i class="fa fa-fw fa-exclamation-circle errorMsg"></i> Failed to update customer contact. ' + pcf.lastError);
                }
            }
            
            function mergePcfDocumentProcessing() {
                console.log('mergePcfDocumentProcessing')
                hideAll();
                jQuery('#processStatus').show();
                jQuery('#mergePcfDocument').empty().append('<i class="fa fa-fw fa-circle-o-notch fa-spin yellow"></i> Creating Professional Certification Form.');
            }
            
            function mergePcfDocumentComplete() {
                console.log('mergePcfDocumentComplete');
                jQuery('#mergePcfDocument').empty().append('<i class="fa fa-fw fa-circle-o-notch fa-spin yellow"></i> Saving Professional Certification Form.');
                saveMergedPcfAttachment();
            }
            
            function saveMergedPcfAttachmentComplete() {
                console.log('saveMergedPcfAttachmentComplete');
                if (pcf.hasPcfAttachment) { 
                    setPcfCreated();
                    if (!pcf.hasLocalPcfApproval){
                        jQuery('#pcfApproval').empty().append('<i class="fa fa-fw fa-circle-o-notch fa-spin yellow"></i> Preparing approval forms.');
                        createPcfApproval();
                    } else {
                        createPcfApprovalComplete();
                    }
                } else {
                    jQuery('#mergePcfDocument').empty().append('<i class="fa fa-fw fa-exclamation-circle errorMsg"></i> Failed to create Professional Certification Form. ' + (pcf.lastError || ''));
                }
            }
            
            function createPcfApprovalComplete() {
                console.log('createPcfApprovalComplete');        
                setPcfApprovalCreated();
                if (!pcf.isEmailApprovalEnabled) {
                    jQuery('#createEslPackage').empty().append('<i class="fa fa-fw fa-ban gray"></i> Healthcare provider has opted out of email approvals.');
                }
                
                if (pcf.hasRemoteEslPackage) {
                    createEslPackageComplete();
                } else {
                    setIsPrepared();
                }
            }
            
            function createEslPackageProcessing() {
                console.log('createEslPackageProcessing');
                jQuery('#createEslPackage').empty().append('<i class="fa fa-fw fa-circle-o-notch fa-spin yellow"></i> Saving approval forms.');
            }
            
            function createEslPackageComplete() {
                console.log('createEslPackageComplete');
                if (pcf.hasRemoteEslPackage) {
                    setEslPackageCreated();
                    setIsPrepared();
                } else {
                    jQuery('#createEslPackage').empty().append('<i class="fa fa-fw fa-exclamation-circle errorMsg"></i> Failed to create approval package - ' + (pcf.lastError || '') + '.');
                }
            }
            
            function setIsPreparedComplete() {
                console.log('setIsPreparedComplete');
            }
            
            </script>
            
            <apex:outputPanel id="out">
                <script language="JavaScript">
                pcf = {!jsonStatus};
                if (pcf.isPrepared) {
                    updateEmailsSent();
                    updateFaxesSent();
                }
                console.log('Reloaded pcf JSON object');
                </script>
            </apex:outputPanel>
            
            <script language="JavaScript">
            function resetUI() {
                hideAll();
                if (!pcf.isValidContactId){
                    jQuery('#eslMessages').append('<i class="fa fa-fw fa-exclamation-triangle errorMsg"></i> Please choose a valid customer contact.').show();
                } else
                    if (pcf.isContactAlreadyAssigned && !pcf.isContactAssignedToCurrentUser){
                        jQuery('#eslMessages').append('<i class="fa fa-fw fa-exclamation-triangle errorMsg"></i> This customer contact is already assigned to a different user.').show();
                        jQuery('#isContactAlreadyAssigned').show();
                    } else
                        if (pcf.isRequiredFieldsMissing){
                            jQuery('#eslMessages').append(pcf.requiredFieldsMessage).show();
                            jQuery('#isRequiredFieldsMissing').show();
                        } else
                            if (pcf.isWarningFields){
                                jQuery('#eslMessages').append(pcf.warningFieldsMessage).show();
                                jQuery('#isWarningFields').show();
                            } else {
                                // show the status
                                jQuery('#processStatus').show();
                            }
                
                setContactNotUpdated();
                setPcfNotCreated();
                setPcfApprovalNotCreated();
                setEslPackageNotCreated();
                setFaxNotSent();
                
                jQuery('#pcf').show(); 
            }
            
            resetUI();
            
            // If everything is valid we can start the processing
            var isAutoExecute = (!pcf.isContactAlreadyAssigned || pcf.isContactAssignedToCurrentUser) && !pcf.isRequiredFieldsMissing && !pcf.isWarningFields;
            if (isAutoExecute){
                updateContactMetadataProcessing();
                updateContactMetadata();
            }
            </script>
        </apex:outputPanel>
        
    </apex:sectionHeader>
    
</apex:page>