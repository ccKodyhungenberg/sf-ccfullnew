({
	callApex : function(component, event, helper) {
        var recordId = component.get("v.recordId");
		var action = component.get("c.getAddressRecord");
        action.setParams({ "addressId" : recordId });
        action.setCallback(this, function(response) {
        
        var parentId = '';
            var name = "parentId=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    parentId = c.substring(name.length, c.length);
                }
            }

        var state = response.getState();
        var addressRecord = response.getReturnValue();
        
		if (state === "SUCCESS") {
            /*
            if( addressRecord.Account__c != null ) {
              parentId = addressRecord.Account__c;
            }
            else if( addressRecord.Contact__c != null ) {
              parentId = addressRecord.Contact__c;
            }
            else if ( addressRecord.Asset__c != null ) {
              parentId = addressRecord.Asset__c;
            }*/
            //parentId = addressRecord.ParentId__c;
         //alert(parentId);   
         //console.log('parentIdis====' +parentId );
         var urlEvent = $A.get("e.force:navigateToURL"); 
         urlEvent.setParams({ "url":"/apex/cc_addressFlow?recordId="+parentId+"&addressId="+addressRecord.Id+"&fromComponent=true",
                            }); 
         urlEvent.fire(); 
        } 
        else {
        console.log(state);
        }
        });
        $A.enqueueAction(action);
    }
})