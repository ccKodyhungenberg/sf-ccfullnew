({
	doInit : function(component, event, helper) {
		
    
        var action = component.get("c.getCase");
        action.setParams({
            CaseId : component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var cases = a.getReturnValue();
                component.set("v.case", cases);
                var url="http://www.surveygizmo.com/s3/3016421/Seven-Day-Follow-up-Confirmation-Call?srstart="; 
				url+=cases.SR_Start_Date__c; 
				url+="&completedby=";	
				url+=cases.SR_Completed_By__c; 
				url+="&srid="; 
                url+=cases.SR_Id__c; 
                url+="&srname="; 
                url+=cases.Work_Order__c; 
                url+="&dossiercontactid="; 
                url+=cases.SR_Dossier_ContactId__c; 
                url+="&opid="; 
                url+=cases.Id; 
                url+="&opname="; 
                url+=cases.Subject; 
                console.log(url);
                if(cases.IsClosed !=true || cases.Service_Request_Status__c != "Completed" || cases.Subject !="7-Day Confirmation") {
                    alert("You can only perform Follow up survey for won 7-Day Confirmation Cases with completed Service Request"); 
                }
                    
                else if(cases.Follow_up_survey_completed__c==true) {
                    alert("Follow up survey was already completed");
                }
				else window.open(url);
            } else if (a.getState() === "ERROR") {
                console.log("oof");
            }
        });
        $A.enqueueAction(action);
   
	}
})