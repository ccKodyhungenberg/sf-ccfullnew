({
	selectVal : function(component, event, helper) {
		var target = event.target;
		var dataEle = target.getAttribute("data-selected-Index");
        console.log('selected value----'+dataEle);
        var sugList = [];
        sugList = component.get("v.suggestedAddresses");
        var isOve = component.get("v.isAddressOverride");
        console.log('selected values address----'+isOve);
        if(!isOve){
            var cmpEvent = component.getEvent("cmpEvent"); 
               cmpEvent.setParams({ 
                   "city" :  sugList[dataEle].oSuggestion.city,
                   "state":sugList[dataEle].oSuggestion.state,
                   "ZipCode" : sugList[dataEle].oSuggestion.zip,
                   "street" : sugList[dataEle].oSuggestion.street,
                   "apt" : sugList[dataEle].oSuggestion.apt,
                   "isOverride" : isOve             
               }); 
               cmpEvent.fire(); 
            component.destroy();
        }else{
             var cmpEvent = component.getEvent("cmpEvent"); 
               cmpEvent.setParams({ 
                   "isOverride" : isOve             
               }); 
               cmpEvent.fire(); 
            component.destroy();
        }
        
	},
    destroy : function(component, event, helper) {
		component.destroy();
	},
})