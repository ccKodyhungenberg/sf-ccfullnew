({
    //helper to show up the taost message
	showToast : function(component, event, helper,msg,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": msg,
            "type" : type,
            "title" : type+'!'
        });
        toastEvent.fire();
	},
    
    //dynamically create the component to reduce the overhead
    createComp : function(component, event, helper,msg) {
        component.set("v.Spinner", false);
        $A.createComponent(
            "c:Event_Suggested_Add_Comp",
            {
                "suggestedAddresses": msg
            },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    
    showConList : function(component, event, helper,msg) {
        component.set("v.Spinner", false);
        $A.createComponent(
            "c:Event_Contact_List",
            {
                "suggestedContacts": msg
            },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    
    //helper method to stringify the data and send to apex
    castData : function(component, event, helper) {
        var obj = {};
       
        obj['Email'] = $("#getet").val();
        obj['HPhone'] = $("#getHomePhone").val();
       
        obj['FirstName'] = $("#getFirstName").val();
        obj['LastName'] = $("#getLastName").val();
        obj['city'] = $("#getCity").val();
        obj['state'] = $("#getState").val();
        obj['ZipCode'] = $("#getZipCode").val();
        obj['street'] = $("#getStreet").val();
        obj['country'] = 'US';
        obj['Mphone'] = $("#getMobilePhone").val();
        obj['apt'] = $("#getApt").val();
        
        return obj;
    },
    
	tabEventPress : function (obj,tabObj){
             
			var liId = $(obj).attr("id");
            console.log('id>>>'+liId);
            $(obj).siblings().closest("li").hide();
			$(obj).closest("li").toggle();
			$(tabObj).closest("li").siblings().removeClass("on");
			$(obj).closest("li").siblings().removeClass('active');
			$("li.tab_value>div.campaign").css("display","none");
          	$(obj).closest("li").toggleClass("active");
            console.log('test1' + $(obj).closest("li").attr("class"));
        	$("li.active>div.campaign").slideDown("slow").css("display","block");
            console.log('idselected>>>'+ $("li.active>span.plus_image").attr("id"));
            $('li.tab_value span.minus_image').each(function(){
                console.log('idother>>>'+ $(this).attr("id"));
                $(this).removeClass("minus_image");
                 $(this).addClass("plus_image");
            });
            //drives the active section display of + to - image
            $("li.active>span.plus_image").addClass("minus_image");
            
			$(tabObj).closest("li").toggleClass("on")
		},
    
    addNewRow: function(component, event) {
        //get the record List from component  
        var workOrderList = component.get("v.workOrderWrapperList");
        if(workOrderList.length==5){
        	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "ERROR!",
                "message": "Cannot add more than 5 Rows",
                "type" : 'error'
                });
            toastEvent.fire();
            return;    
        }
        //var ordrLineItem = 
        //Add New  Row
        console.log('in add row-'+JSON.stringify(workOrderList) );
        var productList = component.get("v.productList");
        component.set("v.ordrLineItemAttribute",{});
        var orderLineItem = component.get("v.ordrLineItemAttribute");
        var technician = component.get("v.technician");
        component.set("v.product",{});
        var product = component.get("v.product");
        //technician.Id = undefined;
        console.log('productList'+JSON.stringify(productList));
        if(productList.length>0)
        	orderLineItem.Product2Id = productList[0].Id;
        product = productList[0];
        //workOrderWrapperList[index].ordrLineItem.Product2Id = value;
        component.set("v.workOrderAttribute",{});
        var workOrder = component.get("v.workOrderAttribute");
        console.log('workOrder--->'+JSON.stringify(workOrder));
        var startTimeWindow = component.get("v.startTimeWindow");
        workOrder.Start_Time_Window__c = startTimeWindow[0];
        workOrder.Status='Ready for Scheduling';
        //workOrder.StartDate = null;
        workOrderList.push({
            'ordrLineItem' : orderLineItem,
            'workOrder': workOrder,
            'technician' : technician,
            'product' : product,
            'productsList': component.get("v.productList"),
            'wrapperOrder': '{}'
        });
        component.set("v.confirmAppointment",false);
        console.log('in add row-after-'+JSON.stringify(workOrderList) );
        component.set("v.workOrderWrapperList", workOrderList);
    },
    
    setAppointmentHelper: function(component, event) {
        //get the record List from component  
        var workOrderList = component.get("v.workOrderWrapperList");
        var confirmAppointment = component.get("v.confirmAppointment");
        var duplicateString='';
        if(confirmAppointment == true){
            if(workOrderList.length>0){
                for(var i=0;i<workOrderList.length;i++){
                    console.log('workOrderList[i].ordrLineItem.Product2Id-'+ workOrderList[i].ordrLineItem.Product2Id);
                    console.log('workOrderList[i].technician.Id'+workOrderList[i].technician.Id);
                    console.log('workOrderList[i].workOrder.StartDate'+workOrderList[i].workOrder.StartDate);
                    console.log('workOrderList[i].workOrder.Start_Time_Window__c)'+workOrderList[i].workOrder.Start_Time_Window__c);
                    console.log('$A.util.isUndefinedOrNullworkOrderList[i].ordrLineItem.Product2Id-'+ $A.util.isUndefinedOrNull(workOrderList[i].ordrLineItem.Product2Id));
                    console.log('$A.util.isUndefinedOrNullworkOrderList[i].technician.Id'+$A.util.isUndefinedOrNull(workOrderList[i].technician.Id));
                    console.log('$A.util.isUndefinedOrNullworkOrderList[i].workOrder.StartDate'+$A.util.isUndefinedOrNull(workOrderList[i].workOrder.StartDate));
                    console.log('$A.util.isUndefinedOrNull(workOrderList[i].workOrder.Start_Time_Window__c)'+$A.util.isUndefinedOrNull(workOrderList[i].workOrder.Start_Time_Window__c));
                    
                    
                    //null check in all the list values
                    if($A.util.isUndefinedOrNull(workOrderList[i].ordrLineItem.Product2Id) || 
                       workOrderList[i].ordrLineItem.Product2Id == 'none' ||
                       //$A.util.isUndefinedOrNull(workOrderList[i].technician.Id) || 
                       $A.util.isUndefinedOrNull(workOrderList[i].workOrder.StartDate) ||
                       $A.util.isUndefinedOrNull(workOrderList[i].workOrder.Start_Time_Window__c)){
                        component.set("v.confirmAppointment",false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please fill all required fields in the Table.",
                            "type" : "error"
                            });
                        toastEvent.fire();
                        return;
                    }
                    /*var todaytime = new Date();
                    var today = Date.UTC(todaytime.getFullYear(), todaytime.getMonth()+1, todaytime.getDate());
                   

                    var workOrderDatetime = new Date(workOrderList[i].workOrder.StartDate)
                    var workOrderDate = Date.UTC(workOrderDatetime.getFullYear(), workOrderDatetime.getMonth()+1, workOrderDatetime.getDate());
                    console.log('workOrderList[i].workOrder.StartDate' +workOrderDate+today);*/
                    
                    var todDate = new Date();
                    var tFormatDate = $A.localizationService.formatDate(todDate, "yyyy MM dd");
                    var workOrderDatetime = workOrderList[i].workOrder.StartDate;
					var wwFormateDate = $A.localizationService.formatDate(workOrderDatetime, "yyyy MM dd");
                    
                    //if(today > workOrderDate){
                    if(tFormatDate>wwFormateDate){
                        console.log('inside if>>>');
                        component.set("v.confirmAppointment",false);
                       var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please select Date greater than or equal to today.",
                            "type" : "error"
                            });
                        toastEvent.fire();
                        return; 
                    }
                   
                    //check duplicate product is not added to the list
                    /*
                    if(duplicateString.includes(workOrderList[i].ordrLineItem.Product2Id)){
                       component.set("v.confirmAppointment",false);
                       var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "You cannot create multiple entries with same Product!",
                            "type" : "error"
                            });
                        toastEvent.fire();
                        return; 
                    }
                    else{
                        duplicateString = duplicateString+workOrderList[i].ordrLineItem.Product2Id +',';
                    }
                    
                     console.log('duplicateString-'+duplicateString);
                     */
                    //var workOrder = workOrderList[i].workOrder;
                    //workOrder.Status = 'Scheduled';
                    //workOrderList[i].workOrder = workOrder;
                    
                    
                }
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Add atleast one Column",
                    "type" : "error"
                });
                toastEvent.fire();
                component.set("v.confirmAppointment",false);
                return;
                
            }
            
           
        }
        
         if(component.get("v.confirmAppointment") == true){
             for(var i=0;i<workOrderList.length;i++){
                 workOrderList[i].workOrder.Status ='Scheduled';
                 
             }
             component.set("v.workOrderWrapperList",workOrderList);
             
         }
            
            	
       
    },
    
    
    saveAppoint: function(component, event, helper) {
        console.log('saveAppoint');
        var action = component.get('c.saveAppointmentWorkOrder');
         action.setParams({
             "contactId": component.get('v.contactId'),
             "listWOWrapper": component.get('v.workOrderWrapperList'),
             
            });
         action.setCallback(this,function(res){
             var state = res.getState();
            // debugger;
             if (state === "SUCCESS") {
                 var response = res.getReturnValue();
                 console.log('response>>>>>????workOrderWrapperList'+JSON.stringify(response));
                 if(response.responseMessage.includes('success')){
                     //var workOrderList = component.get("v.workOrderWrapperList");
                    
                     component.set("v.workOrderWrapperToDisplay",response.worOrderWrapperList);
                     console.log('letsseethe startdate>>>>>'+JSON.stringify(component.get('v.workOrderWrapperToDisplay')));
                     component.set("v.contact",response.contact);
                     var toastEvent = $A.get("e.force:showToast");
                     toastEvent.setParams({
                         "title": "Success!",
                         "message": "Created Appointment Successfully." ,
                         "type" : "success"
                         });
                     toastEvent.fire();
                     component.set("v.savedWorkOrder",true);
                     
                     var liId ="tab5";// id for plus image
                     var tabId = "ab5";// id for circle tab
                     var tabObj = $('#'+tabId);
                     var liObj = $('#'+liId);
                     helper.tabEventPress(liObj,tabObj);
                 }
                 else{
                     var toastEvent = $A.get("e.force:showToast");
                     toastEvent.setParams({
                         "title": "Error!",
                         "message": response.responseMessage ,
                         "type" : "error"
                         });
                     toastEvent.fire();
                     
                 }
             }
             else if (state === "ERROR") {
                //component.set("v.Spinner", false);
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
             }
             component.set("v.Spinner", false);
          
        });
        
        $A.enqueueAction(action);
    	
        
    },
    
    validateFormHelper : function(component, event, helper) {
        var validated = component.get('v.isValidated');
        var obj = {};
        var isvalid ;
    	obj = helper.castData(component,event,helper);
        console.log('input values-----'+JSON.stringify(obj))
        if(obj['Email'] == "" || (obj['HPhone'] == "" && obj['Mphone'] == "") || obj['FirstName'] == "" || obj['LastName']=="" || 
           obj['city']== "" || obj['state']=="" || obj['ZipCode']=="" || obj['street']=="" || obj['country']=="" ){
           component.set("v.EnableValidate",false);
            isvalid = false;
        }else{
            //alert('enabling valdiate')
           component.set("v.EnableValidate",true);
            isvalid = true;
        }
        return isvalid ;
	},
    
    //to validate address
    validateFormHelperAddress : function(component, event, helper) {
        var validated = component.get('v.isValidated');
        var obj = {};
        var isvalid ;
    	obj = helper.castData(component,event,helper);
        console.log('input values-----'+JSON.stringify(obj))
        if( obj['city']== "" || obj['state']=="" || obj['ZipCode']=="" || obj['street']=="" || obj['country']=="" ){
           component.set("v.EnableValidate",false);
            isvalid = false;
        }else{
            //alert('enabling valdiate')
           component.set("v.EnableValidate",true);
            isvalid = true;
        }
        return isvalid ;
	},
    
    createNewUser : function(component,event,helper,contactId){
        
        //alert('inside create user-'+ contactId);
        var action = component.get("c.createUser");
        action.setParams({"contactId": contactId}
                          );
        action.setCallback(this,function(response){
            component.set("v.Spinner", false);
            var resp = response.getReturnValue();
            if(resp.msg.includes('User created Successfully')){
                helper.showToast(component,event,helper,resp.msg,'Success');
            	helper.addNewRow(component,event);
                component.set("v.userCreated",true);
                
            	//if(buttonId == 'next3'){
                    var liId ="tab4";// id for plus image
                    var tabId = "ab4";// id for circle tab
                //}
                var tabObj = $('#'+tabId);
                var liObj = $('#'+liId);
                helper.tabEventPress(liObj,tabObj);
            }
            else{
                var message = resp.msg + ' CBuserId and/or Password could not be created!';
                component.set('v.userCreationError',message);
                helper.showToast(component,event,helper,message,'Error');
                
            }
           
        });
        $A.enqueueAction(action);
    }
    
    

})