({
	doInit : function(component, event, helper) {
		
        var action2 = component.get("c.populateStartTimeWindow");
        action2.setCallback(this,function(res){
        	var response = res.getReturnValue();
            
            if(!$A.util.isUndefinedOrNull(response)){
            	component.set("v.startTimeWindow" , response);	   
                
            }
        });
        $A.enqueueAction(action2);
        
        var action = component.get("c.productList");
        action.setCallback(this,function(res){
        	var response = res.getReturnValue();
            
            if(!$A.util.isUndefinedOrNull(response)){
            	component.set("v.productList" , response);	   
                
            }
        });
        $A.enqueueAction(action);
        
        var action1 = component.get("c.getTechnician");
        action1.setCallback(this,function(res){
        	var response = res.getReturnValue();
            
            if(!$A.util.isUndefinedOrNull(response)){
            	component.set("v.technician" , response);	   
                
            }
        });
        $A.enqueueAction(action1);
       
	},
    
    scriptsLoaded : function(component, event, helper){
 		 var tabId ='ab1';// id for plus image
        //alert('liId'+liId);
         var liId = 't'+tabId;
         var tabObj = $('#'+tabId);
         var liObj = $('#'+liId);
         helper.tabEventPress(liObj,tabObj);
        
    },
    
    onClickOfPlus : function(component,event,helper){
         var liId = event.currentTarget.id;// id for plus image
        //alert('liId'+liId);
         var tabId = liId.substring(1,4);
         var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
         if(tabId== 'ab2' && $A.util.isUndefinedOrNull(selectedLookUpRecord.Id)){
            var message = 'Please Select an Event to continue';
             helper.showToast(component,event,helper,message,'Error');
             
            return;
        }
        
        if(tabId== 'ab3' && $A.util.isUndefinedOrNull(component.get('v.contactId'))){
            var message = 'Please complete 2nd Section first';
             helper.showToast(component,event,helper,message,'Error');
            
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        
        if(tabId== 'ab4' && ($A.util.isUndefinedOrNull(component.get('v.contactId')) 
           						|| component.get('v.userCreated')!=true)){
            var message = 'Please complete 2nd and 3rd Section first';
             helper.showToast(component,event,helper,message,'Error');
            
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        if(tabId== 'ab5' && component.get('v.savedWorkOrder')!=true){
            var message = 'Please complete 4th section first';
             helper.showToast(component,event,helper,message,'Error');
            
           
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        
         var tabObj = $('#'+tabId);
         var liObj = $('#'+liId);
         helper.tabEventPress(liObj,tabObj);
        
    },
    
    onClickOfTabName : function(component,event,helper){
         var liId = event.currentTarget.id;// id for plus image
        //alert('liId'+liId);
         var tabId = liId.substring(2,5);
         var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
         if(tabId== 'ab2' && $A.util.isUndefinedOrNull(selectedLookUpRecord.Id)){
            var message = 'Please Select an Event to continue';
             helper.showToast(component,event,helper,message,'Error');
             
            return;
        }
  
        if(tabId== 'ab3' && $A.util.isUndefinedOrNull(component.get('v.contactId'))){
            var message = 'Please complete 2nd Section first';
             helper.showToast(component,event,helper,message,'Error');
            
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        
        if(tabId== 'ab4' && ($A.util.isUndefinedOrNull(component.get('v.contactId')) || component.get('v.userCreated')!=true)){
            var message = 'Please complete 2nd and 3rd Section first';
             helper.showToast(component,event,helper,message,'Error');
            
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        if(tabId== 'ab5' && component.get('v.savedWorkOrder')!=true){
            var message = 'Please complete 4th section first';
             helper.showToast(component,event,helper,message,'Error');
            
           
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        
         var tabObj = $('#'+tabId);
         var liObj = $('#'+liId);
         helper.tabEventPress(liObj,tabObj);
        
    },
    
    onClickOfNum : function(component,event,helper){
         var tabId = event.currentTarget.id;// id for plus image
        //alert('liId'+liId);
         var liId = 't'+tabId;
         var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
        if(tabId== 'ab2' && $A.util.isUndefinedOrNull(selectedLookUpRecord.Id)){
            var message = 'Please Select an Event to continue';
             helper.showToast(component,event,helper,message,'Error');
            
            return;
        }
        
        if(tabId== 'ab3' && $A.util.isUndefinedOrNull(component.get('v.contactId'))){
             var message = 'Please complete 2nd Section first';
             helper.showToast(component,event,helper,message,'Error');
            
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        if(tabId== 'ab4' && ($A.util.isUndefinedOrNull(component.get('v.contactId')) || component.get('v.userCreated')!=true)){
            var message = 'Please complete 2nd & 3rd Section first';
             helper.showToast(component,event,helper,message,'Error');
            
           
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        if(tabId== 'ab5' && component.get('v.savedWorkOrder')!=true){
            var message = 'Please complete 4th section first';
             helper.showToast(component,event,helper,message,'Error');
            
           
            //component.set("v.message", message);
            //component.set("v.isOpen", true);
            return;    
        }
        
         var tabObj = $('#'+tabId);
         var liObj = $('#'+liId);
         helper.tabEventPress(liObj,tabObj);
        
    },
    
    nextButtonEvent : function(component,event,helper){
        //var isOverride = event.getParam("isOverride");
        
         var buttonId = event.getParam("tabId");// id for plus image
        //alert('buttonId'+buttonId);
         if(buttonId == 'next2'){
            var liId ="tab3";// id for plus image
            var tabId = "ab3";// id for circle tab
        }
        var tabObj = $('#'+tabId);
        var liObj = $('#'+liId);
        var isHearingLoss = event.getParam("isHearLoss");
        var message = event.getParam("message");
        var isOverride = event.getParam("isOverride");
        
        var contactId = event.getParam("conId");
        
        //component.set('v.contact',contact);
        component.set('v.conId',contactId);
        console.log('contactID>>>>>??'+JSON.stringify(component.get('v.conId')));
        //alert('isHearingLoss-'+isHearingLoss);
        if(tabId== 'ab3' && (!isHearingLoss || isOverride)){
            /*var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "message": 'You cannot go further since Hearing loss is set to false'
            });
            toastEvent.fire();*/
            component.set("v.message", message);
            component.set("v.isOpen", true);
            return;    
        }
        
        helper.tabEventPress(liObj,tabObj);
        
    },
    
    onClickOfNext : function(component,event,helper){
        var buttonId = event.currentTarget.id; 
        //alert('buttonId'+buttonId);
        if(buttonId == 'next1'){
            var liId ="tab2";// id for plus image
            var tabId = "ab2";// id for circle tab
        }
        if(buttonId == 'next2'){
            var liId ="tab3";// id for plus image
            var tabId = "ab3";// id for circle tab
        }
        var tabObj = $('#'+tabId);
        var liObj = $('#'+liId);
        var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
        console.log('selectedLookUpRecord-'+selectedLookUpRecord);
        if(tabId== 'ab2' && $A.util.isUndefinedOrNull(selectedLookUpRecord.Id)){
            var message = 'Please Select an Event to continue';
             helper.showToast(component,event,helper,message,'Error');
            
            return;
        }
        
        var isHearingLoss = component.get("v.isHearingLoss");
        console.log('isHearingLoss-'+isHearingLoss);
        if(tabId== 'ab3' && !isHearingLoss){
            var message = 'You cannot go further since Hearing loss is set to false';
             helper.showToast(component,event,helper,message,'Error');
            return;    
        }
        
        helper.tabEventPress(liObj,tabObj);
        
        component.set('v.disableNext',false);
        
    },
        
     handleComponentEvent : function(component, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
       var selectedAccountGetFromEvent = event.getParam("recordByEvent");
	    
         
         //run only for Campign Lookup only
         if(!$A.util.isUndefinedOrNull(selectedAccountGetFromEvent.Id) && selectedAccountGetFromEvent.Id.startsWith("701")){
             component.set("v.selectedLookUpRecord" , selectedAccountGetFromEvent);
             console.log('recordselected>>>'+JSON.stringify(selectedAccountGetFromEvent));
             component.set('v.campName',selectedAccountGetFromEvent.Name);
             var subchannel = $A.util.isUndefinedOrNull(selectedAccountGetFromEvent.Sub_Channel__c)?'':selectedAccountGetFromEvent.Sub_Channel__c;
             var channel = $A.util.isUndefinedOrNull(selectedAccountGetFromEvent.Channel__c)?'':selectedAccountGetFromEvent.Channel__c;
             component.set('v.startdateChannel',"("+channel+":"+subchannel+")");
         }
     },
    
    cancelHandle : function(component, event, helper){
       // alert('clicked cancel');
        component.set('v.valueEntered',"");
        component.set('v.campName',"");
        component.set('v.startdateChannel',"");
        component.set("v.selectedLookUpRecord",[]);
        //fire cancel event
         var appEvent  = $A.get("e.c:Cancelevent");
		appEvent.setParams({"message" : "fire" });  
        	
        	 appEvent.fire();
        
 },
    
    clearAction : function(component, event, helper){
        //alert('clearing');
        component.set('v.disableNext',true);
        component.set("v.selectedLookUpRecord",[]);
        component.set('v.campName',"");
        component.set('v.startdateChannel',"");   
        
    },
    
    openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
   },
 
   refreshPage: function(component, event, helper) {
      // Display alert message on the click on the "Like and Close" button from Model Footer 
      // and set set the "isOpen" attribute to "False for close the model Box.
      //alert('thanks for like Us :)');
      
      component.set("v.isOpen", false);
      $A.get('e.force:refreshView').fire();
   },
   
    QualificationNext: function(component, event, helper) {	
     	component.set("v.Spinner", true);
        var buttonId = event.currentTarget.id;
         // var selectedValselect = cmp.find('select').get('v.value');
		//check if contact already created
		var reponseMessage = component.get("v.responseMessage");
        if(!$A.util.isUndefinedOrNull(reponseMessage) && (reponseMessage.includes('Contact Created') || 
                                                              reponseMessage.includes('Lead Created'))){
            
            if(reponseMessage.includes('Contact Created') ){
                
                if(component.get('v.userCreated')!=true){
                    component.set("v.Spinner", false);
                	helper.showToast(component,event,helper,component.get('v.userCreationError'),'Error');  
                    return;
                }
				var liId ="tab4";// id for plus image
                var tabId = "ab4";// id for circle tab
                
                var tabObj = $('#'+tabId);
                var liObj = $('#'+liId);
                helper.tabEventPress(liObj,tabObj);
                //return;
                    
            }//when hearloss was false
            else if(reponseMessage.includes('Lead Created')){
                component.set("v.Spinner", false);  	
                var message = 'Lead already created, Please click on Okay to start again';
                component.set("v.message", message);
                component.set("v.isOpen", true);
                //return;
            }
            else{
               helper.showToast(component,event,helper,reponseMessage,'Error');     
            }
            component.set("v.Spinner", false);
            return;
            
        }
		
        var selectedVal;
        var callserver = 'false';
        //if the select smart phone then the one of the options is mandatory.
        if(component.get('v.smartPhone')== true){
          selectedVal = component.get('v.valueRadio');
            if(selectedVal == null || selectedVal == '' || selectedVal=='option1' ){
                callserver = 'false';
                var toastEvent = $A.get("e.force:showToast");
            	toastEvent.setParams({
                "message": 'Please select smartphone type',
                    "type" : "error",
                    "title" : "Error!"
           	 });
            	toastEvent.fire();
                component.set("v.Spinner", false);
            }
            else{
                callserver = 'true';
                console.log('setting it true');
            }
        }
        else{
            selectedVal=null;
            callserver = 'true';
        }
       
        console.log('selected val>>>'+selectedVal);
        if( callserver == 'true'){
            console.log('inside server call');
        	var action = component.get('c.createContactWithQualification');
            var internet = component.get('v.internet');
            var smartPhone = component.get('v.smartPhone');
            var obj = helper.castData(component,event,helper);
			var createContact = false;
            //create contact if below conditions satisfy
            if(internet== true || (smartPhone == true && selectedVal =='iOS')){
            	createContact = true;    
			            	    
            }
            
            action.setParams({
                    "CampId" :  component.get("v.campaignId"),
                    "CustomerInfo" : JSON.stringify(obj),
                    "createContact": createContact,
                 	"hasHomePhone": component.get('v.homePhone'),
                 	"internet" : component.get('v.internet'),
                 	"smartPhone" : component.get('v.smartPhone'),
                 	"hearing": component.get('v.hearing'),
                 	"type" : selectedVal
             
            });
            
         
            action.setCallback(this,function(res){
                //debugger;
                //component.set("v.Spinner", false);
                var resp = res.getReturnValue();
                console.log('response of Create contact ' + JSON.stringify(resp));
                if(resp!=null && !resp.msg.includes('Error')){
                        
                    component.set('v.responseMessage',resp.msg);
                    //when contact is created, create user in 3rd party system.
                    if(!$A.util.isUndefinedOrNull(resp.ContactId)){
                        component.set("v.contactId",resp.ContactId);                  
                        helper.showToast(component,event,helper,resp.msg,'Success');
                        helper.createNewUser(component,event,helper,resp.ContactId);
                                           
                    }
                    else{//if only lead is created
                        component.set("v.leadId",resp.leadId);
                        component.set("v.Spinner", false);
                        var message = 'Lead Created for Reporting. Click Okay to refresh.';
                        component.set("v.message", message);
            			component.set("v.isOpen", true);
                    }
                }
                else{
                    component.set("v.Spinner", false);
                    helper.showToast(component,event,helper,resp.msg,'Error');
                }
                /*
                console.log('response>>>>>????workOrderperList'+JSON.stringify(response));
                component.set("v.workOrderWrapperList",response); 
                
                var workOrderWrapperList =  component.get("v.workOrderWrapperList"); 
                //var contact = component.get("v.contact");
                var ContactTemp = workOrderWrapperList[0].technician;
                var productList = workOrderWrapperList[0].productsList;
                workOrderWrapperList[0].technician = '{}';
                component.set("v.workOrderWrapperList",workOrderWrapperList); 
                component.set("v.productList",workOrderWrapperList[0].productsList); 
                component.set("v.contact",ContactTemp);
                var liId ="tab4";// id for plus image
                var tabId = "ab4";// id for circle tab
                var tabObj = $('#'+tabId);
                var liObj = $('#'+liId);
                helper.tabEventPress(liObj,tabObj);*/
                
                
            });
        
        	$A.enqueueAction(action);
        
        
       /* if(component.get('v.homePhone') == true || component.get('v.internet')== true ||
           component.get('v.smartPhone')== true || component.get('v.hearing')==true){*/
          
       /* }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "message": 'Please select atleast one to continue'
            });
            toastEvent.fire();
        }*/
           
        }
   },
    
    smartPhoneSet : function(component, event, helper) {	
        if(component.get('v.smartPhone')== true){
            component.set('v.showPhoneType',true);
        }   
        else{
            component.set('v.valueRadio',false);
            component.set('v.showPhoneType',false);
            
        }
    },
    
    ClearQuestions : function(component, event, helper){
        component.set('v.homePhone',false);
        component.set('v.internet',false);
    	component.set('v.smartPhone',false);
        component.set('v.hearing',false);
         component.set('v.showPhoneType',false);
        component.set('v.valueRadio','option1');
        
    },
    
    addRow: function(component, event, helper) {
        helper.addNewRow(component, event);
    },
    
    removeRow: function(component, event, helper) {
        //Get the account list
        var workOrderWrapperList = component.get("v.workOrderWrapperList");
        //var deleteRecordList = component.get("v.deleteRecordList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        //if(!$A.util.isUndefinedOrNull(workOrderWrapperList[index].recordId)) {
            //deleteRecordList.push(workOrderWrapperList[index].recordId);
            //break;
            //component.set("v.deleteRecordList", deleteRecordList);
        //}
  			
        workOrderWrapperList.splice(index, 1);
        if(workOrderWrapperList.length==0){
            component.set("v.confirmAppointment",false);
        }
        component.set("v.workOrderWrapperList", workOrderWrapperList);
        
    },
    
    onChangeProductValue: function(component, event, helper){
        var selectedItem = event.getSource();
        var index = selectedItem.get("v.label");
        var value = selectedItem.get("v.value");
        var productList = component.get("v.productList");
        component.set("v.product",{});
        var product = component.get("v.product");
        for(var i=0;i<productList.length;i++){
            if(value == productList[i].Id){
               product.Name =  productList[i].Name;
               product.Id =  productList[i].Id; 
                break;
            }
        }
 
        console.log('value>>>>>>changed'+value + "-" + index);
        var workOrderWrapperList = component.get("v.workOrderWrapperList");

        workOrderWrapperList[index].ordrLineItem.Product2Id = value;
        workOrderWrapperList[index].product = product;
        //console.log('after change index>>>'+JSON.stringify(workOrderWrapperList[index]));
        //console.log('after change>>>'+JSON.stringify(workOrderWrapperList));
        component.set("v.workOrderWrapperList",workOrderWrapperList);
        
    },
    
    onChangeOfDate : function(component, event, helper){
        component.set("v.confirmAppointment",false);
    },
    
    onChangeStartTimeWindow: function(component, event, helper){
        var selectedItem = event.getSource();
        var index = selectedItem.get("v.label");
        var value = selectedItem.get("v.value");
        console.log('value>>>>>>changed'+value + "-" + index);
        var workOrderWrapperList = component.get("v.workOrderWrapperList");
        
        workOrderWrapperList[index].workOrder.Start_Time_Window__c = value;
      
        component.set("v.workOrderWrapperList",workOrderWrapperList);
    },
    
    setAppointment: function(component, event, helper) {
       
        helper.setAppointmentHelper(component, event);
    },
    
    saveAppointment: function(component, event, helper){
       
        if(!component.get("v.confirmAppointment")){
        	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "message": 'Please confirm Appointment first to move forward.',
                "type": "error",
                "title": "Error!"
            });
            toastEvent.fire();   
        }
        else{
            component.set("v.Spinner", true);
            console.log('inside saveAppointment')
        	helper.saveAppoint(component, event, helper);
        }
    },
    
    
    //Method to searcf for existing contacts through email
	searchEmail : function(component, event, helper) {
        //alert(component.find("LeadEmail").get("v.value"));
        component.set("v.Spinner", true);
        var emailval = $("#getet").val();
        console.log('email value---'+emailval);
        var atposition=emailval.indexOf("@");  
        console.log('@therate>>'+atposition);
		var dotposition=emailval.lastIndexOf(".");  
        console.log('dot>>>'+dotposition);
        if(emailval==''){
        	helper.showToast(component,event,helper,'Fill in email value','Error'); 
           	component.set("v.Spinner", false);
        }
        //validating email
       else if (atposition<1 || dotposition<atposition+2 || dotposition+2>=emailval.length){  
        	helper.showToast(component,event,helper,'Enter valid email address','Error'); 
           component.set("v.Spinner", false);
            //component.set('v.LeadEmail','');
            //$('#getet').val('');
        }
        else{
            //call the apex to get the existing contact info based on email
            var action = component.get("c.SearchEmail");
            action.setParams({"EmailText": emailval});
            action.setCallback(this,function(res){
                console.log('inside call back conatct email search---'+JSON.stringify(res.getReturnValue()));
                var resp = res.getReturnValue();
                if(resp!=null && resp.length>0){
                     //component.set('v.Order_Lead',resp);
                    $('#getCity').val(resp.MailingCity);
                    $('#getState').val(resp.MailingState);
                    $('#getZipCode').val(resp.MailingPostalCode);
                    $('#getStreet').val(resp.MailingStreet);
                    $('#getFirstName').val(resp.FirstName);
                    $('#getLastName').val(resp.LastName);
                    $('#getHomePhone').val(resp.HomePhone);
                    $('#getMobilePhone').val(resp.MobilePhone);
                    helper.showConList(component,event,helper,resp);
                }else{
                    helper.showToast(component,event,helper,'No existing contacts found in the system','Info');
                }
                component.set("v.Spinner", false);
              
            });
            $A.enqueueAction(action);
        }
       
	},
    
    
    Validate : function(component, event, helper) {
        component.set("v.Spinner", true);
        console.log('phone>>>'+$("#getMobilePhone").val());
        var mobileNumber = $("#getMobilePhone").val();
        var phoneNumber = $("#getHomePhone").val();
		var zipcode = $("#getZipCode").val();
        var apt = $("#getApt").val();
        var ValidChars = "0123456789";
        var IsNumber=true;
        var Char;
        
        console.log('homephone>>>'+phoneNumber);
        
		var emailval = $("#getet").val();
        console.log('email value---'+emailval);
        var atposition=emailval.indexOf("@");  
        console.log('@therate>>'+atposition);
		var dotposition=emailval.lastIndexOf(".");  
        console.log('dot>>>'+dotposition);
        
        var firstName = $("#getFirstName").val();//update:first name value mention spaces.
        var lastName = $("#getLastName").val();
        var alphaExp1 = /^[a-zA-Z\ ]+$/;//update:first name value mention spaces.
        var alphaExp2 = /^[a-zA-Z\-]+$/;
        var alphaExp3 = /^[a-z0-9\- ]+$/i;
        
        if((apt!='') && (!apt.match(alphaExp3))){
            component.set("v.Spinner", false);
            helper.showToast(component,event,helper,'Apt/Unit/Suite can be Alphanumberic only','Error'); 
            return;
        }
        
        if(!firstName.match(alphaExp1)){
            component.set("v.Spinner", false);
            helper.showToast(component,event,helper,'First Name should be letters and spaces only','Error'); 
            return;
        }
        
        if(!lastName.match(alphaExp2)){
            component.set("v.Spinner", false);
            helper.showToast(component,event,helper,'Last Name should be letters and Hyphen only','Error'); 
            return;
        }
       
        //validating email
       if (atposition<1 || dotposition<atposition+2 || dotposition+2>=emailval.length){ 
           component.set("v.Spinner", false);
        	helper.showToast(component,event,helper,'Enter valid email address','Error'); 
           //component.set('v.LeadEmail','');
           return;
            //$('#getet').val('');
        }
        
        if(zipcode.length!=5){
            component.set("v.Spinner", false);
        	helper.showToast(component,event,helper,'Please enter 5 digit Zip Code','Error'); 
           //component.set('v.LeadEmail','');
           return;
            
        }
        
 
        for (var i = 0; i < mobileNumber.length && IsNumber == true; i++){
            Char = mobileNumber.charAt(i);
            console.log('charecter>>>'+Char);
            if (ValidChars.indexOf(Char) == -1 || Char== '-' )
            {
                IsNumber = false;
                console.log('number>>>>>>');
            }
        }
        for (var i = 0; i < phoneNumber.length && IsNumber == true; i++){
            Char = phoneNumber.charAt(i);
             console.log('charecterhome>>>'+Char);
            if(ValidChars.indexOf(Char) == -1 || Char== '-'){
                IsNumber = false;
                console.log('number>>>>>>');
            }
        }
   
       //either mobile or home phone should be of length 10.
        if((mobileNumber!='' && mobileNumber.length!=10) || 
           (phoneNumber!='' && phoneNumber.length!=10) || IsNumber == false ){
           component.set("v.Spinner", false); 
           helper.showToast(component,event,helper,'Please Enter Valid 10 Digit Home Tel./Mobile Number','Error');
           }
        
        
        else{
        var isValidated = component.get("v.isValidated");
        if(!isValidated){
            var isValid = helper.validateFormHelper(component,event,helper);
            if(isValid){
                component.set("v.responseMessage",'');
                var obj = helper.castData(component,event,helper);
                var action = component.get("c.ValidateAddress");
                action.setParams({"conInfo": JSON.stringify(obj)});
                action.setCallback(this,function(res){
                    console.log('inside call back Validate address---'+JSON.stringify(res.getReturnValue().sugList));
                    var resP = res.getReturnValue();
                    var msg = resP.msg;
                    var sugList = [];
                    sugList= resP.sugList; 
                    component.set("v.suggestedAddresses",sugList);
                    if(msg.includes('successfully'))
                    	helper.showToast(component,event,helper,msg,'Success');
                    else if(msg.includes('Error'))
                        helper.showToast(component,event,helper,msg +', Please contact System Administrator','Error'); 
                    else    
                       helper.showToast(component,event,helper,msg,'Error'); 
                    debugger;
                    console.log('-res.StatusCode--'+res.StatusCode);
                    console.log('-msg--'+msg);
                    if(resP.StatusCode == '200' && msg.includes('Duplicate Contacts')){
                        helper.showConList(component,event,helper,resP.contactList);
                        //component.set("v.disableNext",true);
                    }
                    else if(resP.StatusCode!='200'){
                        helper.createComp(component,event,helper,sugList);
                        //component.set("v.isValidated",true);
                    }else if(resP.StatusCode=='200' && !msg.includes('Duplicate Contacts')){
                        component.set("v.Spinner", false);
                        component.set("v.isValidated",true);
                        //component.set("v.disableNext",false);
                    }
                });
                $A.enqueueAction(action);
            }else{
                	component.set("v.Spinner", false);
                    helper.showToast(component,event,helper,'Enter all the required fields','Error');
            }
        }
        else{
            component.set("v.Spinner", false);
        	helper.showToast(component,event,helper,'Already Validated','Success');    
        }
        }
        
	},
    
    setAddress : function(component, event, helper) {
        var isOverride = event.getParam("isOverride");
        component.set("v.isAddressOverride",isOverride);
        if(isOverride){
            component.set("v.isValidated",true);
        }else if( event.getParam("city") == null){
             
        }else{
            component.set("v.isValidated",true);

            $('#getCity').val(event.getParam("city"));
            $('#getState').val(event.getParam("state"));
            $('#getZipCode').val(event.getParam("ZipCode"));
            $('#getStreet').val(event.getParam("street"));  
            
            if(event.getParam("apt") != null && event.getParam("apt") != '')
            	$('#getApt').val(event.getParam("apt"));    
        }
        
    },
    
    validateForm : function(component, event, helper) {
        //alert('vaidateing')
        //component.set("v.isValidated",false);
        helper.validateFormHelper(component,event,helper);
	},
    
    //make the isValidated false only when address changes, called when address fields changes.
    validateAddressForm : function(component, event, helper) {
        //alert('vaidateing')
        component.set("v.isValidated",false);
        helper.validateFormHelperAddress(component,event,helper);
	},
    ClearInfo : function(component, event, helper) {
        component.set('v.LeadEmail','');
        $('#getet').val('');
        $('#getCity').val('');
        $('#getState').val('');
        $('#getZipCode').val('');
        $('#getStreet').val('');
        $('#getFirstName').val('');
        $('#getLastName').val('');
        $('#getHomePhone').val('');
        $('#getMobilePhone').val('');
        $('#getApt').val('');
        component.set('v.isHearingLoss',false);
        component.set('v.isAddressOverride',false);
        component.set('v.disableNext',true);
        component.set('v.EnableValidate',false);
        component.set('v.responseMessage','');
        component.set('v.contactId','');
        component.set('v.suggestedAddresses',[]);
        component.set('v.hearLossValue','default');
        component.set('v.isValidated',false);
    },
    
    createLead : function(component, event, helper) {
        component.set("v.Spinner", true);
        var hearLossValue = component.get('v.hearLossValue');
        console.log('hearLossValue'+hearLossValue);
        //check hearloss is selected or not.
        if(hearLossValue == null || hearLossValue == '' || hearLossValue=='default' ){
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "message": 'Please select Has Hearing Loss',
                "type" : "Error",
                "title" : "Error!"
            });
            toastEvent.fire();
            component.set("v.Spinner", false);
            return;
        }
        else{
            //callserver = 'true';
            if(hearLossValue=='Yes')
                component.set("v.isHearingLoss",true);  
            else
                component.set("v.isHearingLoss",false);  
            console.log('setting it true');
        }
        //var buttonId = event.currentTarget.id;
        var selectedLookUpRecord = component.get('v.selectedLookUpRecord');
        component.set("v.campaignId",selectedLookUpRecord.Id);
        var isOverride = component.get("v.isAddressOverride");
        var isValidated = component.get("v.isValidated");
        var isHearLoss = component.get("v.isHearingLoss");
        if(isValidated){
             var reponseMessage = component.get("v.responseMessage");
            //condition when user comes back to 2nd screen
             if(!$A.util.isUndefinedOrNull(reponseMessage) && (reponseMessage.includes('Contact Created') || 
                                                              reponseMessage.includes('Lead Created'))){
                 
                if(reponseMessage.includes('Lead Created') && (isOverride || !isHearLoss)){
                    //hearLoss = false; 
                    component.set("v.Spinner", false);	
                    var message = 'Lead already created, Please click on Okay to start again';
                    component.set("v.message", message);
            		component.set("v.isOpen", true);
                    return;
                }
                 else{
                     var liId ="tab3";// id for plus image
                     var tabId = "ab3";// id for circle tab
               
                     var tabObj = $('#'+tabId);
                     var liObj = $('#'+liId);
                     helper.tabEventPress(liObj,tabObj);
                     component.set("v.Spinner", false);
                     return;
          
                }
                 
             }
            
            //for the first time move to 3rd section
            if(!isOverride && isHearLoss){
               
                    var liId ="tab3";// id for plus image
                    var tabId = "ab3";// id for circle tab
               
                var tabObj = $('#'+tabId);
                var liObj = $('#'+liId);
                helper.tabEventPress(liObj,tabObj);
                component.set("v.Spinner", false);
                return;
            }
            
            console.log('before callback');
            var obj = helper.castData(component,event,helper);
            var action = component.get("c.createLeadAndContact");
            action.setParams({"CampId": selectedLookUpRecord.Id,
                              "CustomerInfo": JSON.stringify(obj),
                              "isHearingLoss" : isHearLoss,
                              "isOverride" : isOverride});
            action.setCallback(this,function(res){
                console.log('inside create lead and contact---'+JSON.stringify(res.getReturnValue()));
                var resp = res.getReturnValue();
                if(resp!=null && !resp.msg.includes('Error')){//if  no error resposne from the server
                    
                    component.set('v.responseMessage',resp.msg);
                    if(!$A.util.isUndefinedOrNull(resp.leadId)){
                        component.set("v.leadId",resp.leadId);
                        //component.set("v.contactId",resp.ContactId);
                        if(isOverride){//if address is overriden and has hearling loss is either true or false.
                            
                            helper.showToast(component,event,helper,resp.msg,'Success');
                            component.set("v.Spinner", false);
                            var msg = 'Lead Created with Address overridden, please create a case against it.Click Okay to refresh.';
                            component.set("v.message", msg);
            				component.set("v.isOpen", true);
                            //helper.createNewUser(component,event,helper,resp.ContactId,buttonId);
                        }
                        else{//when has hearing loss is false and override is false.
                            component.set("v.Spinner", false);
                            helper.showToast(component,event,helper,resp.msg,'Success'); 
                            var msg = 'Lead Created for Reporting since Hearing loss is set to false. Click Okay to refresh.';
                            component.set("v.message", msg);
            				component.set("v.isOpen", true);
                            
                        }
                        
                    }
                    else{//some error 
                        component.set("v.Spinner", false);
                        var msg = 'Some error occurred, please try again later.'
                        helper.showToast(component,event,helper,msg,'Error'); 
                    }
                }
                else{//if error reponse from the server
                    component.set("v.Spinner", false);
                    helper.showToast(component,event,helper,resp.msg,'Error');
               }
                
            });
            $A.enqueueAction(action); 
        }
        else{
            
            component.set("v.Spinner", false);
            helper.showToast(component,event,helper,'Validate the Address Before proceeding further','Error');
                  
        }
  
    }
    
    
})