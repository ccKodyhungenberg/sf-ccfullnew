({

    actionNames: ['createlead','createcustomer','convertlead','installworkorder','secondarydevice','secondaryuser','additionalminor'],
    actionLabels: ['Create Lead','Create Customer','Convert Lead to Customer','Create Install WorkOrder','Add Secondary Device','Add Secondary User','Add Additional Minor'],

    rolkaBucket1 : ['08','NF','37','51','76','DI','CA','26','66','72'],
    rolkaBucket2 : ['25','ZI','06','CL','IS','MI','MS','RS'],

    //helper to show up the taost message
    showToast : function(component, event, helper,msg,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": msg,
            "type" : type,
            "title" : type+'!'
        });
        toastEvent.fire();
    },

    //dynamically create the component to reduce the overhead
    createComp : function(component, event, helper,msg) {
        component.set("v.Spinner", false);
        $A.createComponent(
            "c:Event_Suggested_Add_Comp",
            {
                "suggestedAddresses": msg
            },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },

    showConList : function(component, event, helper,msg) {
        component.set("v.Spinner", false);
        $A.createComponent(
            "c:Event_Contact_List",
            {
                "suggestedContacts": msg
            },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },

    //helper method to stringify the data and send to apex
    castData : function(component) {
        var obj = {};

        obj['Email'] = $("#getet").val();
        obj['HPhone'] = $("#getHomePhone").val();
        obj['PreferredName'] = $("#getPreferredName").val();
        obj['FirstName'] = $("#getFirstName").val();
        obj['LastName'] = $("#getLastName").val();
        obj['city'] = $("#getCity").val();
        obj['state'] = $("#getState").val();
        obj['ZipCode'] = $("#getZipCode").val();
        obj['street'] = $("#getStreet").val();
        obj['country'] = 'US';
        obj['Mphone'] = $("#getMobilePhone").val();
        obj['apt'] = $("#getApt").val();
        obj['CareGiver'] = $("#getCareGiver").val();
        obj['SSN'] = $("#getSSN").val();
        return obj;
    },

    tabEventPress : function (obj,tabObj){

        var liId = $(obj).attr("id");
        console.log('id>>>'+liId);
        $(obj).siblings().closest("li").hide();
        $(obj).closest("li").toggle();
        $(tabObj).closest("li").siblings().removeClass("on");
        $(obj).closest("li").siblings().removeClass('active');
        $("li.tab_value>div.campaign").css("display","none");
        $(obj).closest("li").toggleClass("active");
        console.log('test1' + $(obj).closest("li").attr("class"));
        $("li.active>div.campaign").slideDown("slow").css("display","block");
        console.log('idselected>>>'+ $("li.active>span.plus_image").attr("id"));
        $('li.tab_value span.minus_image').each(function(){
            console.log('idother>>>'+ $(this).attr("id"));
            $(this).removeClass("minus_image");
            $(this).addClass("plus_image");
        });
        //drives the active section display of + to - image
        $("li.active>span.plus_image").addClass("minus_image");

        $(tabObj).closest("li").toggleClass("on")
    },

    addNewRow: function(component, event) {
        //get the record List from component
        var workOrderList = component.get("v.workOrderWrapperList");
        if(workOrderList.length==5){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "ERROR!",
                "message": "Cannot add more than 5 Rows",
                "type" : 'error'
            });
            toastEvent.fire();
            return;
        }
        //var ordrLineItem =
        //Add New  Row
        console.log('in add row-'+JSON.stringify(workOrderList) );
        var productList = component.get("v.productList");
        component.set("v.ordrLineItemAttribute",{});
        var orderLineItem = component.get("v.ordrLineItemAttribute");
        var technician = component.get("v.technician");
        component.set("v.product",{});
        var product = component.get("v.product");
        //technician.Id = undefined;
        console.log('productList'+JSON.stringify(productList));
        if(productList.length>0)
            orderLineItem.Product2Id = productList[0].Id;
        product = productList[0];
        //workOrderWrapperList[index].ordrLineItem.Product2Id = value;
        component.set("v.workOrderAttribute",{});
        var workOrder = component.get("v.workOrderAttribute");
        console.log('workOrder--->'+JSON.stringify(workOrder));
        var startTimeWindow = component.get("v.startTimeWindow");
        workOrder.Start_Time_Window__c = startTimeWindow[0];
        var laborSource = component.get("v.laborSourceList");
        workOrder.Labor_Source__c = laborSource[0];
        workOrder.Status='Ready for Scheduling';

        component.set("v.orderRecord",{});
        var order = component.get("v.workOrderAttribute");
        console.log('order--->'+JSON.stringify(order));
        var fulfillmentList = component.get("v.fulfillmentList");
        order.Fulfillment__c = fulfillmentList[0];



        //workOrder.StartDate = null;
        workOrderList.push({
            'ordrLineItem' : orderLineItem,
            'workOrder': workOrder,
            'technician' : technician,
            'product' : product,
            'productsList': component.get("v.productList"),
            'wrapperOrder': order
        });
        component.set("v.confirmAppointment",false);
        console.log('in add row-after-'+JSON.stringify(workOrderList) );
        component.set("v.workOrderWrapperList", workOrderList);
    },

    setAppointmentHelper: function(component, event) {
        //get the record List from component
        var workOrderList = component.get("v.workOrderWrapperList");
        var confirmAppointment = component.get("v.confirmAppointment");
        var duplicateString='';
        if(workOrderList.length>0){
            for(var i=0;i<workOrderList.length;i++){
                console.log('workOrderList[i].ordrLineItem.Product2Id-'+ workOrderList[i].ordrLineItem.Product2Id);
                console.log('workOrderList[i].technician.Id'+workOrderList[i].technician.Id);
                console.log('workOrderList[i].workOrder.StartDate'+workOrderList[i].workOrder.StartDate);
                console.log('workOrderList[i].workOrder.Start_Time_Window__c)'+workOrderList[i].workOrder.Start_Time_Window__c);
                console.log('$A.util.isUndefinedOrNullworkOrderList[i].ordrLineItem.Product2Id-'+ $A.util.isUndefinedOrNull(workOrderList[i].ordrLineItem.Product2Id));
                console.log('$A.util.isUndefinedOrNullworkOrderList[i].technician.Id'+$A.util.isUndefinedOrNull(workOrderList[i].technician.Id));
                console.log('$A.util.isUndefinedOrNullworkOrderList[i].workOrder.StartDate'+$A.util.isUndefinedOrNull(workOrderList[i].workOrder.StartDate));
                console.log('$A.util.isUndefinedOrNull(workOrderList[i].workOrder.Start_Time_Window__c)'+$A.util.isUndefinedOrNull(workOrderList[i].workOrder.Start_Time_Window__c));


                //null check in all the list values
                if($A.util.isUndefinedOrNull(workOrderList[i].ordrLineItem.Product2Id) ||
                    workOrderList[i].ordrLineItem.Product2Id == 'none' ||
                    $A.util.isUndefinedOrNull(workOrderList[i].technician.Id) ||
                    $A.util.isUndefinedOrNull(workOrderList[i].workOrder.StartDate) ||
                    $A.util.isUndefinedOrNull(workOrderList[i].workOrder.Start_Time_Window__c)){
                    component.set("v.Spinner",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Please select all required fields.",
                        "type" : "error"
                    });
                    toastEvent.fire();
                    return false;
                }

                var todDate = new Date();
                var tFormatDate = $A.localizationService.formatDate(todDate, "yyyy MM dd");
                var workOrderDatetime = workOrderList[i].workOrder.StartDate;
                var wwFormateDate = $A.localizationService.formatDate(workOrderDatetime, "yyyy MM dd");

                //if(today > workOrderDate){
                if(tFormatDate>wwFormateDate){
                    console.log('inside if>>>');
                    component.set("v.Spinner",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Please select from calendar today or a future date.",
                        "type" : "error"
                    });
                    toastEvent.fire();
                    return false;
                }



            }
        }
        else{
            component.set("v.Spinner",false);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Add atleast one Column",
                "type" : "error"
            });
            toastEvent.fire();

            return false;

        }





        for(var i=0;i<workOrderList.length;i++){
            workOrderList[i].workOrder.Status ='Scheduled';

        }
        component.set("v.workOrderWrapperList",workOrderList);



        return true;

    },


    saveAppoint: function(component, event, helper) {

        var actionName= component.get("v.actionName");
        let sendForApproval = false;
        if(actionName=='secondarydevice'){
            sendForApproval=true;
        }
        var action = component.get('c.saveAppointmentWorkOrder');
        action.setParams({
            "contactId": component.get('v.contactId'),
            "listWOWrapper": component.get('v.workOrderWrapperList'),
            "sendForApproval" : sendForApproval,
            "actionName" : actionName
        });
        action.setCallback(this,function(res){
            var state = res.getState();
            // debugger;
            if (state === "SUCCESS") {
                var response = res.getReturnValue();
                console.log('response>>>>>????workOrderWrapperList'+JSON.stringify(response));
                if(response.responseMessage.includes('success')){
                    //var workOrderList = component.get("v.workOrderWrapperList");

                    component.set("v.workOrderWrapperToDisplay",response.worOrderWrapperList);
                    console.log('letsseethe startdate>>>>>'+JSON.stringify(component.get('v.workOrderWrapperToDisplay')));
                    component.set("v.contact",response.contact);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Appointment Created!" ,
                        "type" : "success"
                    });
                    toastEvent.fire();
                    component.set("v.savedWorkOrder",true);

                    var liId ="tab7";// id for plus image
                    var tabId = "ab7";// id for circle tab
                    var tabObj = $('#'+tabId);
                    var liObj = $('#'+liId);
                    helper.tabEventPress(liObj,tabObj);
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": response.responseMessage ,
                        "type" : "error"
                    });
                    toastEvent.fire();

                }
            }
            else if (state === "ERROR") {
                //component.set("v.Spinner", false);
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.Spinner", false);

        });

        $A.enqueueAction(action);


    },


    validateFormHelper : function(component, event, helper) {
        let bypassLexisValidation = component.get('v.bypassLexisValidation');
        var dateOfBirth = component.get("v.dateOfBirth");
        var validated = component.get('v.isValidated');
        let actionName = component.get("v.actionName");
        var obj = {};
        var isvalid ;
        console.log('dateOfBirth-----'+JSON.stringify(dateOfBirth))
        obj = helper.castData(component);
        console.log()
        console.log('input values-----'+JSON.stringify(obj))

        if(obj['Email'] == "" || (obj['HPhone'] == "" && obj['Mphone'] == "") || obj['FirstName'] == "" || obj['LastName']=="" ||
            obj['city']== "" || obj['state']=="" || obj['ZipCode']=="" || obj['street']=="" || obj['country']=="" ||
            (!bypassLexisValidation && obj['SSN']== "")  ||  (!bypassLexisValidation && dateOfBirth==null) ){
            component.set("v.EnableValidate",false);
            isvalid = false;
        }else{
            //alert('enabling valdiate')
            component.set("v.EnableValidate",true);
            isvalid = true;
        }
        return isvalid ;
    },

    //to validate address
    validateFormHelperAddress : function(component, event, helper) {
        let bypassLexisValidation = component.get('v.bypassLexisValidation');
        var dateOfBirth = component.get("v.dateOfBirth");
        var validated = component.get('v.isValidated');
        var obj = {};
        var isvalid ;
        obj = helper.castData(component);
        console.log('input values-----'+JSON.stringify(obj))
        if( obj['city']== "" || obj['state']=="" || obj['ZipCode']=="" || obj['street']=="" || obj['country']=="" ||
            (!bypassLexisValidation && obj['SSN']== "")  ||  (!bypassLexisValidation && dateOfBirth==null) ){
            component.set("v.EnableValidate",false);
            isvalid = false;
        }else{
            //alert('enabling valdiate')
            component.set("v.EnableValidate",true);
            isvalid = true;
        }
        return isvalid ;
    },

    validateFormHelperForMinor : function(component, event, helper) {

        let validated = component.get('v.isValidated');
        let dateOfBirth = component.get("v.dateOfBirth");
        let obj = {};
        let isvalid ;
        obj = helper.castData(component);
        console.log()
        console.log('input values-----'+JSON.stringify(obj))

        if( obj['FirstName'] == "" || obj['LastName']=="" || dateOfBirth==null ){
            component.set("v.EnableValidate",false);
            component.set("v.isValidated",false);
            isvalid = false;
        }else{
            //alert('enabling valdiate')
            component.set("v.EnableValidate",true);
            isvalid = true;
        }
        return isvalid ;
    },

    validateFormHelperForLead : function(component, event, helper) {

        var validated = component.get('v.isValidated');

        var obj = {};
        var isvalid ;
        obj = helper.castData(component);
        console.log()
        console.log('input values-----'+JSON.stringify(obj))

        if(obj['Email'] == "" || (obj['HPhone'] == "" && obj['Mphone'] == "") || obj['FirstName'] == "" || obj['LastName']=="" ||
            obj['city']== "" || obj['state']=="" || obj['ZipCode']=="" || obj['street']=="" || obj['country']=="" ){
            component.set("v.EnableValidate",false);
            isvalid = false;
        }else{
            //alert('enabling valdiate')
            component.set("v.EnableValidate",true);
            isvalid = true;
        }
        return isvalid ;
    },

    //to validate address
    validateFormHelperAddressForLead : function(component, event, helper) {
        var validated = component.get('v.isValidated');
        var obj = {};
        var isvalid ;
        obj = helper.castData(component);
        console.log('input values-----'+JSON.stringify(obj))
        if( obj['city']== "" || obj['state']=="" || obj['ZipCode']=="" || obj['street']=="" || obj['country']==""){
            component.set("v.EnableValidate",false);
            isvalid = false;
        }else{
            //alert('enabling valdiate')
            component.set("v.EnableValidate",true);
            isvalid = true;
        }
        return isvalid ;
    },

    createNewUser : function(component,event,helper,contactId){

        let actionName = component.get("v.actionName");
        //alert('inside create user-'+ contactId);
        var action = component.get("c.createUser");
        action.setParams({"contactId": contactId}
        );
        action.setCallback(this,function(response){
            component.set("v.Spinner", false);
            var resp = response.getReturnValue();
            if(resp.msg.includes('User created Successfully')){
                helper.showToast(component,event,helper,'User Created!','Success');
                helper.addNewRow(component,event);
                component.set("v.userCreated",true);

                //if(buttonId == 'next3'){
                var liId ="tab6";// id for plus image
                var tabId = "ab6";// id for circle tab
                //}
                if(actionName == 'secondaryuser'){
                    liId ="tab7";// id for plus image
                    tabId = "ab7";// id for circle tab
                }

                var tabObj = $('#'+tabId);
                var liObj = $('#'+liId);
                helper.tabEventPress(liObj,tabObj);
            }
            else{
                var message = resp.msg + ' CBuserId and/or Password could not be created!';
                component.set('v.userCreationError',message);
                helper.showToast(component,event,helper,message,'Error');

            }

        });
        $A.enqueueAction(action);
    },

    pickAction : function (component,event, actionName) {

        /*
        if(actionName == 'installworkorder' ||
           actionName== 'convertlead' ||
           actionName == 'createcustomer' ||
           actionName == 'secondarydevice' ||
           actionName == 'secondaryuser' ||
           actionName == 'additionalminor'){
           component.set("v.showAdditionalField",true);
       }
       */

        if(actionName == 'additionalminor'){
            component.set("v.showAdditionalField",true);
            component.set("v.showSSN",false);

        } else if(actionName == 'installworkorder' ||
            actionName== 'convertlead' ||
            actionName == 'createcustomer' ||
            actionName == 'secondarydevice' ||
            actionName == 'secondaryuser'){
            component.set("v.showAdditionalField",true);
            component.set("v.showSSN",true);
        }

        let actionMap = new Map([['createlead', 'tab3-event'],
            ['createcustomer', 'tab3-event'],
            ['convertlead', 'tab2-lead'],
            ['installworkorder','tab2-contact'],
            ['secondarydevice', 'tab2-contact'],
            ['secondaryuser','tab2-account'],
            ['additionalminor','tab2-account']]);
        let liIdString='';
        console.log('actionName-'+actionName);
        if(actionMap.has(actionName)){
            liIdString = actionMap.get(actionName);
        }
        console.log('liIdString'+liIdString);
        let stringList = liIdString.split("-");
        let liId =  stringList[0];
        component.set("v.selectedSearchObject",stringList[1]);
        component.set("v.searchObject",stringList[1]);
        console.log("liId-"+liId);
        console.log("stringList-"+stringList[1]);
        var tabId = liId.substring(1, 4);
        var tabObj = $('#' + tabId);
        var liObj = $('#' + liId);
        this.tabEventPress(liObj, tabObj);

    },

    navigationAction : function(component, event, tabId, liId){

        let actionName = component.get("v.actionName");
        let selectedLookUpRecord = component.get("v.selectedLookUpRecord");
        let currIndex = this.actionNames.indexOf(actionName);
        let actionLabel = this.actionLabels[currIndex];
        if(!$A.util.isUndefinedOrNull(actionName) && tabId == 'ab1'){

            if((actionName=='createcontact' && component.get('v.contactId') != '') ||
                (actionName=='createlead' && component.get('v.leadId')!='')){

                var message = 'Action already selected :' + actionLabel +' , Please refresh the page to start over';
                this.showToast(component, event, this, message, 'Error');
                return;
            }
            this.refreshPage(component);
            return;

        }

        if($A.util.isUndefinedOrNull(actionName) && (tabId == 'ab2' || tabId == 'ab3' || tabId == 'ab4'|| tabId == 'ab5' || tabId == 'ab6' || tabId == 'ab7')){
            var message = 'Please select an Action.';
            this.showToast(component, event, this, message, 'Error');
            return;
        }

        if(!$A.util.isUndefinedOrNull(actionName) && tabId=='ab2' && (actionName =='createcustomer' ||
            actionName =='createlead') ){
            let currIndex = this.actionNames.indexOf(actionName);
            let actionLabel = this.actionLabels[currIndex];
            var message = 'You cannot open this section for action : ' +actionLabel ;
            this.showToast(component, event, this, message, 'Error');
            return;
        }

        if(!$A.util.isUndefinedOrNull(actionName) && tabId=='ab3' && (actionName =='convertlead' ||
            actionName =='secondaryuser' ||
            actionName == 'additionalminor' ||
            actionName =='installworkorder' ||
            actionName == 'secondarydevice') ){

            var message = 'This section is not available for action : ' +actionLabel ;
            this.showToast(component, event, this, message, 'Error');
            return;
        }

        if (tabId == 'ab4' && $A.util.isUndefinedOrNull(selectedLookUpRecord.Id) && (actionName =='createcustomer' ||
            actionName == 'createlead')) {
            var message = 'Please select an Event.';
            this.showToast(component, event, this, message, 'Error');

            return;
        }

        if (tabId == 'ab5' &&  actionName == 'additionalminor') {
            var message = 'This section is not available for action : Additional Minor';
            this.showToast(component, event, this, message, 'Error');
            return;
        }

        if (tabId == 'ab5' && $A.util.isUndefinedOrNull(component.get('v.contactId'))) {
            var message = 'Please complete "Customer Information" section first';
            this.showToast(component, event, this, message, 'Error');
            return;
        }
        if(tabId == 'ab6' && (actionName == 'secondaryuser' || actionName == 'additionalminor')){
            var message = 'This section is not available for action : ' + actionLabel;
            this.showToast(component, event, this, message, 'Error');
            return;
        }

        if (tabId == 'ab6' && ($A.util.isUndefinedOrNull(component.get('v.contactId')) || component.get('v.userCreated') != true)) {
            var message = 'Please complete "Customer Information" & "Qualification" section first';
            this.showToast(component, event, this, message, 'Error');
            return;
        }


        if (tabId == 'ab7' && ((component.get('v.savedWorkOrder') != true && actionName!='secondaryuser' && actionName!='additionalminor')
            || ((actionName == 'additionalminor' || actionName == 'secondaryuser') && $A.util.isUndefinedOrNull(component.get('v.contactId')) ))) {
            var message = 'Please complete above sections first';
            this.showToast(component, event, this, message, 'Error');
            return;
        }

        var tabObj = $('#' + tabId);
        var liObj = $('#' + liId);
        this.tabEventPress(liObj, tabObj);
    },

    getLeadData : function (component, event, buttonId){
        component.set("v.Spinner", true);
        let self = this;
        var action = component.get("c.getLeadData");
        let leadRecord = component.get("v.selectedLeadOrAccount");
        action.setParams({"leadId": leadRecord.Id});

        action.setCallback(this,function(response){
            var state = response.getState();
            // debugger;
            if (state === "SUCCESS") {
                let lead = response.getReturnValue();

                self.storeLeadData(component,event,lead);
                component.set("v.Spinner", false);
                if(buttonId == 'next3') {
                    var liId = "tab4";// id for plus image
                    var tabId = "ab4";// id for circle tab
                }
                var tabObj = $('#' + tabId);
                var liObj = $('#' + liId);
                self.tabEventPress(liObj, tabObj);

            }
            else{
                component.set("v.Spinner", false);
                var message = 'Failed to fetch the lead record';
                this.showToast(component, event, this, message, 'Error');

            }

        });
        $A.enqueueAction(action);
    },

    getCustomerData : function (component, event, buttonId, actionName){
        component.set("v.Spinner", true);
        let self = this;
        var action = component.get("c.getCustomerData");
        let selectedRecord = component.get("v.selectedLeadOrAccount");
        action.setParams({"recordId": selectedRecord.Id,
            "actionName" : actionName
        });
        console.log('test get getCustomerData' + actionName);
        action.setCallback(this,function(response){
            var state = response.getState();
            // debugger;
            if (state === "SUCCESS") {
                let customerRecord = response.getReturnValue();

                if(actionName =='secondaryuser' || actionName=='additionalminor'){
                    self.prepopulateCustomData(component, event, customerRecord)
                    var liId = "tab4";// id for plus image
                    var tabId = "ab4";// id for circle tab
                }
                else {
                    self.storeCustomerData(component, event, customerRecord);
                    if(buttonId == 'next3') {
                        var liId = "tab6";// id for plus image
                        var tabId = "ab6";// id for circle tab
                    }
                }

                component.set("v.Spinner", false);


                var tabObj = $('#' + tabId);
                var liObj = $('#' + liId);
                self.tabEventPress(liObj, tabObj);

            }
            else{
                component.set("v.Spinner", false);
                var message = 'Failed to fetch the contact record';
                this.showToast(component, event, this, message, 'Error');

            }

        });
        $A.enqueueAction(action);
    },

    //helper method to stringify the data and send to apex
    storeLeadData : function(component, event,leadRecord) {

        console.log('leadrecord'+ JSON.stringify(leadRecord));
        $("#getet").val(leadRecord.Email);
        if(leadRecord.Phone){
            $("#getHomePhone").val(leadRecord.Phone.replace(/[^0-9]/g, ""));
        }
        if(leadRecord.MobilePhone){
            $("#getMobilePhone").val(leadRecord.MobilePhone.replace(/[^0-9]/g, ""));
        }
        //$("#getHomePhone").val(leadRecord.Phone);
        $("#getFirstName").val(leadRecord.FirstName);
        $("#getLastName").val(leadRecord.LastName);
        $("#getCity").val(leadRecord.City);
        $("#getState").val(leadRecord.State);
        $("#getZipCode").val(leadRecord.PostalCode);
        $("#getStreet").val(leadRecord.Street);
        //$("#getMobilePhone").val(leadRecord.MobilePhone);


    },

    //helper method to stringify the data and send to apex
    storeCustomerData : function(component, event,customerRecord) {

        console.log('customerRecord'+ JSON.stringify(customerRecord));

        if(customerRecord.Phone){
            $("#getHomePhone").val(customerRecord.Phone.replace(/[^0-9]/g, ""));
        }
        if(customerRecord.MobilePhone){
            $("#getMobilePhone").val(customerRecord.MobilePhone.replace(/[^0-9]/g, ""));
        }
        //$("#getHomePhone").val(customerRecord.Phone);

        $("#getCity").val(customerRecord.MailingCity);
        $("#getState").val(customerRecord.MailingState);
        $("#getZipCode").val(customerRecord.MailingPostalCode);
        $("#getStreet").val(customerRecord.MailingStreet);
        //$("#getMobilePhone").val(customerRecord.MobilePhone);
        $("#getCareGiver").val(customerRecord.Caregiver__c);

        $("#getet").val(customerRecord.Email);
        $('#getSSN').val(customerRecord.Last_4_of_SSN__c);
        $("#getFirstName").val(customerRecord.FirstName);
        $("#getLastName").val(customerRecord.LastName);
        if(customerRecord.Date_of_Birth__c){
            var parts =customerRecord.Date_of_Birth__c.split('/');
            let dateString = parts[2]+'-'+ parts[0]+'-'+parts[1];
            console.log('customerRecord.Date_of_Birth__c-'+$A.localizationService.formatDate(dateString));
            component.set("v.dateOfBirth",$A.localizationService.formatDate(dateString));
        }

        component.set("v.contactId",customerRecord.Id);

        let selectedHealthCareProfessional = {};
        if(customerRecord.Health_Care_Professional__c!=null && customerRecord.Health_Care_Professional__r){
            selectedHealthCareProfessional.Id = customerRecord.Health_Care_Professional__c;
            selectedHealthCareProfessional.Name = customerRecord.Health_Care_Professional__r.Name;
        }

        component.set("v.selectedHealthCareProfessional",selectedHealthCareProfessional);
        let campaignRecord = {};
        if(customerRecord.Campaign__c != null && customerRecord.Campaign__c!='' && customerRecord.Campaign__r){
            campaignRecord.Id = customerRecord.Campaign__c;
            campaignRecord.Name = customerRecord.Campaign__r.Name;
        }
        component.set("v.selectedLookUpRecord",campaignRecord);
        component.set("v.hearLossValue",customerRecord.Has_Hearing_loss__c);
        component.set("v.homePhone",customerRecord.Has_home_phone_line__c=='Yes'?true:false);
        component.set("v.internet",customerRecord.Has_internet__c=='Yes'?true:false);
        component.set("v.smartPhone",customerRecord.Has_smart_phone__c=='Yes'?true:false);
        component.set("v.hearing",customerRecord.Has_Hearing_loss__c=='Yes'?true:false);
        component.set("v.valueRadio",customerRecord.Smart_Phone_Type__c);
        component.set("v.EnableValidate",true);
        component.set("v.isValidated",true);
        component.set("v.responseMessage","Contact Created");
        component.set('v.userCreated',true);

    },

    prepopulateCustomData : function(component, event,customerRecord){

        let actionName = component.get("v.actionName");
        console.log('customerRecord---'+JSON.stringify(customerRecord));
        if(actionName == 'secondaryuser'){


            //$("#getHomePhone").val(customerRecord.Phone);
            $("#getCareGiver").val(customerRecord.Caregiver__c);
            let selectedHealthCareProfessional = {};
            if(customerRecord.Health_Care_Professional__c!=null && customerRecord.Health_Care_Professional__r){
                selectedHealthCareProfessional.Id = customerRecord.Health_Care_Professional__c;
                selectedHealthCareProfessional.Name = customerRecord.Health_Care_Professional__r.Name;
            }
            component.set("v.selectedHealthCareProfessional",selectedHealthCareProfessional);

        }
        if(customerRecord.Phone){
            $("#getHomePhone").val(customerRecord.Phone.replace(/[^0-9]/g, ""));
        }
        $("#getCity").val(customerRecord.MailingCity);
        $("#getState").val(customerRecord.MailingState);
        $("#getZipCode").val(customerRecord.MailingPostalCode);
        $("#getStreet").val(customerRecord.MailingStreet);
        //$("#getMobilePhone").val(customerRecord.MobilePhone);

        let campaignRecord = {};
        if(customerRecord.Campaign__c != null && customerRecord.Campaign__c!='' && customerRecord.Campaign__r){
            campaignRecord.Id = customerRecord.Campaign__c;
            campaignRecord.Name = customerRecord.Campaign__r.Name;
        }

    },

    refreshPage: function (component) {

        component.set("v.isOpen", false);
        $A.get('e.force:refreshView').fire();

    },


    createMinorContact : function(component, event){

        component.set("v.Spinner", true);
        let self = this;
        let obj = this.castData(component);
        let accountRecord = component.get("v.selectedLeadOrAccount");
        let action = component.get("c.createMinorContact");
        action.setParams({
            "customerInfo" : JSON.stringify(obj),
            "dateOfBirth" : component.get("v.dateOfBirth"),
            "accountId" : accountRecord.Id
        });

        action.setCallback(this,function(response){
            const state = response.getState();
            // debugger;
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                console.log('response of createMinorContact ' + JSON.stringify(resp));

                if (resp != null && !resp.msg.includes('Error')) {
                    component.set('v.responseMessage', resp.msg);
                    component.set("v.contactId", resp.ContactId);
                    component.set("v.contact",resp.createdContact);
                    self.showToast(component, event, self, 'Contact Created!', 'Success');

                    let liId = "tab7";// id for plus image
                    let tabId = "ab7";// id for circle tab

                    let tabObj = $('#' + tabId);
                    let liObj = $('#' + liId);
                    self.tabEventPress(liObj, tabObj);


                }
                else{
                    self.showToast(component, event, self, resp.msg, 'Error');
                }

            }else{
                const message = 'Failed to create the minor contact record. Please contact the administrator!';
                self.showToast(component, event, this, message, 'Error');

            }
            component.set("v.Spinner", false);

        });
        $A.enqueueAction(action);

    },

    controlNavigation : function(component, event, recordId, objectName) {
        let parentTabId='';
        let self = this;
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(response) {

            console.log('isconsole--' + JSON.stringify(response));

            if(response){
                workspaceAPI.getFocusedTabInfo().then(function(resp) {

                    let focusedTabId = resp.tabId;
                    console.log('focusedTabId--'+ focusedTabId);
                    const url = '/lightning/r/'+objectName+'/'+recordId+'/view';
                    console.log('url - ' + '/lightning/r/'+objectName+'/'+recordId+'/view');

                    workspaceAPI.openConsoleURL({
                        "url": url,
                        "focus": true,
                    }).then(function(activeTabId) {
                        console.log('active tab id'+ activeTabId);


                    })
                        .catch(function(error) {
                            console.log('openurl error - ' + error);
                        });
                    self.refreshPage(component);

                    /*
                    //Opening New Tab
                    workspaceAPI.openTab({
                        url: '#/sObject/'+ recordId +'/view'
                    }).then(function(response) {
                        workspaceAPI.focusTab({tabId : response});
                        self.refreshPage(component);
                    })
                    .catch(function(error) {
                        console.log('error in open tab' + error);
                    });*/

                })
                    .catch(function(error) {
                        console.log('error loading123-'+error);
                    });

            }
            else{
                self.navigationNonConsole(component,event,recordId);
            }


        })
            .catch(function(error) {
                console.log('workspace api error-' + error);
            });

    },

    navigationNonConsole : function(component, event, recordId){

        console.log('inside navigationNonConsole-' + recordId);
        let pageReference = {
            "type": "standard__recordPage",
            "attributes": {
                "recordId": recordId,
                "actionName": "view"
            },
            "state":{
                "nooverride":"1"
            }
        };

        let navService = component.find("navService");
        navService.navigate(pageReference);
    },


    populateFieldNation : function (component,index){
        console.log('inside field nation')
        var action = component.get("c.populateFieldNation");
        // set a callBack
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if( storeResponse.length == 1){
                    console.log('storeResponse-'+JSON.stringify(storeResponse[0]))
                    var workOrderWrapperList = component.get("v.workOrderWrapperList");
                    workOrderWrapperList[index].technician = storeResponse[0];
                    component.set("v.workOrderWrapperList", workOrderWrapperList);
                }
            }
        });
        // enqueue the Action
        $A.enqueueAction(action);

    },

    finalCreateContactOrLead : function(component,event,createContact,selectedVal,actionName){
        const self = this;
        component.set("v.Spinner",true);
        let action = component.get('c.createContactWithQualification');
        let obj = this.castData(component);
        let healthCareProfessional = component.get("v.selectedHealthCareProfessional");
        let selectLeadAccountRecord = component.get("v.selectedLeadOrAccount");
        let bypassLexisValidation = component.get("v.bypassLexisValidation");
        action.setParams({
            "CampId": component.get("v.campaignId"),
            "CustomerInfo": JSON.stringify(obj),
            "createContact": createContact,
            "hasHomePhone": component.get('v.homePhone'),
            "internet": component.get('v.internet'),
            "smartPhone": component.get('v.smartPhone'),
            "hearing": component.get('v.hearing'),
            "type": selectedVal,
            "healthCareProfessionalId" : healthCareProfessional.Id==undefined?null:healthCareProfessional.Id,
            "dateOfBirth" : component.get("v.dateOfBirth"),
            "selectLeadAccountId" : selectLeadAccountRecord.Id==undefined?null:selectLeadAccountRecord.Id,
            "actionName" : actionName,
            "riskCodeMap" : component.get("v.riskCodeMap"),
            "dobLevel" : component.get("v.dobLevel"),
            "bypassLexisValidation" : bypassLexisValidation

        });


        action.setCallback(this, function (res) {
            //debugger;
            //component.set("v.Spinner", false);
            let resp = res.getReturnValue();
            console.log('response of Create contact ' + JSON.stringify(resp));
            if (resp != null && !resp.msg.includes('Error')) {

                component.set('v.responseMessage', resp.msg);
                //when contact is created, create user in 3rd party system.
                if (!$A.util.isUndefinedOrNull(resp.ContactId)) {
                    component.set("v.contactId", resp.ContactId);
                    component.set("v.contact",resp.createdContact);
                    self.showToast(component, event, self, 'Contact Created!', 'Success');
                    self.createNewUser(component, event, self, resp.ContactId);

                }
                else {//if only lead is created
                    component.set("v.leadId", resp.leadId);
                    component.set("v.Spinner", false);
                    let profileName = component.get("v.profileName");
                    let userRole = component.get("v.userRole");
                    if(actionName == 'createlead'){

                        if(profileName == 'CC Inside Sales Representative' || profileName == 'CC Inside Sales Manager' ||
                            profileName == 'CC System Administrator' || profileName == 'System Administrator' ||
                            (userRole && userRole == 'CC User Contractor')){
                            var message = 'Lead Created! Click Okay to refresh and go to Lead record.';
                        }else {
                            var message = 'Lead Created! Click Okay to refresh.';
                        }
                    }
                    else{
                        if(profileName == 'CC Inside Sales Representative' || profileName == 'CC Inside Sales Manager' ||
                            profileName == 'CC System Administrator' || profileName == 'System Administrator' ||
                            (userRole && userRole == 'CC User Contractor')){
                            var message = 'Qualifications have not been met to create a Contact. \n A Lead has been created. Click Okay to refresh and go to the Lead record.';
                        }else {
                            var message = 'Qualifications have not been met to create a Contact. \n A Lead has been created. Click Okay to refresh.';
                        }
                    }
                    component.set("v.message", message);
                    component.set("v.isOpen", true);
                }
            }
            else {
                component.set("v.Spinner", false);
                self.showToast(component, event, self, resp.msg, 'Error');
            }

        });
        $A.enqueueAction(action);
    },

    lexisNexusCallout : function( component, event,selectedVal,actionName) {
        let bucketSelect = 'bucket1';
        let obj = this.castData(component);
        component.set("v.Spinner", true);
        let self = this;
        let lexisPass = component.get("v.lexisPass");
        if(!lexisPass){

            let action = component.get("c.makeLexisNexisCallout");
            action.setParams({
                "CustomerInfo" : JSON.stringify(obj),
                "dateOfBirth" : component.get("v.dateOfBirth")
            });

            action.setCallback(this,function(response){
                var state = response.getState();
                let lexisCount = component.get('v.lexisCount');
                // debugger;
                if (state === "SUCCESS") {
                    let resp = response.getReturnValue();
                    console.log('resp lexisNexis--'+JSON.stringify(resp));
                    console.log('lexisCount--'+lexisCount);
                    component.set("v.Spinner", false);
                    if(resp.requestStatus == 200){
                        console.log('resp.requestCalloutMap-'+JSON.stringify(resp.requestCalloutMap));
                        let riskCodeList = [];
                        for (var key in resp.requestCalloutMap) {
                            riskCodeList.push(key);
                        }
                        console.log('riskCodeList-'+riskCodeList);
                        let dobLevel = resp.dobLevel;
                        console.log('dobLevel-'+dobLevel);
                        let bucketContain1 = riskCodeList.filter(x => self.rolkaBucket1.includes(x));
                        let bucketContain2 = riskCodeList.filter(x => self.rolkaBucket2.includes(x));
                        console.log('bucketContain1--'+bucketContain1);
                        console.log('bucketContain2--'+bucketContain2);
                        if(lexisCount<2 && (bucketContain2.length>0 || bucketContain1.length>0 || (dobLevel && dobLevel!='8'))){

                            if(lexisCount == 0){
                                //check if Flow 1
                                if(dobLevel !='8' || (bucketContain1.length>0 && bucketContain2.length==0
                                    && riskCodeList.length>0)){ //first flow
                                    let finalArray = [];
                                    const group1 = ['NF','37','76']
                                    const group2 = ['51','26'];
                                    const group3 = ['CA'];
                                    const group4 =['08'];
                                    const group5 = ['DI','66','72']; //denoted as group6 in flow diagram
                                    if(bucketContain1.length>0 && bucketContain1.filter(x => !group1.includes(x)).length == 0 && dobLevel =='8'){

                                        let warning = 'Check Spelling of First and Last Names.\n This would reflect what would be on a driver\'s license (Legal Name).';
                                        component.set('v.showWarning',true);
                                        component.set('v.warningMessage',warning);
                                        component.set('v.warningMessage1','');
                                        component.set('v.lexisCount',lexisCount+1);

                                        let liId = "tab4";// id for plus image
                                        let tabId = "ab4";// id for circle tab
                                        let tabObj = $('#' + tabId);
                                        let liObj = $('#' + liId);
                                        self.tabEventPress(liObj, tabObj);

                                    }
                                    else if(bucketContain1.length>0 && bucketContain1.filter(x => !group2.includes(x)).length == 0 && dobLevel =='8'){

                                        let warning = 'Check Last 4 SSN. This should be 4 digits.';
                                        component.set('v.showWarning',true);
                                        component.set('v.warningMessage',warning);
                                        component.set('v.warningMessage1','');
                                        component.set('v.lexisCount',lexisCount+1);

                                        let liId = "tab4";// id for plus image
                                        let tabId = "ab4";// id for circle tab
                                        let tabObj = $('#' + tabId);
                                        let liObj = $('#' + liId);
                                        self.tabEventPress(liObj, tabObj);

                                    }
                                    else if(bucketContain1.length>0 && bucketContain1.filter(x => !group3.includes(x)).length == 0 && dobLevel =='8'){

                                        let warning = 'Check the address. This should be a home address.';
                                        component.set('v.showWarning',true);
                                        component.set('v.warningMessage',warning);
                                        component.set('v.warningMessage1','');
                                        component.set('v.lexisCount',lexisCount+1);

                                        let liId = "tab4";// id for plus image
                                        let tabId = "ab4";// id for circle tab
                                        let tabObj = $('#' + tabId);
                                        let liObj = $('#' + liId);
                                        self.tabEventPress(liObj, tabObj);

                                    }
                                    else if(bucketContain1.length>0 && bucketContain1.filter(x => !group4.includes(x)).length == 0 && dobLevel =='8'){

                                        let warning = 'Check Home/Mobile Phone. This should be a 10 digits.';
                                        component.set('v.showWarning',true);
                                        component.set('v.warningMessage',warning);
                                        component.set('v.warningMessage1','');
                                        component.set('v.lexisCount',lexisCount+1);

                                        let liId = "tab4";// id for plus image
                                        let tabId = "ab4";// id for circle tab
                                        let tabObj = $('#' + tabId);
                                        let liObj = $('#' + liId);
                                        self.tabEventPress(liObj, tabObj);

                                    }
                                    else if(bucketContain1.length==0 && dobLevel !='8'){ //show DOB error only

                                        let warning = 'Check Date of Birth. Please select from calendar.';
                                        component.set('v.showWarning',true);
                                        component.set('v.warningMessage',warning);
                                        component.set('v.warningMessage1','');
                                        component.set('v.lexisCount',lexisCount+1);

                                        let liId = "tab4";// id for plus image
                                        let tabId = "ab4";// id for circle tab
                                        let tabObj = $('#' + tabId);
                                        let liObj = $('#' + liId);
                                        self.tabEventPress(liObj, tabObj);

                                    }
                                    else if((bucketContain1.length>0 && bucketContain1.filter(x => !group5.includes(x)).length == 0) || bucketContain1.length>0){
                                        //display Message B
                                        let warning = 'Double check First Name, Last Name, Address,\n Home/Mobile Phone, Date of Birth and Last 4 SSN.';
                                        component.set('v.showWarning',true);
                                        component.set('v.warningMessage',warning);
                                        component.set('v.warningMessage1','');
                                        component.set('v.lexisCount',lexisCount+2);

                                        let liId = "tab4";// id for plus image
                                        let tabId = "ab4";// id for circle tab
                                        let tabObj = $('#' + tabId);
                                        let liObj = $('#' + liId);
                                        self.tabEventPress(liObj, tabObj);

                                    }
                                }
                                else if(bucketContain2.length>0 && riskCodeList.length>0){ //check if flow 2
                                    //Display Message C
                                    let warning = 'Please Proceed!';
                                    let warning1 = '\n Risk Code(s) still persist. To be addressed with CEX support.';
                                    component.set('v.showWarning',true);
                                    component.set('v.warningMessage',warning);
                                    component.set('v.warningMessage1',warning1);
                                    component.set('v.lexisCount',3);
                                    component.set('v.lexisPass',true);
                                    component.set('v.riskCodeMap',resp.requestCalloutMap);
                                    component.set('v.dobLevel',dobLevel);

                                    let liId = "tab4";// id for plus image
                                    let tabId = "ab4";// id for circle tab
                                    let tabObj = $('#' + tabId);
                                    let liObj = $('#' + liId);
                                    self.tabEventPress(liObj, tabObj);
                                }
                                else { //if the risk codes are not in both bucket
                                    component.set('v.lexisPass',true);
                                    component.set('v.showWarning',false);
                                    component.set('v.lexisCount',3);
                                    component.set('v.riskCodeMap',resp.requestCalloutMap);
                                    component.set('v.dobLevel',dobLevel);
                                    //call create contact
                                    self.finalCreateContactOrLead(component,
                                        event,
                                        true,
                                        selectedVal,
                                        actionName);
                                }
                            }
                            else if(lexisCount == 1){ //second iteration show Message B error irrespective of LexisCount.

                                if(dobLevel !='8' || (bucketContain1.length>0 && bucketContain2.length==0
                                    && riskCodeList.length>0)){

                                    let warning = 'Double check First Name, Last Name, Address,\n Home/Mobile Phone, Date of Birth and Last 4 SSN.';
                                    component.set('v.showWarning',true);
                                    component.set('v.warningMessage',warning);
                                    component.set('v.warningMessage1','');
                                    component.set('v.lexisCount',lexisCount+1);

                                    let liId = "tab4";// id for plus image
                                    let tabId = "ab4";// id for circle tab
                                    let tabObj = $('#' + tabId);
                                    let liObj = $('#' + liId);
                                    self.tabEventPress(liObj, tabObj);
                                }
                                else if(bucketContain2.length>0 && riskCodeList.length>0){
                                    let warning = 'Please Proceed!';
                                    let warning1 = '\n Risk Code(s) still persist. To be addressed with CEX support.';
                                    component.set('v.showWarning',true);
                                    component.set('v.warningMessage',warning);
                                    component.set('v.warningMessage1',warning1);
                                    component.set('v.lexisCount',3);
                                    component.set('v.lexisPass',true);
                                    component.set('v.riskCodeMap',resp.requestCalloutMap);
                                    component.set('v.dobLevel',dobLevel);

                                    let liId = "tab4";// id for plus image
                                    let tabId = "ab4";// id for circle tab
                                    let tabObj = $('#' + tabId);
                                    let liObj = $('#' + liId);
                                    self.tabEventPress(liObj, tabObj);
                                }

                            }

                        }
                        else if(lexisCount == 2 && (bucketContain2.length>0 || bucketContain1.length>0 || dobLevel!='8')){
                            // if 3 validation done and still error persist
                            //Display Message C
                            let warning = 'Please Proceed!';
                            let warning1 = '\n Risk Code(s) still persist. To be addressed with CEX support.';
                            component.set('v.showWarning',true);
                            component.set('v.warningMessage',warning);
                            component.set('v.warningMessage1',warning1);
                            component.set('v.lexisCount',3);
                            component.set('v.lexisPass',true);
                            component.set('v.riskCodeMap',resp.requestCalloutMap);
                            component.set('v.dobLevel',dobLevel);

                            let liId = "tab4";// id for plus image
                            let tabId = "ab4";// id for circle tab
                            let tabObj = $('#' + tabId);
                            let liObj = $('#' + liId);
                            self.tabEventPress(liObj, tabObj);

                        }
                        else{
                            // call create contact method
                            //component.set("v.Spinner", false);
                            //component.set('v.riskCodeMap',resp.requestCalloutMap);
                            //component.set('v.dobLevel',dobLevel);
                            let message = 'Lexis Nexis Validation Pass';
                            this.showToast(component, event, this, message, 'Success');
                            self.finalCreateContactOrLead(component,
                                event,
                                true,
                                selectedVal,
                                actionName);
                        }

                    }
                    else{
                        component.set("v.Spinner", false);
                        let message = 'Failed to make lexis callout, please try again.';
                        this.showToast(component, event, this, message, 'Error');
                    }

                }
                else{
                    component.set("v.Spinner", false);
                    let message = 'Failed to make lexis callout, please try again.';
                    this.showToast(component, event, this, message, 'Error');

                }

            });
            $A.enqueueAction(action);
        }
        else{
            //component.set("v.Spinner", false);
            if(!component.get("v.bypassLexisValidation")){
                if(!component.get("v.showWarning")){
                    let message = 'Lexis Nexis Validation Pass!';
                    this.showToast(component, event, this, message, 'Success');
                }
            }


            //create contact
            self.finalCreateContactOrLead(component,
                event,
                true,
                selectedVal,
                actionName);

        }

    },
	
    finalValidationOnNonRequiredFields : function(component,event){
        let actionName = component.get("v.actionName");
        let bypassLexisValidation = component.get("v.bypassLexisValidation");
        var dateOfBirth = component.get("v.dateOfBirth");
        var mobileNumber = $("#getMobilePhone").val();
        var phoneNumber = $("#getHomePhone").val();
        var zipcode = $("#getZipCode").val();
        var SSN = $("#getSSN").val();
        var apt = $("#getApt").val();
        var ValidChars = "0123456789";
        var IsNumber = true;
        var Char;
        var emailval = $("#getet").val();
        var atposition = emailval.indexOf("@");
        var dotposition = emailval.lastIndexOf(".");
        var firstName = $("#getFirstName").val();
        var lastName = $("#getLastName").val();
        var alphaExp1 = /^[a-zA-Z\ ]+$/;
        var alphaExp2 = /^[a-zA-Z\-]+$/;
        var alphaExp3 = /^[a-z0-9\- ]+$/i;
        console.log('SSN-' +SSN );
        let isSSNNumber = /^\d+$/.test(SSN);
        if (actionName!='createlead' && actionName!='additionalminor' 
            && !bypassLexisValidation && ( SSN == ''  || SSN.length != 4 || !isSSNNumber)) {
            component.set("v.Spinner", false);
            this.showToast(component, event, this, 'Please enter 4 digit SSN.', 'Error');
            return false;
        }
        
        if(dateOfBirth == null && actionName!='createlead'){           
            if(actionName =='additionalminor' || (!bypassLexisValidation && actionName!='additionalminor' ) ){
                component.set("v.Spinner", false);
                this.showToast(component, event, this, 'Please enter Date of Birth.', 'Error');
                return false;
            }
        }

        for (var i = 0; i < mobileNumber.length && IsNumber == true; i++) {
            Char = mobileNumber.charAt(i);
            console.log('charecter>>>' + Char);
            if (ValidChars.indexOf(Char) == -1 || Char == '-') {
                IsNumber = false;
            }
        }
        
        for (var i = 0; i < phoneNumber.length && IsNumber == true; i++) {
            Char = phoneNumber.charAt(i);
            console.log('charecterhome>>>' + Char);
            if (ValidChars.indexOf(Char) == -1 || Char == '-') {
                IsNumber = false;
            }
        }

        //either mobile or home phone should be of length 10.
        if ((mobileNumber != '' && mobileNumber.length != 10) ||
            (phoneNumber != '' && phoneNumber.length != 10) || IsNumber == false) {
            component.set("v.Spinner", false);
            this.showToast(component, event, this, 'Please Enter Valid 10 Digit Home Tel./Mobile Number.', 'Error');
            return false;
        }
        
        //either mobile or home phone should be of length 10.
        if (actionName!='additionalminor' && mobileNumber == '' && phoneNumber == '') {
            component.set("v.Spinner", false);
            this.showToast(component, event, this, 'Please Enter Valid 10 Digit Home Tel./Mobile Number.', 'Error');
            return false;
        }
        
        if ((apt != '') && (!apt.match(alphaExp3))) {
            component.set("v.Spinner", false);
            this.showToast(component, event, this, 'Apt/Unit/Suite can be Alphanumberic only.', 'Error');
            return false;
        }

        if (!firstName.match(alphaExp1)) {
            component.set("v.Spinner", false);
            this.showToast(component, event, this, 'First Name should be letters and spaces only.', 'Error');
            return false;
        }

        if (!lastName.match(alphaExp2)) {
            component.set("v.Spinner", false);
            this.showToast(component, event, this, 'Last Name should be letters and Hyphen only.', 'Error');
            return false;
        }

        if(actionName!='additionalminor' || (actionName=='additionalminor' && emailval)){
            //validating email
            if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= emailval.length) {
                component.set("v.Spinner", false);
                this.showToast(component, event, this, 'Enter a valid email address.', 'Error');
                return false;           
            }
        }

        if (zipcode == '' || zipcode.length != 5) {
            component.set("v.Spinner", false);
            this.showToast(component, event, this, 'Please enter 5 digit Zip Code.', 'Error');
            return false;
        }
        
        return true;      
    }

})