({

    doInit: function (component, event, helper) {

        var action2 = component.get("c.populateStartTimeWindow");
        action2.setCallback(this, function (res) {
            var response = res.getReturnValue();

            if (!$A.util.isUndefinedOrNull(response)) {
                component.set("v.startTimeWindow", response);

            }
        });
        $A.enqueueAction(action2);

        var action = component.get("c.productList");
        action.setCallback(this, function (res) {
            var response = res.getReturnValue();

            if (!$A.util.isUndefinedOrNull(response)) {
                component.set("v.productList", response);

            }
        });
        $A.enqueueAction(action);

        var action3 = component.get("c.getLabourSource");
        action3.setCallback(this, function (res) {
            var response = res.getReturnValue();

            if (!$A.util.isUndefinedOrNull(response)) {
                component.set("v.laborSourceList", response);

            }
        });
        $A.enqueueAction(action3);

        var action1 = component.get("c.populateFulfillment");
        action1.setCallback(this, function (res) {
            var response = res.getReturnValue();

            if (!$A.util.isUndefinedOrNull(response)) {
                component.set("v.fulfillmentList", response);

            }
        });
        $A.enqueueAction(action1);

        var action4 = component.get("c.getProfileName");
        action4.setCallback(this, function (res) {
            var response = res.getReturnValue();

            if (!$A.util.isUndefinedOrNull(response)) {
                console.log('profilename--'+response);
                component.set("v.profileName", response);

            }
        });
        $A.enqueueAction(action4);

        var action5 = component.get("c.getUserRoleName");
        action5.setCallback(this, function (res) {
            var response = res.getReturnValue();

            if (!$A.util.isUndefinedOrNull(response)) {
                console.log('userROle--'+response);
                component.set("v.userRole", response);

            }
        });
        $A.enqueueAction(action5);

        let recordTypeList = [];
        let recordtype = {name:'Local',RecordtypeName:'Event'};
        recordTypeList.push(recordtype);
        recordtype = {name:'Large',RecordtypeName:'Large Event'};
        recordTypeList.push(recordtype);
        recordtype = {name:'HCP',RecordtypeName:'Hearing Professional'};
        recordTypeList.push(recordtype);
        recordtype = {name:'Marketing',RecordtypeName:'Marketing'};
        recordTypeList.push(recordtype);
        component.set("v.recordtypeList",recordTypeList);
        var workspaceAPI = component.find("workspace");

        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: "SOAP"
            });
        })
    },

    scriptsLoaded: function (component, event, helper) {
        var tabId = 'ab1';// id for plus image
        //alert('liId'+liId);
        var liId = 't' + tabId;
        var tabObj = $('#' + tabId);
        var liObj = $('#' + liId);
        helper.tabEventPress(liObj, tabObj);

    },



    onClickOfPlus: function (component, event, helper) {

        var liId = event.currentTarget.id;// id for plus image
        //alert('liId'+liId);
        var tabId = liId.substring(1, 4);
        helper.navigationAction(component,event, tabId,liId);

    },

    onClickOfTabName: function (component, event, helper) {
        var liId = event.currentTarget.id;// id for plus image
        //alert('liId'+liId);
        var tabId = liId.substring(2, 5);

        helper.navigationAction(component,event, tabId,liId);

    },

    onClickOfNum: function (component, event, helper) {

        var tabId = event.currentTarget.id;// id for plus image
        //alert('liId'+liId);
        var liId = 't' + tabId;

        helper.navigationAction(component,event, tabId,liId);

    },

    nextButtonEvent: function (component, event, helper) {
        //var isOverride = event.getParam("isOverride");

        var buttonId = event.getParam("tabId");// id for plus image
        //alert('buttonId'+buttonId);
        if (buttonId == 'next2') {
            var liId = "tab3";// id for plus image
            var tabId = "ab3";// id for circle tab
        }
        var tabObj = $('#' + tabId);
        var liObj = $('#' + liId);
        var isHearingLoss = event.getParam("isHearLoss");
        var message = event.getParam("message");
        var isOverride = event.getParam("isOverride");

        var contactId = event.getParam("conId");

        //component.set('v.contact',contact);
        component.set('v.conId', contactId);
        console.log('contactID>>>>>??' + JSON.stringify(component.get('v.conId')));
        //alert('isHearingLoss-'+isHearingLoss);
        if (tabId == 'ab3' && (!isHearingLoss || isOverride)) {
            /*var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "message": 'You cannot go further since Hearing loss is set to false'
            });
            toastEvent.fire();*/
            component.set("v.message", message);
            component.set("v.isOpen", true);
            return;
        }

        helper.tabEventPress(liObj, tabObj);

    },

    onClickOfNextFromLeadAccount : function (component, event, helper){


        let buttonId = event.currentTarget.id;
        let actionName = component.get("v.actionName");
        console.log('actionName' +actionName);
        let selectedLeadOrAccount = component.get("v.selectedLeadOrAccount");
        if ($A.util.isUndefinedOrNull(selectedLeadOrAccount.Id)) {
            var message = 'Please Select a record to continue';
            helper.showToast(component, event, helper, message, 'Error');

            return;
        }

        if(actionName == 'convertlead'){
            helper.getLeadData(component, event, buttonId);
        }
        else if(actionName == 'installworkorder' || actionName == 'secondarydevice' ||
            actionName =='secondaryuser' || actionName == 'additionalminor'){
            console.log('actonName onClickOfNextFromLeadAccount'+ actionName);
            helper.getCustomerData(component,event,buttonId, actionName);
        }


    },


    onClickOfNext: function (component, event, helper) {
        //helper.controlNavigation(component, event, '00Q0S0000062w9jUAA','lead');
        //return;

        var buttonId = event.currentTarget.id;
        //alert('buttonId'+buttonId);
        if (buttonId == 'next1') {
            var liId = "tab4";// id for plus image
            var tabId = "ab4";// id for circle tab
        }
        if (buttonId == 'next2') {
            var liId = "tab5";// id for plus image
            var tabId = "ab5";// id for circle tab
        }

        var tabObj = $('#' + tabId);
        var liObj = $('#' + liId);
        var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
        console.log('selectedLookUpRecord-' + selectedLookUpRecord);
        if (tabId == 'ab4' && $A.util.isUndefinedOrNull(selectedLookUpRecord.Id)) {
            var message = 'Please Select an Event to continue';
            helper.showToast(component, event, helper, message, 'Error');

            return;
        }

        var isHearingLoss = component.get("v.isHearingLoss");
        console.log('isHearingLoss-' + isHearingLoss);
        if (tabId == 'ab5' && !isHearingLoss) {
            var message = 'You cannot go further since Hearing loss is set to false';
            helper.showToast(component, event, helper, message, 'Error');
            return;
        }

        helper.tabEventPress(liObj, tabObj);

        component.set('v.disableNext', false);

    },

    handleComponentEvent: function (component, event, helper) {
        // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");


        //run only for Campign Lookup only
        if (!$A.util.isUndefinedOrNull(selectedAccountGetFromEvent.Id) && selectedAccountGetFromEvent.Id.startsWith("701")) {
            component.set("v.selectedLookUpRecord", selectedAccountGetFromEvent);
            console.log('recordselected>>>' + JSON.stringify(selectedAccountGetFromEvent));
            component.set('v.campName', selectedAccountGetFromEvent.Name);
            var subchannel = $A.util.isUndefinedOrNull(selectedAccountGetFromEvent.Sub_Channel__c) ? '' : selectedAccountGetFromEvent.Sub_Channel__c;
            var channel = $A.util.isUndefinedOrNull(selectedAccountGetFromEvent.Channel__c) ? '' : selectedAccountGetFromEvent.Channel__c;
            component.set('v.startdateChannel', "(" + channel + ":" + subchannel + ")");
        }
    },

    cancelHandle: function (component, event, helper) {
        // alert('clicked cancel');
        component.set('v.valueEntered', "");
        component.set('v.campName', "");
        component.set('v.startdateChannel', "");
        component.set("v.selectedLookUpRecord", []);
        //fire cancel event
        var appEvent = $A.get("e.c:Cancelevent");
        appEvent.setParams({ "message": "fire" });

        appEvent.fire();

    },

    clearAction: function (component, event, helper) {
        //alert('clearing');
        component.set('v.disableNext', true);
        component.set("v.selectedLookUpRecord", []);
        component.set('v.campName', "");
        component.set('v.startdateChannel', "");

    },

    openModel: function (component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },

    closeModel: function (component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "False"
        component.set("v.isOpen", false);
    },

    refreshOnDone: function (component, event, helper) {

        let actionName = component.get("v.actionName");
        // and set set the "isOpen" attribute to "False for close the model Box.
        let profileName = component.get("v.profileName");
        let userRole = component.get("v.userRole");
        if((actionName == 'convertlead' || actionName == 'createcustomer')
            && (profileName == 'CC Inside Sales Representative' || profileName == 'CC Inside Sales Manager' ||
                profileName == 'CC System Administrator' || profileName == 'System Administrator' ||
                (userRole && userRole == 'CC User Contractor'))) {
            let contactId = component.get("v.contactId");
            if(contactId){
                helper.controlNavigation(component, event, contactId,'contact');
            }

            //helper.refreshPage(component);
        }
        else{

            helper.refreshPage(component);
        }

    },

    refreshPage: function (component, event, helper) {

        let message = component.get("v.message");
        // and set set the "isOpen" attribute to "False for close the model Box.
        let profileName = component.get("v.profileName");
        let actionName = component.get("v.actionName");
        let userRole = component.get("v.userRole");
        console.log('message--' + message);
        console.log('profileName--' + profileName);
        if(message && message.includes('Lead')
            && (profileName == 'CC Inside Sales Representative' || profileName == 'CC Inside Sales Manager' ||
                profileName == 'CC System Administrator' || profileName == 'System Administrator' ||
                (userRole && userRole == 'CC User Contractor'))) {

            if(actionName == 'convertlead')
                helper.refreshPage(component);

            let leadId = component.get("v.leadId");
            if(leadId){
                helper.controlNavigation(component, event, leadId,'lead');
            }

        }
        else{
            console.log('else - refresh page');
            helper.refreshPage(component);
        }

    },

    QualificationNext: function (component, event, helper) {
        component.set("v.Spinner", true);
        var buttonId = event.currentTarget.id;
        let actionName = component.get("v.actionName");
        // var selectedValselect = cmp.find('select').get('v.value');
        //check if contact already created
        var reponseMessage = component.get("v.responseMessage");
        if (!$A.util.isUndefinedOrNull(reponseMessage) && (reponseMessage.includes('Contact Created') ||
            reponseMessage.includes('Lead Created'))) {

            if (reponseMessage.includes('Contact Created')) {

                if (component.get('v.userCreated') != true) {
                    component.set("v.Spinner", false);
                    helper.showToast(component, event, helper, component.get('v.userCreationError'), 'Error');
                    return;
                }
                var liId = "tab6";// id for plus image
                var tabId = "ab6";// id for circle tab

                if(actionName == 'secondaryuser'){
                    liId = "tab7";
                    tabId = "ab7";
                }

                var tabObj = $('#' + tabId);
                var liObj = $('#' + liId);
                helper.tabEventPress(liObj, tabObj);
                //return;

            }//when hearloss was false
            else if (reponseMessage.includes('Lead Created')) {
                component.set("v.Spinner", false);
                var message = 'Lead already created, Please click on Okay to start again';
                component.set("v.message", message);
                component.set("v.isOpen", true);
                //return;
            }
            else {
                helper.showToast(component, event, helper, reponseMessage, 'Error');
            }
            component.set("v.Spinner", false);
            return;

        }

        let selectedVal;
        let callserver = false;
        //if the select smart phone then the one of the options is mandatory.
        if (component.get('v.smartPhone') == true) {
            selectedVal = component.get('v.valueRadio');
            if (selectedVal == null || selectedVal == '' || selectedVal == 'option1') {
                callserver = false;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Please select smartphone type.',
                    "type": "error",
                    "title": "Error!"
                });
                toastEvent.fire();
                component.set("v.Spinner", false);
            }
            else {
                callserver = true;
                console.log('setting it true');
            }
        }
        else {
            selectedVal = null;
            callserver = true;
        }

        console.log('selected val>>>' + selectedVal);
        if (callserver) {
            console.log('inside server call');
            let internet = component.get('v.internet');
            let smartPhone = component.get('v.smartPhone');
            let createContact = false;

            let healthCareProfessional = component.get("v.selectedHealthCareProfessional");
            let selectLeadAccountRecord = component.get("v.selectedLeadOrAccount");
            if(actionName == 'createcustomer' || actionName == 'convertlead' || actionName == 'secondaryuser'){
                //create contact if below conditions satisfy
                if (internet == true || (smartPhone == true && selectedVal == 'iOS')) {
                    createContact = true;

                }
                else {
                    //functionality for convert lead
                    if(actionName == 'convertlead'){
                        component.set("v.Spinner", false);

                        var message = 'Lead is not Qualified to be converted. Click Okay to refresh.';

                        component.set("v.message", message);
                        component.set("v.isOpen", true);
                        return;
                    }


                }

            }
            if(createContact){
                helper.lexisNexusCallout(component,event,selectedVal,actionName);

            }else{//create lead

                helper.finalCreateContactOrLead(component,
                    event,
                    createContact,
                    selectedVal,
                    actionName);

            }
        }
    },

    smartPhoneSet: function (component, event, helper) {
        if (component.get('v.smartPhone') == true) {
            component.set('v.showPhoneType', true);
        }
        else {
            component.set('v.valueRadio', false);
            component.set('v.showPhoneType', false);

        }
    },

    ClearQuestions: function (component, event, helper) {
        component.set('v.homePhone', false);
        component.set('v.internet', false);
        component.set('v.smartPhone', false);
        component.set('v.hearing', false);
        component.set('v.showPhoneType', false);
        component.set('v.valueRadio', 'option1');

    },

    addRow: function (component, event, helper) {
        helper.addNewRow(component, event);
    },

    removeRow: function (component, event, helper) {
        //Get the account list
        var workOrderWrapperList = component.get("v.workOrderWrapperList");
        //var deleteRecordList = component.get("v.deleteRecordList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;

        workOrderWrapperList.splice(index, 1);
        if (workOrderWrapperList.length == 0) {
            component.set("v.confirmAppointment", false);
        }
        component.set("v.workOrderWrapperList", workOrderWrapperList);

    },

    onChangeProductValue: function (component, event, helper) {
        var selectedItem = event.getSource();
        var index = selectedItem.get("v.label");
        var value = selectedItem.get("v.value");
        var productList = component.get("v.productList");
        component.set("v.product", {});
        var product = component.get("v.product");
        for (var i = 0; i < productList.length; i++) {
            if (value == productList[i].Id) {
                product.Name = productList[i].Name;
                product.Id = productList[i].Id;
                break;
            }
        }

        console.log('value>>>>>>changed' + value + "-" + index);
        var workOrderWrapperList = component.get("v.workOrderWrapperList");

        workOrderWrapperList[index].ordrLineItem.Product2Id = value;
        workOrderWrapperList[index].product = product;
        //console.log('after change index>>>'+JSON.stringify(workOrderWrapperList[index]));
        //console.log('after change>>>'+JSON.stringify(workOrderWrapperList));
        component.set("v.workOrderWrapperList", workOrderWrapperList);

    },

    onChangeOfDate : function (component, event, helper) {
        component.set("v.confirmAppointment", false);
    },

    onChangeStartTimeWindow : function (component, event, helper) {
        var selectedItem = event.getSource();
        var index = selectedItem.get("v.label");
        var value = selectedItem.get("v.value");
        console.log('value>>>>>>changed' + value + "-" + index);
        var workOrderWrapperList = component.get("v.workOrderWrapperList");

        workOrderWrapperList[index].workOrder.Start_Time_Window__c = value;

        component.set("v.workOrderWrapperList", workOrderWrapperList);
    },

    onChangeFulfillment : function (component, event, helper) {
        var selectedItem = event.getSource();
        var index = selectedItem.get("v.label");
        var value = selectedItem.get("v.value");
        console.log('value>>>>>>changed' + value + "-" + index);
        var workOrderWrapperList = component.get("v.workOrderWrapperList");

        workOrderWrapperList[index].wrapperOrder.Fulfillment__c = value;

        component.set("v.workOrderWrapperList", workOrderWrapperList);
    },

    onChangeLaborSource: function (component, event, helper) {
        var selectedItem = event.getSource();
        var index = selectedItem.get("v.label");
        var value = selectedItem.get("v.value");
        console.log('value>>>>>>changed' + value + "-" + index);
        var workOrderWrapperList = component.get("v.workOrderWrapperList");

        workOrderWrapperList[index].workOrder.Labor_Source__c = value;

        if(value == 'Field Nation'){
            helper.populateFieldNation(component, index);
        }


        component.set("v.workOrderWrapperList", workOrderWrapperList);
    },


    saveAppointment: function (component, event, helper) {

        component.set("v.Spinner", true);
        if(helper.setAppointmentHelper(component, event)){
            helper.saveAppoint(component, event, helper);
        }

    },


    //Method to searcf for existing contacts through email
    searchEmail: function (component, event, helper) {
        //alert(component.find("LeadEmail").get("v.value"));
        component.set("v.Spinner", true);
        var emailval = $("#getet").val();
        console.log('email value---' + emailval);
        var atposition = emailval.indexOf("@");
        console.log('@therate>>' + atposition);
        var dotposition = emailval.lastIndexOf(".");
        console.log('dot>>>' + dotposition);
        if (emailval == '') {
            helper.showToast(component, event, helper, 'Fill in email value', 'Error');
            component.set("v.Spinner", false);
        }
        //validating email
        else if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= emailval.length) {
            helper.showToast(component, event, helper, 'Enter a valid email address.', 'Error');
            component.set("v.Spinner", false);
            //component.set('v.LeadEmail','');
            //$('#getet').val('');
        }
        else {
            //call the apex to get the existing contact info based on email
            var action = component.get("c.SearchEmail");
            action.setParams({ "EmailText": emailval });
            action.setCallback(this, function (res) {
                console.log('inside call back conatct email search---' + JSON.stringify(res.getReturnValue()));
                var resp = res.getReturnValue();
                if (resp != null && resp.length > 0) {
                    //component.set('v.Order_Lead',resp);
                    $('#getCity').val(resp.MailingCity);
                    $('#getState').val(resp.MailingState);
                    $('#getZipCode').val(resp.MailingPostalCode);
                    $('#getStreet').val(resp.MailingStreet);
                    $('#getFirstName').val(resp.FirstName);
                    $('#getLastName').val(resp.LastName);
                    $('#getHomePhone').val(resp.HomePhone);
                    $('#getMobilePhone').val(resp.MobilePhone);
                    helper.showConList(component, event, helper, resp);
                } else {
                    helper.showToast(component, event, helper, 'No existing contacts found in the system.', 'Info');
                }
                component.set("v.Spinner", false);

            });
            $A.enqueueAction(action);
        }

    },


    Validate: function (component, event, helper) {
        component.set("v.Spinner", true);
        let actionName = component.get("v.actionName");
        let bypassLexisValidation = component.get("v.bypassLexisValidation");
        console.log('phone>>>' + $("#getMobilePhone").val());
        var mobileNumber = $("#getMobilePhone").val();
        var phoneNumber = $("#getHomePhone").val();
        var zipcode = $("#getZipCode").val();
        var SSN = $("#getSSN").val();
        var apt = $("#getApt").val();
        var ValidChars = "0123456789";
        var IsNumber = true;
        var Char;

        console.log('homephone>>>' + phoneNumber);

        var emailval = $("#getet").val();
        console.log('email value---' + emailval);
        var atposition = emailval.indexOf("@");
        console.log('@therate>>' + atposition);
        var dotposition = emailval.lastIndexOf(".");
        console.log('dot>>>' + dotposition);

        var firstName = $("#getFirstName").val();//update:first name value mention spaces.
        var lastName = $("#getLastName").val();
        var alphaExp1 = /^[a-zA-Z\ ]+$/;//update:first name value mention spaces.
        var alphaExp2 = /^[a-zA-Z\-]+$/;
        var alphaExp3 = /^[a-z0-9\- ]+$/i;
        console.log('firstName-'+ firstName);
        if ((apt != '') && (!apt.match(alphaExp3))) {
            component.set("v.Spinner", false);
            helper.showToast(component, event, helper, 'Apt/Unit/Suite can be Alphanumberic only.', 'Error');
            return;
        }

        if (!firstName.match(alphaExp1)) {
            component.set("v.Spinner", false);
            helper.showToast(component, event, helper, 'First Name should be letters and spaces only.', 'Error');
            return;
        }

        if (!lastName.match(alphaExp2)) {
            component.set("v.Spinner", false);
            helper.showToast(component, event, helper, 'Last Name should be letters and Hyphen only.', 'Error');
            return;
        }

        if(actionName!='additionalminor' || (actionName=='additionalminor' && emailval)){
            //validating email
            if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= emailval.length) {
                component.set("v.Spinner", false);
                helper.showToast(component, event, helper, 'Enter a valid email address.', 'Error');
                //component.set('v.LeadEmail','');
                return;
                //$('#getet').val('');
            }

        }


        if (zipcode.length != 5) {
            component.set("v.Spinner", false);
            helper.showToast(component, event, helper, 'Please enter 5 digit Zip Code.', 'Error');
            //component.set('v.LeadEmail','');
            return;

        }
        let hasHearingHoss = component.get("v.hearLossValue");
        if (actionName!='createlead' && !bypassLexisValidation && SSN  && SSN.length != 4) {
            component.set("v.Spinner", false);
            helper.showToast(component, event, helper, 'Please enter 4 digit SSN.', 'Error');
            return;

        }


        for (var i = 0; i < mobileNumber.length && IsNumber == true; i++) {
            Char = mobileNumber.charAt(i);
            console.log('charecter>>>' + Char);
            if (ValidChars.indexOf(Char) == -1 || Char == '-') {
                IsNumber = false;

            }
        }
        for (var i = 0; i < phoneNumber.length && IsNumber == true; i++) {
            Char = phoneNumber.charAt(i);
            console.log('charecterhome>>>' + Char);
            if (ValidChars.indexOf(Char) == -1 || Char == '-') {
                IsNumber = false;

            }
        }

        //either mobile or home phone should be of length 10.
        if ((mobileNumber != '' && mobileNumber.length != 10) ||
            (phoneNumber != '' && phoneNumber.length != 10) || IsNumber == false) {
            component.set("v.Spinner", false);
            helper.showToast(component, event, helper, 'Please Enter Valid 10 Digit Home Tel./Mobile Number.', 'Error');
        }
        else {

            //code for minor validate
            if(actionName == 'additionalminor'){
                var minorValid = helper.validateFormHelperForMinor(component, event, helper);
                if(minorValid){
                    helper.showToast(component, event, helper, 'Details has been validated!', 'Success');
                    component.set("v.isValidated",true);
                }
                else{

                    helper.showToast(component, event, helper, 'Enter all the required fields.', 'Error');
                }
                component.set("v.Spinner", false);
                return;
            }



            var isValidated = component.get("v.isValidated");
            if (!isValidated) {
                var isValid = actionName=='createlead'?helper.validateFormHelperForLead(component, event, helper):helper.validateFormHelper(component, event, helper);
                if (isValid) {
                    component.set("v.responseMessage", '');
                    var obj = helper.castData(component);
                    var action = component.get("c.ValidateAddress");
                    action.setParams({ "conInfo": JSON.stringify(obj),
                        "actionName" : actionName
                    });
                    action.setCallback(this, function (res) {
                        console.log('inside call back Validate address---' + JSON.stringify(res.getReturnValue().sugList));
                        var resP = res.getReturnValue();
                        var msg = resP.msg;
                        var sugList = [];
                        sugList = resP.sugList;
                        component.set("v.suggestedAddresses", sugList);
                        if (msg.includes('successfully'))
                            helper.showToast(component, event, helper, 'Address has been validated!', 'Success');
                        else if (msg.includes('Error'))
                            helper.showToast(component, event, helper, msg + ', Please contact System Administrator.', 'Error');
                        else
                            helper.showToast(component, event, helper, msg, 'Error');
                        debugger;
                        console.log('-res.StatusCode--' + res.StatusCode);
                        console.log('-msg--' + msg);
                        if (resP.StatusCode == '200' && msg.includes('Duplicate Contacts')) {
                            helper.showConList(component, event, helper, resP.contactList);
                            //component.set("v.disableNext",true);
                        }
                        else if (resP.StatusCode != '200') {
                            helper.createComp(component, event, helper, sugList);
                            //component.set("v.isValidated",true);
                        } else if (resP.StatusCode == '200' && !msg.includes('Duplicate Contacts')) {
                            component.set("v.Spinner", false);
                            component.set("v.isValidated", true);
                            //component.set("v.disableNext",false);
                        }
                    });
                    $A.enqueueAction(action);
                } else {
                    component.set("v.Spinner", false);
                    helper.showToast(component, event, helper, 'Enter all the required fields.', 'Error');
                }
            }
            else {
                component.set("v.Spinner", false);
                helper.showToast(component, event, helper, 'Already Validated!', 'Success');
            }
        }

    },

    setAddress: function (component, event, helper) {
        var isOverride = event.getParam("isOverride");
        component.set("v.isAddressOverride", isOverride);
        if (isOverride) {
            component.set("v.isValidated", true);
        } else if (event.getParam("city") == null) {

        } else {
            component.set("v.isValidated", true);

            $('#getCity').val(event.getParam("city"));
            $('#getState').val(event.getParam("state"));
            $('#getZipCode').val(event.getParam("ZipCode"));
            $('#getStreet').val(event.getParam("street"));

            if (event.getParam("apt") != null && event.getParam("apt") != '')
                $('#getApt').val(event.getParam("apt"));
        }

    },

    validateForm: function (component, event, helper) {

        let actionName = component.get("v.actionName");
        let hasHearingHoss = component.get("v.hearLossValue");
        if (actionName == 'createcustomer' || actionName == 'secondaryuser'){
            helper.validateFormHelper(component, event, helper);

        }else if(actionName == 'additionalminor'){
            helper.validateFormHelperForMinor(component, event, helper);
        }
        else{
            helper.validateFormHelperForLead(component, event, helper);
        }

    },

    //make the isValidated false only when address changes, called when address fields changes.
    validateAddressForm: function (component, event, helper) {
        //alert('vaidateing')
        let actionName = component.get("v.actionName");
        component.set("v.isValidated", false);
        console.log('actionName'+actionName )
        let hasHearingHoss = component.get("v.hearLossValue");
        if(actionName == 'createcustomer')
            helper.validateFormHelperAddress(component, event, helper);
        else
            helper.validateFormHelperAddressForLead(component, event, helper);
    },
    ClearInfo: function (component, event, helper) {
        component.set('v.LeadEmail', '');
        $('#getet').val('');
        $('#getCity').val('');
        $('#getState').val('');
        $('#getZipCode').val('');
        $('#getStreet').val('');
        $('#getFirstName').val('');
        $('#getLastName').val('');
        $('#getHomePhone').val('');
        $('#getMobilePhone').val('');
        $('#getApt').val('');
        $('#getSSN').val('');
        $('#getCareGiver').val('');
        component.set('v.isHearingLoss', false);
        component.set('v.isAddressOverride', false);
        component.set('v.disableNext', true);
        component.set('v.EnableValidate', false);
        component.set('v.responseMessage', '');
        component.set('v.contactId', '');
        component.set('v.suggestedAddresses', []);
        component.set('v.hearLossValue', 'default');
        component.set('v.isValidated', false);
        component.set('v.selectedHealthCareProfessional',[]);
        component.set('v.dateOfBirth',null);
    },

    createLead: function (component, event, helper) {
        let bypassLexisValidation = component.get("v.bypassLexisValidation");
        component.set("v.Spinner", true);
        var hearLossValue = component.get('v.hearLossValue');
        var actionName = component.get('v.actionName');
        console.log('hearLossValue' + hearLossValue);
        //check hearloss is selected or not.
        if (hearLossValue == null || hearLossValue == '' || hearLossValue == 'default') {

            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "message": 'Please select "Has Hearing Loss?"',
                "type": "Error",
                "title": "Error!"
            });
            toastEvent.fire();
            component.set("v.Spinner", false);
            return;
        }
        else {
            //callserver = 'true';
            if (hearLossValue == 'Yes')
                component.set("v.isHearingLoss", true);
            else
                component.set("v.isHearingLoss", false);

        }

        if(actionName == 'additionalminor'){
            var reponseMessage = component.get("v.responseMessage");
            //condition when user comes back to 2nd screen
            if (!$A.util.isUndefinedOrNull(reponseMessage) && reponseMessage.includes('Contact Created')) {

                let liId = "tab7";// id for plus image
                let tabId = "ab7";// id for circle tab

                let tabObj = $('#' + tabId);
                let liObj = $('#' + liId);
                helper.tabEventPress(liObj, tabObj);
                component.set("v.Spinner", false);
                return;

            }
			
            if(helper.finalValidationOnNonRequiredFields(component,event))
            	helper.createMinorContact(component, event);


        }
        else{//if other than minor

            //var buttonId = event.currentTarget.id;
            var selectedLookUpRecord = component.get('v.selectedLookUpRecord');
            component.set("v.campaignId", selectedLookUpRecord.Id);
            var isOverride = component.get("v.isAddressOverride");
            var isValidated = component.get("v.isValidated");
            var isHearLoss = component.get("v.isHearingLoss");
            if (isValidated) {
                
                if(helper.finalValidationOnNonRequiredFields(component,event)){
                    var reponseMessage = component.get("v.responseMessage");
                    //condition when user comes back to 2nd screen
                    if (!$A.util.isUndefinedOrNull(reponseMessage) && (reponseMessage.includes('Contact Created') ||
                        reponseMessage.includes('Lead Created'))) {
    
                        if (reponseMessage.includes('Lead Created') && (isOverride || !isHearLoss)) {
                            //hearLoss = false;
                            component.set("v.Spinner", false);
                            var message = 'Lead already created, Please click on Okay to start again.';
                            component.set("v.message", message);
                            component.set("v.isOpen", true);
                            return;
                        }
                        else {
                            var liId = "tab5";// id for plus image
                            var tabId = "ab5";// id for circle tab
    
                            var tabObj = $('#' + tabId);
                            var liObj = $('#' + liId);
                            helper.tabEventPress(liObj, tabObj);
                            component.set("v.Spinner", false);
                            return;
    
                        }
    
                    }
    
                    //for the first time move to 3rd section
                    if (!isOverride && isHearLoss) {
    
                        if(bypassLexisValidation){
                            component.set("v.Spinner", false);
                            console.log('setting recheck value');
                            component.set("v.isRecheck",true);
                            return;
                        }
    
                        var liId = "tab5";// id for plus image
                        var tabId = "ab5";// id for circle tab
    
                        var tabObj = $('#' + tabId);
                        var liObj = $('#' + liId);
                        helper.tabEventPress(liObj, tabObj);
                        component.set("v.Spinner", false);
                        return;
                    }
    
                    //for convert lead functionality
                    if(actionName == 'convertlead' && (isOverride || !isHearLoss)){
                        var msg = '';
                        if(isOverride){
                            msg = 'Lead cannot be converted since address is overridden. Click Okay to refresh.';
                        }
                        else{
                            msg = 'Lead cannot be converted since Hearing loss is set to false. Click Okay to refresh.';
                        }
                        component.set("v.Spinner", false);
                        //helper.showToast(component, event, helper, msg, 'Success');
    
                        component.set("v.message", msg);
                        component.set("v.isOpen", true);
                        return;
                    }
    
                    console.log('before callback');
                    var obj = helper.castData(component);
                    var action = component.get("c.createLeadAndContact");
    
                    action.setParams({
                        "CampId": selectedLookUpRecord.Id,
                        "CustomerInfo": JSON.stringify(obj),
                        "isHearingLoss": isHearLoss,
                        "isOverride": isOverride
                        //"healthCareProfessional" : component.get("v.selectedHealthCareProfessional")!=
                    });
                    action.setCallback(this, function (res) {
                        console.log('inside create lead and contact---' + JSON.stringify(res.getReturnValue()));
                        var resp = res.getReturnValue();
                        if (resp != null && !resp.msg.includes('Error')) {//if  no error resposne from the server
    
                            component.set('v.responseMessage', resp.msg);
                            if (!$A.util.isUndefinedOrNull(resp.leadId)) {
                                component.set("v.leadId", resp.leadId);
                                //component.set("v.contactId",resp.ContactId);
                                if (isOverride) {//if address is overriden and has hearling loss is either true or false.
    
                                    helper.showToast(component, event, helper, resp.msg, 'Success');
                                    component.set("v.Spinner", false);
                                    var msg = 'Lead Created with Address overridden.\n Please create a case against it. Click Okay to refresh.';
                                    component.set("v.message", msg);
                                    component.set("v.isOpen", true);
                                    //helper.createNewUser(component,event,helper,resp.ContactId,buttonId);
                                }
                                else {//when has hearing loss is false and override is false.
                                    component.set("v.Spinner", false);
                                    helper.showToast(component, event, helper, resp.msg, 'Success');
                                    var msg = 'Lead Created for Reporting since "Has Hearing Loss" is set to false. Click Okay to refresh.';
                                    component.set("v.message", msg);
                                    component.set("v.isOpen", true);
    
                                }
    
                            }
                            else {//some error
                                component.set("v.Spinner", false);
                                var msg = 'Some error occurred, please try again later.'
                                helper.showToast(component, event, helper, msg, 'Error');
                            }
                        }
                        else {//if error reponse from the server
                            component.set("v.Spinner", false);
                            helper.showToast(component, event, helper, resp.msg, 'Error');
                        }
    
                    });
                    $A.enqueueAction(action);
                }
                
            }
            else {
                component.set("v.Spinner", false);
                helper.showToast(component, event, helper, 'Validate the Address Before proceeding further', 'Error');
            }

        }//additonal minor else part END


    },

    pickAction : function(component, event, helper){

        let actionName = event.currentTarget.id;
        component.set("v.actionName",actionName);
        console.log('testing'+actionName);
        helper.pickAction(component, event, actionName);


    },

    changeRecordtype: function (component, event, helper) {
        let selectedItem = event.getSource();
        let value = selectedItem.get("v.value");
        console.log('value-recordtype->'+value);
        component.set("v.selectedRecordtype",value);


    },

    onDeclineChange : function(component, event, helper){
        console.log('decline-' + component.get('v.bypassLexisValidation'));
        $('#getSSN').val('');
        component.set("v.dateOfBirth",null);
        helper.validateFormHelper(component, event, helper);
    },

    allowNavigate : function( component, event, helper){
        component.set("v.isRecheck",false);
        component.set("v.lexisPass",true);
        component.set('v.showWarning',false);
        const liId = "tab5";// id for plus image
        const tabId = "ab5";// id for circle tab
        const tabObj = $('#' + tabId);
        const liObj = $('#' + liId);
        helper.tabEventPress(liObj, tabObj);
    },

    showSSNMessage : function( component, event, helper) {
        component.set("v.bypassLexisValidation",false);
        component.set("v.isValidated",false);
        component.set('v.EnableValidate',false);
        component.set("v.isRecheck",false);
        helper.showToast(component, event, helper, 'Please enter "Last 4 digits of SSN" and "Date of Birth"', 'Error');
    },
    
    
})