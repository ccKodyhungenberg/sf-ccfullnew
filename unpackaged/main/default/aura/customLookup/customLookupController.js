({	
    doInit : function(component, event, helper) {
        let addToCache = component.get("v.addToCache");
        let  selectedRecord = component.get("v.selectedRecord");
        console.log('addToCache-'+addToCache);
     console.log('selected recLabel>>>'+component.get("v.selectedRecord.Name"));
        if(component.get('v.objectAPIName') == 'Campaign' && addToCache && $A.util.isUndefinedOrNull(selectedRecord.Id)){
            //if the object is campaign then load the past searched campaign from cache
            var action = component.get("c.getSelectEventFromCache");
            
            action.setCallback(this,function(res){
                var response = res.getReturnValue();
                
                if(!$A.util.isUndefinedOrNull(response)){ 
                    component.set("v.selectedRecord" , response);	   
                    console.log('loaded from cache'+ JSON.stringify(response));
                    
                    var compEvent = component.getEvent("oSelectedRecordEvent");
                    // set the Selected sObject Record to the event attribute.  
                         compEvent.setParams({"recordByEvent" : response });  
                    // fire the event  
                         compEvent.fire();
                    var forclose = component.find("lookup-pill");
                       $A.util.addClass(forclose, 'slds-show');
                       $A.util.removeClass(forclose, 'slds-hide');
              
                    var forclose = component.find("searchRes");
                       $A.util.addClass(forclose, 'slds-is-close');
                       $A.util.removeClass(forclose, 'slds-is-open');
                    
                    var lookUpTarget = component.find("lookupField");
                        $A.util.addClass(lookUpTarget, 'slds-hide');
                        $A.util.removeClass(lookUpTarget, 'slds-show');
            
                }
            });
            $A.enqueueAction(action);
        }

	},

    onChangeOfSelectedRecord : function(component,event){

        console.log('selected recordId>>>'+JSON.stringify(component.get('v.selectedRecord')));
        //component.set('v.selectedRecord','');
        var selectedRecord = component.get("v.selectedRecord");
        if(selectedRecord && !$A.util.isUndefinedOrNull(selectedRecord.Id)) {
            var forclose = component.find("lookup-pill");
            $A.util.addClass(forclose, 'slds-show');
            $A.util.removeClass(forclose, 'slds-hide');

            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');

            var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');
        }
    },
    
   onfocus : function(component,event,helper){
       $A.util.addClass(component.find("mySpinner"), "slds-show");
        var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC  
         var getInputkeyWord = '';
         helper.searchHelper(component,event,getInputkeyWord);
    },
    onblur : function(component,event,helper){       
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    keyPressController : function(component, event, helper) {
       // get the search Input keyword   
         var getInputkeyWord = event.currentTarget.value;
       // check if getInputKeyWord size id more then 0 then open the lookup result List and 
       // call the helper 
       // else close the lookup result List part.

        if(getInputkeyWord && getInputkeyWord.length > 0 ){
             var forOpen = component.find("searchRes");
               $A.util.addClass(forOpen, 'slds-is-open');
               $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
             component.set("v.listOfSearchRecords", null ); 
             var forclose = component.find("searchRes");
               $A.util.addClass(forclose, 'slds-is-close');
               $A.util.removeClass(forclose, 'slds-is-open');
          }
	},
    
  // function for clear the Record Selaction 
    clear :function(component,event,heplper){
         const objectAPIName = component.get("v.objectAPIName");
         var pillTarget = component.find("lookup-pill");
         var lookUpTarget = component.find("lookupField"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');

         document.getElementById(objectAPIName).value ="";
         component.set("v.SearchKeyWord",null);
         component.set("v.listOfSearchRecords", null );
         component.set("v.selectedRecord", {} );  
        if(objectAPIName == 'Campaign'){
        //fire the clear event
         var compEvent = component.getEvent("Clearevent");
   
         compEvent.setParams({"message" : "Fire" });  
    // fire the event  
         compEvent.fire();
        }
    },
    
  // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        let addToCache = component.get("v.addToCache");
        console.log('inside handleComponentEvent'+addToCache);
    // get the selected Account record from the COMPONETN event 	 
       var selectedAccountGetFromEvent = event.getParam("recordByEvent");
       console.log('inside selectedAccountGetFromEvent'+JSON.stringify(selectedAccountGetFromEvent));
	   component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
         
        if(addToCache) {
            
            var action = component.get("c.putRecordSession");
            action.setParams({"sobj": selectedAccountGetFromEvent});
            action.setCallback(this,function(res){
                var response = res.getReturnValue();
                if(!$A.util.isUndefinedOrNull(response) && response.includes('success'))
                    console.log('added to session variable');    
                
            });
            $A.enqueueAction(action);
      
        }
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');
       
        
        
      
	},
    
    Cancelhandle : function(component, event, helper){
        const objectAPIName = component.get("v.objectAPIName");
        var pillTarget = component.find("lookup-pill");
         var lookUpTarget = component.find("lookupField"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
        document.getElementById(objectAPIName).value ="";
         component.set("v.SearchKeyWord",null);
         component.set("v.listOfSearchRecords", null );
         component.set("v.selectedRecord", {} );  
        
    },

})