({
	selectVal : function(component, event, helper) {
        var target = event.target;
		var dataEle = target.getAttribute("data-selected-Index");
        var sugList = [];
        sugList = component.get("v.suggestedContacts");
		var cmpEvent = component.getEvent("cmpEvent"); 
               cmpEvent.setParams({ 
                   "selectedCon" : sugList[dataEle]             
               }); 
            cmpEvent.fire(); 
             var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                  "recordId": sugList[dataEle].Id
                });
                navEvt.fire();
	},
    destroy : function(component, event, helper) {
		component.destroy();
	},
})