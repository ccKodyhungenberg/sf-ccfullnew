({
    fetchRecordTypes : function(component, event, helper) {
        var action = component.get("c.fetchRecordTypeValues");
        action.setParams({
            "sObjectName" : component.get("v.sObjectName")
        })
        action.setCallback(this, function(response) {
            component.set("v.lstOfRecordType", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    fetchRecord : function(component, event, helper){
        var action = component.get("c.fetchCaseRecord");
        var recId = component.get("v.recordId");
        
        action.setParams({
            "recordId" : recId
        })
        action.setCallback(this, function(response) {
            component.set("v.caseRecord", response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    }
    
})