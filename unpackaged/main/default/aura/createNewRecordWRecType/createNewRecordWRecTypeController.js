({
/* 
 * On the component Load this function call the apex class method,fetches Record Types 
*/
   fetchListOfRecordTypes: function(component, event, helper) {
      helper.fetchRecordTypes(component, event, helper);
      helper.fetchRecord(component, event, helper);
   },
 
   /*
    * This validates the record type and redirect to create a record
    */
   createRecord: function(component, event, helper) {
      component.set("v.isOpen", true);
      var action = component.get("c.getRecTypeId");
      var recordTypeLabel = component.find("selectid").get("v.value");
       console.log('nn',recordTypeLabel);
       if(recordTypeLabel === undefined){
           recordTypeLabel = 'Small Parts Order';
       }
      action.setParams({
          "sObjectName" : component.get("v.sObjectName"),
         "recordTypeLabel": recordTypeLabel
      });
      action.setCallback(this, function(response) {
         var state = response.getState();
         if (state === "SUCCESS") {
            var parId = component.get("v.recordId");
            var parRec = component.get("v.caseRecord");
             debugger;
             var createRecordEvent = $A.get("e.force:createRecord");
            var RecTypeID  = response.getReturnValue();
            console.log('pp',createRecordEvent,RecTypeID);
            createRecordEvent.setParams({
               "entityApiName": component.get("v.sObjectName"),
                "defaultFieldValues": {
                    "ParentId": parId,
                    "AccountId" : parRec.AccountId,
                    "ContactId" : parRec.ContactId
                },
               "recordTypeId": RecTypeID
            });
            createRecordEvent.fire();
             
         } else if (state == "INCOMPLETE") {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
               "title": "Oops!",
               "message": "No Internet Connection"
            });
            toastEvent.fire();
             
         } else if (state == "ERROR") {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
               "title": "Error!",
               "message": "Please contact your administrator"
            });
            toastEvent.fire();
         }
      });
      $A.enqueueAction(action);
   },
 
    closeModal: function(component, event, helper) {
        // set "isOpen" attribute to false for hide/close model box 
        component.set("v.isOpen", false);
    },
    
    openModal: function(component, event, helper) {
        // set "isOpen" attribute to true to show model box
        component.set("v.isOpen", true);
    },
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    }
    
})