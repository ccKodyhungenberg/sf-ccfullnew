trigger CC_TaskTrigger on Task (before insert, after insert) { // Added after insert by Nilesh Grover for S-584775
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())) {
        if(Trigger.isBefore && Trigger.IsInsert){
            CC_TaskTriggerHandler.onBeforeInsert(Trigger.new);
        }
    }
    /* Name: onAfterInsert
     * Descreption: After Insert Operations to be Performed in this method
     * Created By : Nilesh Grover for S-584775 
     */
    if(Trigger.isAfter && Trigger.isInsert) {
        CC_TaskTriggerHandler.onAfterInsert(Trigger.new);
    }
}