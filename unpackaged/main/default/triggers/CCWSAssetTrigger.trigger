trigger CCWSAssetTrigger on CCWS_Asset__c (after update, after insert) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())){
        if(Trigger.isAfter && Trigger.IsUpdate) {
            CCWSAssetTriggerHandler.tr_After_Update(trigger.New, trigger.Old, trigger.NewMap, trigger.OldMap);
        }
        else if( Trigger.isAfter&& Trigger.isInsert) {
            CCWSAssetTriggerHandler.tr_After_Insert(trigger.New, trigger.Old, trigger.NewMap, trigger.OldMap);
        }
    }
}