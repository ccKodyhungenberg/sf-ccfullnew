trigger CC_LeadTrigger on Lead (after update) {
    if(Trigger.isAfter && Trigger.isUpdate){
        CC_LeadTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}