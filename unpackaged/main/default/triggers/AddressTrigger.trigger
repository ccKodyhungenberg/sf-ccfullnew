trigger AddressTrigger on Addresses__c ( after insert, after update ) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())) {
        if( Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate) ) {
            AddressTriggerHandler.uncheckAllActiveCheckbox( Trigger.new, Trigger.oldMap);
        }
    }
}