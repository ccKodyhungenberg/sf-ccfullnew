trigger UsageDetailTrigger on UsageDetail__c ( before insert ) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())){
        if (UserInfo.getUserId() == CC_GlobalUtility.idBoomi) {
            for (UsageDetail__c oNewUsageDetail : Trigger.new) {
                oNewUsageDetail.DateCreated__c = datetime.now();
            }
        }
        
    }
}