trigger CC_EvaluationScriptTrigger on EvaluationScript__c (before insert, before update) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())){
        if(trigger.isBefore){
            if(trigger.isInsert){
                CC_EvaluationScriptTriggerHandler.beforeInsert(trigger.new);
            }
            else if(trigger.isUpdate){
                CC_EvaluationScriptTriggerHandler.beforeUpdate(trigger.new);
            }
        }
    }
}