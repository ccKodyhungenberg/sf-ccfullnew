trigger CaseTrigger on Case ( before insert, before update, before delete, after Insert ) {
    
    CaseTriggerHandler handler = new CaseTriggerHandler();
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())) {
        if (Trigger.isBefore && Trigger.isInsert) {
            handler.onBeforeInsert( Trigger.new );
        }
        
        if (Trigger.isBefore && Trigger.isUpdate) {
            handler.onBeforeUpdate( Trigger.new, Trigger.oldMap );
        }
        
        if (Trigger.isBefore && Trigger.isDelete) {
            handler.onBeforeDelete( Trigger.old );
        }
        if(Trigger.isAfter && Trigger.isInsert){
            CaseTriggerHandler.isAfterInsert(Trigger.new);
        }
    }
}