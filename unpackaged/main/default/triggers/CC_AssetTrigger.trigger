trigger CC_AssetTrigger on Asset (before insert, after insert, before delete , before update, after delete, after update) { //Modified By Nilesh for Case #00250394 && MKLICH S-628700
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())) {
        if(Trigger.isInsert && Trigger.isBefore){
            CC_AssetTriggerHandler.beforeInsert(Trigger.new);
        }
        if(Trigger.isDelete && Trigger.isBefore){
            //CC_AssetTriggerHandler.beforeDelete(Trigger.old); // MKLICH S-628700 Removed Before Delete methods
        }
        // MKLICH S-628700 START
        if(Trigger.isAfter && Trigger.isDelete) {
            CC_AssetTriggerHandler.afterDelete(Trigger.Old);
        }
        
        if(Trigger.isAfter && Trigger.isUpdate) {
             CC_AssetTriggerHandler.afterUpdate(Trigger.Old, Trigger.New);
        }  // MKLICH S-628700 START
    }
    //START - Added By Nilesh for Case #00250394
    if(Trigger.isBefore && Trigger.isUpdate){
        CC_AssetTriggerHandler.beforeUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
    }
    //END - Added By Nilesh for Case #00250394
    if( Trigger.isAfter && Trigger.isInsert ) {
      	CC_AssetTriggerHandler.afterInsert(Trigger.New); // MKLICH S-628700 method Call to afterInsert
    }
    
}