trigger CC_OrderItemTrigger on OrderItem (before insert, before update) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())) {
        if(Trigger.isBefore && Trigger.IsInsert){
            CC_OrderItemTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isBefore && Trigger.IsUpdate){
            CC_OrderItemTriggerHandler.onBeforeupdate(Trigger.new,Trigger.oldMap);
        }
    }
}