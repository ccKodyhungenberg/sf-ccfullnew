trigger CCWSAssetHistoryTrigger on CCWS_Asset_History__c (after insert, after update) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())){
        if(trigger.isAfter&& trigger.isInsert) {
            CCWSAssetHistoryTriggerHandler.tr_After_Insert(trigger.New, trigger.Old, trigger.NewMap, trigger.OldMap);
        }
        else if(trigger.isAfter&& trigger.isUpdate) {
            CCWSAssetHistoryTriggerHandler.tr_After_Update(trigger.New, trigger.Old, trigger.NewMap, trigger.OldMap);
        }
    }
}