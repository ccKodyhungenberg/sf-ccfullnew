// Appirio 2019
//
//
// MODIFIED - NBOCK - 1/25/2019 - C00249916:added if statement to check custom label before processing retention cases
trigger ContactTrigger on Contact ( after delete, before Update, after update, before insert, after insert ) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getuserid())) {
        if( Trigger.isAfter && Trigger.isDelete ) {
            ContactTriggerHandler.updateCombinedUsageOnAccount( Trigger.old );
        }
        
        
        if( Trigger.isBefore && Trigger.isUpdate ) {
            ContactTriggerHandler.updateAddressOnCCWS( Trigger.new, Trigger.oldMap );
            ContactTriggerHandler.activeInactiveOnCCWS( Trigger.new, Trigger.oldMap );
            ContactTriggerHandler.tr_Before_Update(Trigger.New, Trigger.Old, Trigger.NewMap, Trigger.OldMap);
            // c-00249602 1/23/19 NCarson - remove legacy PCF Form if field is cleared
            ContactTriggerHandler.updateLegacyPCFStatus( Trigger.new, Trigger.oldMap);
            ContactTriggerHandler.updateNumberOfActiveAssets(Trigger.new, Trigger.NewMap);// MKLICH S-628700

        }
        
        else if(Trigger.isUpdate && Trigger.isAfter) { 
            ContactTriggerHandler.tr_After_Update(Trigger.New, Trigger.Old, Trigger.NewMap, Trigger.OldMap);
            //NBOCK C00249916 Start
            /*
            if(Label.CC_Retention_Cases_On == 'true'){
                ContactTriggerHandler.createRetentionCaseDate(Trigger.New, Trigger.Old, Trigger.NewMap, Trigger.OldMap);
            }*/
            //NBOCK C00249916 End
            ContactTriggerHandler.updateCombinedUsageOnAccount(Trigger.new,Trigger.oldMap);
        }
        else if(Trigger.isInsert && Trigger.isBefore) { 
            ContactTriggerHandler.tr_Before_Insert(Trigger.New, Trigger.Old, Trigger.NewMap, Trigger.OldMap);
            ContactTriggerHandler.updateNumberOfActiveAssets(Trigger.new, Trigger.NewMap);// MKLICH S-628700

        }
        else if(Trigger.isInsert && Trigger.isAfter) { 
            ContactTriggerHandler.tr_After_Insert(Trigger.New, Trigger.Old, Trigger.NewMap, Trigger.OldMap);
            ContactTriggerHandler.updateCombinedUsageOnAccount(Trigger.new,Trigger.oldMap);

        }
    }
}