trigger WorkOrderTriger on WorkOrder (before update, after update, before insert, after insert) {
     
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())){
        if(Trigger.isUpdate && Trigger.isBefore) {
            WorkOrderTriggerHandler.tr_Before_Update(Trigger.New, Trigger.Old, Trigger.newMap, Trigger.oldMap);
        } else if(Trigger.isUpdate && Trigger.isAfter) {
            //Start Added by Rohit Pachauri for Case -00258370
            if(Constants.isAfterUpdateFirst){ //End Case -00258370
            WorkOrderTriggerHandler.tr_After_Update(Trigger.New, Trigger.Old, Trigger.newMap, Trigger.oldMap);
           }
        } else if(Trigger.isInsert && Trigger.isBefore) {
            WorkOrderTriggerHandler.tr_Before_Insert(Trigger.New, Trigger.Old, Trigger.newMap, Trigger.oldMap);
        } else if(Trigger.isInsert && Trigger.isAfter) {
            WorkOrderTriggerHandler.tr_After_Insert(Trigger.New, Trigger.Old, Trigger.newMap, Trigger.oldMap);
        }
    }
}