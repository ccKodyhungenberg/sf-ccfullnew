trigger CC_EvaluationTrigger on Evaluation__c (before insert, before update) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId())) {
        if(trigger.isBefore){
            if(trigger.isInsert){
                CC_EvaluationTriggerHandler.beforeInsert(trigger.new);
            }
            else if(trigger.isUpdate){
                CC_EvaluationTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);    
            }
        }
    }
}