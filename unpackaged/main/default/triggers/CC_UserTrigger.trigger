trigger CC_UserTrigger on User (after insert, after update) {
    if(CC_GlobalUtility.IsTriggerRunning(UserInfo.getUserId()) ) {
        if(Trigger.isInsert && Trigger.IsAfter){
            CC_UserTriggerHandler.isAfterInsert(Trigger.new);
        }
        if(Trigger.isUpdate && Trigger.IsAfter){
            CC_UserTriggerHandler.isAfterUpdate(Trigger.new,Trigger.oldMap);
        }
    }
}