//
// (c) 2018 Appirio, Inc.
//
// Apex Class Name: CC_AutoRedirectionToFlowController.cls
// For Apex Page: CC_AutoRedirectionToFlow.page
// Description: This apex class is used to do override Standard Edit button on Addresses__c Object to redirect to a specific flow
//
// 1st June, 2018   Vishwas Gupta   Original (Task # 	T-703954) - Please see the task description for more details.
//

public class CC_AutoRedirectionToFlowController {
  
  public Addresses__c address {get;set;}
  public String recordId = '';
  public String retURL = '';
  
  public CC_AutoRedirectionToFlowController(ApexPages.StandardController stdController) {
      this.address = (Addresses__c)stdController.getRecord();
      retURL = ApexPages.currentPage().getParameters().get('retURL');
      if( !String.isBlank( retURL ) && retURL.startsWith('/')) {
        retURL = retURL.substring(1,retURL.length());
      }
    }
  
  
  public PageReference redirectToFlow() {
    
    if( address.Account__c != null ) {
      recordId = address.Account__c;
    }
    else if( address.Contact__c != null ) {
      recordId = address.Contact__c;
    }
    else if ( address.Asset__c != null ) {
      recordId = address.Asset__c;
    }
    
    PageReference pg = new PageReference('/apex/cc_addressFlow?recordId='+recordId+'&addressId='+address.Id+'&retURL='+retURL+'&fromComponent=true');
    pg.setRedirect( true );
    return pg;
  }
  
  
  @auraEnabled
  public static Addresses__c getAddressRecord( String addressId ) {
    /*String retURL = ApexPages.currentPage().getParameters().get('retURL');
    if( !String.isBlank( retURL ) && retURL.startsWith('/')) {
      retURL = retURL.substring(1,retURL.length());
    }*/
    Addresses__c addToReturn = new Addresses__c();
    for( Addresses__c add : [ SELECT Id,Account__c, Contact__c, Asset__c FROM Addresses__c WHERE Id =: addressId ]) {
      addToReturn = add;
    }
    //addToReturn.ParentId__c = retURL;
    return addToReturn;
  }
  
}