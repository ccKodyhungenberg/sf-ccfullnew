@isTest
global class MockCC_EventPortal implements HttpCalloutMock{
    String counter = '';
    global MockCC_EventPortal(String counter){
        this.counter = counter;
    } 
    global HTTPResponse respond(HTTPRequest req) {
        if(counter == '1'){
            // Create a fake response
            HttpResponse res = new HttpResponse();
            System.assertEquals('POST', req.getMethod());
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            //res.setBody('data={"apt": "APT 125","city": "Grand Rapids","country": "","e911LocationID": null,"primary": false,"state": "MI","street": "100 50TH ST SW","types": [],"zip": "49548","id": null,"type": "address"}');
            res.setBody('{"code": 200,"data": {"apt": "APT 125","city": "Grand Rapids","country": "","e911LocationID": null,"primary": false,"state": "MI","street": "100 50TH ST SW","types": [],"zip": "49548","id": null,"type": "address"},"message": "","result": true}');
            res.setStatusCode(200);
            return res;
            
        }
        else if(counter == '2'){
            // Create a fake response
            HttpResponse res = new HttpResponse();
            System.assertEquals('POST', req.getMethod());
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            res.setBody('{"code": 200,"data":{"suggestedAddresses": [{"street": "100 50TH ST SW","apt": "APT 125","city": "GRAND RAPIDS","state": "MI","zip": "49548"}]},"message": "","result": false}');
            res.setStatusCode(200);
            return res;
        }        
         else if(counter == '3'){
            // Create a fake response
            HttpResponse res = new HttpResponse();
            System.assertEquals('POST', req.getMethod());
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            res.setBody('{"code": 607,"data":{"suggestedAddresses": [{"street": "100 50TH ST SW","apt": "APT 125","city": "GRAND RAPIDS","state": "MI","zip": "49548"}]},"message": "","result": false}');
            res.setStatusCode(200);
            return res;
        }  
        else{
            // Create a fake response
            HttpResponse res = new HttpResponse();
            System.assertEquals('POST', req.getMethod());
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            res.setBody('{"code": 200,"data":{"suggestedAddresses": []},"message": "","result": false}');
            res.setStatusCode(607);
            return res;
        }  
    }   
    
}