@isTest
private class CC_PcfApprovalCongaAuthSchedulableTest {
    
    @isTest static void test_oauth() {
        CC_TestPcfDataFactory.createSettings();
        
        Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"access_token":1234567890}'));
        
        Test.startTest();
        CC_PcfApprovalCongaAuthSchedulable job = new CC_PcfApprovalCongaAuthSchedulable();
        job.execute(null);
        Test.stopTest();
        
        PCF_Settings__c settings = PCF_Settings__c.getInstance('Default');
        System.assertEquals(settings.Session_ID__c, '1234567890');
        
    }  
    
}