/*
* Appirio, Inc
* Name: Sched_CC_CampaignFeedbackEmailBatch
* Description: S-611297
* Created Date: 26 Mar, 2019
* Created By: Nilesh Grover
* 
* Date Modified                Modified By                  Description of the update
*/

global class CC_CampaignFeedbackEmailBatch implements Database.Batchable<sObject> {
    global String query = null;
    Date lastEmailDate = Date.today().addDays(-32);
    Date firstEmailDate = Date.today().addDays(-2);     
    public CC_CampaignFeedbackEmailBatch(String sQuery){
            query = sQuery;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query == null){
            //Date lastEmailDate = Date.today().addDays(-32);
            //Date firstEmailDate = Date.today().addDays(-2); 
            //Modified below query by Prateek Sisodiya for case# 00265015 on 17/Jul/2019      new fields : Campaign_Record_Type_Name__c, Event_Request_Status__c, Status 
            query = 'SELECT id, Event_POC__c, Event_POC__r.email, Gross_Sales__c, Event_Well_Attended__c, Good_Booth_Location__c, Was_Event_Attended_by_Our_Demographic__c, Should_We_Attend_Next_Year__c, Number_of_Attendees__c, Number_of_People_over_age_65__c, Average_Age__c, Location__c, Weather__c, Demographic__c, Placement__c, Setup__c, Other_Comments__c, Campaign_Record_Type_Name__c, Event_Request_Status__c, Status ' + 
                        'FROM Campaign ' +
                            'WHERE ' +
                                'EndDate > :lastEmailDate AND ' +
                                'EndDate <= :firstEmailDate';
            return Database.getQueryLocator(query);
        }else {
            return Database.getQueryLocator(query);
        }   
    }
    global void execute(Database.BatchableContext BC, List<Campaign> scope) {       
        List<Campaign> listOfSelectedCampaigns =  new List<Campaign>(); //Added by Prateek Sisodiya for case# 00265015 on 17/Jul/2019
        if(scope.size() > 0 ){
                //START - Added by Prateek Sisodiya for case# 00265015 on 17/Jul/2019
                for(Campaign cmp : scope) {
                    if(cmp.Campaign_Record_Type_Name__c == 'Event' && (cmp.Status == 'Cancelled' || cmp.Event_Request_Status__c == 'Rejected')){
                        //Do nothing
                    }else if((cmp.Campaign_Record_Type_Name__c == 'Large Event' || cmp.Campaign_Record_Type_Name__c == 'Large Event Request') && (cmp.Status == 'Cancelled' || cmp.Event_Request_Status__c == 'Rejected')) {
                        //Do nothing
                    }else{
                        listOfSelectedCampaigns.add(cmp);
                    }
                }
                // END - Added by Prateek Sisodiya for case# 00265015 on 17/Jul/2019
                system.debug('List Campaign >>' + scope);
                System.debug('firstEmailDate = ' + firstEmailDate + ', lastEmailDate = ' + lastEmailDate);
                List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
                Id randomContactId = [SELECT Id FROM Contact LIMIT 1].Id; // Any Contact Id pass to TargetObjectID to make use of Campaign as WhatId
                Id emailTemplateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Campaign_Feedback_Email_Notification' LIMIT 1].Id;
                Id orgWideEmailAddressId = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :System.label.Org_Wide_Email_for_Campaign_FeedBack LIMIT 1].Id;
                for(Campaign cmp : listOfSelectedCampaigns){ // Added by Prateek Sisodiya for case# 00265015 on 17/Jul/2019
                    if(cmp.Gross_Sales__c == null || String.isBlank(cmp.Event_Well_Attended__c) || String.isBlank(cmp.Good_Booth_Location__c) || String.isBlank(cmp.Was_Event_Attended_by_Our_Demographic__c) || String.isBlank(cmp.Should_We_Attend_Next_Year__c) || cmp.Number_of_Attendees__c == null || cmp.Number_of_People_over_age_65__c == null || cmp.Average_Age__c == null || String.isBlank(cmp.Location__c) || String.isBlank(cmp.Weather__c) || String.isBlank(cmp.Demographic__c) || String.isBlank(cmp.Placement__c) || String.isBlank(cmp.Setup__c) || String.isBlank(cmp.Other_Comments__c)) 
                        {                           
                                if(cmp.Event_POC__c != null && cmp.Event_POC__r.email != null) {
									Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
									List<String> emailOfEventPOC = new List<String>();
									emailOfEventPOC.add(cmp.Event_POC__c);
									mail.setToAddresses(emailOfEventPOC);                               
									mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
									mail.setTemplateId(emailTemplateId);
									mail.setTargetObjectId(randomContactId);
									mail.setSaveAsActivity(false);
									mail.setTreatTargetObjectAsRecipient(false);
									mail.setWhatId(cmp.Id);                                 
									emailsToSend.add(mail);
								}
                        }
                }
            
            // Prateek Sisodiya for case# 00265015 on 17/Jul/2019
                if(emailsToSend.size() > 0){    
                    Messaging.SendEmailResult [] r = Messaging.sendEmail(emailsToSend,false);
                    System.debug('Email Response :: ' + r);
                }
            }   
        }       
    global void finish(Database.BatchableContext BC) {      
    }
}