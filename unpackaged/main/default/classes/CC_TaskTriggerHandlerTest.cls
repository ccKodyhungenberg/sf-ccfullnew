@isTest
private class CC_TaskTriggerHandlerTest {
	@isTest
    static void validateTaskTrigger(){
        List<Lead> leadList = CC_TestUtility.createLead(1,true);
        Task t = new Task();
        t.Subject = 'Email';
        t.WhoId = leadList[0].id;
        insert t;
        system.assertEquals(true, [SELECT activity_created__C FROM Lead WHERE Id=: leadList[0].id].activity_Created__C);
        Case c = new Case();
        c.Status = 'New';
        insert c;
        Task t1 = new Task();
        t1.Subject = 'Email';
        t1.WhatId = c.id;
        insert t1;
    }
    /* Name: testUpdateCallAttemptCheckBox
     * Descreption: Test Method to updateCallAttemptCheckBox()
     * Created By : Nilesh Grover for S-584775 
     */
    @isTest
    static void testUpdateCallAttemptCheckBox() {
        
        Test.startTest();
        List<Lead> leadList = CC_TestUtility.createLead(1,true);
        System.assertEquals(1, [SELECT Id FROM Lead].size());
        List<Task> tasks = new List<Task>();
                
        Task t1 = new Task();
        t1.Subject = 'Call';
        t1.TaskSubtype = 'Call';
        t1.WhoId = leadList[0].id;
        insert t1; 
        
        Task t2 = new Task();
        t2.Subject = 'Call';
        t2.TaskSubtype = 'Call';
        t2.WhoId = leadList[0].id;
        insert t2;
        
        Task t3 = new Task();
        t3.Subject = 'Call';
        t3.TaskSubtype = 'Call';
        t3.WhoId = leadList[0].id;
        insert t3;
        
        Task t4 = new Task();
        t4.Subject = 'Call';
        t4.TaskSubtype = 'Call';
        t4.WhoId = leadList[0].id;
        insert t4;
        
        Task t5 = new Task();
        t5.Subject = 'Call';
        t5.TaskSubtype = 'Call';
        t5.WhoId = leadList[0].id;
        insert t5;
        
        Task t6 = new Task();
        t6.Subject = 'Call';
        t6.TaskSubtype = 'Call';
        t6.WhoId = leadList[0].id;
        insert t6;
        
        Task t7 = new Task();
        t7.Subject = 'Call';
        t7.TaskSubtype = 'Call';
        t7.WhoId = leadList[0].id;
        insert t7;
        
        Task t8 = new Task();
        t8.Subject = 'Call';
        t8.TaskSubtype = 'Call';
        t8.WhoId = leadList[0].id;
        insert t8;
        
        Task t9 = new Task();
        t9.Subject = 'Call';
        t9.TaskSubtype = 'Call';
        t9.WhoId = leadList[0].id;
        insert t9;
        
        Task t10 = new Task();
        t10.Subject = 'Call';
        t10.TaskSubtype = 'Call';
        t10.WhoId = leadList[0].id;
        insert t10;
		Test.stopTest();
        List<Lead> updatedLead =  [SELECT Id,	Call_Attempt_1__c, Call_Attempt_2__c, 
                                        	Call_Attempt_3__c, Call_Attempt_4__c, 
                                 			Call_Attempt_5__c, Call_Attempt_6__c, 
                                 			Call_Attempt_7__c, Call_Attempt_8__c, 
                                 			Call_Attempt_9__c, Call_Attempt_10__c
                                	FROM Lead
                                	WHERE Id = :leadList[0].id];
        for(Lead l : updatedLead){
            	system.assertEquals(true, l.Call_Attempt_1__c);
                system.assertEquals(true, l.Call_Attempt_2__c);
                system.assertEquals(true, l.Call_Attempt_3__c);
                system.assertEquals(true, l.Call_Attempt_4__c);
                system.assertEquals(true, l.Call_Attempt_5__c);
                system.assertEquals(true, l.Call_Attempt_6__c);
                system.assertEquals(true, l.Call_Attempt_7__c);
                system.assertEquals(true, l.Call_Attempt_8__c);
                system.assertEquals(true, l.Call_Attempt_9__c);
                system.assertEquals(true, l.Call_Attempt_10__c);    
        }
         
    }
}