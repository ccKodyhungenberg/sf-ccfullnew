@isTest
private class customlookupcontroller_test {
    
    @testSetup
    static void testSetup(){
        List<Campaign> campList = CC_TestUtility.createCampaign(4,false); 
        campList[0].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Large Event').getRecordTypeId();
        campList[1].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Event').getRecordTypeId();
        campList[2].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Hearing Professional').getRecordTypeId();
       	campList[3].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Marketing').getRecordTypeId();
        
        for(Campaign camp : camplist){
          camp.StartDate = Date.today().addDays(-10);
          camp.EndDate = Date.today();
          camp.Channel__c = 'Direct Sales';
          camp.IsActive = true;
          camp.Event_POC__c = UserInfo.getUserId();
          camp.Secondary_Event_POC__c = UserInfo.getUserId();
          camp.Location_State__c = 'OH';
          camp.Location_Postal_Code__c = '45601';
          camp.Location_City__c = 'TEST';
          camp.Location_Street_Address__c = 'TEST';   
        }
      insert campList;  
        
      List<Account> accList = CC_TestUtility.createAccount(1,true);
      List<Contact> customerContact = CC_TestUtility.createContact(1, true, 'Technician', accList[0].id,'test@clearcaptions.com'); 
        
    }  
  static testMethod void customlookupcontrollermethod() {
      //create campaign
      List<Campaign> campList = new List<Campaign>();
      List<Contact> conList = new List<Contact>();
      Test.startTest();
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','Large Event','CC Customer Care Rep','');
      customLookUpController.putRecordSession(campList[0]);
      customLookUpController.getSelectEventFromCache();
      System.assert(campList.size()>0);
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','Large Event','System Admin','');
      System.assert(campList.size()>0);
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','Hearing Professional','CC Inside Sales Representative','');
      System.assert(campList.size()>0);
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','Marketing','CC Inside Sales Representative','');
      System.assert(campList.size()>0);
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','none','CC Inside Sales Representative','');
      System.assert(campList.size()>0);
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','none','CC Customer Care Rep','');
      System.assert(campList.size()>0);
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','none','System Admin','');
      System.assert(campList.size()>0);
      campList=customLookUpController.fetchLookUpValues('test','Campaign','','','System Admin','');
      System.assert(campList.size()>0);
      conList = customLookUpController.fetchLookUpValues('Test Contact0','Contact','FirstName','','','');
      customLookUpController.putRecordSession(conList[0]); 
      System.assert(conList.size()>0);
      List<Account> accList = new List<Account>();
      accList = customLookUpController.fetchLookUpValues('test account','Account','createdDate','','','');
      System.assert(accList.size()>0);
      Test.stopTest();
      
	}
}