@isTest
private class CC_UserTriggerHandlerTest {
    @isTest
    static void testUserTrigger(){
        UserRole territoryRole = [SELECT id FROM UserRole WHERE DeveloperName='CC_Territory_Manager'];
        UserRole eventRole = [SELECT id FROM UserRole WHERE DeveloperName='CC_Event_Manager'];
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'CC Territory Manager'].Id,
            LastName = 'last',
            //Email = 'puser000@appirio.com', //Commented by Shreya for Case-00274489
            Email = 'puser000@clearcaptions.com', //Modified by Shreya for Case-00274489
            Username = 'puser000@appirio.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = territoryRole.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street        	  = '809 HEATHERWOOD DR',
            country			  = 'USA',
            postalCode		  = '57005'
        );
        Test.startTest();
        insert u;
        Test.stopTest();
        system.assertEquals(true, [SELECT Active__c FROM Contact WHERE LastName = 'last'].Active__c);
        u.UserRoleId = eventRole.id;
        update u;
        u.UserRoleId = territoryRole.id;
        update u;
        u.isActive = false;
        update u;
        u.isActive = true;
        update u;
        u.lastName = 'last name';
        update u;
    }
    @isTest
    static void testUserTrigger2(){
        UserRole territoryRole = [SELECT id FROM UserRole WHERE DeveloperName='CC_Territory_Manager'];
        UserRole eventRole = [SELECT id FROM UserRole WHERE DeveloperName='CC_Event_Manager'];
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'CC Territory Manager'].Id,
            LastName = 'last',
            //Email = 'puser000@appirio.com',     //Commented by Shreya for Case-00274489
            Email = 'puser000@clearcaptions.com', //Modified by Shreya for Case-00274489
            Username = 'puser000@appirio.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = eventRole.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street        	  = '809 HEATHERWOOD DR',
            country			  = 'USA',
            postalCode		  = '57005'
        );
        Test.startTest();
        insert u;
        List<Contact> con = [SELECT Active__c FROM Contact WHERE LastName = 'last'];
        //system.assertEquals(0, con.size());
        u.userroleId = territoryRole.id;
        update u;
        
        Test.stopTest();
        system.assertEquals(true, [SELECT Active__c FROM Contact WHERE LastName = 'last'].Active__c);
    }
}