@isTest
private class WorkOrderTriggerHandlerTest {
    @isTest
    static void testWotrigger(){
        List<Account> accList = CC_TestUtility.createAccount(1, true);
        List<Contact> conList = CC_TestUtility.createContact(1, false, 'Customer', accList[0].id, 'cust@con.com');
        conList[0].Qualified__c = true;
        insert conList;
        List<Order> orderList = CC_TestUtility.createOrder(1, true, accList[0].id);
        List<WorkOrder> woList = CC_TestUtility.createWorkOrder(3, false, orderList[0].id);
        woList[1].Service_Request_Status__c = 'Completed';
        woList[1].StartDate = Date.today();
        woList[1].contactId = conList[0].id;
        woList[0].Service_Request_Status__c = 'Scheduled';
        woList[0].Labor_Source__c = 'Field Solutions';
        woList[0].Start_Time_Window__c = null;//Added By saurabh S-648388
        woList[0].StartDate = Date.today();
        woList[0].contactId = conList[0].id;
        woList[2].Service_Request_Status__c = 'Scheduled';
        woList[2].StartDate = Date.today();
        woList[0].Start_Time_Window__c = '7:00 am - 9:00 am';//Added By saurabh S-648388
        woList[2].contactId = conList[0].id; //Added By saurabh S-648388
        woList[0].status = 'Scheduled'; //Added By saurabh S-648388
        insert woList;
        woList[0].StartDate=Date.today().addDays(1);//Added By saurabh S-648388
        woList[1].Service_Request_Status__c = 'Scheduled';//Added By saurabh S-648388
        woList[0].status = 'Completed'; //Added By saurabh S-648388

        WorkOrderTriggerHandler.geo_distance(333.33,1111.22,9999.44,22223.44);
    }
    @isTest
    static void testWotrigger2(){
            List<Account> accList = CC_TestUtility.createAccount(1, true);
            List<Contact> conList = CC_TestUtility.createContact(1, false, 'Customer', accList[0].id, 'cust@con.com');
            conList[0].Qualified__c = true;
            insert conList;
            List<Order> orderList = CC_TestUtility.createOrder(1, true, accList[0].id);
            List<WorkOrder> woList = CC_TestUtility.createWorkOrder(4, false, orderList[0].id);
            woList[1].Service_Request_Status__c = 'Completed';
            woList[1].StartDate = Date.today();
            woList[1].contactId = conList[0].id;
            woList[0].Service_Request_Status__c = 'Scheduled';
            woList[0].Labor_Source__c = 'Field Solutions';
            woList[0].StartDate = Date.today();
            woList[0].contactId = conList[0].id;
            woList[2].contactId = conList[0].id;//Added By saurabh S-648388
            woList[3].contactId = conList[0].id;//Added By saurabh S-648388
            woList[2].Service_Request_Status__c = 'Scheduled';
            woList[2].StartDate = Date.today();
            woList[0].Service_Request_Type__c = 'Install and Training';
            woList[0].status = 'Scheduled'; //Added By saurabh S-648388
            insert woList;
            List<Opportunity> oppList = CC_TestUtility.createopportunity(2,false,'Purchase');
            oppList[0].Work_Order__c = woList[2].id;
            oppList[0].CloseDate = Date.today().addDays(3); //Uncommented by Tanisha Gupta 00275565
            oppList[1].Work_Order__c = woList[0].id;
            oppList[1].CloseDate = Date.today().addDays(3);
            insert oppList;
            woList[0].Service_Request_Status__c = 'Completed';
            woList[1].Service_Request_Status__c = 'Scheduled';
            woList[2].Service_Request_Status__c = 'Scheduled-Confirmed';
            woList[0].StartDate = Date.today().addDays(1);
            woList[3].Service_Request_Type__c = 'Install and Training';
            woList[0].status = 'Completed'; //Added By saurabh S-648388
            update woList;
       
    }
    //START-- Created by Parag Bhatt for story S-612220
    static testmethod void testSendingWelcomeEmailOnInstall(){

            Id woRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install and Training').getRecordTypeId();
            List<Account> accList = CC_TestUtility.createAccount(1, true);
            List<Contact> conList = CC_TestUtility.createContact(1, false, 'Customer', accList[0].id, 'cust@con.com');
            conList[0].Qualified__c = true;
            conList[0].HasOptedOutOfEmail = FALSE;
            conList[0].Certified__c = TRUE;
            insert conList;
            Product2 prod = new Product2();
            prod.Name = 'CC Blue Phone';
            prod.IsActive = true;
            insert prod;
            Product2 prod2 = new Product2();
            prod2.Name = 'iPhone Phone VOIP';
            prod2.IsActive = true;
            insert prod2;
            Asset testAsset = new Asset();
            testAsset.AccountId = accList[0].Id;
            testAsset.ContactId = conList[0].Id;
            testAsset.Type__c = 'iPhone Phone VOIP';
            testAsset.Name = 'Test Asset';
            testAsset.Identifier__c = 'test123';
            testAsset.Product2Id =prod2.id ;
            insert testAsset;
            Asset testAsset1 = new Asset();
            testAsset1.AccountId = accList[0].Id;
            testAsset1.ContactId = conList[0].Id;
            testAsset1.Type__c = 'CC Blue Phone';
            testAsset1.Name = 'Test Asset';
            testAsset1.Identifier__c = 'test1';
            testAsset.Product2Id = prod.id ;
            insert testAsset1;
            List<Order> orderList = CC_TestUtility.createOrder(1, true, accList[0].id);
            List<WorkOrder> woList = CC_TestUtility.createWorkOrder(4, false, orderList[0].id);
            woList[1].Service_Request_Status__c = 'Completed';
            woList[1].StartDate = Date.today();
            woList[1].contactId = conList[0].id;
            woList[0].Service_Request_Status__c = 'Scheduled';
            woList[0].Labor_Source__c = 'Field Solutions';
            woList[0].StartDate = Date.today();
            woList[0].contactId = conList[0].id;
            woList[2].Service_Request_Status__c = 'Scheduled';
            woList[2].StartDate = Date.today();
            woList[0].Service_Request_Type__c = 'Install and Training';
            woList[0].recordTypeId = woRecordTypeId;
            woList[0].Status = 'Scheduled';
            woList[1].recordTypeId = woRecordTypeId;
            woList[1].Status = 'Scheduled';
            woList[2].recordTypeId = woRecordTypeId;
            woList[2].Status = 'Scheduled';
            woList[2].contactId = conList[0].id;//Added By saurabh S-648388
            woList[3].contactId = conList[0].id;//Added By saurabh S-648388
            insert woList;
            List<Opportunity> oppList = CC_TestUtility.createopportunity(2,false,'Purchase');
            oppList[0].Work_Order__c = woList[2].id;
            oppList[0].CloseDate = Date.today().addDays(3);
            oppList[1].Work_Order__c = woList[0].id;
            oppList[1].CloseDate = Date.today().addDays(3);
            insert oppList;
            woList[0].Service_Request_Status__c = 'Completed';
            woList[1].Service_Request_Status__c = 'Scheduled';
            woList[2].Service_Request_Status__c = 'Scheduled-Confirmed';
            woList[0].StartDate = Date.today().addDays(1);
            woList[3].Service_Request_Type__c = 'Install and Training';
            woList[0].Status = 'Completed';
            woList[1].Status = 'Completed';
            woList[2].Status = 'Completed';
            update woList;
    }
    // END --- Created by Parag Bhatt for Story S-612220
}