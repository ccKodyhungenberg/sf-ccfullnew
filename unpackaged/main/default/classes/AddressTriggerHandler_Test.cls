@isTest
public class AddressTriggerHandler_Test {
  
  public static testmethod void uncheckAllActiveCheckbox_Test() {
    
    List<Account> listAccount = CC_TestUtility.createAccountRecords('Test Account', 10, TRUE);
    system.assertEquals(10,[SELECT Id FROM Account WHERE Name LIKE '%Test Account%'].size());
    
    List<Addresses__c> listAddress = CC_TestUtility.createAddressRecords('Test City', 'Test State', 'Test Country', FALSE, String.valueOf(listAccount.get(0).Id),'','', 5, TRUE );
    system.assertEquals(5,[SELECT Id FROM Addresses__c WHERE Account__c = :listAccount.get(0).Id].size());
    
    listAddress.get(2).Active__c = TRUE;
    update listAddress.get(2);
    
    List<Contact> listContact = CC_TestUtility.createContactRecords('Test Contact', 10, TRUE);
    system.assertEquals(10,[SELECT Id FROM Contact WHERE Name LIKE '%Test Contact%'].size());
    
    List<Addresses__c> listAddressCon = CC_TestUtility.createAddressRecords('Test City', 'Test State', 'Test Country', FALSE,'', String.valueOf(listContact.get(0).Id),'', 5, TRUE );
    system.assertEquals(5,[SELECT Id FROM Addresses__c WHERE Contact__c = :listContact.get(0).Id].size());
    
    listAddressCon.get(2).Active__c = TRUE;
    update listAddressCon.get(2);
    
    List<Asset> listAsset = CC_TestUtility.createAssetRecords('Test Asset', 10, TRUE,listAccount[0].id);
    system.assertEquals(10,[SELECT Id FROM Asset WHERE Name LIKE '%Test Asset%'].size());
    
    
    List<Addresses__c> listAddressAsset = CC_TestUtility.createAddressRecords('Test City', 'Test State', 'Test Country', FALSE,'','', String.valueOf(listAsset.get(0).Id), 5, TRUE );
    system.assertEquals(5,[SELECT Id FROM Addresses__c WHERE Asset__c = :listAsset.get(0).Id].size());
    
    listAddressAsset.get(2).Active__c = TRUE;
    update listAddressAsset.get(2);
  }
  
}