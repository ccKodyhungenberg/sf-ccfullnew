/** 
* (c) 2019 Appirio, Inc.
* Apex Class Name: BatchPopulateRecommendedCallFieldTest.cls
* Description: BatchPopulateRecommendedCallFieldTest test class for BatchPopulateRecommendedCallField class.
* @Created By : Vinit for S-637726
**/
@isTest
public class BatchPopulateRecommendedCallFieldTest {

    /** 
    * Setting Up test data.
    * @Param none.
    * @Return none.
    **/
    static testMethod void setUpMethod() {
        
        System.debug('record type id '+Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Large Event').getRecordTypeId());
        
        Campaign camp = new Campaign();
        camp.Name = 'test camp';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today();
        camp.Channel__c = 'Marketing';
        camp.IsActive = true;
        camp.Event_POC__c = UserInfo.getUserId();
        camp.Secondary_Event_POC__c = UserInfo.getUserId();
        camp.RecordTypeId=Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Marketing').getRecordTypeId();
        insert camp;
        
        //Campaign c = new Campaign(Name='Test', Channel__c='Marketing', Sub_Channel__c = 'Traditional');
        
		//insert c;
        
        Lead ld = new Lead(Campaign__c = camp.id, Company = 'TestCompany', LastName='LastName');
        
        insert ld;
        
        Task tsk = new Task(whoID=ld.id, status = 'Completed', type='Call', Subject='Call');
        
        insert tsk;
        
        Test.startTest();
        
        String query = 'SELECT id, createdDate, whoid from Task where whoid In (SELECT Id FROM Lead WHERE Channel__c = \'Marketing\') AND status = \'Completed\' AND (type = \'call\' OR type = \'Call - Attempt\') Order By createdDate DESC LIMIT 1';
        BatchPopulateRecommendedCallField btch = new BatchPopulateRecommendedCallField(query);
        Database.executeBatch(btch, 50);
        
        Test.stopTest();
    }
}