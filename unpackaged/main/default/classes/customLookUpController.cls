public class customLookUpController {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord,
                                                     String ObjectName,
                                                     String displayFields,
                                                     String additionalWhereClause,
                                                     String profileName,
                                                     String userRole) {


        system.debug('ObjectName-->' + ObjectName);
        system.debug('profileName-->' + profileName);
        system.debug('userRole--' + userRole);
        system.debug('additionalWhereClause-->' + additionalWhereClause);
        String searchKey = searchKeyWord + '%'; 
        //List<Campaign> filteredCampaignList = new List<Campaign>();
        if(ObjectName == 'Campaign'){


            List<Campaign> listCamp = new List<Campaign>();

            if(additionalWhereClause!=null && additionalWhereClause!=''){

                if(additionalWhereClause == 'Event' || additionalWhereClause == 'Large Event' ){


                    if(profileName == 'CC Customer Care Rep'){

                        listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                                    FROM Campaign
                                    WHERE
                                    (StartDate <= :Date.Today() AND EndDateAdd14__c >= :Date.Today())
                                    AND IsActive = true
                                    AND RecordType.Name = : additionalWhereClause AND Name like :searchKey
                                    ORDER BY StartDate];

                    }
                    else{

                        listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                                    FROM Campaign
                                    WHERE
                                    (StartDate <= :Date.Today() AND EndDateAdd14__c >= :Date.Today())
                                    AND Channel__c != null
                                    AND IsActive = true
                                    AND ( Event_POC__c=:UserInfo.getUserId() OR Secondary_Event_POC__c =: UserInfo.getUserId())  //As per Task T-701782
                                    AND RecordType.Name = : additionalWhereClause AND Name like :searchKey
                                    ORDER BY StartDate];

                    }



                }else if(additionalWhereClause == 'Hearing Professional' || additionalWhereClause == 'Marketing' ){

                    if((profileName == 'CC Inside Sales Representative' || profileName == 'CC System Administrator' || userRole == 'CC User Contractor' || profileName == 'CC Inside Sales Manager' ||profileName == 'System Administrator' )
                            && additionalWhereClause == 'Marketing' ){

                        listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                                    FROM Campaign
                                    WHERE
                                    Status != 'Cancelled'
                                    AND RecordType.Name =: additionalWhereClause AND Name Like :searchKey
                                    ORDER BY StartDate];

                    }
                    else{
                        listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                                    FROM Campaign
                                    WHERE
                                    OwnerId = : UserInfo.getUserId() AND Status != 'Cancelled'
                                    AND RecordType.Name =: additionalWhereClause AND Name Like :searchKey
                                    ORDER BY StartDate];

                    }


                }
                else{//if No Recordtype filter selected


                    if(profileName == 'CC Inside Sales Representative' || userRole == 'CC User Contractor'){

                        listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                                    FROM Campaign
                                    WHERE
                                    ((StartDate <= :Date.Today() AND EndDateAdd14__c >= :Date.Today())
                                        AND Channel__c != null
                                        AND IsActive = true
                                        AND ( Event_POC__c=:UserInfo.getUserId() OR Secondary_Event_POC__c =: UserInfo.getUserId())  //As per Task T-701782
                                        AND RecordType.Name IN ('Event', 'Large Event') AND Name like :searchKey) //condtion for Event,Large event end
                                    OR (StartDate <= :Date.Today()
                                        AND EndDateAdd14__c >= :Date.Today()
                                        AND IsActive = true
                                        AND RecordType.Name = 'Marketing' AND Name like :searchKey) //condition for marketing END
                                    OR(OwnerId = : UserInfo.getUserId() AND Status != 'Cancelled'
                                        AND RecordType.Name IN ('Hearing Professional','Marketing')
                                        AND Name Like :searchKey)
                                    ORDER BY StartDate];

                    }
                    else if(profileName == 'CC Customer Care Rep'){

                        listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                                    FROM Campaign
                                    WHERE
                                    StartDate <= :Date.Today()
                                    AND EndDateAdd14__c >= :Date.Today()
                                    AND IsActive = true
                                    AND RecordType.Name IN ('Event', 'Large Event','Marketing')
                                    AND Name Like :searchKey
                                    ORDER BY StartDate];

                    }
                    else{
                        listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                                    FROM Campaign
                                    WHERE
                                    ((StartDate <= :Date.Today() AND EndDateAdd14__c >= :Date.Today())
                                    AND Channel__c != null
                                    AND IsActive = true
                                    AND ( Event_POC__c=:UserInfo.getUserId() OR Secondary_Event_POC__c =: UserInfo.getUserId())  //As per Task T-701782
                                    AND RecordType.Name IN ('Event', 'Large Event','Marketing') AND Name like :searchKey)
                                    OR(OwnerId = : UserInfo.getUserId() AND Status != 'Cancelled'
                                    AND RecordType.Name IN ('Hearing Professional','Marketing') AND Name Like :searchKey)
                                    ORDER BY StartDate];

                    }



                }

            }//for CAP portal campaign search
            else{

                listCamp = [SELECT Id, Name, Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c
                FROM Campaign
                WHERE
                ((StartDate <= :Date.Today() AND EndDateAdd14__c >= :Date.Today())
                AND Channel__c != null
                AND IsActive = true
                AND ( Event_POC__c=:UserInfo.getUserId() OR Secondary_Event_POC__c =: UserInfo.getUserId())  //As per Task T-701782
                AND RecordType.Name IN ('Event','Large Event') AND Name like :searchKey)
                OR(OwnerId = : UserInfo.getUserId() AND Status != 'Cancelled'
                AND RecordType.Name = 'Hearing Professional' AND Name Like :searchKey)
                ORDER BY StartDate];
            }

            return listCamp;
        }
		
                                                        
		List < sObject > returnList = new List < sObject > ();
      	searchKey = '%' +searchKeyWord + '%';
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name ' +
            (String.isBlank(displayFields) ? '' : (',' + displayFields)) ;
        if(ObjectName == 'account') {
            
            if(searchKeyWord.isNumeric()){
               sQuery += ' FROM ' +ObjectName + ' WHERE GP_Customer_ID__c LIKE: searchKey ' ; 
            }
            else{
                sQuery += ' FROM ' +ObjectName + ' WHERE Name LIKE: searchKey ';
            }           
            sQuery += 'AND Recordtype.Name !=\'Non-Customer\' order by createdDate desc LIMIT 50';
            
        }
        else{
            sQuery += ' FROM ' +ObjectName + ' where Name LIKE: searchKey ' +
            (String.isBlank(additionalWhereClause) ? '' : ('AND ' + additionalWhereClause)) +
            'order by createdDate desc LIMIT 50';                                                 
        }

		List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        
        return returnList;
        
    }

    @AuraEnabled
    public static String putRecordSession(sObject sobj){
    	system.debug('selectedEventRecord put-'+sObj);
        if(Campaign.sObjectType == sObj.Id.getsObjectType()){
        	Cache.Session.put('selectedEventRecord', sobj);
        	return 'success';
        }
        else
            return 'false';
          
    }
    
    @AuraEnabled
    public static sObject getSelectEventFromCache() {
        sObject sobj;
        if (Cache.Session.contains('selectedEventRecord')) {
    		sObj = (sObject)Cache.Session.get('selectedEventRecord');
		}
        system.debug('selectedEventRecord retrieved-'+sObj);
        return sobj;
    }



}