// C-00249521 1/31/19 NCarson - When a PCF Form is returned via Fax, create a new case and associate it with the appropriate contact
global class CC_PcfApprovalSfaxBatch implements Queueable, Database.AllowsCallouts {
    
    public void execute(QueueableContext context) {
        processInboundFaxes();
        processOutboundFaxes();
    }
    
    private static void processInboundFaxes() {
        System.debug('Starting processInboundFaxes');
        PCF_Settings__c settings = PCF_Settings__c.getInstance('Default');
        
        String prefix = Case.sObjectType.getDescribe().getKeyPrefix();
        
        Map<String, Case> pcfApprovalsByFaxId = new Map<String, Case>();
        
        Datetime now = Datetime.now();
        
        // look for new faxes since this date
        Datetime lastChecked = settings.Incoming_Fax_Last_Checked__c;
        if (lastChecked == null) {
            lastChecked = now.addDays(-100);
        } else {
            // Just recheck for a day to make sure we get them
            lastChecked = now.addDays(-1);
        }
        
        // callout to the API
        SFaxAPIService.InboundFaxes faxes = SFaxAPIService.ReceiveInboundFax(lastChecked);
        
        Set<Id> barcodeIds = new Set<Id>();
        Map<String, String> barcodeIdsByFaxId = new Map<String, String>();
        
        // loop over the faxes to track the barcodes
        Set<String> incomingFaxIds = new Set<String>();
        if (faxes != null && faxes.InboundFaxItems != null) {
            for (SFaxAPIService.Fax fax : faxes.InboundFaxItems) {
                incomingFaxIds.add(fax.FaxId);
                
                // Check for a barcode
                Boolean isValidId = !fax.Barcodes.BarcodeItems.isEmpty() && 
                    fax.Barcodes.BarcodeItems[0].BarcodeData.length() == 18 && 
                    fax.Barcodes.BarcodeItems[0].BarcodeData.startsWith(prefix);
                
                if (isValidId) {
                    String barcodeId = fax.Barcodes.BarcodeItems[0].BarcodeData;
                    System.debug(barcodeId);
                    // use this to validate and lookup barcode values
                    barcodeIdsByFaxId.put(fax.FaxId, barcodeId);
                    
                    // we will remove these below to find out what's existing what's missing.
                    barcodeIds.add(barcodeId);
                }
            }
            System.debug('Found inbound faxes: ' + incomingFaxIds);
            
            Set<String> processedIncomingFaxIds = new Set<String>();
            Map<String, Case> deletedApprovalsById = new Map<String, Case>();
            for (Case pcfApproval : [SELECT Id, ContactId, Sfax_Incoming_Id__c, isDeleted FROM Case WHERE Id IN :barcodeIds OR Sfax_Incoming_Id__c IN :incomingFaxIds ALL ROWS]) {
                if (pcfApproval.isDeleted) {
                    deletedApprovalsById.put(pcfApproval.Id, pcfApproval);
                } else {
                    processedIncomingFaxIds.add(pcfApproval.Sfax_Incoming_Id__c);
                }
                barcodeIds.remove(pcfApproval.Id);
            }
            System.debug('Existing inbound faxes: ' + processedIncomingFaxIds);
            System.debug('Deleted inbound faxes: ' + deletedApprovalsById);
            System.debug('Missing inbound faxes: ' + barcodeIds);
            
            Map<Id, Case> alreadyAddedApprovals = new Map<Id, Case>();
            Set<String> pcfApprovalIds = new Set<String>();
            
            // C-00249521 1/31/19 NCarson - START Need to find associated contact to populate ContactId field on new case
            // soql query outside of for loop
            List<Case> caseList = [SELECT Id, ContactId FROM Case WHERE Id IN : barcodeIdsByFaxId.values()];
            Map<Id, Id> caseToContactMap = New Map<Id, Id>();
            for (Case c : caseList) {
                caseToContactMap.put(c.Id, c.ContactId);
            } // C-00249521 1/31/19 NCarson END 
            
            for (SFaxAPIService.Fax fax : faxes.InboundFaxItems) {
                if (!processedIncomingFaxIds.contains(fax.FaxId)) {
                    System.debug(fax);
                                        
                    // Assign the ID if we get it from the QR code and it starts with the prefix
                    Boolean isValidId = barcodeIdsByFaxId.containsKey(fax.FaxId);
                    String barcodeId = null;
                    if (isValidId && fax.Barcodes.BarcodeItems != null && !fax.Barcodes.BarcodeItems.isEmpty()) {
                        barcodeId = fax.Barcodes.BarcodeItems[0].BarcodeData;
                    }
                    
                    Boolean isMissing = isValidId && barcodeIds.contains(barcodeId);
                    Boolean isDeleted = isValidId && deletedApprovalsById.containsKey(barcodeId);
                    String PCFVerificationRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PCF Verification').getRecordTypeId();
                    Case pcfApproval;
                    if ((isDeleted && !isMissing) || alreadyAddedApprovals.containsKey(barcodeId)) {
                        if (alreadyAddedApprovals.containsKey(barcodeId)) {
                            // This one is in the feed twice
                            System.debug('duplicate ids in feed');
                            pcfApproval = alreadyAddedApprovals.get(barcodeId).clone();
                        } else {
                            System.debug('isDeleted && !isMissing');
                            pcfApproval = deletedApprovalsById.get(barcodeId).clone();
                        }
                        pcfApproval.Id = null;
                        pcfApproval.RecordTypeId = PCFVerificationRecTypeId;
                        pcfApproval.Sfax_Incoming_Id__c = fax.FaxId;
                        pcfApproval.Status__c = PcfApprovalConstants.REVIEW;
                    } else {
                        pcfApproval = new Case(
                            Sfax_Incoming_Id__c = fax.FaxId
                        );
                        if (isValidId && !isMissing) {
                            System.debug('isValidId && !isMissing');
                            pcfApprovalIds.add(barcodeId);
                            // START C-00249521 1/31/19 NCarson - New case should be created
                            // pcfApproval.Id = barcodeId; C-00249521 Should not glom on to previous case (barcodeId represents previous case Id)
                            pcfApproval.Status__c = PcfApprovalConstants.REVIEW;
                            pcfApproval.RecordTypeId = PCFVerificationRecTypeId;
                            // alreadyAddedApprovals.put(barcodeId, pcfApproval); // New case, thus has not been added - NCarson
                            pcfApproval.ContactId = caseToContactMap.get(barcodeId); // New case should be associated with contact from previous case - NCarson
                            pcfApproval.Subject = 'PCF Approval'; // END C-00249521 1/31/19 NCarson
                        } else {
                            System.debug('no match on incoming fax');
                            pcfApproval.RecordTypeId =PCFVerificationRecTypeId;//Added By Saurabh Chaturvedi C-00248810 on 1/16/2019
                            pcfApproval.Title__c = 'Incoming fax from ' + fax.FromFaxNumber;
                            pcfApproval.Status__c = PcfApprovalConstants.INCOMING;
                        }
                    }
                    pcfApprovalsByFaxId.put(fax.FaxId, pcfApproval);
                } else {
                    System.debug('Already processed ' + fax.FaxId);
                }
            }
            
            // Upsert the approval with the new information
            System.debug('New PCF_Approval__c ' + pcfApprovalsByFaxId);
            if (!pcfApprovalsByFaxId.isEmpty()) {
                upsert pcfApprovalsByFaxId.values();
            }
            
            List<String> pcfDownloadApprovalIds = new List<String>();
            List<String> pcfStatuses = new List<String>();
            List<String> pcfFaxIDs = new List<String>();
            
            Set<Id> newApprovals = new Set<Id>();
            for (Case pcfApproval : pcfApprovalsByFaxId.values()) {
                newApprovals.add(pcfApproval.Id);
                
                pcfDownloadApprovalIds.add(pcfApproval.Id);
                pcfStatuses.add(pcfApproval.Status__c);
                pcfFaxIDs.add(pcfApproval.Sfax_Incoming_Id__c);
            }
            
            for (Case pcfApproval : [
                SELECT Id, Sfax_Incoming_Id__c, Status__c
                FROM Case 
                WHERE Sfax_Incoming_Id__c != NULL 
                AND Id NOT IN :newApprovals
                AND Status__c = :PcfApprovalConstants.INCOMING
            ]){
                pcfDownloadApprovalIds.add(pcfApproval.Id);
                pcfStatuses.add(pcfApproval.Status__c);
                pcfFaxIDs.add(pcfApproval.Sfax_Incoming_Id__c);
            }
            system.debug('pcfDownloadApprovalIds'+pcfDownloadApprovalIds);
            if (!pcfDownloadApprovalIds.isEmpty()) {
                List<String> ids = new List<String>();
                List<String> statuses = new List<String>();
                List<String> faxIds = new List<String>();
                Integer index = 0;
                for (String pcfApprovalId : pcfDownloadApprovalIds) {
                    ids.add(pcfApprovalId);
                    statuses.add(pcfStatuses.get(index));
                    faxIds.add(pcfFaxIDs.get(index));
                    index += 1;
                    if (Math.mod(index, 100) == 0) {
                        downloadFaxes(ids, statuses, faxIds);
                        ids = new List<String>();
                        statuses = new List<String>();
                        faxIds = new List<String>();
                    }
                }
                if (!ids.isEmpty()) {
                    downloadFaxes(ids, statuses, faxIds);
                }
            }
        }
        
        settings.Incoming_Fax_Last_Checked__c = now;
        update settings;
    }
    
    @future (callout=true)
    private static void processOutboundFaxes() {
        System.debug('Starting processOutboundFaxes');
        PCF_Settings__c settings = PCF_Settings__c.getInstance('Default');
        
        String prefix = Case.sObjectType.getDescribe().getKeyPrefix();
        
        Map<String, Case> pcfApprovalsByFaxId = new Map<String, Case>();
        
        Datetime now = Datetime.now();
        
        // look for new faxes since this date
        Datetime lastChecked = settings.Outgoing_Fax_Last_Checked__c;
        if (lastChecked == null) {
            lastChecked = now.addDays(-100);
        } else {
            lastChecked = now.addHours(-3);
        }
        
        // callout to the API
        SFaxAPIService.OutboundFaxes faxes = SFaxAPIService.ReceiveOutboundFax(lastChecked);
        
        if (faxes != null && faxes.OutboundFaxItems != null) {
            // loop over the faxes to track the barcodes
            Map<String, SFaxAPIService.Fax> faxesByQueueId = new Map<String, SFaxAPIService.Fax>();
            for (SFaxAPIService.Fax fax : faxes.OutboundFaxItems) {
                // we might have the same fax in the same feed twice with different statuses...
                if (faxesByQueueId.containsKey(fax.SendFaxQueueId)) {
                    SFaxAPIService.Fax existing = faxesByQueueId.get(fax.SendFaxQueueId);
                    // only update if it the existing one is a failure
                    if (existing.isSuccess == false) {
                        faxesByQueueId.put(fax.SendFaxQueueId, fax);
                    }
                } else {
                    faxesByQueueId.put(fax.SendFaxQueueId, fax);
                }
            }
            
            // only update if it isn't already true - sometimes the faxes come through twice with a failure/success or vice versa
            List<Case> approvals = [SELECT Id, Sfax_Queue_Id__c FROM Case WHERE Sfax_Queue_Id__c IN :faxesByQueueId.keySet() AND Fax_Success__c != true];
            for (Case approval : approvals) {
                SFaxAPIService.Fax fax = faxesByQueueId.get(approval.Sfax_Queue_Id__c);
                approval.Fax_Status__c = fax.ResultMessage;
                approval.Fax_Success__c = fax.isSuccess;
            }
            
            if (!approvals.isEmpty()) {
                System.debug('Updating outbound faxes ' + approvals);
                //system.assert(false, 'Updating outbound faxes ' + approvals);
                update approvals;
            }
        }
        
        settings.Outgoing_Fax_Last_Checked__c = now;
        update settings;
    }
    
    @future (callout=true)
    public static void downloadFaxes(String[] pcfApprovalIds, String[] pcfStatuses, String[] pcfFaxIDs) {
        system.debug('in download faxes'+ pcfApprovalIds+pcfStatuses+pcfFaxIDs);
        PCF_Settings__c settings = PCF_Settings__c.getInstance('Default');
        
        List<Case> approvals = new List<Case>();
        Map<String, Blob> downloadedBlobs = new Map<String, Blob>();
        Map<String, String> downloadedStatuses = new Map<String, String>();
        
        for (Integer index = 0; index < pcfApprovalIds.size(); index += 1) {
            String pcfApprovalId = pcfApprovalIds.get(index);
            String pcfFaxID = pcfFaxIDs.get(index);
            String pcfStatus = pcfStatuses.get(index);
            try {
                // Download the PDF from Sfax
                Blob docContents = SFaxAPIService.DownloadInboundFaxAsPdf(pcfFaxID);
                downloadedBlobs.put(pcfApprovalId, docContents);
                downloadedStatuses.put(pcfApprovalId, pcfStatus);
            } catch (Exception e) {
                approvals.add(new Case(
                    Id = pcfApprovalId,
                    Last_Error_Message__c = e.getMessage() + '\n\n' + e.getStackTraceString()
                )); 
            }
        }
        
        
        
        for (String pcfApprovalId : downloadedBlobs.keySet()) {
            String pcfStatus = downloadedStatuses.get(pcfApprovalId);
            
            // Attach the signed form to the PCF Approval
            Attachment attachment = new Attachment(
                Name = settings.PCF_Signed_Document_Type__c + '.pdf',
                ParentId = pcfApprovalId,
                Body = downloadedBlobs.get(pcfApprovalId)
            );
            insert attachment;
            system.debug('attachment'+attachment);
            // Attach Files in lightning with content version
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = settings.PCF_Signed_Document_Type__c + '.pdf'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = settings.PCF_Signed_Document_Type__c; // Display name of the files
            //conVer.VersionData = EncodingUtil.base64Decode(downloadedBlobs.get(pcfApprovalId).toString()); // converting your binary string to Blog
            conVer.VersionData = EncodingUtil.base64Decode(EncodingUtil.base64Encode(downloadedBlobs.get(pcfApprovalId)));
            insert conVer;
            
            system.debug('conVer'+conVer);
            // link the object with file
            // First get the content document Id from ContentVersion
            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
            
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDoc;
            cDe.LinkedEntityId = pcfApprovalId; // you can use objectId,GroupId etc
            cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            system.debug('cde'+cDe);
            // Update the Approval
            approvals.add(new Case(
                Id = pcfApprovalId,
                PCF_Signed_Attachment_Id__c = attachment.Id,
                Status__c = pcfStatus == PcfApprovalConstants.INCOMING ? PcfApprovalConstants.UNASSIGNED : pcfStatus
            ));
        }
        //system.assert(false, approvals);
        if (!approvals.isEmpty()) {
              update approvals;
        }
        
        
    }
}