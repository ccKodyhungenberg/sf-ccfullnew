@isTest
private without sharing class CC_OrderItemTriggerHandlerTest {
	@isTest
    static void testOrderItemTriggerTest(){
        List<Account> accList = CC_TestUtility.createAccount(1, true);
        List<Product2> productList = CC_TestUtility.createProduct(1, true);
        List<Order> orderList = CC_TestUtility.createOrder(1, false, accList[0].id);
        orderList[0].Pricebook2Id = Test.getStandardPricebookId();
        insert orderList;
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = productList[0].id;
        pbe.UnitPrice = 2;
        pbe.Pricebook2Id = Test.getStandardPricebookId();
        insert pbe;
        OrderItem oi = new OrderItem();
        oi.OrderId = orderList[0].id;
        oi.Product2Id = productList[0].id;
        oi.Serial_Number__c = '123';
        oi.Quantity = 1;
        oi.UnitPrice = 2;
        oi.PricebookEntryId = pbe.id;
        test.startTest();
        insert oi;
        test.stopTest();
        oi.Serial_Number__c = '345';
        try{
        	update oi;
        }catch(Exception e){
            system.assertEquals('Records With Duplicate Products and Serial Number cannot be inserted', e.getDMLMessage(0));
        }
    }
}