/**
 * Class Name: ESignLiveDocumentUtilities.cls
 *
 * This class contains method used for encoding documents for sending
 * to eSignLive.
 */

public with sharing class ESignLiveDocumentUtilities 
{
    public ESignLiveDocumentUtilities(){}

    /**
     * Method to encode documents passed in the documentMap
     */
    public static List<String> encodeDocuments(Map<String,Blob> documentMap, String name)
    {
        String footerEncoded ='';
        String headerPlusBodyEncoded = '';
        String newLineEncoded = encodeString(ESignLiveSDK.NEWLINE);
        String footer = '--' + ESignLiveSDK.BOUNDARY + '--';             

        for(String fileName : documentMap.keySet())
        {
            String header = '--' + ESignLiveSDK.BOUNDARY + ESignLiveSDK.NEWLINE +
                'Content-Disposition: form-data; name="' + name + '"; filename="' + fileName + '"' + ESignLiveSDK.NEWLINE +
                'Content-Type: application/pdf';    
            String headerEncoded = encodeString(header);
            String bodyEncoded = EncodingUtil.base64Encode(documentMap.get(fileName));

            String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());                    
            if(last4Bytes.endsWith('==')) 
            {
                last4Bytes = last4Bytes.substring(0,2) + '0K';
                bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;                
                footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            }
            else if(last4Bytes.endsWith('=')) 
            {
                last4Bytes = last4Bytes.substring(0,3) + 'N';
                bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;                
                footer = ESignLiveSDK.NEWLINE + footer;
                footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            }
            else 
            {
                footer = ESignLiveSDK.CARRIAGE + footer;
                footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            }

            if(String.isBlank(headerPlusBodyEncoded))
            {
                headerPlusBodyEncoded = headerEncoded + bodyEncoded ;
            }
            else
            {
                headerPlusBodyEncoded = headerPlusBodyEncoded + newLineEncoded + headerEncoded + bodyEncoded ;    
            }
        }

        return new List<String> {headerPlusBodyEncoded,footerEncoded};
    }

    /** 
     * Method to endocde the string passed
     */
    public static String encodeString(String stringToEncode)
    {
        String doubleCarriage = ESignLiveSDK.CARRIAGE + ESignLiveSDK.CARRIAGE;
        String encodedString = EncodingUtil.base64Encode(Blob.valueOf(stringToEncode + doubleCarriage));
        // pad the end with spaces until the encoded string doesn't end in an equals sign
        while(encodedString.endsWith('='))
        {
            stringToEncode += ' ';
            encodedString = EncodingUtil.base64Encode(Blob.valueOf(stringToEncode + doubleCarriage));
        }
        return encodedString;
    }

    /**
     * Method to encode the content passed
     */
    public static String encodeContent(String content)
    {
        String contentEncoded = EncodingUtil.base64Encode(Blob.valueOf(content + ESignLiveSDK.CARRIAGE));
        String last4BytesForContent = contentEncoded.substring(contentEncoded.length()-4,contentEncoded.length());
        if(contentEncoded.endsWith('=='))
        {
            last4BytesForContent = last4BytesForContent.substring(0,2) + '0K';
            contentEncoded = contentEncoded.substring(0,contentEncoded.length()-4) + last4BytesForContent;
        }
        else if(last4BytesForContent.endsWith('=')) 
        {
            last4BytesForContent =  last4BytesForContent.substring(0,3) + 'N';
            contentEncoded = contentEncoded.substring(0,contentEncoded.length()-4) + last4BytesForContent;
        }

        return contentEncoded;
    }

}