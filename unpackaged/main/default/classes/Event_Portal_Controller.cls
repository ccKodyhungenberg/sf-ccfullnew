public class Event_Portal_Controller {
    
    @AuraEnabled
    public static  Contact getTechnician() {
  
        Contact SR_Technician;       
        String technicianRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        
        List<Contact> lTech=new List<Contact>([SELECT Id, FirstName, LastName,Phone,Email,Name,Labor_Source__c, 
                                               MailingState, MailingPostalCode FROM Contact 
                                               WHERE Active__c = true 
                                               AND Name=:userinfo.getname() 
                                               AND RecordTypeId =:technicianRecordTypeId LIMIT 1]);
        if(!lTech.isEmpty()) { 
            SR_Technician=lTech[0];
        } 
        else SR_Technician=new Contact();
            
        return SR_Technician; 
       
    }
    
    @AuraEnabled
    public static sObject getSelectEventFromCache() {
        sObject sobj;
        if (Cache.Session.contains('selectedEventRecord')) {
            sObj = (sObject)Cache.Session.get('selectedEventRecord');
        }
        system.debug('selectedEventRecord retrieved-'+sObj);
        return sobj;
    }
    
    //Method to search for campaigns
    @AuraEnabled
    public static List<Campaign> searchForCamps(String searchText) {
        System.debug('searchText@@@@'+searchText);
         string query = 'SELECT Id, Name, Channel__c,Sub_Channel__c, Event_POC__c, RecordType.Name, StartDate,EndDate, Location_Street_Address__c, Location_City__c, Location_State__c, Location_Postal_Code__c '  
                         +' FROM Campaign WHERE ((StartDate <= :Date.Today() AND EndDateAdd14__c >= :Date.Today())' 
                     +' AND Channel__c != null' 
                      +' AND IsActive = true'
                     +' AND ( Event_POC__c=:UserInfo.getUserId() OR Secondary_Event_POC__c =: UserInfo.getUserId() ) '
                     + ' AND RecordType.Name IN (\'Large Event\',\'Event\'))'          
                     +' OR (OwnerId= : UserInfo.getUserId() AND Status !=\' Cancelled \' AND RecordType.Name =\' Hearing Professional \')';
       
         
        if(searchText!=null && searchText!=''){
            string serachKey = '%'+searchText+'%';
            query = query + ' AND Name LIKE :serachKey';
        }
        
        System.debug('queryResults1@@@@'+query);
        
        return database.query(query);
    }

    //method to query the Contacts based on the email Entered
    @AuraEnabled
    public static List<Contact> SearchEmail(String EmailText) {
    String quer = '%'+ EmailText + '%';
        system.debug('Contact email----'+EmailText);
        List<Contact> conList = [Select id,FirstName,LastName,Email,MobilePhone,HomePhone,Mailingstreet,MailingCity,MailingState,MailingPostalCode 
                                    from Contact WHERE Email LIKE: quer AND RecordType.Name != 'Non-Customer'];
        return conList;
    }
    
    //method to validate the entered address of customer
     @AuraEnabled
    public static exceptionWrap ValidateAddress(String conInfo) { 
        exceptionWrap wrp = new exceptionWrap();
        try{
            system.debug('conInfoJson@@@@@'+conInfo);
            //Contact Order_Con = (Contact) JSON.deserialize(conInfo, Contact.Class);
            Map<string,string> Order_Con = new Map<string,string>();
            Order_Con = (Map<string,string>) JSON.deserialize(conInfo, Map<string,string>.class);
            system.debug('inoput info of customer----'+Order_Con);
            Lead Order_Lead = new Lead();
            Order_Lead.city = Order_Con.get('city');
            Order_Lead.State = Order_Con.get('state');
            Order_Lead.Country = Order_Con.get('country');
            Order_Lead.PostalCode = Order_Con.get('ZipCode');
            Order_Lead.MobilePhone = Order_Con.get('Mphone');
            Order_Lead.Phone = Order_Con.get('HPhone');
            Order_Lead.street = Order_Con.get('street') ;
            Order_Lead.email = Order_Con.get('Email');
            List<Contact> contactList = new List<Contact>();
            String leadEmail = Order_Lead.email;
            
            /*
            contactList=[SELECT Id, Name, FirstName, LastName, Phone, HomePhone, OtherPhone, MobilePhone, 
                         Email, MailingCity, MailingCountry, MailingState, MailingStreet, MailingPostalCode, RecordType.Name 
                         FROM Contact 
                         WHERE (Email=:Order_Lead.Email OR HomePhone=:homePhone OR
                                MobilePhone=:mobilePhone OR HomePhone=:formattedhomephone 
                                OR MobilePhone=:formattedmobilephone)
                                AND RecordType.Name != 'Non-Customer'
                                ORDER BY CreatedDate];
            */
            
            String query = 'SELECT Id, Name, FirstName, LastName, Phone, HomePhone, OtherPhone, MobilePhone,' +  
                           'Email, MailingCity, MailingCountry, MailingState, MailingStreet, MailingPostalCode, RecordType.Name ' +
                           'FROM Contact WHERE RecordType.Name != \'Non-Customer\' AND ( Email = :leadEmail ';
            if(Order_Lead.Phone!=null && Order_Lead.Phone!=''){
                String homePhone = Order_Lead.Phone;
                String formattedhomephone = homePhone.replaceAll('\\D','');   
                query+= ' OR HomePhone=:homePhone OR HomePhone=:formattedhomephone';
            }
            
            if(Order_Lead.MobilePhone!=null && Order_Lead.MobilePhone!=''){
                String mobilePhone = Order_Lead.MobilePhone;
                String formattedmobilephone = mobilePhone.replaceAll('\\D','');   
                query+= ' OR MobilePhone=:mobilePhone OR MobilePhone=:formattedmobilephone';
            }
            query += ' ) ORDER BY CreatedDate';
            contactList = database.query(query);
            
            if(contactList.size()>0){

                wrp.contactList = contactList;
                system.debug('list od address=='+wrp.contactList);
                wrp.msg = 'Duplicate Contacts Found';
                wrp.StatusCode = '200';  
            }
            else{
                String street =CCGlobal.removeLineBreak(Order_Lead.Street);
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                String sURL=Label.CC_CCWS_ENDPOINT+'/address/validate';
                request.setEndpoint(sURL);
                request.setMethod('POST');
                request.setTimeout(20000); 
                request.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
                // Set the body as a JSON object
                Order_Lead.country = '';
                String requestBody = 'data={"apt":"'+ Order_Con.get('apt') + '","city": "'+Order_Lead.City+'","country": "'+Order_Lead.country+'","e911LocationID": null,"primary": false,"state":"'+Order_Lead.State+'","street":"'+street+'","types": [],"zip":"'+Order_Lead.PostalCode+'","id": null,"type":"address"}';
                request.setBody(requestBody);
                system.debug('requestBody====='+requestBody);
                //if( !Test.isRunningTest() ) {
                HttpResponse response = http.send(request);
                system.debug('integration response====='+response);
                
                // Parse the JSON response
                if (response.getStatusCode() != 200) {
                    wrp.msg =  'Error#'+response.getStatusCode() + ' ' + response.getStatus();   //If there is something wrong in integration connection;
                    //return wrp;
                }else{
                    //if the Integration connection is successfull
                    String addressResponse=response.getBody();
                    system.debug('addressResponse'+addressResponse);
                    if( !String.isBlank( addressResponse ) ) {
                        clValidateAddressResult responseResult = (clValidateAddressResult)JSON.deserialize(addressResponse, clValidateAddressResult.class);
                        system.debug('responseResult'+responseResult);
        
                        //if the data is not present at all and code is not 200
                        if(responseResult.result == 'false' && responseResult.data.suggestedAddresses == null && responseResult.code != '200') {
                            wrp.msg = responseResult.message;
                            //return wrp;
                        }
                        //if the data is returning more than one Address and status code is not 200
                        else if(responseResult.result == 'false' && responseResult.data.suggestedAddresses != null && responseResult.code != '200' ){
                            List<clSuggestion_Item> lSuggestion_Item = new List<clSuggestion_Item> ();
                            integer I=0; 
                            system.debug('suggested addresses====='+responseResult.data.suggestedAddresses);    
                            for(clSuggestion oSug : responseResult.data.suggestedAddresses ) {
                                clSuggestion_Item oItem = new clSuggestion_Item(); 
                                oItem.index = I++; 
                                oItem.oSuggestion = oSug; 
                                oItem.sAddress = oSug.street +' ' + oSug.apt + ' ' + oSug.city+ ' ' + oSug.state + ' '+ oSug.zip;
                                lSuggestion_Item.add(oItem);
                            }
                            wrp.sugList = lSuggestion_Item;
                            system.debug('list od address=='+wrp.sugList);
                            wrp.msg = responseResult.message;
                            wrp.StatusCode = responseResult.code;
                            //return wrp;
                        }
        
                        //VALID CASE and returned address correctly matches the entered data
                        else if( (responseResult.result == 'true' && responseResult.code == '200') /*|| skipCase*/) {
                            wrp.msg = 'Address was successfully validated';
                            wrp.StatusCode = responseResult.code;
                            //return wrp;
                        }
        
                        else {
                            
                            wrp.msg = 'Could not call validation service, ' + responseResult.message;
                            wrp.StatusCode = responseResult.code;
                            //return wrp;
                        }
                    }

                }
                
            }

        }
        catch(Exception e){
            system.debug('exception - ' + e.getMessage() +'-' + e.getLineNumber());
            wrp.msg = 'Error! Could not call validation service ' + e.getMessage();
            wrp.StatusCode = '404';
              
        }
        return wrp;    
        
    }
    
    @AuraEnabled
    public static exceptionWrap createLeadAndContact(String CampId,String CustomerInfo,Boolean isHearingLoss, Boolean isOverride){
        exceptionWrap exWrap = new exceptionWrap();
        try{
            Map<String,String> Order_Lead = new Map<String,String>();
            Order_Lead = (Map<string,string>) JSON.deserialize(CustomerInfo,Map<String,String>.class);
            //Insert Lead with the Info We got
            Lead ld = new Lead();
            ld.Company = Order_Lead.get('LastName').touppercase();
            ld.LastName = Order_Lead.get('LastName').touppercase();
            ld.FirstName = Order_Lead.get('FirstName').touppercase();
            ld.Email = Order_Lead.get('Email');
            ld.Phone = Order_Lead.get('HPhone');
            ld.MobilePhone = Order_Lead.get('Mphone');
            ld.Created_From_Portal__c = true;
            ld.Campaign__c = CampId;
            ld.city = Order_Lead.get('city').touppercase();
            ld.State = Order_Lead.get('state').touppercase();
            ld.Country = 'US';
            ld.Street = Order_Lead.get('street').touppercase()  +' '+ Order_Lead.get('apt').toUpperCase() ;
            ld.PostalCode = Order_Lead.get('ZipCode');
            ld.Has_smart_phone__c  = 'No';
            if(isHearingLoss){
                ld.Has_Hearing_loss__c  = 'Yes';   
            }else{
                ld.Has_Hearing_loss__c  = 'No';
            }
            
            ld.Has_internet__c   = 'No';
            ld.Has_home_phone_line__c   = 'No';
            ld.Has_Hearing_aide__c   = 'No';
            
            system.debug('ld-'+ld);
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true; 
            //Account duplicateAccount = new Account(Name='dupe');
            Database.SaveResult saveResult = Database.insert(ld, dml);
            Id leadId = saveResult.getId();
            //insert ld;
            
            //if hearing loss no contact is created
            if(isHearingLoss && !isOverride){
                Database.LeadConvert lc = new database.LeadConvert();
                        //lc.setOverwriteLeadSource(true);
                lc.setLeadId(leadId);
                lc.donotcreateopportunity = TRUE;
                LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1].get(0);
                lc.setConvertedStatus(convertStatus.MasterLabel);
                Database.LeadConvertResult lcr = Database.convertLead(lc,dml);
                system.debug('lcr'+lcr);
                List<Contact> Order_Contact = [SELECT Id,FirstName,AccountId,LastName,Email,MobilePhone,
                                               homePhone,otherPhone,phone,RecordTypeId,MailingStreet, MailingCountry, 
                                               MailingState ,MailingCity,MailingPostalCode FROM Contact WHERE Id=: lcr.contactid]; 
                system.debug('Order_Contact'+Order_Contact);
                Order_Contact[0].Email = Order_Lead.get('Email');
                Order_Contact[0].LastName = Order_Lead.get('LastName').touppercase();
                Order_Contact[0].FirstName = Order_Lead.get('FirstName').touppercase();
                Order_Contact[0].MailingStreet = Order_Lead.get('street').touppercase()  +' '+ Order_Lead.get('apt').touppercase();
                Order_Contact[0].Qualified__c = true;
                Order_Contact[0].MailingCity = Order_Lead.get('city').touppercase();
                Order_Contact[0].MailingState = Order_Lead.get('state').touppercase();
                Order_Contact[0].MailingCountry = Order_Lead.get('country'); 
                Order_Contact[0].MailingPostalCode = Order_Lead.get('ZipCode');
                Order_Contact[0].Created_From_Portal__c = true;
                Order_Contact[0].HomePhone = Order_Lead.get('HPhone');
                Order_Contact[0].MobilePhone = Order_Lead.get('Mphone');//added by Apoorva for case:00253298 19/3/19 start
                //Order_Contact[0].Phone = null;
                Order_Contact[0].Phone = (Order_Lead.get('HPhone')!=null && Order_Lead.get('HPhone')!='')?Order_Lead.get('HPhone'):Order_Lead.get('Mphone');
                Order_Contact[0].Password__c = generatePassword(Order_Contact[0]);
                Update Order_Contact;
                
                List<Contact> cnId = [Select id,CB_userID__c,MAilingCity,MAilingState,MAilingCountry,MailingStreet,MAilingPostalCode,
                                        CCWS_User_Create_Response__c,email,password__c,LastName,FirstName 
                                        from Contact Where id=:Order_Contact[0].Id];
                
                
                //exWrap = createUser(cnId[0]);  //Creation of user in the third party
                exWrap.ContactId = cnId[0].Id;
                
                exWrap.msg = 'Contact Created Successfully';
               
                //return exWrap;
            }else{
                 exWrap.leadId = leadId;
                exWrap.msg = 'Lead Created Successfully';
                //return exWrap;
            }
        }
        catch(Exception ex){
            system.debug('Error:' + ex.getMessage()+ '-'+ex.getLineNumber());  
            exWrap.msg = 'Error:' + ex.getMessage();   
        }
        return exWrap;
    }
    
    //Method to create the user in the third party system
    @AuraEnabled
    public static exceptionWrap createUser(String contactId){
        exceptionWrap ex = new exceptionWrap();
        ex.msg='';
        try{
            Contact conRec = [Select id,CB_userID__c,MAilingCity,MAilingState,MAilingCountry,MailingStreet,MAilingPostalCode,
                                  CCWS_User_Create_Response__c,email,password__c,LastName,FirstName,Phone, MobilePhone,HomePhone
                                  from Contact Where id=:contactId];        
            String street = CCGlobal.removeLineBreak(conRec.MailingStreet);
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(Label.CC_CCWS_ENDPOINT+'/user/create');
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            // Set the body as a JSON object
            String addressBodyString = '"addresses": [{"city":"'+ conRec.MAilingCity+'","state":"'+conRec.MAilingState.touppercase()+'","street":"'+street+'","country":"'+conRec.MAilingCountry+'","zip":"'+conRec.MAilingPostalCode+'","primary": true}]';
            request.setBody('data={"email": "'+conRec.email+'","password": "'+conRec.password__c+'","registrationSource": '+Integer.valueOf(Math.random())+',"active": true,"lastName": "'+conRec.LastName+'","firstName": "'+conRec.FirstName+'","contactNumber": "'+conRec.Phone+'",'+addressBodyString+'}');
            system.debug('user request-'+ request.getBody());
            HttpResponse response = http.send(request);
            String CCWSCreateUserResponse = response.getBody();  //response after creating the user
            system.debug('user reponse '+ response.getBody());
            if(!String.isBlank(CCWSCreateUserResponse)){
                conRec.CCWS_User_Create_Response__c = CCWSCreateUserResponse; 
            }
            
            
             // Parse the JSON response
            if (response.getStatusCode() != 200) {
                System.debug('The status code returned was: ' +
                             response.getStatusCode() + ' ' + response.getStatus());
                if(response.getBody() == NULL || response.getBody()==''){
                    ex.msg = 'Error#' + response.getStatusCode() + ' ' + response.getStatus();
                }
                else{ 
                    JSONParser parser = JSON.createParser(response.getBody());
                    while (parser.nextToken() != null) {
                        if( parser.getText() == 'message' ) {
                            parser.nextToken();
                            ex.msg = parser.getText();
                        }
                    }
                }
            }else{
                String cbUserId;
                JSONParser parser = JSON.createParser(response.getBody());
                while (parser.nextToken() != null) {
                    if(parser.getText() == 'code'){
                        parser.nextToken();
                        if(parser.getText() != '200'){
                            ex.msg = 'Error#' + parser.getText();
                        }
                    }
                    if (parser.getText() == 'id') {
                        // Get the value.
                        parser.nextToken();
                        // Compute the grand total price for all invoices.
                        cbUserId = parser.getText();
                    }
                    if( parser.getText() == 'message' ) {
                        parser.nextToken();
                        ex.msg += ' ' + parser.getText();
                    }
                }
                if(cbUserId != null && !String.isBlank(cbUserId)){
                    ConRec.CB_userID__c = cbUserId; 
                    //ConRec.Password__c = Event_Portal_Controller.generatePassword(ConRec);
                    ex.msg = 'User created Successfully';
                    
                }
                update ConRec;
            }
        }
        catch(Exception e){
            system.debug('Error:' + e.getMessage()+ '-'+e.getLineNumber());
            ex.msg ='User Creation Error-' + e.getMessage();
        }
        system.debug('exx-' + ex);
        return ex;

    }
    
    //method for generating the password
    public static String generatePassword(Contact conRec){
        Double rnd =  Math.random();
        string mob = (conRec.MobilePhone!=null && conRec.MobilePhone!='')?conRec.MobilePhone:conRec.HomePhone;
        Integer num = mob.length();
        String ran = String.valueof(rnd);
        String lstname = conRec.LastName.trim();
        lstname = lstname.replaceAll( '\\s+', '');
        lstname = (lstname.length()>20)?lstname.substring(0, 19):lstname;
        String PassWrd = lstname + mob.substring(num-4, num) + ran.substring(2,4);
        system.debug('PassWrd-'+PassWrd);
        return PassWrd;
    }
    //wrapper class to wrap the integration response data 
    public class clValidateAddressResult {
        public string code; //"Error"
        public suggestedAddr data;
        public string result; //"The supplied address was not found, although similar matches were found."
        public string message;         
        
    }

    public class suggestedAddr{
        //    public clSuggestion[] suggestedAddress;
        public clSuggestion[] suggestedAddresses;
    }
    
    //update the contact with the customer enterered qualification.
    @AuraEnabled
    public static exceptionWrap createContactWithQualification(String CampId,String CustomerInfo,Boolean createContact, string hasHomePhone,string internet, string smartPhone, string hearing, string type){
        system.debug('CampId>>>>>>'+CampId);
         system.debug('CustomerInfo>>>>>>'+CustomerInfo);
         system.debug('createContact>>>>>>'+createContact);
         system.debug('hasHomePhone>>>>>>'+hasHomePhone);
         system.debug('internet>>>>>>'+internet);
         system.debug('smartPhone>>>>>>'+smartPhone);
         system.debug('hearing>>>>>>'+hearing);
         system.debug('type>>>>>>'+type);
        
        exceptionWrap wrap;
        Contact returnCon = new Contact();
        if(createContact){
            wrap = createLeadAndContact(CampId,CustomerInfo,true,false);
        }
        else{
            wrap = createLeadAndContact(CampId,CustomerInfo,true,true);    
        }
        
        if(wrap.msg.contains('Lead Created')){
            Map<string,string> trueYesfalseNoMap = new Map<string,string>();
            trueYesfalseNoMap.put('true','Yes');
            trueYesfalseNoMap.put('false','No');
            List<Lead> listLead =[select Id,Name,Has_home_phone_line__c ,Has_internet__c ,Has_smart_phone__c ,Has_Hearing_aide__c  from Lead where id=:wrap.leadId];
            if(listLead.size()>0){
                listLead[0].Has_home_phone_line__c = trueYesfalseNoMap.get(hasHomePhone);
                listLead[0].Has_internet__c = trueYesfalseNoMap.get(internet);
                 listLead[0].Has_smart_phone__c = trueYesfalseNoMap.get(smartPhone);
                 listLead[0].Has_Hearing_aide__c =  trueYesfalseNoMap.get(hearing);
                listLead[0].Smart_Phone_Type__c = type;
                 update listLead[0]; 
                //returnCon = listCon[0];
            }
        }
        
        if(wrap.msg.contains('Contact Created')){
            Map<string,string> trueYesfalseNoMap = new Map<string,string>();
            trueYesfalseNoMap.put('true','Yes');
            trueYesfalseNoMap.put('false','No');
            List<Contact> listCon =[select Id,Name,Has_home_phone_line__c ,Has_internet__c ,Has_smart_phone__c ,Has_Hearing_aide__c  from Contact where id=:wrap.ContactId];
            if(listCon.size()>0){
                listCon[0].Has_home_phone_line__c = trueYesfalseNoMap.get(hasHomePhone);
                listCon[0].Has_internet__c = trueYesfalseNoMap.get(internet);
                 listCon[0].Has_smart_phone__c = trueYesfalseNoMap.get(smartPhone);
                 listCon[0].Has_Hearing_aide__c =  trueYesfalseNoMap.get(hearing);
                listCon[0].Smart_Phone_Type__c = type;
                 update listCon[0]; 
                //returnCon = listCon[0];
            }
        }
        
        return wrap;
        
        
        /*
        WorkOrder work = new WorkOrder( Start_Time_Window__c = populateStartTimeWindow()[0],Status='Ready for Scheduling');
        OrderItem ordrItm = new OrderItem(Product2Id= productList()[0].Id);
        WorkOrderWrapper wrap = new WorkOrderWrapper( ordrItm,work, productList(), listCon[0] );
        //System.debug('listWOWrapper for the first time for savePassword()::::::'+listWOWrapper.size());
        List<WorkOrderWrapper> listWOWrapper = new List<WorkOrderWrapper>();
        listWOWrapper.add(wrap);
        System.debug('listWOWrapper for the first time for savePassword()::::::'+listWOWrapper.size());
       
        return listWOWrapper;*/
    }
    
    @AuraEnabled
    public static List<Product2> productList()  {
        //List<SelectOption> options = new List<SelectOption> {};
            
            List<Product2> listProducts = new List<Product2>();
        
        listProducts = [SELECT Id, Name, Is_Default_Product__c FROM Product2 WHERE Is_Portal_Product__C = true order by Name ASC];
        
        return listProducts;
    }
    
    @AuraEnabled
    public static List<String> populateStartTimeWindow()  {
        //List<SelectOption> options = new List<SelectOption> {};
            
        list<String> returnValue = new list<String>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        // Get the object type of the SObject.
        Schema.sObjectType objType = schemaMap.get('WorkOrder');
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values = fieldMap.get('Start_Time_Window__c').getDescribe().getPickListValues();
        //pickListFieldWrapper picklistDetails = new pickListFieldWrapper();
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            //picklistWrapper aa = new picklistWrapper();
            //aa.pickListLabel = a.getLabel();
            //aa.pickListValue = a.getValue();
            returnValue.add(a.getLabel());
        }
        system.debug('*****returnValue'+returnValue);
        return returnValue;
    }
    
    @AuraEnabled
    public static WorkOrderWrapperResponse saveAppointmentWorkOrder(String contactId,List<WorkOrderWrapper> listWOWrapper) {
        WorkOrderWrapperResponse wrapperResponse = new WorkOrderWrapperResponse();
        String response ='';
        List<Order> listOrders = new List<Order>();
        List<OrderItem> listOrderItems = new List<OrderItem>();
        List<WorkOrder> listWorkOrders = new List<WorkOrder>();
        Set<Id> productIdsForPBE = new Set<Id>();
        Map<Id,Id> mapProductIdToPBEId = new Map<Id,Id>();
        Boolean createAddressRecord = false;
        try {
            System.debug('*****listWOWrapper'+listWOWrapper);
            Contact Order_Contact = new Contact();
            Order_Contact = [SELECT Id,AccountId,Name,FirstName,LastName,Phone, Email,MailingPostalCode,MailingCountry,
                                     Account.Name,Campaign__c,MailingStreet,MailingCity,MailingState
                                     FROM Contact WHERE Id =: contactId];
            wrapperResponse.contact = Order_Contact;
        System.debug(listWOWrapper.size());
        if( listWOWrapper.size() > 0 ) {
            for( WorkOrderWrapper wow : listWOWrapper) {
                productIdsForPBE.add(wow.ordrLineItem.Product2Id);
            }
        }
        
        
        if( productIdsForPBE.size() > 0  ) {
            for( PricebookEntry pbe : [ SELECT Product2Id,Id FROM PricebookEntry WHERE Product2Id IN : productIdsForPBE ]) {
                mapProductIdToPBEId.put(pbe.Product2Id, pbe.Id );
            }
        }
        
        

            if( listWOWrapper.size() > 0 ) {
                for( WorkOrderWrapper wow : listWOWrapper) {
                    
                    //connect order to customer and create Line Item
                    wow.wrapperOrder.Contact__c = contactId;
                    wow.wrapperOrder.Campaign__c = Order_Contact.Campaign__c;
                    wow.wrapperOrder.Shipping_Name__c = (Order_Contact.FirstName != null ? Order_Contact.FirstName : '') +' ' +(Order_Contact.LastName != null ? Order_Contact.LastName : '');
                    wow.wrapperOrder.ShippingStreet = Order_Contact.MailingStreet;
                    wow.wrapperOrder.ShippingCity = Order_Contact.MailingCity;
                    wow.wrapperOrder.ShippingState = Order_Contact.MailingState;
                    wow.wrapperOrder.ShippingPostalCode = Order_Contact.MailingPostalCode;
                    wow.wrapperOrder.Shipping_Notification_Phone__c = Order_Contact.Phone;
                    wow.wrapperOrder.Shipping_Notification_Email__c = Order_Contact.Email;
                    wow.wrapperOrder.AccountId = Order_Contact.AccountId;
                    wow.wrapperOrder.EffectiveDate = System.Today();
                    wow.wrapperOrder.Status = 'Approved';
                    wow.wrapperOrder.Pricebook2Id = Label.CC_STANDARD_PRICE_BOOK_ID;
                    wow.wrapperOrder.Fulfillment__c = 'On-Hand inventory';
                    wow.wrapperOrder.created_from_portal__c = true;
                    //if(SR_Technician.Id != null) {
                        wow.wrapperOrder.Technician__c = wow.technician.Id;
                    //}
                    wow.wrapperOrder.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Product Order').getRecordTypeId();
                    //ordr.OpportunityId = opp.Id;
                    listOrders.add(wow.wrapperOrder);
                    
                }
            }
            
            if( listOrders.size() > 0 ) {
                upsert listOrders;
            }
            
            
            if( listWOWrapper.size() > 0 ) {
                System.debug('listWOWrapper::::::::'+listWOWrapper);
                for( WorkOrderWrapper wow : listWOWrapper) {
                    //Inserting Order Product
                    wow.ordrLineItem.OrderId = wow.wrapperOrder.Id;
                    wow.ordrLineItem.PricebookEntryId = mapProductIdToPBEId.get(wow.ordrLineItem.Product2Id);
                    wow.ordrLineItem.Quantity = 1;
                    wow.ordrLineItem.UnitPrice = 0;
                    listOrderItems.add( wow.ordrLineItem ); //Conflicting now for price book entry -- By Vishwas Gupta Need Clarity
                }
            }
            
            if( listOrderItems.size() > 0 ) {
                System.debug('listOrderItems::::::::::::::::'+listOrderItems);
                insert listOrderItems;
            }
            
           //get the record type id
            string recordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install and Training').getRecordTypeId();
            if( listWOWrapper.size() > 0 ) {
                for( WorkOrderWrapper wow : listWOWrapper) {
                    //prepare service request/Work Order
                    wow.workOrder.ContactId = Order_Contact.Id;
                    wow.workOrder.Order__c = wow.wrapperOrder.Id;
                    wow.workOrder.created_from_portal__c = true;
                    wow.workOrder.Service_Request_Type__c='Install and Training';
                   // wow.workOrder.Customer_Email__c  = Order_Contact.Email;
                    //to fix the 1 day prior date issue
                    Datetime now = wow.workOrder.StartDate;
                    Integer offset = UserInfo.getTimeZone().getOffset(now);
                    Datetime local = now.addhours(10);
                   system.debug('local>>>>'+local);
                    wow.workOrder.StartDate= local;
                    wow.workOrder.Technician__c = wow.technician.id;
                    wow.workOrder.Labor_Source__c = wow.technician.Labor_Source__c; 
                    wow.workOrder.RecordTypeId = recordTypeId;
                    
                    listWorkOrders.add(wow.workOrder);
                }
            }
            
            if( listWorkOrders.size() > 0 ) {
                insert listWorkOrders;
                System.debug('listWOWrapper::::::::'+listWOWrapper);
                Map<id,WorkOrder> workOrderMap = new Map<id,WorkOrder>([Select id,WorkOrderNumber,Status,StartDate,Start_Time_Window__c,Service_Notes__c  
                                                                        from WorkOrder where ContactId =:Order_Contact.Id]);
                for( WorkOrderWrapper wow : listWOWrapper) {
                    if(workOrderMap.containsKey(wow.workOrder.Id)){
                        wow.workOrder = workOrderMap.get(wow.workOrder.Id);
                    }
                }
                
                createAddressRecord = true;
            }

            if( createAddressRecord ) {
                Addresses__c address = new Addresses__c( Account__c = Order_Contact.AccountId, Contact__c = Order_Contact.Id );
                address.Street__c = Order_Contact.MailingStreet.toUpperCase();
                address.City__c = Order_Contact.MailingCity.toUpperCase();
                address.State_Province__c = Order_Contact.MailingState.toUpperCase();
                address.Country__c = Order_Contact.MailingCountry;
                address.Zip_Postal_Code__c = Order_Contact.MailingPostalCode;
                address.isValidated__c = TRUE;
                address.Primary__c = true;
                insert address;
            }
            wrapperResponse.responseMessage = 'success';
            wrapperResponse.worOrderWrapperList =  listWOWrapper;
            //response = 'success';  
        } catch(Exception e) {
             //if there is an error - 
            system.debug('Error -'+ e.getMessage() + '-' + e.getLineNumber());
            wrapperResponse.responseMessage = 'Error - ' + e.getMessage() + '-' + e.getLineNumber();
            wrapperResponse.worOrderWrapperList =  listWOWrapper;
        }
        return wrapperResponse;
       
    }    
    /*
     @AuraEnabled
    public static List< confirmAppointment(List<WorkOrderWrapper> listWOWrapper){
        //system.assert(false,listWOWrapper); 
        System.debug(listWOWrapper);
        Set<Id> selectedProductIds = new Set<Id>();
        
        if( listWOWrapper.size() > 0 ) {
            for( WorkOrderWrapper wow : listWOWrapper ) {
                
                if(wow.wo.StartDate == null || wow.wo.Start_Time_Window__c == null ) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You have to select both Start Date and Start Time Window to schedule!'));
                    return; 
                }
                
                if( !String.isBlank(wow.ordrLineItem.Product2Id) ) {
                    selectedProductIds.add(wow.ordrLineItem.Product2Id);
                }
                //check if SR is ready for confirmation
                if(wow.wo.StartDate != null && wow.wo.Start_Time_Window__c != null ) {
                    wow.wo.Status='Scheduled';
                }
            }
            
            
            if( selectedProductIds.size() != listWOWrapper.size() ) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You cannot create multiple entries with same Product!'));
                return; 
            }
        
    }
    */
    

    //wrapper to split of address data
    public class clSuggestion {
        @AuraEnabled
        public string city {get; set;} //"ROCKLIN"
        @AuraEnabled
        public string state {get; set;} //"CA"
        @AuraEnabled
        public string street {get; set;} //"595 MENLO DR"
        @AuraEnabled
        public string apt {get; set;} //""
        @AuraEnabled
        public string zip {get; set;} //"95765"     
    }

    public class clSuggestion_Item {
        @AuraEnabled
        public integer index {get; set;} 
        @AuraEnabled
        public string sAddress {get; set;}
        @AuraEnabled
        public clSuggestion oSuggestion {get; set;}
    }
    
    //wrapper class to show up the error message on the lightning Component
    public class exceptionWrap{
        @AuraEnabled
        public String msg;
        @AuraEnabled
        public List<clSuggestion_Item> sugList = new List<clSuggestion_Item>();
        @AuraEnabled 
        public List<Contact> contactList{get;set;}
        @AuraEnabled
        public String StatusCode;
        @AuraEnabled
        public String ContactId;
        @AuraEnabled
        public String leadId;
    }
    
    public class WorkOrderWrapperResponse {
        @AuraEnabled
        public List<WorkOrderWrapper> worOrderWrapperList{get;set;}
        
        @AuraEnabled
        public Contact contact {get;set;}
        @AuraEnabled
        public String responseMessage{get;set;}
        
    }
    
    
    public class WorkOrderWrapper {
        @AuraEnabled
        public OrderItem ordrLineItem {get; set;} 
        @AuraEnabled
        public WorkOrder workOrder {get; set;}
        @AuraEnabled
        public Contact technician {get; set;}
        @AuraEnabled
        public Product2 product {get;set;}
        

        //CC_EventPortalController ccEventPortal = new CC_EventPortalController();
        public Order wrapperOrder = new Order();
        public WorkOrderWrapper(){}
        public WorkOrderWrapper( OrderItem ordrLineItem, WorkOrder wo, Product2 product ,Contact contact) {
            this.ordrLineItem = ordrLineItem;
            this.workOrder = wo;
            this.product = product;
            this.technician = contact;
        }
        
    }
   
}