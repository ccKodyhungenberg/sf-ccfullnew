public class CC_AddressFlowController {
    
    public Addresses__c address {get;set;}
    public String radioResponse {get;set;}
    public List<addressWrapper> listAvailableAddress {get;set;}
    public Boolean showRadio {get;set;}
    public String selectedAddress {get;set;}
    public Boolean readOnlyAddress {get;set;}
    public String recordId = ''; 
    public Boolean updateShippingAddress {get;set;}
    public Boolean updateBillingAddress {get;set;}
    public List<CC_EventPortalController.clSuggestion_Item> lSuggestion_Item {get;set;}
    public Boolean showModal {get;set;}
    public Boolean fromComponent = FALSE;
    public String accountId = '';
    public String addressId = '';
    public Boolean is911Address{get;set;}
    public Boolean show911CheckBox{get;set;}
    public Boolean wantToCreateCase{get;set;}
    public Boolean showCreateCase{get;set;}
    public Boolean isSave{get;set;}
    public Boolean haveSuggestion{get;set;}
    public String deviceId;
    public Contact existingContact;
    public List<Addresses__c> existingConAddress = new List<Addresses__c>();
    public CC_AddressFlowController( ApexPages.StandardController stdController ) {
        is911Address = false;
        show911CheckBox = false;
        wantToCreateCase = false;
        showCreateCase = false;
        isSave = false;
        haveSuggestion = true;
        addressId = ApexPages.currentPage().getParameters().get('addressId');
        if( String.isBlank(addressId) ) {
            addressId = ApexPages.currentPage().getParameters().get('id');
        }
        if( getIsClassic() ) {
            recordId = ApexPages.currentPage().getParameters().get('retURL');
            //system.assert(false, recordId);
            recordId = recordId.substring(recordId.lastIndexOf('/')+1,recordId.length());
        }
        else {
            recordId = ApexPages.currentPage().getParameters().get('recordId');
        }
        if(recordId.startsWith('02i')){
            deviceId = [SELECT id, Identifier__c FROM Asset WHERE Id =: recordId].Identifier__c;
            show911CheckBox = true;
        }
        if(recordId.startsWith('003')){
            existingContact = [SELECT id, CB_userID__c FROM Contact WHERE id=: recordId];
            if(existingContact.id != null)
            existingConAddress = [SELECT id, City__c,primary__c,State_Province__c,Street__c,Zip_Postal_Code__c FROM Addresses__c WHERE contact__c =:existingContact.id];
        }
        accountId = ApexPages.currentPage().getParameters().get('accountId');
        
        if( !String.isBlank( String.valueOf(ApexPages.currentPage().getParameters().get('fromComponent')))) {
            fromComponent = TRUE;
        }
        
        if( !String.isBlank( recordId ) && recordId.startsWith('/')) {
            recordId = recordId.substring(1,recordId.length());
        }
        
        listAvailableAddress = new List<addressWrapper>();
        showRadio = FALSE;
        selectedAddress = '';
        readOnlyAddress = FALSE;
        updateShippingAddress = FALSE;
        updateBillingAddress = FALSE;
        showModal = FALSE;
        
        
        if( !String.isBlank(addressId) ) {
            for( Addresses__c add : [ SELECT Id,
                                     Contact__c, 
                                     Account__c, 
                                     Asset__c,
                                     City__c,
                                     State_Province__c,
                                     Street__c,
                                     Country__c,
                                     Zip_Postal_Code__c,
                                     Active__c,
                                     Update_Account_Billing_Address__c,
                                     Update_Account_Shipping_Addres__c,
                                     IsValidated__c 
                                     FROM Addresses__c 
                                     WHERE Id =: addressId
                                    ]) {
                                        address = add;
                                    }
        }
        else {
            address = new Addresses__c();
        }
        
        
        //system.assert(false,recordId);
        if( !String.isBlank(recordId) && recordId.startsWith('001') ) {
            showRadio = TRUE;
            readOnlyAddress = FALSE;
        }
        else if( !String.isBlank(accountId) && String.isBlank(addressId) ) {
            for( Addresses__c add :  [SELECT Id, Account__c,IsValidated__c ,Update_Account_Billing_Address__c,Update_Account_Shipping_Addres__c,Active__c,City__c,Street__c,State_Province__c,Country__c,Zip_Postal_Code__c FROM Addresses__c WHERE Account__c =: accountId AND IsValidated__c = TRUE ]) {
                addressWrapper aw = new addressWrapper( add, add.Active__c);
                listAvailableAddress.add(aw);
            }
            System.debug(listAvailableAddress);
        }
        
        
        /*if( !String.isBlank(recordId) ) {
if( recordId.startsWith('003')) {
for( Addresses__c add :  [SELECT Id, Account__c,Active__c,City__c,Street__c,State_Province__c,Country__c,Zip_Postal_Code__c FROM Addresses__c WHERE Contact__c =: recordId ]) {
addressWrapper aw = new addressWrapper( add, add.Active__c);
listAvailableAddress.add(aw);
}
}
if( recordId.startsWith('001')) {
showRadio = TRUE;
/*for( Addresses__c add :  [SELECT Id,Active__c,City__c,Street__c,State_Province__c,Country__c,Zip_Postal_Code__c FROM Addresses__c WHERE Account__c =: recordId ]) {
addressWrapper aw = new addressWrapper( add, add.Active__c);
listAvailableAddress.add(aw);
}
readOnlyAddress =FALSE;
}
if( recordId.startsWith('02i')) {
for( Addresses__c add :  [SELECT Id, Active__c,City__c,Street__c,State_Province__c,Country__c,Zip_Postal_Code__c FROM Addresses__c WHERE Asset__c =: recordId ] ) {
addressWrapper aw = new addressWrapper( add, add.Active__c);
listAvailableAddress.add(aw);
}
}*/
        //}
        
        if( listAvailableAddress.size() > 0 && !recordId.startsWith('001') ) {
            readOnlyAddress = TRUE;
        }
        
    }
    
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Update Mailing Address','Update Mailing Address')); 
        options.add(new SelectOption('Update Billing Address','Update Billing Address')); 
        return options;
    }
    
    
    public PageReference updateAddress() {
        PageReference pg = null;
        try{
            if( !String.isBlank(recordId) ) {
                if( recordId.startsWith('003')) {
                    address.Contact__c = recordId;
                }
                if( recordId.startsWith('02i')) {
                    address.Asset__c = recordId;
                    if(is911Address){
                        Asset ass = new Asset();
                        ass.id = recordId;
                        ass.X911_City__c = address.City__c;
                        ass.X911_Country__c = address.Country__c;
                        ass.X911_Postal_Code__c = address.Zip_Postal_Code__c;
                        ass.X911_State__c = address.State_Province__c;
                        ass.X911_Street__c = address.Street__c;
                        update ass;
                    }
                }
                if( recordId.startsWith('001')) {
                    address.Account__c = recordId;
                }
            }
            
            if( address.Account__c == null ) {
                if( !String.isBlank(accountId) ) {
                    address.Account__c = accountId;
                }
            }
            if(deviceId != null){
                String street = CCGlobal.removeLineBreak(address.Street__c);
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                string sURL = Label.CC_CCWS_ENDPOINT+'/device/update';
                req.setEndpoint(sURL);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
                String requestBody = 'data={"deviceID": "'+deviceId+'","address": {"street": "'+street+'","apt": "APT 125","city": "'+address.City__c+'","state": "'+address.State_Province__c+'","zip": "'+address.Zip_Postal_Code__c+'"}}';
                System.debug('************'+requestBody);
                req.setBody(requestBody);
                System.debug('************'+requestBody);
                // Send the request, and return a response
                HttpResponse res = h.send(req);
                string addressResponse = res.getBody();
                if (res.getStatusCode() != 200) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,res.getBody() + ' '+
                                                               res.getStatusCode() + ' ' + res.getStatus()));
                    return null;
                } 
                else{
                    system.debug('success');
                }
                system.debug('Identifier__c'+deviceID);
            }
            if(recordId.startsWith('003') && existingContact.CB_userID__c != null){
                String street = CCGlobal.removeLineBreak(address.Street__c);
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                string sURL = Label.CC_CCWS_ENDPOINT+'/user/update';
                req.setEndpoint(sURL);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
                String addressBodyString = '"addresses": []';
                String addressString = '"addresses": [';
                for(Addresses__c add: existingConAddress){
                    if(address.Active__c){
                        add.Primary__c = false;
                        addressString = addressString + '{"city":"'+ add.City__c+'","state":"'+add.State_Province__c+'","street":"'+street+'","country":"","zip":"'+add.Zip_Postal_Code__c+'","primary":false},';
                    }
                    else{
                        addressString = addressString + '{"city":"'+ add.City__c+'","state":"'+add.State_Province__c+'","street":"'+street+'","country":"","zip":"'+add.Zip_Postal_Code__c+'","primary":'+add.Primary__c+'},';
                    }
                }
                //addressString = addressString.removeEnd(',');
                if(existingConAddress.size()>0 ){
                    if(address.Active__c){
                        addressString = addressString + '{"city":"'+ address.City__c+'","state":"'+address.State_Province__c+'","street":"'+address.Street__c+'","country":"","zip":"'+address.Zip_Postal_Code__c+'","primary": true}]';
                        address.Primary__c = true;
                    }
                    else{
                        addressString = addressString + '{"city":"'+ address.City__c+'","state":"'+address.State_Province__c+'","street":"'+address.Street__c+'","country":"","zip":"'+address.Zip_Postal_Code__c+'","primary": false}]';
                    }
                }
                else{
                    addressString = addressString + '{"city":"'+ address.City__c+'","state":"'+address.State_Province__c+'","street":"'+address.Street__c+'","country":"","zip":"'+address.Zip_Postal_Code__c+'","primary": true}]';
                    address.Primary__c = true;
                }
                String requestBody = 'data={"userID": "'+existingContact.CB_userID__c+'",'+addressString+'}';
                System.debug('************'+requestBody);
                req.setBody(requestBody);
                System.debug('************'+requestBody);
                // Send the request, and return a response
                HttpResponse res = h.send(req);
                string addressResponse = res.getBody();
                system.debug('responsebody---' + addressResponse );
                if (res.getStatusCode() != 200) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,res.getBody() + ' '+
                                                               res.getStatusCode() + ' ' + res.getStatus()));
                    return null;
                } 
                else{
                    system.debug('success');
                }
            }
            update existingConAddress;
            upsert address;
            
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Address Updated Successfully!'));
        }catch( DMLException ex ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            return null;
        }
        
        pg = new PageReference('/'+recordId);
        pg.setRedirect(true);
        return pg;
    }
    
    public void fillAddressOnPage() {
        System.debug(listAvailableAddress);
        for(  addressWrapper aw : listAvailableAddress ) {
            
            if( aw.address.Id == selectedAddress ) {
                address = aw.address;
            }
        }
        isSave = true;
    }
    
    public void fillAddressToUpdate() {
        String radioValue = ApexPages.currentPage().getParameters().get('radioValue');
        if( !String.isBlank(radioValue)) {
            if( radioValue == 'Shipping') {
                address.Update_Account_Shipping_Addres__c = TRUE;
            }
            if( radioValue == 'Billing') {
                address.Update_Account_Billing_Address__c = TRUE;
            }
        }
    }
    
    public void validateAddress() {
        showModal = FALSE;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        string sURL = Label.CC_CCWS_ENDPOINT+'/address/validate';
        string street1 = '';
        string street2 = '';
        if( !String.isBlank(address.Street__c) ) {
            list<string> lStreet=address.Street__c.split('\n',2);
            street1 = lStreet[0];
            street2 = (lStreet.size()==2)?(lStreet[1]):('');      
                }
        
        req.setEndpoint(sURL);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        String street = CCGlobal.removeLineBreak(address.Street__c);
        address.country__c = '';
        String requestBody = 'data={"apt": "","city": "'+address.City__c+'","country": "'+address.country__c+'","e911LocationID": null,"primary": false,"state": "'+address.State_Province__c+'","street": "'+street+'","types": [],"zip": "'+address.Zip_Postal_Code__c+'","id": null,"type": "address"}';
        System.debug('************'+requestBody);
        req.setBody(requestBody);
        System.debug('************'+requestBody);
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        string addressResponse = res.getBody();
        
        System.debug('addressResponse:::::::::::::::'+addressResponse);
        
        if( !String.isBlank(addressResponse) ) {
            //received some result
            System.debug('*****************'+JSON.deserialize(addressResponse, CC_EventPortalController.clValidateAddressResult.class));
            CC_EventPortalController.clValidateAddressResult responseResult = (CC_EventPortalController.clValidateAddressResult) JSON.deserialize(addressResponse, CC_EventPortalController.clValidateAddressResult.class);
            
            // if(responseResult.result == 'false' && responseResult.data.suggestedAddress == null && responseResult.code != '200') {
            //   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,responseResult.message));
            //   return ;
            //  }
            
            //  else if( responseResult.result == 'false' && responseResult.data.suggestedAddress != null && responseResult.code != '200' ) {
            //  if( responseResult.data.suggestedAddress != null ) {
            //     if( !responseResult.data.suggestedAddress.isEmpty() ) {
            //display suggestions
            //      lSuggestion_Item = new List<CC_EventPortalController.clSuggestion_Item> ();
            //    integer I=0; 
            //   for(CC_EventPortalController.clSuggestion oSug : responseResult.data.suggestedAddress ) {
            //     CC_EventPortalController.clSuggestion_Item oItem = new CC_EventPortalController.clSuggestion_Item(); 
            //    oItem.index = I++; 
            //   oItem.oSuggestion = oSug; 
            //  oItem.sAddress = oSug.street + ' ' + oSug.apt + ' ' + oSug.city+ ' ' + oSug.state + ' '+ oSug.zip;
            // lSuggestion_Item.add(oItem);
            //}  
            
            if(responseResult.result == 'false' && responseResult.data.suggestedAddresses == null && responseResult.code != '200') {
                //if( recordId.startsWith('02i')) {
                
                //if(is911Address){
                showCreateCase = true;
                showModal = true;
                //}
                //}
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,responseResult.message));
                return;
                
            }
            
            else if( responseResult.result == 'false' && responseResult.data.suggestedAddresses != null && responseResult.code != '200' ) {
                //haveSuggestion = true;
                lSuggestion_Item = null;
                if( responseResult.data.suggestedAddresses != null ) {
                    if( !responseResult.data.suggestedAddresses.isEmpty() ) {
                        //display suggestions
                        lSuggestion_Item = new List<CC_EventPortalController.clSuggestion_Item> ();
                        integer I=0; 
                        for(CC_EventPortalController.clSuggestion oSug : responseResult.data.suggestedAddresses ) {
                            CC_EventPortalController.clSuggestion_Item oItem = new CC_EventPortalController.clSuggestion_Item(); 
                            oItem.index = I++; 
                            oItem.oSuggestion = oSug; 
                            oItem.sAddress = oSug.street + ' ' + oSug.apt + ' ' + oSug.city+ ' ' + oSug.state + ' '+ oSug.zip;
                            lSuggestion_Item.add(oItem);
                        }  
                        if( lSuggestion_Item.size() > 0 ) {
                            showModal = TRUE;
                            //if(is911Address){
                            showCreateCase = true;
                            haveSuggestion = true;
                            //}
                            
                        }
                    }
                    else {
                        //if( recordId.startsWith('02i')) {
                        
                        //if(is911Address){
                        showCreateCase = true;
                        showModal = true;
                        haveSuggestion = false;
                        //}
                        //}
                        //else{
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, responseResult.message));
                        //showModal = FALSE;
                        return ;
                        // }
                    }
                }
                else{
                    haveSuggestion = false;
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'The address provided was not found. Please verify the address entered and try again, or select override.'));
                }
            }
            
            else if( responseResult.result == 'true' && responseResult.code == '200' ) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Address was successfully validated'));
                showModal = FALSE;
                address.IsValidated__c = TRUE;
                isSave = TRUE;
            }
            
            else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Could not call address validation. Error message returned by validation service: '+responseResult));
                showModal = FALSE;
                return ;
            } 
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Could not call validation service'));
            showModal = FALSE;
            return ;
        }
    }
    
    
    public void pickAddress() {
        Integer iSuggestion_index = null ; 
        if( !String.isBlank(String.valueOf(ApexPages.currentPage().getParameters().get('iSuggestion_index')))) {
            iSuggestion_index = Integer.valueOf( ApexPages.currentPage().getParameters().get('iSuggestion_index') );
        }
        
        if(lSuggestion_Item!=null && iSuggestion_index != null) {
            address.City__c = lSuggestion_Item[iSuggestion_index].oSuggestion.City;
            address.State_Province__c = lSuggestion_Item[iSuggestion_index].oSuggestion.State;
            address.Street__c = lSuggestion_Item[iSuggestion_index].oSuggestion.Street;
            address.Zip_Postal_Code__c = lSuggestion_Item[iSuggestion_index].oSuggestion.Zip;
            
            /*if( lSuggestion_Item[iSuggestion_index].oSuggestion.Street2 != null && lSuggestion_Item[iSuggestion_index].oSuggestion.Street2 != '' ) {
address.Street__c+='\n'+lSuggestion_Item[iSuggestion_index].oSuggestion.Street2;
}*/
            
            address.IsValidated__c = TRUE;
            isSave = TRUE;
        }
        showModal = FALSE;
    }
    public PageReference overrideRequest(){
        
        Case c = new Case();
        c.OwnerId = Label.CC_Address_Validation_Override_Queue_ID;
        c.RecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Support').getRecordTypeId();
        c.Type = 'Address Validation Override';
        c.Description = address.Street__c+', '+address.City__c+', '+address.State_Province__c+', '+address.Zip_Postal_Code__c;
        c.Subject= 'Address Validation Request';
        if( recordId.startsWith('003')) {
            c.ContactId = recordId;
        }
        if( recordId.startsWith('02i')) {
            c.AssetId = recordId;
        }
        if( recordId.startsWith('001')) {
            c.AccountId = recordId;
        }
        insert c;
        //showModal = false;
        address.IsValidated__c = FALSE;
        //isSave = TRUE;
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Override Request Successfully Submitted.'));
        PageReference pg = null;
        try{
            if( !String.isBlank(recordId) ) {
                if( recordId.startsWith('003')) {
                    address.Contact__c = recordId;
                }
                if( recordId.startsWith('02i')) {
                    address.Asset__c = recordId;
                    if(is911Address){
                        Asset ass = new Asset();
                        ass.id = recordId;
                        ass.X911_City__c = address.City__c;
                        ass.X911_Country__c = address.Country__c;
                        ass.X911_Postal_Code__c = address.Zip_Postal_Code__c;
                        ass.X911_State__c = address.State_Province__c;
                        ass.X911_Street__c = address.Street__c;
                        update ass;
                    }
                }
                if( recordId.startsWith('001')) {
                    address.Account__c = recordId;
                }
            }
            
            if( address.Account__c == null ) {
                if( !String.isBlank(accountId) ) {
                    address.Account__c = accountId;
                }
            }
            
            upsert address;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Address Updated Successfully!'));
        }catch( DMLException ex ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            return null;
        }
        
        pg = new PageReference('/'+recordId);
        pg.setRedirect(true);
        return pg;
    }
    
    public void validateRequired() {
        address.IsValidated__c = FALSE;
        isSave = false;
    }
    
    
    public Boolean getIsClassic() {
        return (UserInfo.getUiThemeDisplayed() == 'Theme3' && ApexPages.currentPage().getParameters().get('beLightning') == null);
    }
    
    
    
    public class addressWrapper {
        
        public Addresses__c address {get;set;}
        public Boolean active {get;set;}
        
        
        public addressWrapper( Addresses__c address, Boolean active ) {
            this.address = address;
            this.active = active;
        }
    }  
}