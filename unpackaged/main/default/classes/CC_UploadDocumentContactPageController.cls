// C-00249602 1/11/19 NCarson - Form should not associate on upload 

public class CC_UploadDocumentContactPageController {

    public string sRendered_Upload {
        get {
            if(sRendered_Upload==null) sRendered_Upload='true';
            return sRendered_Upload;
        } set; 
    }

    public string sContactID {
        get {
            if(sContactID==null) {
                sContactID=ApexPages.currentPage().getParameters().get('cid');
            }
            return sContactID;
        }
        set;
    }
    public Contact objContact {
        get {
            if(objContact==null && sContactID!=null) {
                List<Contact> lCt=new List<Contact>([SELECT Id, Name, RecordType.Name, Medical_Release_Form__c FROM Contact WHERE Id=:sContactID]);
                if(!lCt.isEmpty()) { objContact=lCt[0];}
            }
            return objContact;
        } set; 
    }
    
    public Customer_Document__c objDoc {
        get {
            if(objDoc==null) objDoc=new Customer_Document__c(Contact__c=(objContact!=null)?(objContact.Id):(null));
            return objDoc;
        } set; 
    }
    
    public Transient Attachment objDocFile;  
    public Attachment getobjDocFile() {
            if(objDocFile==null) objDocFile=new Attachment();
            return objDocFile;
    } 
        
    public CC_UploadDocumentContactPageController() {
        if(objContact==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'System error: Cannot locate contact record with provided ID: '+sContactID));
            sRendered_Upload='false';
        }
    }

    public List<SelectOption> lType_option { 
        get {
            if(lType_option==null) { 
                lType_option = new List<SelectOption>{ 
                    new SelectOption('-- None --','-- None --')
                    , new SelectOption('Medical Release Form','Medical Release Form')
                    , new SelectOption('PCF Form','PCF Form') 
                    , new SelectOption('Installation Checklist','Installation Checklist')
                };
            }
            return lType_option;
        } set;}
    
    public PageReference SaveDoc() {
        if(objContact==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'You must select customer/potential customer to upload document!'));
            return null;
        }       
        if(objDocFile.body==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'You must select document to upload!'));
            return null;
        }
        if(objDoc.Document_Type__c==null || objDoc.Document_Type__c=='-- None --') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'You must select document type!'));
            return null;
        }
        Savepoint sp = Database.setSavepoint();
        try {
            //save customer document first
            objDoc.Original_File_Name__c=objDocFile.Name;
            objDoc.Document_Name__c=objDoc.Document_Type__c+'.'+objDocFile.Name.substringAfter('.');
            insert objDoc;
            objDocFile.Name=objDoc.Document_Type__c+'.'+objDocFile.Name.substringAfter('.');
            objDocFile.ParentId=objDoc.Id;
            objDocFile.IsPrivate = false;
            //insert objDocFile;
            
            
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = objDoc.Document_Type__c+'.'+objDocFile.Name.substringAfter('.'); // The files name, extension is very important here which will help the file in preview.
            conVer.Title = objDoc.Document_Type__c+'.'+objDocFile.Name.substringAfter('.'); // Display name of the files
            conVer.VersionData = EncodingUtil.base64Decode(EncodingUtil.base64Encode(objDocFile.Body)); // converting your binary string to Blog
            
            insert conVer;
            
            system.debug('conVer'+conVer);
            // link the object with file
            // First get the content document Id from ContentVersion
            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
            
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDoc;
            cDe.LinkedEntityId = objDoc.Id; // you can use objectId,GroupId etc
            cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            
            insert cDe;
            /* C-00249602 1/11/19 NCarson - Form should not associate on upload 
             if(objDoc.Document_Type__c=='PCF Form' ) {
                objContact.PCF_Form__c=objDoc.Id; 
                update objContact;
            }         */   
        } catch(System.DMLException e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }     
        return new PageReference('/'+((objDoc.Id!=null)?(objDoc.Id):(sContactID)));
    }
    public PageReference Cancel() {
        return new PageReference('/'+sContactID);
    }        

}