public with sharing class CC_CustomCampaignLookupSalesPortal {
    
    public Campaign newCampaign {get;set;} // new account to create
    public List<Campaign> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    public string searchChannel{get;set;}
    public string searchSubChannel{get;set;}
    public string searchSalesRep{get;set;}
    public CC_CustomCampaignLookupSalesPortal() {
        newCampaign = new Campaign();
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        searchChannel = System.currentPageReference().getParameters().get('channel');
        searchSubChannel = System.currentPageReference().getParameters().get('subchannel');
        String searchNAme = System.currentPageReference().getParameters().get('salesRep');
        if(searchName != '' && searchName != null){
            searchSalesRep = [SELECT Id FROM User WHERE Name =: searchName][0].id;
        }
        runSearch();  
    }
    
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        
        results = performSearch(searchString,searchChannel,searchSubChannel,searchSalesRep);               
    } 
    
    // run the search and return the records found. 
    private List<Campaign> performSearch(string searchString,string searchChannel,string searchSubChannel,String searchSalesRep) {
        
        String soql = 'select id, name, Channel__c,Sub_Channel__c from campaign';
        if((searchString != '' && searchString != null) ||(searchChannel != '' && searchChannel != null && searchChannel.length()>3)|| (searchSubChannel != '' && searchSubChannel != null && searchSubChannel.length()>3) || (searchSalesRep != '' && searchSalesRep != null && searchSalesRep.length()>3))
            soql = soql +  ' WHERE';
        if(searchString != '' && searchString != null)
            soql = soql +  ' name LIKE \'%' + searchString +'%\' ';
        if(searchChannel != '' && searchChannel != null && searchChannel.length()>3){
            //system.assert(false,searchChannel);
            if(searchString != '' && searchString != null){
                soql = soql +  ' AND Channel__c= \''+ searchChannel + '\'';
            }
            else{
                soql = soql +  ' Channel__c= \''+ searchChannel + '\'';
            }
        }
        if(searchSubChannel != '' && searchSubChannel != null && searchSubChannel.length()>3){
            
            
            if((searchString != '' && searchString != null) || (searchChannel != '' && searchChannel != null && searchChannel.length()>3)){
                soql = soql +  ' AND Sub_Channel__c= \''+ searchSubChannel + '\'';
            }
            else{
                soql = soql +  ' Sub_Channel__c= \''+ searchSubChannel + '\'';
            }
            
        }
        if(searchSalesRep != '' && searchSalesRep != null && searchSalesRep.length()>3){
            if((searchString != '' && searchString != null) || (searchChannel != '' && searchChannel != null && searchChannel.length()>3) || (searchSubChannel != '' && searchSubChannel != null && searchSubChannel.length()>3)){
                soql = soql +  ' AND Event_POC__c= \''+ searchSalesRep + '\'';
            }
            else{
                soql = soql +  ' Event_POC__c= \''+ searchSalesRep + '\'';
            }
            
        }
        soql = soql + ' limit 25';
        
        //System.assert(false,soql);
        //system.assert(false,searchSubChannel+soql);
        return database.query(soql); 
        
    }
    public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
	public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
}