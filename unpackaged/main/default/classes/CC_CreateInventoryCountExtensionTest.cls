/*
 * MODIFIED MKLICH 05/10/2019 - S-618699 - Updated test classes due to new restrictions on Products and Serial Number Lengths
 */ 

@isTest
private class CC_CreateInventoryCountExtensionTest {
	@isTest
    static void validateCreateInventoryPage(){
        // MKLICH Start S-618699
        List<Product2> productList = new List<Product2>();
        productList.add(new product2(isActive = true, Name = 'Thor B Ensemble'));
        productList.add(new product2(isActive = true, Name = 'CC Blue Phone'));
        insert productList;
        // MKLICH Stop S-618699
        
        List<Inventory_count__c> icList = CC_TestUtility.createInventoryCount(1, true);
        List<Inventory_Count_Line_Item__c> icliList = CC_TestUtility.createInventoryCountLineItem(2, icList[0].id, productList[0].Id, false); //MKLICH Update to false
        // MKLICH Start S-618699
        for (Integer i = 0; i < icliList.size(); i++) {
            icliList[i].Serial_Number__c = '123456789101234' + String.valueOf(i); // Thor B Ensemble is product so needs 16 digit length
        }
        insert icliList; 
        // MKLICH STOP S-618699
        
        //System.assert(false,icliList[0].createdDate);
       Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    	String userName = String.valueOf(Datetime.Now().getTime());

    	User u = new User(
	    	FirstName         = 'Test',
		    LastName          = 'User',
		    Email             = 'test@user.com',
		    Alias             = 'tuser',
		    Username          = userName + 'test@user.com',
		    LocaleSidKey      = 'en_US',
		    TimeZoneSidKey    = 'GMT',
		    LanguageLocaleKey = 'en_US',
		    EmailEncodingKey  = 'UTF-8',
		    ProfileId		  = p.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street        	  = '809 HEATHERWOOD DR',
            country			  = 'USA',
            postalCode		  = '57005'
		);
		insert u;
        PageReference pageRef = Page.CC_CreateInventoryCount;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('rowIndex', '0');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(icliList[0]);
        CC_CreateInventoryCountExtension cicE = new CC_CreateInventoryCountExtension(sc);
        //cicE.addRow();
        system.runAs(u){
        cicE.save();
        cicE.Submit();
        
        System.assertEquals('Submitted', [SELECT Status__c FROM Inventory_count__c WHERE ID = : icList[0].id].Status__C);
        cicE.deleteRow();
        }
    }
    @isTest
    static void validateWithCreateNewInventoryLineItem(){
        // MKLICH S-618699 START
        List<Product2> productList = new List<Product2>();
        productList.add(new product2(isActive = true, Name = 'Thor B Ensemble'));
        productList.add(new product2(isActive = true, Name = 'CC Blue Phone'));
        insert productList;
        // MKLICH S-618699 START
        List<Inventory_count__c> icList = CC_TestUtility.createInventoryCount(1, true);
        List<Inventory_Count_Line_Item__c> icliList = CC_TestUtility.createInventoryCountLineItem(2, icList[0].id, productList[0].Id, false);
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    	String userName = String.valueOf(Datetime.Now().getTime());

    	User u = new User(
	    	FirstName         = 'Test',
		    LastName          = 'User',
		    Email             = 'test@user.com',
		    Alias             = 'tuser',
		    Username          = userName + 'test@user.com',
		    LocaleSidKey      = 'en_US',
		    TimeZoneSidKey    = 'GMT',
		    LanguageLocaleKey = 'en_US',
		    EmailEncodingKey  = 'UTF-8',
		    ProfileId		  = p.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street        	  = '809 HEATHERWOOD DR',
            country			  = 'USA',
            postalCode		  = '57005'
		);
		insert u;
        PageReference pageRef = Page.CC_CreateInventoryCount;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('rowIndex', '0');
        ApexPages.StandardController sc = new ApexPages.StandardController(icliList[0]);
        CC_CreateInventoryCountExtension cicE = new CC_CreateInventoryCountExtension(sc);
        cice.ICList = icliList;
        system.runAs(u){
        cice.save();
        cicE.Submit();
        //System.assertEquals(DateTime.now(), [SELECT Last_True_Up_Date__c FROM User WHERE id = :UserInfo.getUserId()].Last_True_Up_Date__c);
        cice.deleteRow(); 
        }
    }
    @isTest
    static void testAddRow(){
        // MKLICH Start S-618699
        List<Product2> productList = new List<Product2>();
        productList.add(new product2(isActive = true, Name = 'Thor B Ensemble'));
        productList.add(new product2(isActive = true, Name = 'CC Blue Phone'));
        insert productList;
        // MKLICH Stop S-618699
        
        List<Inventory_count__c> icList = CC_TestUtility.createInventoryCount(1, true);
        List<Inventory_Count_Line_Item__c> icliList = CC_TestUtility.createInventoryCountLineItem(2, icList[0].id, productList[0].Id, true);
        //System.assert(false,icliList[0].createdDate);
       
        PageReference pageRef = Page.CC_CreateInventoryCount;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('rowIndex', '0');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(icliList[0]);
        CC_CreateInventoryCountExtension cicE = new CC_CreateInventoryCountExtension(sc);
        cicE.addRow();
    }
    
    // MKLICH S-618699 Start
    @isTest
    static void checkThorBEnsembleInsertion(){
        Product2 prod = new Product2(isActive = True, Name = 'Thor B Ensemble'); // Test new Serial Number length on Inventory Count
        insert prod;
        
        List<Inventory_count__c> icList = CC_TestUtility.createInventoryCount(1, true);
        List<Inventory_Count_Line_Item__c> icliList = CC_TestUtility.createInventoryCountLineItem(6, icList[0].id, prod.Id, false);
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    	String userName = String.valueOf(Datetime.Now().getTime());

    	User u = new User(
	    	FirstName         = 'Test',
		    LastName          = 'User',
		    Email             = 'test@user.com',
		    Alias             = 'tuser',
		    Username          = userName + 'test@user.com',
		    LocaleSidKey      = 'en_US',
		    TimeZoneSidKey    = 'GMT',
		    LanguageLocaleKey = 'en_US',
		    EmailEncodingKey  = 'UTF-8',
		    ProfileId		  = p.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street        	  = '809 HEATHERWOOD DR',
            country			  = 'USA',
            postalCode		  = '57005'
		);
		insert u;
        PageReference pageRef = Page.CC_CreateInventoryCount;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('rowIndex', '0');
        ApexPages.StandardController sc = new ApexPages.StandardController(icliList[0]);
        CC_CreateInventoryCountExtension cicE = new CC_CreateInventoryCountExtension(sc);
        cice.ICList = icliList;
        system.runAs(u){
        cice.save();
        cicE.Submit();
        List<Inventory_Count_Line_Item__c> afterInserted = [SELECT ID FROM Inventory_Count_Line_Item__c];
            // Validation that no Inventory_Count_Line_Item__c were inserted due to the Serial number for the Thor B ensemble product not matching length
        System.assertEquals(afterInserted.size(), 0);
        cice.deleteRow(); 
        }
    }
    
        @isTest
    static void checkDuplicateSerial(){
        List<Product2> productList = new List<Product2>();
        productList.add(new product2(isActive = true, Name = 'Thor B Ensemble'));
        productList.add(new product2(isActive = true, Name = 'CC Blue Phone'));
        insert productList;
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        String userName = String.valueOf(Datetime.Now().getTime());
    	User u = new User(
	    	FirstName         = 'Test',
		    LastName          = 'User',
		    Email             = 'test@user.com',
		    Alias             = 'tuser',
		    Username          = userName + 'test@user.com',
		    LocaleSidKey      = 'en_US',
		    TimeZoneSidKey    = 'GMT',
		    LanguageLocaleKey = 'en_US',
		    EmailEncodingKey  = 'UTF-8',
		    ProfileId		  = p.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street        	  = '809 HEATHERWOOD DR',
            country			  = 'USA',
            postalCode		  = '57005'
		);
        
		insert u;
        system.runAs(u){
            
            List<Inventory_count__c> icList = CC_TestUtility.createInventoryCount(1, true);
            List<Inventory_Count_Line_Item__c> icliList = CC_TestUtility.createInventoryCountLineItem(2, icList[0].id, productList[0].Id, false); //MKLICH Update to false
            for (Integer i = 0; i < icliList.size(); i++) {
                icliList[i].Serial_Number__c = '123456789101'; // Thor B Ensemble is product so needs 16 digit length but putting as the same
            }
            insert icliList; 
            System.debug(icliList);
           
            PageReference pageRef = Page.CC_CreateInventoryCount;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('rowIndex', '0');
            ApexPages.StandardController sc = new ApexPages.StandardController(icliList[0]);
            CC_CreateInventoryCountExtension cicE = new CC_CreateInventoryCountExtension(sc);
            cice.ICList = icliList;
            
            //cice.save();
            cicE.Submit();
            List<Inventory_Count_Line_Item__c> afterInserted = [SELECT ID, status__c, serial_Number__c FROM Inventory_Count_Line_Item__c where CreatedById = :u.Id and Status__c = 'Submitted'];
            system.debug(afterInserted);
            System.assertEquals(afterInserted.size(), 0); // Size should be 0 due to duplicate serial numbers
            cice.deleteRow(); 
        }
    }
    // MKLICH S-618699 Stop

}