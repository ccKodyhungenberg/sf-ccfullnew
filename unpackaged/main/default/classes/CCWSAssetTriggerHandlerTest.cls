@isTest
public class CCWSAssetTriggerHandlerTest {

    static testMethod void testDevice() {
        Test.startTest();
        List<Account> accList = CC_TestUtility.createAccount(1, true);
           List<Contact> conList = CC_TestUtility.createContact(1, true, 'Customer',accList[0].id , 'yo@test.com');
        Asset dev=new Asset(
            Name = 'testDeviceID3124'
            ,DateCreated__c=datetime.now()
            ,Deleted__c= false
            ,contactId = conList[0].id
            ,Identifier__c= 'testDeviceID3124'
            ,Last_Check_in__c= datetime.now()
            ,LastUpdated__c= datetime.now()
            ,PartnerCode__c= '1'
            ,POMStatus__c= true
            ,Status = 'Active'
        );
        insert dev;

        CCWS_Asset__c oDevice=new CCWS_Asset__c(
            Name='testDeviceID3124'
            ,deviceID__c='testDeviceID3124'
            ,lastUpdated__c=datetime.now().addDays(1)
            ,createdOn__c=datetime.now()
            ,countryCode__c='1'
            ,active__c='true'
            ,lastCheckIn__c=datetime.now()
            ,number__c='9177779900'
            ,partnerCode__c='1'
            ,POMStatus__c='true'
            ,deviceType__c='28'
            );
        upsert oDevice deviceID__c;
        
        oDevice.lastUpdated__c=datetime.now().addDays(2);
        update oDevice;
        
        Asset devUpdated = [SELECT LastUpdated__c FROM Asset WHERE Name = 'testDeviceID3124'];
        System.assertEquals(oDevice.lastUpdated__c, devUpdated.LastUpdated__c);
        
        Test.stopTest();
    }
    
    @isTest
    static void testGetCountry() {
        System.assert(CCWSAssetTriggerHandler.getCountry('1') == 'US');
        System.assert(CCWSAssetTriggerHandler.getCountry('2') == 'Canada');
        System.assert(CCWSAssetTriggerHandler.getCountry('3') == 'Other');
        System.assert(CCWSAssetTriggerHandler.getCountry(null) == null);
    }
    
    @isTest
    static void testGetPartnerCode() {
        System.assert(CCWSAssetTriggerHandler.getPartnerCode('1') == 'Clear Captions');
        System.assert(CCWSAssetTriggerHandler.getPartnerCode('2') == 'Clarity');
        System.assert(CCWSAssetTriggerHandler.getPartnerCode('3') == 'Frontier');
        System.assert(CCWSAssetTriggerHandler.getPartnerCode('4') == 'Other');
        System.assert(CCWSAssetTriggerHandler.getPartnerCode(null) == null);
    }
}