@isTest
private class CC_PcfApprovalSfaxBatchTest {

  @isTest static void test_Outbound() {
    CC_TestPcfDataFactory.createSettings();
    Contact customer = CC_TestPcfDataFactory.createCustomer(true, true);
    Case approval = CC_TestPcfDataFactory.createApproval(customer);
    approval.Sfax_Queue_Id__c = '1234567890';
    update approval;

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"OutboundFaxItems":[{"SendFaxQueueId":1234567890,"OutboundFaxId":1234567890,"ResultMessage":"Yup", "isSuccess": true}]}'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
      b.execute(null);

    Test.stopTest();

    approval = [SELECT Id, Status__c, Sfax_Queue_Id__c, Fax_Status__c, Fax_Success__c FROM Case WHERE Sfax_Queue_Id__c = '1234567890'];

    System.assertEquals('Yup', approval.Fax_Status__c);
  }

  @isTest static void test_Outbound_Dupe_Success_Fail() {
    CC_TestPcfDataFactory.createSettings();
    Contact customer = CC_TestPcfDataFactory.createCustomer(true, true);
    Case approval = CC_TestPcfDataFactory.createApproval(customer);
    approval.Sfax_Queue_Id__c = '1234567890';
    update approval;

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"OutboundFaxItems":[{"SendFaxQueueId":1234567890,"OutboundFaxId":1234567890,"ResultMessage":"Yup", "isSuccess": true},{"SendFaxQueueId":1234567890,"OutboundFaxId":1234567890,"ResultMessage":"Nope", "isSuccess": false}]}'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
      b.execute(null);

    Test.stopTest();

    approval = [SELECT Id, Status__c, Sfax_Queue_Id__c, Fax_Status__c, Fax_Success__c FROM Case WHERE Sfax_Queue_Id__c = '1234567890'];

    System.assertEquals('Yup', approval.Fax_Status__c);
  }

  @isTest static void test_Outbound_Dupe_Fail_Success() {
    CC_TestPcfDataFactory.createSettings();
    Contact customer = CC_TestPcfDataFactory.createCustomer(true, true);
    Case approval = CC_TestPcfDataFactory.createApproval(customer);
    approval.Sfax_Queue_Id__c = '1234567890';
    update approval;

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"OutboundFaxItems":[{"SendFaxQueueId":1234567890,"OutboundFaxId":1234567890,"ResultMessage":"Nope", "isSuccess": false},{"SendFaxQueueId":1234567890,"OutboundFaxId":1234567890,"ResultMessage":"Yup", "isSuccess": true}]}'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
      b.execute(null);

    Test.stopTest();

    approval = [SELECT Id, Status__c, Sfax_Queue_Id__c, Fax_Status__c, Fax_Success__c FROM CAse WHERE Sfax_Queue_Id__c = '1234567890'];

    System.assertEquals('Yup', approval.Fax_Status__c);
  }

  @isTest static void test_Unmatched() {
    CC_TestPcfDataFactory.createSettings();

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"InboundFaxItems":[{"FaxId":1234567890,"Barcodes":{"BarcodeItems":[]}}]}'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
      b.execute(null);

    Test.stopTest();

    Case approval = [SELECT Id, Status__c, Sfax_Incoming_Id__c, Last_Error_Message__c FROM Case WHERE Sfax_Incoming_Id__c = '1234567890'];

    if (approval.Status__c != PcfApprovalConstants.UNASSIGNED) {
      System.assertEquals(PcfApprovalConstants.INCOMING, approval.Status__c);

      // we get this error when we try to download the fax, so we might as well test the exception handling :)
      System.assert(approval.Last_Error_Message__c.contains('You have uncommitted work pending'));
    }
  }

  @isTest static void test_Unmatched_downloadFaxes() {
    CC_testPcfDataFactory.createSettings();
    Contact customer = CC_testPcfDataFactory.createCustomer(true, true);
    Case approval = CC_testPcfDataFactory.createApproval(customer);
    approval.Sfax_Incoming_Id__c = '1234567890';
    approval.Status__c = PcfApprovalConstants.INCOMING;
    update approval;

    Test.setMock(HttpCalloutMock.class, new cc_PcfCalloutMock('Incoming Fax'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch.downloadFaxes(new List<String>{ approval.Id }, new List<String>{ approval.Status__c }, new List<String>{ approval.Sfax_Incoming_Id__c });

    Test.stopTest();

    approval = [SELECT Id, Status__c FROM Case WHERE Id = :approval.Id];
    System.assertEquals(PcfApprovalConstants.UNASSIGNED, approval.Status__c);
  }

  @isTest static void test_BarcodeId() {
    CC_testPcfDataFactory.createSettings();
    Contact customer = CC_testPcfDataFactory.createCustomer(true, true);
    Case approval = CC_testPcfDataFactory.createApproval(customer);

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"InboundFaxItems":[{"FaxId":1234567890,"Barcodes":{"BarcodeItems":[{"BarcodeData":"' + approval.Id + '"}]}}]}'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
      b.execute(null);

    Test.stopTest();

    // TODO: assertions
    // approval = [SELECT Id, Status__c FROM PCF_Approval__c WHERE Id = :approval.Id];
    // System.assertEquals(PcfApprovalConstants.REVIEW, approval.Status__c);
  }

  @isTest static void test_BarcodeId_deleted() {
    CC_testPcfDataFactory.createSettings();
    Contact customer = CC_testPcfDataFactory.createCustomer(true, true);
    Case approval = CC_testPcfDataFactory.createApproval(customer);
    approval.Status__c = PcfApprovalConstants.FAXED;
    update approval;

    delete approval;

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"InboundFaxItems":[{"FaxId":1234567890,"Barcodes":{"BarcodeItems":[{"BarcodeData":"' + approval.Id + '"}]}}]}'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
      b.execute(null);

    Test.stopTest();

    // TODY: assertions
    // approval = [SELECT Id, Status__c FROM PCF_Approval__c WHERE Sfax_Incoming_Id__c = '1234567890'];
    // System.assertEquals(PcfApprovalConstants.REVIEW, approval.Status__c);
  }
@isTest
  public static void test_BarcodeId_downloadFaxes() {
    CC_testPcfDataFactory.createSettings();
    Contact customer = CC_testPcfDataFactory.createCustomer(true, true);
    Case approval = CC_testPcfDataFactory.createApproval(customer);
    approval.Sfax_Incoming_Id__c = '1234567890';
    approval.Status__c = PcfApprovalConstants.REVIEW;
    update approval;

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('Incoming Fax'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch.downloadFaxes(new List<String>{ approval.Id }, new List<String>{ approval.Status__c }, new List<String>{ approval.Sfax_Incoming_Id__c });

    Test.stopTest();

    approval = [SELECT Id, Status__c FROM Case WHERE Id = :approval.Id];
    System.assertEquals(PcfApprovalConstants.REVIEW, approval.Status__c);
  }


  @isTest static void test_Incoming_downloadFaxes() {
    CC_testPcfDataFactory.createSettings();
    Contact customer = CC_testPcfDataFactory.createCustomer(true, true);
    Case approval = CC_testPcfDataFactory.createApproval(customer);
    approval.Sfax_Incoming_Id__c = '1234567890';
    approval.Status__c = PcfApprovalConstants.INCOMING;
    update approval;

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"InboundFaxItems":[]}'));

    Test.startTest();

      CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
      b.execute(null);

    Test.stopTest();
  }

  @isTest static void test_scheduleable() {
    CC_testPcfDataFactory.createSettings();
    Contact customer = CC_testPcfDataFactory.createCustomer(true, true);
    Case approval = CC_testPcfDataFactory.createApproval(customer);
    approval.Sfax_Incoming_Id__c = '1234567890';
    approval.Status__c = PcfApprovalConstants.REVIEW;
    update approval;

    Test.setMock(HttpCalloutMock.class, new CC_PcfCalloutMock('{"InboundFaxItems":[{"FaxId":1234567890,"Barcodes":{"BarcodeItems":[]}}]}'));

    Test.startTest();
      CC_PcfApprovalSfaxSchedulable b = new CC_PcfApprovalSfaxSchedulable();
      b.execute(null);
    Test.stopTest();
      SFaxAPIService.SendFax('test name', '0000000000', new Map<String,blob>(), null, null);
  }
}