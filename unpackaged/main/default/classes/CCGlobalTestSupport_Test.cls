@isTest
public class CCGlobalTestSupport_Test {

    static testMethod void testCCGlobalTestSupport() {
        Test.startTest();
        List<Contact> lCt=CCGlobalTestSupport.CreateCustomer(2);
        system.assert(lCt!=null, 'CCGlobalTestSupport.CreateCustomer(2) could not create test customer contacts');
        List<Opportunity> lOp=CCGlobalTestSupport.CreateOpportunity(2);
        system.assert(lOp!=null, 'CCGlobalTestSupport.CreateOpportunity(2) could not create test opportunities');
        Test.stopTest();
    }
}