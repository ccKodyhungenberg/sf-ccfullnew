global class CCWSAssetTriggerHandler {
    
    /*public static map<string, string> m_deviceTypeId_Type=new map<string, string> {
         '1' => 'Skype'
        ,'2' => 'iOS'
        ,'3' => 'Android'
        ,'4' => 'BlackBerry'
        ,'10' => 'iPhone Phone VoIP'
        ,'11' => 'iPhone Tablet VoIP'
        ,'20' =>    'Cisco'
        ,'26' =>    'ezLink'
        ,'27' =>    'Clarity'
        ,'28' =>    'Clarity2'
        ,'30' => 'Android Phone VoIP'
        ,'31' => 'Android Tablet VoIP'
        ,'40' =>    'Flash'
    }; */
    public static map<string, string> m_deviceTypeId_Type=new map<string, string>();
    static {
        for(DeviceType_Code_Mapping__mdt dc: [Select Device_Code__c, Device_Type__c from DeviceType_Code_Mapping__mdt]){
            m_deviceTypeId_Type.put(dc.Device_Code__c, dc.Device_Type__c);
        }
    }
    
    //CountryCode=CASE WHEN CountryCode=1 THEN 'US' WHEN CountryCode=2 THEN 'Canada' WHEN CountryCode IS NULL THEN NULL ELSE 'Other' END
    public static string getCountry(string sCountryCode) {
        if(sCountryCode=='1') return 'US';
        else if(sCountryCode=='2') return 'Canada';
        else if(sCountryCode==null) return null;
        else return 'Other';
    }

    //PartnerCode=CASE WHEN PartnerCode=1 THEN 'Clear Captions' WHEN PartnerCode=2 THEN 'Clarity' WHEN PartnerCode=3 THEN 'Frontier' WHEN PartnerCode IS NULL THEN NULL ELSE 'Other' END
    public static string getPartnerCode(string sPartnerCode) {
        if(sPartnerCode=='1') return 'Clear Captions';
        else if(sPartnerCode=='2') return 'Clarity';
        else if(sPartnerCode=='3') return 'Frontier';
        else if(sPartnerCode==null) return null;
        else return 'Other';
    }

    public static void tr_After_Insert(List<CCWS_Asset__c> triggerNew, List<CCWS_Asset__c> triggerOld, Map<Id,CCWS_Asset__c> triggerNewMap, Map<Id,CCWS_Asset__c> triggerOldMap) {
        Map<string, CCWS_Asset__c> m_SN_CCWSDevice=new Map<string, CCWS_Asset__c>();
        Map<string, List<Asset>> m_SN_DeviceList=new Map<string, List<Asset>>();
        List<Asset>lDevice2Update=new List<Asset>();
        for(CCWS_Asset__c dev_new: triggerNew) { 
            if(!m_SN_CCWSDevice.containsKey(dev_new.deviceID__c)) m_SN_CCWSDevice.put(dev_new.deviceID__c, dev_new);
            else {
                if(m_SN_CCWSDevice.get(dev_new.deviceID__c).lastUpdated__c < dev_new.lastUpdated__c) m_SN_CCWSDevice.put(dev_new.deviceID__c, dev_new); //only use most recent device changes.
            }
        }
        //find existing devices that were not deleted
        for(Asset dev_existing: [select id, Identifier__c, Last_Check_in__c, LastUpdated__c, PartnerCode__c, POMStatus__c,Deleted__c from Asset where Identifier__c in :m_SN_CCWSDevice.keyset() and Deleted__c=false]) {
            if(!m_SN_DeviceList.containsKey(dev_existing.Identifier__c)) m_SN_DeviceList.put(dev_existing.Identifier__c, new List<Asset>());
            m_SN_DeviceList.get(dev_existing.Identifier__c).add(dev_existing); 
        }
        //build list of devices to update
        for(string sSN: m_SN_CCWSDevice.keySet()) {
            if(m_SN_DeviceList.containsKey(sSN)) {
                for(Asset dev: m_SN_DeviceList.get(sSN)) {
                    if(dev.Last_Check_in__c < m_SN_CCWSDevice.get(sSN).lastCheckIn__c || dev.LastUpdated__c<m_SN_CCWSDevice.get(sSN).lastUpdated__c) { 
                        dev.Last_Check_in__c=m_SN_CCWSDevice.get(sSN).lastCheckIn__c;
                        dev.LastUpdated__c=m_SN_CCWSDevice.get(sSN).lastUpdated__c;
                        dev.PartnerCode__c=CCWSAssetTriggerHandler.getPartnerCode(m_SN_CCWSDevice.get(sSN).partnerCode__c);
                        dev.CountryCode__c=CCWSAssetTriggerHandler.getCountry(m_SN_CCWSDevice.get(sSN).countryCode__c);
                        lDevice2Update.add(dev);
                    }
                }
            }
        }
        if(!lDevice2Update.isEmpty()) update lDevice2Update;
    }

    public static void tr_After_Update(List<CCWS_Asset__c> triggerNew, List<CCWS_Asset__c> triggerOld, Map<Id,CCWS_Asset__c> triggerNewMap, Map<Id,CCWS_Asset__c> triggerOldMap) {
        Map<string, CCWS_Asset__c> m_SN_CCWSDevice=new Map<string, CCWS_Asset__c>();
        Map<string, List<Asset>> m_SN_DeviceList=new Map<string, List<Asset>>();
        List<Asset>lDevice2Update=new List<Asset>();

        for(CCWS_Asset__c dev_new: triggerNew) { 
            if(!m_SN_CCWSDevice.containsKey(dev_new.deviceID__c)) m_SN_CCWSDevice.put(dev_new.deviceID__c, dev_new);
            else {
                if(m_SN_CCWSDevice.get(dev_new.deviceID__c).lastUpdated__c < dev_new.lastUpdated__c) m_SN_CCWSDevice.put(dev_new.deviceID__c, dev_new); //only use most recent device changes.
            }
        }
        set<string> st_SN2Create=new set<string>(m_SN_CCWSDevice.keySet());
        //find existing devices that were not deleted and find devices that were not created
        for(Asset dev_existing: [select id, Identifier__c, Last_Check_in__c, LastUpdated__c, PartnerCode__c, POMStatus__c,Deleted__c from Asset where Identifier__c in :m_SN_CCWSDevice.keyset()]) {
            if( dev_existing.Deleted__c==false) {
                if(!m_SN_DeviceList.containsKey(dev_existing.Identifier__c)) m_SN_DeviceList.put(dev_existing.Identifier__c, new List<Asset>());
                m_SN_DeviceList.get(dev_existing.Identifier__c).add(dev_existing);
            }
            st_SN2Create.remove(dev_existing.Identifier__c); //there is a device with given SN - no need to create
        }
        //build list of devices to update
        for(string sSN: m_SN_CCWSDevice.keySet()) {
            if(m_SN_DeviceList.containsKey(sSN)) {
                for(Asset dev: m_SN_DeviceList.get(sSN)) {
                        if(dev.Last_Check_in__c < m_SN_CCWSDevice.get(sSN).lastCheckIn__c || dev.LastUpdated__c<m_SN_CCWSDevice.get(sSN).lastUpdated__c) { 
                            dev.Last_Check_in__c=m_SN_CCWSDevice.get(sSN).lastCheckIn__c;
                            dev.LastUpdated__c=m_SN_CCWSDevice.get(sSN).lastUpdated__c;
                            dev.PartnerCode__c=CCWSAssetTriggerHandler.getPartnerCode(m_SN_CCWSDevice.get(sSN).partnerCode__c);
                            dev.CountryCode__c=CCWSAssetTriggerHandler.getCountry(m_SN_CCWSDevice.get(sSN).countryCode__c);
                            lDevice2Update.add(dev);
                        }
                }
            }
        }
        if(!lDevice2Update.isEmpty()) update lDevice2Update; 
        if(!st_SN2Create.isEmpty()) { //touch history records of the devices that did not get created
            List<CCWS_Asset_History__c>lDH2Update=new List<CCWS_Asset_History__c>();
            for(CCWS_Asset_History__c oDH: [select id from CCWS_Asset_History__c WHERE deviceID__c in :st_SN2Create]) lDH2Update.add(oDH);
            if(!lDH2Update.isEmpty()) update lDH2Update;
        }
    }     
}