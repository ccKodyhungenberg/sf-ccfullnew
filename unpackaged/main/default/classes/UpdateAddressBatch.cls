public class UpdateAddressBatch implements Database.Batchable<sObject>,Schedulable, Database.AllowsCallouts {
    
    
    public void execute( SchedulableContext sc ) {
      UpdateAddressBatch uab = new UpdateAddressBatch();
      Database.executeBatch(uab,10);
    }
    
    public Database.QueryLocator start( Database.BatchableContext bc ) {
      return Database.getQueryLocator('SELECT MailingCity,MailingState,MailingStreet,MailingCountry, MailingPostalCode, CB_userID__c FROM Contact WHERE validForAPI__c = TRUE');
      
    }
    public void execute(Database.BatchableContext bc, List<Contact> scope) {
      for( Contact con : scope ) {
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://ccws.test.clearcaptions.com/user/update');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        // Set the body as a JSON object
        String addressBodyString = '"addresses": [{"city":"'+ con.MailingCity+'","state":"'+con.MailingState+'","street":"'+con.MailingStreet+'","country":"'+con.MailingCountry+'","zip":"'+con.MailingPostalCode+'"}]';
        request.setBody('data={"id": "'+con.CB_userID__c+'","userID": "'+con.CB_userID__c+'",'+addressBodyString+'}');
        
        //if( !Test.isRunningTest() ) {
          HttpResponse response = http.send(request);
        
          // Parse the JSON response
          if (response.getStatusCode() != 200) {
              con.addError('The status code returned was: ' +
                           response.getStatusCode() + ' ' + response.getStatus());
          } 
          else {
            con.validForAPI__c = False;
          }
        //}
      }
      
      update scope;
    }
    
    public void finish( Database.BatchableContext bc ) {
      
    }
    
}