public with sharing  class CalendarAnythingVFUpdatedController{
    public String sUserID{
        get{
            if (sUserID == null && sContactID != '') {
                 Contact[] aU = [SELECT related_to_user__c FROM contact WHERE id=:sContactID LIMIT 1];
                 if (aU.size() > 0) {
                    sUserID = '' + aU[0].related_to_user__c;     
                 }       
            } else {
                sUserID = '';
            }
            return sUserID;
        }
        set;
    }
    
    public String sContactID{
        get{
            if (sContactID == null) {
                if (ApexPages.CurrentPage().getParameters().containsKey('pv1')) {
                    sContactID = ApexPages.CurrentPage().getParameters().get('pv1');
                } else {
                    sContactID = '';
                }
            }
            return sContactID;
        }
        set;
    }
}