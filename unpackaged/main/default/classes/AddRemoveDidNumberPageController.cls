public class AddRemoveDidNumberPageController {
    public Boolean isRemoveDisabled{get;set;}
    public Boolean isAddDisabled{get;set;}
    public String zipCode{get;set;}
    public String ccVoiceNumber{get;set;}
    public Contact con{get;set;}
    public AddRemoveDidNumberPageController(){
        isRemoveDisabled = true;
        isAddDisabled = false;
        String conId = Apexpages.currentPage().getParameters().get('Id');
        //String didNumber = Apexpages.currentPage().getParameters().get('DID');
        con = [SELECT Id,CC_Voice_Number__c,CB_UserId__c,MailingPostalCode FROM Contact WHERE Id =: conId];
        zipCode = con.MailingPostalCode;
        ccVoiceNumber = con.CC_Voice_Number__c;
        
        if(String.isBlank(zipCode)){
            //system.assert(false,zipCode);
            if(ccVoiceNumber != null){
                isRemoveDisabled = false;
            }
            else{
                isRemoveDisabled = true;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Zip Code not available!'));
            //isRemoveDisabled = true;
            
        }
        else if(con.CC_Voice_Number__c != null){
            isRemoveDisabled = false;
            isAddDisabled = true;
        }
    }
    public PageReference addNumber(){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        String sURL=Label.CC_CCWS_ENDPOINT+'/number/getNumbersByZip';
        req.setEndpoint(sURL);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        String requestBody = 'data={"limit":'+Label.CC_DID_FETCH_LIMIT+',"zip":"'+zipCode+'"}';
        system.debug('getnumberbyzipRequest---' + requestBody );
        req.setBody(requestBody);
        String result;
        // if( !Test.isRunningTest() ) {
        HttpResponse res = h.send(req);
        string call_result=res.getBody();
        String cc_voiceNumber;
        String message;
        Set<String> voiceMailSet = new Set<String>();
        String resultCCws;
        String resultCCws2 = null; // Start - Ravi Jain - 00268312 - 05th September, 2019 - Intialize the variable resultCCws2
        if (res.getStatusCode() != 200) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,res.getBody() + ' '+
                                                       res.getStatusCode() + ' ' + res.getStatus()));
            //return null;
        } 
        else{
            system.debug('false,'+res.getBody());
            system.debug('getnumberbyzipResponse---' +res.getBody());
            String addressResponse=res.getBody();
            JSONParser parser = JSON.createParser(res.getBody());
            while (parser.nextToken() != null) {
                if(parser.getText() == 'data'){
                    parser.nextToken();
                    parser.nextToken();
                    while(parser.getText() != 'message' && parser.getText() != ']'){
                        voiceMailSet.add(parser.getText());
                        parser.nextToken();
                    }
                }
                if(parser.getText() == 'message'){
                    parser.nextToken();
                    message = parser.getText();
                }
                if(parser.getText() == 'result'){
                    parser.nextToken();
                    resultCCws = parser.getText();
                }
                
            }
        }
        Contact cont = [SELECT Removed_DIDs__c FROM Contact WHERE Id =: Apexpages.currentPage().getParameters().get('Id')];
        if(cont.Removed_DIDs__c !=null){
            String removeDids = cont.Removed_DIDs__c;
            List<String> DidList = new List<String>();
            if(removeDids.indexOf(',')!=null){
                DidList = removeDids.split(',');
            }
            voiceMailSet.removeAll(DidList);
        }
        for(Contact con: [SELECT id,CC_Voice_Number__c 
                          FROM Contact 
                          WHERE CC_Voice_Number__c IN: voiceMailSet 
                          AND MailingPostalCode = : zipCode]){
                              
                              If(voiceMailSet.contains(con.CC_Voice_Number__c)){
                                  voiceMailSet.remove(con.CC_Voice_Number__c);
                              }
            
        }
        if(voiceMailSet.size()>0){
            for(String mailSet:voiceMailSet ){
                cc_voiceNumber = mailset;
            }
        }
        system.debug('>><<<final Number to Order and Assign to user:'+message+resultCCws+cc_voiceNumber);
        
        // Code changes done by Ravi Jain for case 00268312
        // Start - Ravi Jain - 00268312 - 05th September, 2019 - Call the order service to check the bandwidth
        if(resultccws == 'true'){
            Http h1 = new Http();
            HttpRequest req1 = new HttpRequest();
            String sURLOrder = Label.CC_CCWS_ENDPOINT+'/number/order';
            System.debug('Order number End point:'+sURLOrder);
            req1.setEndpoint(sURLOrder);
            req1.setMethod('POST');
            req1.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            String requestBody1 = 'data={"phoneNumber":"'+cc_voiceNumber+'","userID": "'+con.CB_userID__c+'"}';
            system.debug('Order number WebService Request Body'+requestBody1);
                req1.setBody(requestBody1);
                String result1;
                HttpResponse res1 = h.send(req1);
                string call_result1=res1.getBody();
                System.debug('Response for Order number Request:'+ call_result1);
                String message1;
                if (res1.getStatusCode() != 200) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,res1.getBody() 
                                                               + ' '
                                                               +res1.getStatusCode() 
                                                               + ' ' 
                                                               + res1.getStatus()));
                    system.debug('Failed status code - Order number call');
                    return null;
                } 
                else{
                    
                    String addressResponse=res1.getBody();
                    //system.debug('ordernumberResponse--'+res1.getBody());
                    JSONParser parser = JSON.createParser(res1.getBody());
                    while (parser.nextToken() != null) {
                        if(parser.getText() == 'message'){
                            parser.nextToken();
                            message1 = parser.getText();
                        }
                        if(parser.getText() == 'result'){
                            parser.nextToken();
                            resultCCws2 = parser.getText();
                            
                        }
                        
                    }
                }
            
        }
        // End - Ravi Jain - 00268312 - 05th September, 2019
        
        //if(resultCCws =='true' && voiceMailSet.size()>0){
        if(resultCCws2 =='true'){
            if(con.CB_userID__c != null){
                Http h1 = new Http();
                HttpRequest req1 = new HttpRequest();
                String sURL1=Label.CC_CCWS_ENDPOINT+'/user/assignDID';
                req1.setEndpoint(sURL1);
                req1.setMethod('POST');
                req1.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
                String requestBody1 = 'data={"userID":"'+con.CB_userID__c+'","phoneNumber": "'+cc_voiceNumber+'"}';
                system.debug('requestBody1'+requestBody1);
                req1.setBody(requestBody1);
                String result1;
                // if( !Test.isRunningTest() ) {
                HttpResponse res1 = h.send(req1);
                string call_result1=res1.getBody();
                //String cc_voiceNumber;
                String message1;
                String resultCCws1;
                if (res1.getStatusCode() != 200) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,res1.getBody() + ' '+
                                                               res1.getStatusCode() + ' ' + res1.getStatus()));
                    return null;
                } 
                else{
                    system.debug('responseAssigndid--,'+res1.getBody());
                    
                    String addressResponse=res1.getBody();
                    JSONParser parser = JSON.createParser(res1.getBody());
                    while (parser.nextToken() != null) {
                        if(parser.getText() == 'message'){
                            parser.nextToken();
                            message1 = parser.getText();
                        }
                        if(parser.getText() == 'result'){
                            parser.nextToken();
                            resultCCws1 = parser.getText();
                        }
                        
                    }
                }
                
                if(resultCCws1 == 'true'){
                    
                    Contact c = new Contact();
                    c.Id = Apexpages.currentPage().getParameters().get('Id');
                    c.CC_Voice_Number__c = cc_voiceNumber;
                    update c;
                    ccVoiceNumber = cc_voiceNumber;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,message1));
                    isRemoveDisabled = false;
                    isAddDisabled = true;
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message1));
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'User Id Not Available'));
            }
            
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message));
        }
        return null;
    }
    public PageReference removeNumber(){
        if(con.CB_userID__c != null){
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            String sURL=Label.CC_CCWS_ENDPOINT+'/user/deleteDID';
            req.setEndpoint(sURL);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            String requestBody = 'data={"userID":"'+con.CB_userID__c+'"}';
            system.debug('requestBodydeleetid'+requestBody);
            req.setBody(requestBody);
            String result;
            // if( !Test.isRunningTest() ) {
            HttpResponse res = h.send(req);
            string call_result=res.getBody();
            //String cc_voiceNumber;
            String message;
            String resultCCws;
            if (res.getStatusCode() != 200) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,res.getBody() + ' '+
                                                           res.getStatusCode() + ' ' + res.getStatus()));
                return null;
            } 
            else{
                system.debug('deleteIdResponse,'+res.getBody());
                String addressResponse=res.getBody();
                JSONParser parser = JSON.createParser(res.getBody());
                while (parser.nextToken() != null) {
                    if(parser.getText() == 'message'){
                        parser.nextToken();
                        message = parser.getText();
                    }
                    if(parser.getText() == 'result'){
                        parser.nextToken();
                        resultCCws = parser.getText();
                    }
                    
                }
                
                if(resultCCws == 'true'){
                    Contact Cont = [SELECT id,CC_Voice_Number__c,Removed_DIDs__c FROM Contact WHERE Id =: Apexpages.currentPage().getParameters().get('Id')];
                    //Contact c = new Contact();
                    //if(cont.Removed_DIDs__c.contains(',')){
                        
                        cont.Removed_DIDs__c = cont.removed_Dids__c+cont.CC_Voice_Number__c+',';
                    //}
                    //else{
                        //cont.Removed_DIDs__c = cont.CC_Voice_Number__c+',';
                    //}
                    //c.id = Apexpages.currentPage().getParameters().get('Id');
                    cont.CC_Voice_Number__c = null;
                    update cont;
                    isRemoveDisabled = true;
                    isAddDisabled = false;
                    ccVoiceNumber = null;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,message));
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message));
                }
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'User ID Not Available'));
        }
        return null;
    }
}