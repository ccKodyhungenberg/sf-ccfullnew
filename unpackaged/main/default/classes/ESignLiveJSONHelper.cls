/**
 * Class Name: ESignLiveJSONHelper.cls
 *
 * Class that contains helper methods for working with JSON
 * requests and responses
 */

public with sharing class ESignLiveJSONHelper
{
    /**
     * Method to prepare outbound JSON to include removing _x
     * values and removing null properties and values
     */
    public static String prepareOutboundJSON(String jsonString)
    {
        if (String.isNotEmpty(jsonString)) {
            jsonString = remove_x(jsonString);
        }

        return jsonString;
    }

    /**
     * Method to replace inbound values using the getInboundReplacementMap method
     * This is needed to handle reserved words in Apex
     */
    public static String prepareInboundJSON(String jsonString)
    {
        if(String.isNotEmpty(jsonString))
        {
            Map<String, String> inboundReplacementMap = getInboundReplacementMap();
            for(String key : inboundReplacementMap.keySet())
            {
                if(jsonString.contains(key))
                {
                    jsonString = jsonString.replace(key, inboundReplacementMap.get(key));
                }
            }
        }
        return jsonString;
    }

    /**
     * Method to remove all _x values used for handling reserved words
     * in Apex
     */
    private static String remove_x(String jsonString)
    {
        if(String.isNotEmpty(jsonString)) 
        {
            jsonString = jsonString.replaceAll('(_[xX])+":', '":');
        }

        return jsonString;
    }

    /**
     * Method to build map for replacing inbound values to Salesforce safe values
     * This is needed to get around reserved words in Apex
     */
    private static Map<String, String> getInboundReplacementMap()
    {
        Map<String, String> inboundReplacementMap = new Map<String, String>();
        inboundReplacementMap.put('"enum"', '"enum_x"');
        inboundReplacementMap.put('"from"', '"from_x"');
        inboundReplacementMap.put('"group"', '"group_x"');
        inboundReplacementMap.put('NEW', 'NEW_X');
        inboundReplacementMap.put('NEW_X_X', 'NEW_X');
        inboundReplacementMap.put('PACKAGE', 'PACKAGE_X');
        inboundReplacementMap.put('PACKAGE_X_X', 'PACKAGE_X');

        return inboundReplacementMap;
    }
}