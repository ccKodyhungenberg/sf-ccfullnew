@isTest 
private class CC_SendProspect_Test {
    static testMethod void testController() {
        Test.startTest();
        
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = 'https://clearcaptions123--ccdevnew.cs19.my.salesforce.com/services/apexrest/SendProspect/';
        request.addHeader('Content-Type', 'application/json; charset=UTF-8');
        request.addHeader('Accept', 'application/json');
        //for prod-QA-435CE039-D298-7009-4D21-DAB239709E52 and for UAT-QA-EB8149C4-957D-43E9-88D3-7A2C83249F39
        request.addHeader('token', 'DEV-EB8149C4-957D-43E9-88D3-7A2C83249F39');	
        request.addHeader('FirstName', 'Akshat1');
        request.addHeader('LastName', 'Singhhh');
        request.addHeader('Email', 'anb@bhs.com');
        request.addHeader('Phone', '9876543212');
        //Start-edited by Gursehaj Pal Singh Aneja for the story S-606009
        request.addHeader('utm_term', 'retired');
        //End-edited by Gursehaj Pal Singh Aneja for the story S-606009
        
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = response;
        // Call the method to test
        CC_SendProspect.SendProspectWS();
        // Verify results
        
        
        //GlobalFunctions.getInstance().Rebuild();
        CC_SendProspect ctr_ws=new CC_SendProspect();
        
        CC_SendProspect.clMessage oReply;
        //no token
        //SendProspect(string sAccessToken, string sFirstName, string sLastName, string sEmail, string sPhone, string sMessage, string sBusinessName)
        //oReply=CC_SendProspect.SendProspect('','','','','','','','','','','','','');
        //good token
        CC_SendProspectEndpoint__mdt spe = [SELECT MasterLabel,CampaignID__c,Token__c FROM CC_SendProspectEndpoint__mdt limit 1];
        //Start - Modified by Prachi for Case 00266795
       oReply=CC_SendProspect.SendProspect(spe.Token__c,'abc','abc','abc@abc.com','1234567890','9087654321','abc','abc' ,'abc','323212','','','','','','','','','','');
       oReply=CC_SendProspect.SendProspect(null,null,'','','','','' ,'','','','','','','','','','','','','');
        //End - Modified by Prachi for Case 00266795
        Test.stopTest();
    }
}