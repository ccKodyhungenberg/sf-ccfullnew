public class CC_TaskTriggerHandler {
    public static void onBeforeInsert(List<Task> newList){
        updateActivityCreatedOnLead(newList);
    }
    /* Name: onAfterInsert
     * Descreption: After Insert Operations to be Performed in this method
     * Created By : Nilesh Grover for S-584775 
     */
    public static void onAfterInsert(List<Task> newList){
        updateCallAttemptCheckBox(newList);
        updateRcommendedCallTime(newList); // Added By Vinit for S-637726 [23-Sep-2019] 
    }
        
    private static void updateActivityCreatedOnLead(List<Task> newList){
        List<Lead> leadList = new List<Lead>();
        List<Case> caseList = new List<Case>();
        Set<Id> caseIdSet = new Set<Id>();
        String sObjName;
        for(Task newTask: newList){
            if(newTask.WhoId!=null){
                sObjName = newTask.whoId.getSObjectType().getDescribe().getName();
                if(sObjName.equalsIgnoreCase('Lead') && newTask.Subject.startsWith('Email')){
                    Lead leadToUpdate = new lead();
                    leadToUpdate.id = newTask.WhoId;
                    leadToUpdate.Activity_Created__c = true;
                    leadList.add(leadToUpdate);
                }
            }
            if(newTask.WhatId!=null){
                sObjName=newTask.WhatId.getSObjectType().getDescribe().getName();
                if(sObjName.equalsIgnoreCase('Case') && newTask.Subject.startsWith('Email')){
                    caseIdSet.add(newTask.WhatId);
                }
            }
        }
        if(caseIdSet.size()>0){
            Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PCF Collection').getRecordTypeId();
            for(Case ca: [SELECT Id,recordtypeId,Emails_Sent__c,status__c, Email_Attempt__c FROM Case WHERE Id IN : caseIdSet]){
                Case caseToUpdate = new Case();
                caseToUpdate.Id = ca.id;
                if(ca.Email_Attempt__c != null){
                    caseToUpdate.Email_Attempt__c = ca.Email_Attempt__c + 1;
                }
                else{
                    caseToUpdate.Email_Attempt__c =1;
                }
                if(ca.RecordTypeId == devRecordTypeId){
                    caseToUpdate.Last_Emailed__c = System.now();
                    if(ca.Emails_Sent__c != null){
                        caseToUpdate.Emails_Sent__c = ca.Emails_Sent__c + 1;
                    }
                    else{
                        caseToUpdate.Emails_Sent__c =1;
                    }
                    if (ca.Status__c == PcfApprovalConstants.PENDING) {
                        caseToUpdate.Status__c = PcfApprovalConstants.EMAILED;
                    } else if (ca.Status__c == PcfApprovalConstants.FAXED) {
                        caseToUpdate.Status__c = PcfApprovalConstants.EMAILED_FAXED;
                    }
                }
                caseList.add(caseToUpdate);
            }
        }
        
        if(leadList.size()>0){
            update leadList;
        }
        if(caseList.size()>0){
            update caseList;
        }
        
    }
    /* Name: updateCallAttemptCheckBox
     * Descreption: To update Call attempt CheckBox on Corresponding Lead
     * Created By : Nilesh Grover for S-584775 
     */
    private static void updateCallAttemptCheckBox(List<Task> newList) {
        Set<Id> setOfLeadIds = new Set<Id>();      
        List<Lead> leadstoUpdate = new List<Lead>();
        for(Task newTask: newList) {
            if(newTask.WhoId != null){
                if(newTask.TaskSubtype == 'Call' && newTask.WhoId.getSObjectType().getDescribe().getName() == 'Lead') {
                    setOfLeadIds.add(newTask.WhoId);
                }
            }
        } 
        if(setOfLeadIds.size() > 0) {
                leadsToUpdate = [SELECT Id, Call_Attempt_1__c, Call_Attempt_2__c, 
                                            Call_Attempt_3__c, Call_Attempt_4__c, 
                                            Call_Attempt_5__c, Call_Attempt_6__c, 
                                            Call_Attempt_7__c, Call_Attempt_8__c, 
                                            Call_Attempt_9__c, Call_Attempt_10__c
                                 FROM Lead
                                 WHERE Id IN :setOfLeadIds];                
                for(Lead lead: leadsToUpdate) {
                    if(lead.Call_Attempt_1__c == false){
                        lead.Call_Attempt_1__c = true;   
                    }else if(lead.Call_Attempt_1__c == true && lead.Call_Attempt_2__c == false) {
                        lead.Call_Attempt_2__c = true;           
                    }else if(lead.Call_Attempt_2__c == true && lead.Call_Attempt_3__c == false) {
                        lead.Call_Attempt_3__c = true;
                    }else if(lead.Call_Attempt_3__c == true && lead.Call_Attempt_4__c == false) {
                        lead.Call_Attempt_4__c = true;
                    }else if(lead.Call_Attempt_4__c == true && lead.Call_Attempt_5__c == false) {
                        lead.Call_Attempt_5__c = true;
                    }else if(lead.Call_Attempt_5__c == true && lead.Call_Attempt_6__c == false) {
                        lead.Call_Attempt_6__c = true;
                    }else if(lead.Call_Attempt_6__c == true && lead.Call_Attempt_7__c == false) {
                        lead.Call_Attempt_7__c = true;
                    }else if(lead.Call_Attempt_7__c == true && lead.Call_Attempt_8__c == false) {
                        lead.Call_Attempt_8__c = true;
                    }else if(lead.Call_Attempt_8__c == true && lead.Call_Attempt_9__c == false) {
                        lead.Call_Attempt_9__c = true;
                    }else if(lead.Call_Attempt_9__c == true && lead.Call_Attempt_10__c == false){
                        lead.Call_Attempt_10__c = true;
                    }
                }
                update leadsToUpdate;
            }
    }
    
    // Added By Vinit for S-637726 [23-Sep-2019] start
    /**@Name: Update Rcommended Call Time field
     * @Descreption: To Update Rcommended Call Time on Lead
     * @Created By : Vinit for S-637726
     * @Param  newList of Task
     * @Return none
     **/
    private static void updateRcommendedCallTime(List<Task> newList) {
        Map<Id, String> setOfLeadIds = new Map<Id, String>();      
        List<Lead> leadstoUpdate = new List<Lead>();
        System.debug('newList '+newList);
        for(Task newTask: newList) {
            System.debug('newTask '+newTask);
            if(newTask.WhoId != null){
                
                if((newTask.Type == 'Call' || newTask.Type == 'Call - Attempt') && newTask.status == 'Completed' && newTask.WhoId.getSObjectType().getDescribe().getName() == 'Lead') {
                    System.debug('created date '+newTask.CreatedDate+' after formatting '+newTask.CreatedDate.format('HH:mm:ss','America/Indiana/Indianapolis'));
                    setOfLeadIds.put(newTask.WhoId, newTask.CreatedDate.format('HH:mm:ss','America/Indiana/Indianapolis'));
                }
            }
        }
        System.debug('setOfLeadIds '+setOfLeadIds);
         if(setOfLeadIds.keySet().size() > 0) {
                leadsToUpdate = [SELECT Id, Recommended_Call_Time__c
                                   FROM Lead
                                   WHERE Id IN :setOfLeadIds.keySet() AND Channel__c = 'Marketing'];
             
             for (Lead leadItem :leadsToUpdate) {
                 
                 Integer hour = Integer.valueOf(setOfLeadIds.get(leadItem.id).split(':')[0]);
				 Integer minute = Integer.valueOf(setOfLeadIds.get(leadItem.id).split(':')[1]);
                
                  
                 if (hour >= 06 && hour <= 12) {
                        if ((hour == 12 && minute == 00) || hour < 12) {
                            
                            leadItem.Recommended_Call_Time__c = 'Afternoon 12pm-4pm';
                            
                        } 
                        
                    } 
                
                    if (hour  >= 12 && hour <= 16) {
                        if ((hour == 16 && minute == 00) || hour < 16) {
                            leadItem.Recommended_Call_Time__c = 'Evening 4pm-8pm';
                        } 
                        
                    } 
                
                    if (hour  >= 16 && hour <= 20) {
                        if ((hour == 20 && minute == 00) || hour < 20) {
                            leadItem.Recommended_Call_Time__c = 'Morning 6am-12pm';
                        } 
                    }
                 
             }
             
             
             update leadsToUpdate;
         }
    }
    
     // Added By Vinit for S-637726 [23-Sep-2019] end
}