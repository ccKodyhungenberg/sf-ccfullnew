/** 
* (c) 2019 Appirio, Inc.
* Apex Class Name: BatchPopulateRecommendedCallField.cls
* Description: Batch to Populate Recommended Call Field value on Lead after call task created.
* @Created By : Vinit for S-637726
**/

global class BatchPopulateRecommendedCallField Implements Database.Batchable<SObject>{

    String query='';
    
    public BatchPopulateRecommendedCallField(String qry) {
        query = qry;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        //List<Task> listTask = new List<Task>(scope);

Map<Id,String> leadTaskMap = new Map<Id,String>();
        
        System.debug('scope '+scope.size());

for (Task tsk : (List<Task>)scope) {

	if (!leadTaskMap.containsKey(tsk.whoid)) {
		leadTaskMap.put(tsk.whoid, tsk.createdDate.format('HH:mm:ss','America/Indiana/Indianapolis'));
       // System.debug(tsk.createdDate.format('HH:mm:ss') );
	} 
}

List<Lead> tmpLead = [SELECT Id, Recommended_Call_Time__c FROM Lead WHERE Id=: leadTaskMap.keySet()];

for (Lead lid : tmpLead) {

		Integer hour = Integer.valueOf(leadTaskMap.get(lid.id).split(':')[0]);
		Integer minute = Integer.valueOf(leadTaskMap.get(lid.id).split(':')[1]);
		
	if (hour >= 06 && hour <= 12) {
		if ((hour == 12 && minute == 00) || hour < 12) {
            
            lid.Recommended_Call_Time__c = ' Afternoon 12pm-4pm';
            
		} 
		
	} 

	if (hour  >= 12 && hour <= 16) {
		if ((hour == 16 && minute == 00) || hour < 16) {
            lid.Recommended_Call_Time__c = 'Evening 4pm-8pm';
		} 
		
	} 

	if (hour  >= 16 && hour <= 20) {
		if ((hour == 20 && minute == 00) || hour < 20) {
            lid.Recommended_Call_Time__c = ' Morning 6am-12pm';
		} 
	}
}

        update tmpLead;
        
        
        
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}