//
// (c) 2018 Appirio, Inc.
//
// Apex Class Name: AddressTriggerHandler.cls
// For Apex Trigger: AddressTrigger.trigger
// Description: This apex class is used to do specific functionality related to Addresses__c Object.
//
// 14th June, 2018   Vishwas Gupta   Original (Task # 	T-705347) - Please see the task description for more details.
//

public class AddressTriggerHandler {
  
  public static void uncheckAllActiveCheckbox( List<Addresses__c> newList, Map<Id,Addresses__c> oldMap ) {
    
    Set<Id> accountIds = new Set<Id>();
    Set<Id> contactIds = new Set<Id>();
    Set<Id> assetIds = new Set<Id>();
    Set<Id> addressIds = new Set<Id>();
    Boolean runProcess = TRUE;
    List<Addresses__c> listAddresses = new List<Addresses__c>();
    
    for( Addresses__c add : newList ) {
      
      if( (oldMap != null && oldMap.get(add.Id).Active__c != add.Active__c && add.Active__c) ||
          (oldMap == null && add.Active__c )
        ) {
        
        addressIds.add(add.Id);
        
        if( add.Account__c != null ) {
          accountIds.add(add.Account__c);
        }
        if( add.Contact__c != null ) {
          contactIds.add(add.Contact__c);
        }
        if( add.Asset__c != null ) {
          assetIds.add(add.Asset__c);
        }
        
        if( accountIds.size() == 0 &&
            contactIds.size() == 0 &&
            assetIds.size() == 0
          ) {
          runProcess = FALSE;
        }
        
      }
    }
    
    
    if( runProcess ) {
      String appendingAddressQuery = '';
      String addressQuery = ' SELECT Id, Active__c FROM Addresses__c WHERE Active__c = TRUE AND Id NOT IN :addressIds ';
      
      if( !accountIds.isEmpty() ) {
        appendingAddressQuery += ' Account__c IN : accountIds ';
      }
      
      if( !contactIds.isEmpty() ) {
        if( appendingAddressQuery.contains( 'Account__c IN : accountIds' ) ) {
          appendingAddressQuery += ' OR ';
        }
        appendingAddressQuery += ' Contact__c IN :contactIds ';
      }
      
      if( !assetIds.isEmpty()) {
        if( appendingAddressQuery.contains( 'Account__c IN : accountIds' ) || 
            appendingAddressQuery.contains( 'Contact__c IN :contactIds' )
          ) {
          appendingAddressQuery += ' OR ';
        }
        appendingAddressQuery += ' Asset__c IN :assetIds ';
      }
      
      if( !String.isBlank(appendingAddressQuery)) {
        addressQuery += ' AND ( '+appendingAddressQuery+ ' ) ';
      }
      else{
        return;
      }      
      
     for( Addresses__c add : Database.Query( addressQuery )) {
       add.Active__c = FALSE;
       listAddresses.add( add );
     }
     
     if( listAddresses.size() > 0 ) {
       update listAddresses;
     }
      
    }
  }
}