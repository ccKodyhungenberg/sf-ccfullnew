/**********************************************
* Name :updateAccountOnAssetsBatchTest
* Description : Test Class for UpdateAccountOnAssetsBatch (Story S-609398) 
* Author : Nilesh Grover
**********************************************/

@isTest
private class updateAccountOnAssetsBatchTest {
	
	private static testmethod void testUpdateAccountOnAssetsBatch() {
		List<Account> accList = CC_TestUtility.createAccount(1, true);
        system.assertEquals(1, [SELECT Id FROM Account].size());
        
        List<Contact> contactList = CC_TestUtility.createContact(1, true, 'Customer',accList[0].Id , 'Test123@gmail.com');
        system.assertEquals(1, [SELECT Id FROM Contact].size());
        
        List<Asset> listAsset = new List<Asset>();
		for( Integer i = 0; i < 5; i++) {
			Asset ass = new Asset();
			ass.Name = 'TestAsset' + String.valueOf(i);
			ass.ContactId = contactList[0].Id;
			listAsset.add(ass);
      	}
		insert listAsset;		
        List<Asset> insertedAssets = [SELECT Id, AccountId, ContactId FROM Asset];
       	for(Asset a : insertedAssets){
			if(a.AccountId != null){
				a.AccountId = null;
			}
		}
		update insertedAssets;		
		Test.startTest();
		Database.executeBatch(new updateAccountOnAssetsBatch());
		Test.stopTest();
		for(Asset a : [SELECT Id, AccountId, ContactId FROM Asset]) {
			System.assertEquals(accList[0].Id , a.AccountId);
		}
	}	
}