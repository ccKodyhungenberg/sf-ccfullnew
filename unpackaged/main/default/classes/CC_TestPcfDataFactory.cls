@isTest
public class CC_TestPcfDataFactory {
    public static final String MRF_DOCUMENT_TYPE = 'Medical Release Form';
    public static final String PCF_DOCUMENT_TYPE = 'PCF Unsigned Form';
    public static final String CONGA_TEMPLATE_NAME = 'PCF Form Template';
    
    public static void createSettings() {
        CC_EsignLiveTestDataUtility.createConnectionSetting();
        
        List<SFaxSettings__c> sfaxSettings = new List<SFaxSettings__c>();
        Map<String, String> sfaxValues = new Map<String, String>{
            'Username' => 'Username',
                'APIKey' => 'APIKey',
                'EncryptionKey' => '12345678901234567890123456789012',
                'InitVector' => '1234567890123456'
                };
                    for (String name : sfaxValues.keySet()) {
                        String value = sfaxValues.get(name);
                        SFaxSettings__c setting = new SFaxSettings__c();
                        setting.Name = name;
                        setting.Value__c = value;
                        sfaxSettings.add(setting);
                    }
        insert sfaxSettings;
        
        Map<String, String> globalValues = new Map<String, String>{
            'Company Email RegEx' => 'abc'
                };
                    /*List<GlobalConfigurationVariable__c> globalSettings = new List<GlobalConfigurationVariable__c>();
for (String name : globalValues.keySet()) {
String value = globalValues.get(name);
GlobalConfigurationVariable__c setting = new GlobalConfigurationVariable__c();
setting.Name = name;
setting.Value__c = value;
globalSettings.add(setting);
}
insert globalSettings;

// prevent the claris integration from running
insert new ClarisIntegrationSettings__c(
Name = 'Integration_Is_Active',
Value__c = 'false'
);*/
                    
                    APXTConga4__Conga_Template__c congaTemplate = new APXTConga4__Conga_Template__c(
                        APXTConga4__Name__c = CONGA_TEMPLATE_NAME
                    );
        insert congaTemplate;
        
        Document intructions = new Document(
            Name = 'Instructions Doc',
            Body = Blob.valueOf('test'),
            // hack for testing
            FolderId = UserInfo.getUserId()
        );
        insert intructions;
        
        insert new PCF_Settings__c(
            Name='Default',
            Fax_Remarks_Template_Id_With_MRF__c='Default',
            Fax_Remarks_Template_Id_Without_MRF__c='Default',
            Fax_Cover_Sheet_With_MRF__c='Cover_With_MRF',
            Fax_Cover_Sheet_Without_MRF__c='Cover_Without_MRF',
            oAuth_Client_ID__c = '123',
            oAuth_Client_Secret__c = 'abc',
            oAuth_Password__c = 'abc',
            oAuth_Username__c = 'abc',
            Signature_Top__c = 123,
            Signature_Left__c = 123,
            Signature_Width__c = 123,
            Signature_Height__c = 123,
            Signature_Date_Height__c = 123,
            Signature_Date_Left__c = 123,
            Signature_Date_Width__c = 123,
            Signature_Date_Top__c = 123,
            Conga_Template_Id__c = congaTemplate.Id,
            Instruction_Document_Id__c = intructions.Id,
            Delete_Pcf_Attachment_After_Signing__c = true
        );
    }
    
    public static Contact createCustomer(Boolean createMrf, Boolean createPcf) {
        Contact customer = null;
        
        /*User uBoomi = [SELECT Id FROM User WHERE Id = :CCGlobal.idBoomi];
System.runAs(uBoomi) {*/
        Account hcpAccount = new Account(
            Name = 'HCP Account',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId()
        );  
        
        insert hcpAccount;
        System.debug('Account Created from testDataFactory'+hcpAccount);
        Integer ident0 = Math.round(Math.random()*100000);
        Contact hcp = new Contact(                   
            AccountId = hcpAccount.Id,
            ContactID__c = ident0,
            CB_userID__c=string.valueof(ident0),
            DateCreated__c = datetime.now(),
            Email = 'hcp@simpsons.com',
            Fax = '1234567890',
            FirstName = 'Healthcare',
            LastName = 'Provider',
            Silanis_Fast_Track_Id__c = 'YdZV5CEJV_JGySmrHlC-yyvuZnc=',
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Non-Customer').getRecordTypeId()
        );
        insert hcp;
        
        Account account = new Account(
            Name = 'Simpson Household'
        );    
        insert account;
        
        Integer ident = Math.round(Math.random()*100000);
        customer = new Contact(                   
            AccountId = account.Id,
            Health_Care_Professional__c = hcp.Id,
            ContactID__c = ident,
            CB_userID__c=string.valueof(ident),
            DateCreated__c = datetime.now(),
            Email = 'homer@simpsons.com',
            HasOptedOutOfEmail = True,
            FBID__c = 456,
            FirstName = 'Homer' + ident,
            LanguagePreference__c = 'English',
            LastName = 'Simpson' + ident,
            LastUpdated__c = Datetime.now(),
            MailingCity = 'Springfield',
            MailingCountry = 'United States',
            MailingState = 'CA',
            MailingStreet = '55 Evergreen Terr',
            MailingPostalCode = '93410',
            RelayType__c = 'Setec',
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId()
        );    
        
        insert customer;
        
        if (createMrf) {
            Customer_Document__c mrfDocument = new Customer_Document__c(
                Document_Type__c = MRF_DOCUMENT_TYPE,
                Contact__c = customer.Id
            );
            insert mrfDocument;
            
            Attachment mrfAttachment = new Attachment(
                Name = 'MRF',
                Body = Blob.valueOf('test'),
                ParentId = mrfDocument.Id
            );
            insert mrfAttachment;
            
            customer.Medical_Release_Form__c = mrfDocument.Id;
        }
        
        if (createPcf) {
            Customer_Document__c pcfDocument = new Customer_Document__c(
                Document_Type__c = PCF_DOCUMENT_TYPE,
                Contact__c = customer.Id
            );
            insert pcfDocument;
            
            Attachment pcfAttachment = new Attachment(
                Name = CONGA_TEMPLATE_NAME,
                Body = Blob.valueOf('test'),
                ParentId = pcfDocument.Id
            );
            insert pcfAttachment;
            
            customer.PCF_Unsigned_Form__c = pcfDocument.Id;
        }
        
        if (createMrf || createPcf) {
            update customer;
        }
        return customer;
    }
    public static Case createApproval(Contact customer) {
        Attachment pcfAttachment = new Attachment(
            Name = CONGA_TEMPLATE_NAME,
            Body = Blob.valueOf('test'),
            ParentId = customer.Id
        );
        insert pcfAttachment;
        
        Attachment mrfAttachment = new Attachment(
            Name = CONGA_TEMPLATE_NAME,
            Body = Blob.valueOf('test'),
            ParentId = customer.Id
        );
        insert mrfAttachment;
        
        
        // create new ESL package object
        Case objPcfApproval = new Case(
            ContactId = customer.Id,
            Title__c = customer.Name + ' PCF Approval',
            MRF_Attachment_Id__c = mrfAttachment.Id,
            PCF_Attachment_Id__c = pcfAttachment.Id,
            Emails_Sent__c = 0,
            Faxes_Sent__c = 0,
            Status__c = PcfApprovalConstants.PENDING,
            RecordTypeId  = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PCF Verification').getRecordTypeId()
            //Health_Care_Professional__c = null
        );
        insert objPcfApproval;
        
        customer.Latest_PCF_Approval__c = objPcfApproval.Id;
        update customer;
        
        return objPcfApproval;
    }
    
}