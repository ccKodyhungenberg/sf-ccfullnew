@isTest
private class CC_EvaluationTriggerHandlerTest {
    

    static testMethod void testAddEvaluation() {

        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        String userName = String.valueOf(Datetime.Now().getTime());

        User u = new User(
            FirstName         = 'Test',
            LastName          = 'User',
            Email             = 'test@user.com',
            Alias             = 'tuser',
            Username          = userName + 'test@user.com',
            LocaleSidKey      = 'en_US',
            TimeZoneSidKey    = 'GMT',
            LanguageLocaleKey = 'en_US',
            EmailEncodingKey  = 'UTF-8',
            ProfileId         = p.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street            = '809 HEATHERWOOD DR',
            country           = 'USA',
            postalCode        = '57005'
        );
        insert u;
        Group g = [SELECT id  FROM Group WHERE DeveloperName = 'EvaluationTeam'];
        /*Group g = new Group(
            DeveloperName = 'EvaluationTeam',
            Name = 'Evaluation Team',
            Type = 'Regular'
        );
        insert g;*/

        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = u.Id
        );
        insert gm;
        
        System.runAs(u) {
        
        //Modified below 1 line by NSINGH - S-584772
        EvaluationScript__c evas1= new EvaluationScript__c(
                Script__c='test script text1',
                Title__c='test script title1', 
                CallType__c='Answering Machine',
                TotalWords__c=3
        );
        insert evas1;
        //Modified below 1 line by NSINGH - S-584772
        EvaluationScript__c evas2= new EvaluationScript__c(
                Script__c='test script text2',
                Title__c='test script title2', 
                CallType__c='Answering Machine',
                TotalWords__c=3
        );
        insert evas2;        
        List<EvaluationScript__c> les=new List<EvaluationScript__c> ([select id from EvaluationScript__c limit 2]);
        system.assert(les.size()==2, '@@@ ERROR: There must be at least 2 Evaluation Scripts in the system!');

        //Modified below 1 line by NSINGH - S-584772
        Evaluation__c eva=new Evaluation__c(
                AgentNumber__c='12345',
                AudioIssues__c='No',
                CallbackNumber__c='916-777-9900',
                CalledNumber__c='916-777-9990',
                CallStartTime__c=datetime.now(),
                ApproximateDragonerrors__c=1,
                EvaluationScript__c=les[0].Id,
                TextClumping__c='No',
                WordsAdded__c=3,
                WordsChanged__c=1,
                WordsOmitted__c=2,
                Provider__c='Provider'
        );
        insert eva;
        eva.EvaluationScript__c=les[1].Id;
        update eva;
        
        }
    }
}