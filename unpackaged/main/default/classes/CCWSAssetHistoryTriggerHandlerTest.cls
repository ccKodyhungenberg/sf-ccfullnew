@isTest
private class CCWSAssetHistoryTriggerHandlerTest {

    static testMethod void testSR() {
    
     //C-00224692 - gcurry - 4/9/2018 - Setup Test class to run as Clear Captions Admin to prevent test failure when running/deploying as other users.
        //User u = [select Alias, Email, EmailEncodingKey, LastName, LanguageLocaleKey, LocaleSidKey, ProfileId, TimeZoneSidKey, UserName from User where UserName like 'integration@clearcaptions.com%'];

        //System.runAs(u)
          //{    
            Test.StartTest();
            List<Contact>lct=CCGlobalTestSupport.CreateCustomer(1);
            system.debug(CCGlobal.companyEmailRegex); //init for the regex value for emails (not set in global functions)
            system.debug(CCGlobal.Integration_Is_Active); //init for the claris integration    
            GlobalFunctions.getInstance().Rebuild();
            lct[0].CB_userID__c='userid1231';
            insert lct[0];
            CCWS_Asset__c oDevice=new CCWS_Asset__c(
                Name='testDeviceID3124'
                ,deviceID__c='testDeviceID3124'
                ,lastUpdated__c=datetime.now()
                ,createdOn__c=datetime.now()
                ,countryCode__c='1'
                ,active__c='true'
                ,lastCheckIn__c=datetime.now()
                ,number__c='9177779900'
                ,partnerCode__c='1'
                ,POMStatus__c='true'
                ,deviceType__c='28'
                );
            upsert oDevice deviceID__c;
            CCWS_Asset_History__c oDH=new CCWS_Asset_History__c(
                deviceID__c='testDeviceID3124'
                ,userID__c='userid1231'
                ,createdOn__c=datetime.now()
                ,lastUpdated__c=datetime.now()
                ,active__c='true'
                ,lastUpdated_deviceID_userID__c='testupdate#testDeviceID3124#userid1231'
                );
            insert oDH;
        	oDH.lastUpdated__c = Datetime.now();
        	update oDH;
            Test.stopTest();
          //}
    }
    
    static testMethod void testSRupdate() {
    
     //C-00224692 - gcurry - 4/9/2018 - Setup Test class to run as Clear Captions Admin to prevent test failure when running/deploying as other users.
        //User u = [select Alias, Email, EmailEncodingKey, LastName, LanguageLocaleKey, LocaleSidKey, ProfileId, TimeZoneSidKey, UserName from User where UserName like 'integration@clearcaptions.com%'];

        //System.runAs(u)
          //{    
            Test.StartTest();
            List<Contact>lct=CCGlobalTestSupport.CreateCustomer(1);
            system.debug(CCGlobal.companyEmailRegex); //init for the regex value for emails (not set in global functions)
            system.debug(CCGlobal.Integration_Is_Active); //init for the claris integration    
            GlobalFunctions.getInstance().Rebuild();
            lct[0].CB_userID__c='userid1231';
            insert lct[0];
            CCWS_Asset__c oDevice=new CCWS_Asset__c(
                Name='testDeviceID3124'
                ,deviceID__c='testDeviceID3124'
                ,lastUpdated__c=datetime.now()
                ,createdOn__c=datetime.now()
                ,countryCode__c='1'
                ,active__c='true'
                ,lastCheckIn__c=datetime.now()
                ,number__c='9177779900'
                ,partnerCode__c='1'
                ,POMStatus__c='true'
                ,deviceType__c='28'
                );
            upsert oDevice deviceID__c;
            CCWS_Asset_History__c oDH=new CCWS_Asset_History__c(
                deviceID__c='testDeviceID3124'
                ,createdOn__c=datetime.now()
                ,lastUpdated__c=datetime.now()
                ,active__c='true'
                ,lastUpdated_deviceID_userID__c='testupdate#testDeviceID3124#userid1231'
                );
            insert oDH;
        	oDH.userID__c='userid1231';
        	oDH.lastUpdated__c = Datetime.now();
        	try {
            	update oDH;
            } catch(Exception e){
                System.assert(e.getMessage().contains('REQUIRED_FIELD_MISSING'));
            }
        	
            Test.stopTest();
          //}
    }

}