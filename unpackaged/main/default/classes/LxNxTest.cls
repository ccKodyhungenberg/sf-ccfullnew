@isTest
public class LxNxTest {

     @isTest
    static void testWrapper(){
        
        
        LxNx.RootObject rootObj = new LxNx.RootObject();
        LxNx.FlexIDRequest flexObj = new LxNx.FlexIDRequest();
        LxNx.SearchBy srchObj = new LxNx.SearchBy();
        LxNx.Name nmObj = new LxNx.Name();
        LxNx.User  usrObj = new LxNx.User();
        LxNx.EndUser endUsrObj = new LxNx.EndUser();
        LxNx.Options  optObj = new LxNx.Options  ();
        LxNx.DOB  dob = new LxNx.DOB() ;

        dob.Year =11993;
        dob.Month =12;
        dob.Day =12;
        
        LxNx.Address  add = new LxNx.Address();
        add.Zip5 = '12342' ;
        add.StreetAddress1 = '12 street';
        add.City = 'City';
        add.State = 'CA';
        add.StreetNumber = 'null';
        add.StreetPreDirection= 'null';
        add.StreetName= 'null';
        add.StreetSuffix= 'null';
        add.StreetPostDirection= 'null';
        add.UnitDesignation= 'null';
        add.UnitNumber= 'null';
        add.StreetAddress2= 'null';
        add.Zip4= 'null';
        add.County= 'null';
        add.PostalCode= 'null';
        add.StateCityZip= 'null';
        add.Latitude= 'null';
        add.Longitude= 'null';

        LxNx.Passport pass = new LxNx.Passport();
        pass.Number1 = null;
        LxNx.ExpirationDate expPass = new LxNx.ExpirationDate();
        expPass.Year = 0;
        expPass.Month=0;
        expPass.Day=0;
        pass.ExpirationDate = expPass;
        pass.Country ='null';
        pass.MachineReadableLine1 ='null';
        pass.MachineReadableLine2 ='null';

        srchObj.DOB = dob;
        srchObj.Address = add;
        srchObj.Passport =pass;
        srchObj.SSNLast4 ='1234' ;
        srchObj.SSN = '514467051';
        srchObj.DriverLicenseNumber ='null';
        srchObj.DriverLicenseState ='null';
        srchObj.IPAddress ='null';
        srchObj.WorkPhone ='null';
        srchObj.Gender ='null';
        srchObj.Email ='null';
        srchObj.Channel ='null';
        srchObj.Income ='null';
        srchObj.OwnOrRent ='null';
        srchObj.LocationIdentifier ='null';
        srchObj.OtherApplicationIdentifier1 ='null';
        srchObj.OtherApplicationIdentifier2 ='null';
        srchObj.OtherApplicationIdentifier3 ='null';
            
        flexObj.SearchBy = srchObj;
      
        nmObj.First='STAR';
        nmObj.Middle=null;         
        nmObj.Last ='AASIAN';
        nmObj.Suffix =null;
        nmObj.Prefix=null;
        nmObj.full=null;
        srchObj.Name = nmObj;
        srchObj.Age = 0;
        srchObj.HomePhone = '1021234567';
        
        usrObj.GLBPurpose =1;
        usrObj.DLPurpose =0;

        endUsrObj.CompanyName='null';
        endUsrObj.StreetAddress1='null';
        endUsrObj.City='null';
        endUsrObj.State='null';
        endUsrObj.Zip5='null';
        endUsrObj.Phone='null';
        usrObj.EndUser  = endUsrObj;
        usrObj.MaxWaitSeconds = 0;
        usrObj.ReferenceCode = 'null';
         usrObj.BillingCode= 'null';
            usrObj.QueryId= 'null';
            usrObj.AccountNumber= 'null';
            usrObj.OutputType= 'null';
        flexObj.User = usrObj;

        LxNx.CVICalculationOptions cv = new LxNx.CVICalculationOptions();
        cv.IncludeDOB = true;
        cv.IncludeDriverLicense = false;
        cv.DisableCustomerNetworkOption = false;
        optObj.CVICalculationOptions =cv;
        optObj.UseDOBFilter = false;
        optObj.IncludeMSOverride  = false;
        optObj.IncludeSSNVerification = false;
        optObj.IncludeVerifiedElementSummary = false;
        optObj.PoBoxCompliance= false;
        optObj.DOBRadius = 0 ;
        optObj.GlobalWatchlistThreshold = 0;
        optObj.IncludeAllRiskIndicators= true;
        optObj.IncludeDLVerification= false;
        optObj.IncludeMIOverride= false;
        optObj.NameInputOrder = 'Unknown';
        optObj.CustomCVIModelName = 'null';
        optObj.LastSeenThreshold = 'null';
        optObj.InstantIDVersion = 'null';

        LxNx.DOBMatch dobmatch = new LxNx.DOBMatch();
        dobmatch.MatchType = 'FuzzyCCYYMMDD';
        dobmatch.MatchYearRadius = 0;
        optObj.DOBMatch = dobmatch;

        LxNx.IncludeModels inc = new LxNx.IncludeModels();

        LxNx.FraudPointModel fraoud = new LxNx.FraudPointModel();
        fraoud.ModelName = null;
        fraoud.IncludeRiskIndices =true;
        inc.FraudPointModel = fraoud;

        List<LxNx.ModelOption> lstModOptions = new List<LxNx.ModelOption>();
        LxNx.ModelOption modOpt = new LxNx.ModelOption();
        modOpt.OptionName = null;
        modOpt.OptionValue = null;
        lstModOptions.add(modOpt);

        LxNx.ModelOptions modoptoons = new LxNx.ModelOptions();
        modoptoons.ModelOption = lstModOptions;

        List<LxNx.ModelRequest> lstModReq =new List<LxNx.ModelRequest>();
        LxNx.ModelRequest nodrequest = new LxNx.ModelRequest();
        nodrequest.ModelName = null;
        nodrequest.ModelOptions =modoptoons;
        lstModReq.add(nodrequest);

        LxNx.ModelRequests model = new LxNx.ModelRequests();
        model.ModelRequest = lstModReq;

        inc.ModelRequests = model;
        optObj.IncludeModels = inc;

        LxNx.RequireExactMatch reqExactMatch = new  LxNx.RequireExactMatch();
        reqExactMatch.LastName = false;
        reqExactMatch.FirstName = false;
        reqExactMatch.FirstNameAllowNickname= false;
        reqExactMatch.Address= false;
        reqExactMatch.HomePhone = false;
        reqExactMatch.SSN = false;
        reqExactMatch.DriverLicense= false;
        optObj.RequireExactMatch = reqExactMatch;

        List<object> WatchList = new List<object>();
        WatchList.add(null);
        LxNx.WatchLists watch = new LxNx.WatchLists();
        //WatchList.add(watch);
        watch.WatchList = WatchList;

        optObj.WatchLists= watch;

        flexObj.Options = optObj;
        rootObj.FlexIDRequest = flexObj;
        
        system.assertNotEquals(null,rootObj);
        
    }
}