//class with global "magic" numbers
public class CCGlobal {
    
    public static String removeLineBreak(String Address){
        String street = Address;
        if(street !=null && street.length()>0){
            street = street.replace('\r\n', ' ');
            street = street.replace('\n', ' ');
            street = street.replace('\r', ' ');
            
        }
        return street;
    }
    
    //RecordType
    public static map<Id, RecordType> map_Id_RecordType {
        get {
            if(map_Id_RecordType==null) {
                map_Id_RecordType=new map <Id, RecordType>([select id, Name, SobjectType from recordtype where Name!=null and SobjectType!=null]);
            }
            return map_Id_RecordType; 
        } set;
    }
    public static map<string, recordtype> map_RTNameObject_recordtype {
        get {
            if(map_RTNameObject_recordtype==null) {
                map_RTNameObject_recordtype=new map <string, recordtype>();
                for(recordtype rt: map_Id_RecordType.values()) { map_RTNameObject_recordtype.put(rt.Name + '_' + rt.SobjectType, rt);}
            }
            return map_RTNameObject_recordtype; 
        } set;
    }
    public static id getRecordTypeIdByName(string rt_name, string rt_sobjecttype) {
        system.assert(map_RTNameObject_recordtype.containskey(rt_name+'_' + rt_sobjecttype), 'Invalid record type name '+rt_name+' for the object: '+ rt_sobjecttype);
        return map_RTNameObject_recordtype.get(rt_name+'_' + rt_sobjecttype).Id;
    }   
    public static string getRecordTypeNameById(id rt_id) {
        system.assert(map_Id_RecordType.containskey(rt_id), 'Invalid record type Id: '+ rt_id);
        return map_Id_RecordType.get(rt_id).Name;
    }   
    //User
    public static Map<Id,User> m_Id_UserActive {
        get {
            if(m_Id_UserActive==null) m_Id_UserActive=new Map<Id,User>([SELECT Id,Name, username, UserRoleId, UserRole.Name, FIRSTNAME,LASTNAME, Email, Phone, Region__c, Street, City, State, PostalCode FROM User WHERE IsActive=true]); 
            return m_Id_UserActive;
        } set;
    }
    
    public static id idProductManager { //Craigh Roth
        get {
            if(idProductManager==null) for(User us: m_Id_UserActive.values()) { if(us.FIRSTNAME=='Craig' && us.LASTNAME=='Roth') { idProductManager=us.Id; break; }}
            return idProductManager;
        } set; 
    }
    public static id idConcierge { //Shared inside sales
        get {
            if(idConcierge==null) for(User us: m_Id_UserActive.values()) { if(us.FIRSTNAME=='Information' && us.LASTNAME=='ClearCaptions') {idConcierge=us.Id; break; }}
            return idConcierge;
        } set; 
    }   
    public static id idBoomi { //Dell Boomi Appliance
        get {
            if(idBoomi==null) for(User us: m_Id_UserActive.values()) { if(us.UserName.startsWithIgnoreCase('integration@clearcaptions.com')) { idBoomi=us.Id; break;}}
            return idBoomi;
        } set; 
    }   
    
    public static boolean IsTerritoryManager(id idUser) {
        boolean bReply=false;
        if(m_Id_UserActive.containsKey(idUser)) if(m_Id_UserActive.get(idUser).UserRole.Name=='CC Territory Manager') bReply=true;
        return bReply;
    }
    public static Map<string,User> m_Email_UserActive {
        get {
            if(m_Email_UserActive==null) { 
                m_Email_UserActive=new Map<string,User>(); 
                for(User us: m_Id_UserActive.values()) m_Email_UserActive.put(us.Email, us);
            }
            return m_Email_UserActive;
        } set;
    }
    
    //VFC009_Contact
    public static string VFC009_Contact_ProfileEmailEnabled { //Allow profile change emails
        get {
            if(VFC009_Contact_ProfileEmailEnabled==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('VFC009_Contact_ProfileEmailEnabled');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='VFC009_Contact_ProfileEmailEnabled', Value__c='false');
                    insert objCustomSetting;
                }
                VFC009_Contact_ProfileEmailEnabled=objCustomSetting.Value__c;
            }
            return VFC009_Contact_ProfileEmailEnabled;
        } 
    }       
    public static string VFC009_Contact_ProfileEmailTemplateId { //Template Id for emails
        get {
            if(VFC009_Contact_ProfileEmailTemplateId==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('VFC009_Contact_ProfileEmailTemplateId');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='VFC009_Contact_ProfileEmailTemplateId', Value__c='00X8A000000DbLB');
                    insert objCustomSetting;
                }
                VFC009_Contact_ProfileEmailTemplateId=objCustomSetting.Value__c;
            }
            return VFC009_Contact_ProfileEmailTemplateId;
        } 
    }       
    public static string VFC009_Contact_ProfileOrgEmailId { //Org wide email Id
        get {
            if(VFC009_Contact_ProfileOrgEmailId==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('VFC009_Contact_ProfileOrgEmailId');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='VFC009_Contact_ProfileOrgEmailId', Value__c='0D2C0000000CbPq');
                    insert objCustomSetting;
                }
                VFC009_Contact_ProfileOrgEmailId=objCustomSetting.Value__c;
            }
            return VFC009_Contact_ProfileOrgEmailId;
        } 
    }       
    //global functions replacement
    public static string companyEmailRegex { //Org wide email Id
        get {
            if(companyEmailRegex==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('Company Email RegEx');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='Company Email RegEx', Value__c='.*(@purple\\.us|@goamerica\\.com|@prcnet\\.net|@stellarrelay\\.com|@hovrs\\.com|@handsonsvs\\.com|@signlanguage\\.com|@purplenetwork\\.net|@clearcaptions\\.com).*');
                    insert objCustomSetting;
                }
                companyEmailRegex=objCustomSetting.Value__c;
            }
            return companyEmailRegex;
        } 
    }    
    public static string Integration_Is_Active { //claris test support
        get {
            if(Integration_Is_Active==null) {
                ClarisIntegrationSettings__c objCustomSetting=ClarisIntegrationSettings__c.getInstance('Integration_Is_Active');
                if(objCustomSetting==null) {
                    objCustomSetting=new ClarisIntegrationSettings__c(name='Integration_Is_Active', Value__c='false');
                    insert objCustomSetting;
                }
                Integration_Is_Active=objCustomSetting.Value__c;
            }
            return Integration_Is_Active;
        } 
    }       
    public static string CCDaily_CompleteFollowUp_TemplateId { //Email template id for campaign follow up reminder
        get {
            if(CCDaily_CompleteFollowUp_TemplateId==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('CCDaily_CompleteFollowUp_TemplateId');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='CCDaily_CompleteFollowUp_TemplateId', Value__c='00X1A000001LW58');
                    insert objCustomSetting;
                }
                CCDaily_CompleteFollowUp_TemplateId=objCustomSetting.Value__c;
            }
            return CCDaily_CompleteFollowUp_TemplateId;
        } 
    }
    public static final id PRODUCTION_ORG_ID='00D2900000098dFEAQ';
    public static final id CURRENT_ORG_ID=UserInfo.getOrganizationId();
    
    public static boolean IsProductionOrg() {
        if(PRODUCTION_ORG_ID==CURRENT_ORG_ID) return true; 
        return false;
    }
    
    public static string sURL_wsValidateAddress {
        get {
            if(sURL_wsValidateAddress==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('URL_wsValidateAddress');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='URL_wsValidateAddress', Value__c=(CCGlobal.IsProductionOrg())?('https://websvc.prod.purple.us/UserVerification/ValidateAddress?input='):('https://websvc.dev.purple.us/UserVerification/ValidateAddress?input='));
                    insert objCustomSetting;
                }
                sURL_wsValidateAddress=objCustomSetting.Value__c;
            }
            return sURL_wsValidateAddress;
        } set; 
    }
    public static string sAuthorizationWebsvc {
        get {
            if(sAuthorizationWebsvc==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('AuthorizationWebsvc');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='AuthorizationWebsvc', Value__c=(CCGlobal.IsProductionOrg())?('REPLACEWITH PRODUCTION: Basic c2FsZXNmb3JjZS53ZWI6RzgzI3R3SEYzQHE3'):('Basic c2FsZXNmb3JjZS53ZWI6RzgzI3R3SEYzQHE3'));
                    insert objCustomSetting;
                }
                sAuthorizationWebsvc=objCustomSetting.Value__c;
            }
            return sAuthorizationWebsvc;
        } set; 
    }    
    //VFC017_CaseComment
    public static string VFC017_CaseComment_EmailEnabled { //Allow comment notification emails
        get {
            if(VFC017_CaseComment_EmailEnabled==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('VFC017_CaseComment_EmailEnabled');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='VFC017_CaseComment_EmailEnabled', Value__c='false');
                    insert objCustomSetting;
                }
                VFC017_CaseComment_EmailEnabled=objCustomSetting.Value__c;
            }
            return VFC017_CaseComment_EmailEnabled;
        } 
    }   
    //sort SelectOption
    public static List<SelectOption> SortSelectOptions(List<SelectOption> lSO) {
        map<string,SelectOption> m_key_SO=new map<string,SelectOption>();
        list<string> lKey=new list<string>();
        for(SelectOption so: lSO) {
            m_key_SO.put(so.getlabel(),so);
            lKey.add(so.getlabel()); 
        }
        lKey.sort();
        List<SelectOption> lSO_Sorted=new List<SelectOption>();
        for(string sKey: lKey) lSO_Sorted.add(m_key_SO.get(sKey));
        return lSO_Sorted;
    }
    
    public static boolean IsSandbox {
        get {
            if(IsSandbox==null) {
                Organization objCurrentOrg=[select Id, IsSandbox from Organization limit 1];
                IsSandbox=objCurrentOrg.IsSandbox;
            }
            return IsSandbox;
        } set;
    }       
    public static string VFC021_SendProspect_LogCalls { //Controls log level for GetIO
        get {
            if(VFC021_SendProspect_LogCalls==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('VFC021_SendProspect_LogCalls');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='VFC021_SendProspect_LogCalls', Value__c='false');
                    insert objCustomSetting;
                }
                VFC021_SendProspect_LogCalls=objCustomSetting.Value__c;
            }
            return VFC021_SendProspect_LogCalls;
        } 
    }  
    public static string VFC021_SendProspect_AccessToken { //Stores access token
        get {
            if(VFC021_SendProspect_AccessToken==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('VFC021_SendProspect_AccessToken');
                if(objCustomSetting==null) {
                    if(CCGlobal.IsSandbox) objCustomSetting=new GlobalConfigurationVariable__c(name='VFC021_SendProspect_AccessToken', Value__c='9672335A-2B40-43DA-A231-8515438601C1'); //sandbox
                    else objCustomSetting=new GlobalConfigurationVariable__c(name='VFC021_SendProspect_AccessToken', Value__c='9672335A-2B40-43DA-A231-8515438601C1-NOTVALIDREPLACEWITHREALTOKEN');
                    insert objCustomSetting;
                }
                VFC021_SendProspect_AccessToken=objCustomSetting.Value__c;
            }
            return VFC021_SendProspect_AccessToken;
        } 
    }
    public static string VFC021_SendProspect_CampaignId { //Stores cc.com web form campaign id
        get {
            if(VFC021_SendProspect_CampaignId==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('VFC021_SendProspect_CampaignId');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='VFC021_SendProspect_CampaignId', Value__c='701c0000000Vnv3'); 
                    insert objCustomSetting;
                }
                VFC021_SendProspect_CampaignId=objCustomSetting.Value__c;
            }
            return VFC021_SendProspect_CampaignId;
        } 
    }    
    
    public static boolean IsValidEmailAddress(string sEmail) {
        if(sEmail==null || sEmail=='') return false; 
        Pattern oEmailPattern = Pattern.compile('^[a-z0-9._%+-/!#$%&\'*=?^_`{|}~]+@[a-z0-9.-]+\\.[a-z]{2,4}$');
        return oEmailPattern.matcher(sEmail.trim().toLowerCase()).matches();
    }
    
    public static string sPhone2Like(string sPhone) {
        if(sPhone==null || sPhone=='') return '-1111111111';
        if(sPhone.length()!=10) return '-1111111111';
        return '%'+sPhone.substring(0,3)+'%'+sPhone.substring(3,6)+'%'+sPhone.substring(6,8)+'%'+sPhone.substring(8,10)+'%';
    }
    public static string CC15Min_Enable_Integration_Monitor {
        get {
            if(CC15Min_Enable_Integration_Monitor==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('CC15Min_Enable_Integration_Monitor');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='CC15Min_Enable_Integration_Monitor', Value__c='false');
                    insert objCustomSetting;
                }
                CC15Min_Enable_Integration_Monitor=objCustomSetting.Value__c;
            }
            return CC15Min_Enable_Integration_Monitor;
        } 
    }  
    public static string CC15Min_Enable_Integration_Restart {
        get {
            if(CC15Min_Enable_Integration_Restart==null) {
                GlobalConfigurationVariable__c objCustomSetting=GlobalConfigurationVariable__c.getInstance('CC15Min_Enable_Integration_Restart');
                if(objCustomSetting==null) {
                    objCustomSetting=new GlobalConfigurationVariable__c(name='CC15Min_Enable_Integration_Restart', Value__c='false');
                    insert objCustomSetting;
                }
                CC15Min_Enable_Integration_Restart=objCustomSetting.Value__c;
            }
            return CC15Min_Enable_Integration_Restart;
        } 
    }      
}