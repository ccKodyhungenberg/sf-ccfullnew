global class BatchUpdate implements  Schedulable, Database.Batchable <SObject> {

    global Database.QueryLocator start(Database.BatchableContext bc){
    string status = 'New';
    boolean convertedStatus= False;
    date recordsfrom = Date.newInstance(2020, 01, 15);
   
    LIST<INTEGER> NUM = NEW LIST<INTEGER>();
    NUM.ADD(3);
    NUM.ADD(14);
    NUM.ADD(30);
    
      String Query='Select id,Batch_Update__c,LeadSource,Lead_Aging__c  from Lead where createddate > :recordsfrom and  isconverted=:convertedStatus and  status = :status and Lead_Aging__c IN : NUM ' ;

        return Database.getQueryLocator(Query);
            }

    global void execute(Database.BatchableContext bc, List<Lead> scope){
        for(Lead l: scope){
           
            l.Batch_Update__c=l.Lead_Aging__c ;
        }
        update scope;
    }

    global void finish(Database.BatchableContext bc){
        
    }
    
    global void execute (SchedulableContext SC) {
        Database.executeBatch(new BatchUpdate ());
    }
        
}