public class CC_OrderItemTriggerHandler {
    public static void onBeforeInsert(List<OrderItem> newList){
        CheckForDuplicates(newList,null);
    }
    public static void onBeforeupdate(List<OrderItem> newList, Map<Id,OrderItem> oldMap){
        CheckForDuplicates(newList,oldMap);
    }
    
    private static void CheckForDuplicates(List<OrderItem> newList,Map<Id,OrderItem> oldMap){
        Set<Id> orderIdSet = new Set<id>();
        List<String> serialNumList = new List<String>();
        Set<String> serialNumSet = new Set<String>();
        Map<Id,List<OrderItem>> orderIdToOrderItemListMap = new Map<Id, List<OrderItem>>();
        for(OrderItem newOi: newList){
            if(oldMap==null && newOi.OrderId != null && (newOi.Product2Id != null || newOi.Serial_Number__c != null)){
                orderIdSet.add(newOi.OrderId);
                serialNumList.add(newOi.Serial_Number__c);
                serialNumSet.add(newOi.Serial_Number__c);
            }
            if(oldMap!=null && (newOi.Product2Id != oldMap.get(newOi.id).Product2Id || newOi.Serial_Number__c != oldMap.get(newOi.id).Serial_Number__c)){
                orderIdSet.add(newOi.OrderId);
            }
        }
        if(orderIdSet.size() > 0 ){
          Id orderProductRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Product Order').getRecordTypeId();
          Id orderPartsRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Parts Order').getRecordTypeId();  //added by Jason 20Dec
            for(Order ord: [SELECT id,RecordTypeId, (SELECT id, Product2Id, Serial_Number__c FROM OrderItems) 
                            FROM Order 
                            WHERE Id IN : orderIdSet
                            AND RecordTypeId <> :orderProductRecordTypeId
                            AND RecordTypeId <> :orderPartsRecordTypeId]){
                orderIdToOrderItemListMap.put(ord.Id, ord.OrderItems);
            }
        }
                
        
        if(orderIdToOrderItemListMap.size()>0){
            for(OrderItem oitem: newList){
                if(orderIdToOrderItemListMap.containsKey(oitem.OrderId)){
                    
                    for(OrderItem oi: orderIdToOrderItemListMap.get(oitem.OrderId)){
                        if(oi.Product2Id == oitem.Product2Id || (oi.Serial_Number__c == oitem.Serial_Number__c && oItem.Serial_number__c != null)){
                            oitem.Serial_Number__c.addError('Records With Duplicate Products and Serial Number cannot be inserted');
                        }
                    }
                }
            }
        }
        if(serialNumList.size() != serialNumSet.size()){
            for(OrderItem oitem: newList){
              if( orderIdToOrderItemListMap.containskey(oitem.OrderId)) {
                oitem.Serial_Number__c.addError('Records With Duplicate Products and Serial Number cannot be inserted');
              }
            }
        }
    }
}