global class LexisNexisMock implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req) {
       
            // Create a fake response
            HttpResponse res = new HttpResponse();
            System.assertEquals('GET', req.getMethod());
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        		
        	String response =  '   {  '  + 
                                 '     "FlexIDResponseEx": {  '  + 
                                 '       "@xmlns": "http://webservices.seisint.com/WsIdentity",  '  + 
                                 '       "response": {  '  + 
                                 '         "Header": {  '  + 
                                 '           "Status": 0  '  + 
                                 '         },  '  + 
                                 '         "Result": {  '  + 
                                 '           "InputEcho": {  '  + 
                                 '             "Name": {  '  + 
                                 '               "Full": "BROOKE ABALOS",  '  + 
                                 '               "First": "BROOKE",  '  + 
                                 '               "Last": "ABALOS"  '  + 
                                 '             },  '  + 
                                 '             "DOB": {  '  + 
                                 '               "Year": 1950,  '  + 
                                 '               "Month": 10,  '  + 
                                 '               "Day": 3  '  + 
                                 '             },  '  + 
                                 '             "Age": 0,  '  + 
                                 '             "SSN": "265479555"  '  + 
                                 '           },  '  + 
                                 '           "UniqueId": "385135712",  '  + 
                                 '           "VerifiedSSN": "265479555",  '  + 
                                 '           "NameAddressPhone": {  '  + 
                                 '             "Summary": "2",  '  + 
                                 '             "Type": "S"  '  + 
                                 '           },  '  + 
                                 '           "VerifiedElementSummary": {  '  + 
                                 '             "DOB": false,  '  + 
                                 '             "DOBMatchLevel": "0"  '  + 
                                 '           },  '  + 
                                 '           "ValidElementSummary": {  '  + 
                                 '             "SSNValid": true,  '  + 
                                 '             "SSNDeceased": false,  '  + 
                                 '             "PassportValid": false,  '  + 
                                 '             "AddressPOBox": false,  '  + 
                                 '             "AddressCMRA": false,  '  + 
                                 '             "SSNFoundForLexID": true  '  + 
                                 '           },  '  + 
                                 '           "NameAddressSSNSummary": 9,  '  + 
                                 '           "ComprehensiveVerification": {  '  + 
                                 '             "ComprehensiveVerificationIndex": 20,  '  + 
                                 '             "RiskIndicators": {  '  + 
                                 '               "RiskIndicator": [  '  + 
                                 '                 {  '  + 
                                 '                   "RiskCode": "MI",  '  + 
                                 '                   "Description": "Multiple identities associated with the input SSN",  '  + 
                                 '                   "Sequence": 1  '  + 
                                 '                 },  '  + 
                                 '                 {  '  + 
                                 '                   "RiskCode": "78",  '  + 
                                 '                   "Description": "The input address was missing",  '  + 
                                 '                   "Sequence": 2  '  + 
                                 '                 },  '  + 
                                 '                 {  '  + 
                                 '                   "RiskCode": "28",  '  + 
                                 '                   "Description": "Unable to verify date-of-birth",  '  + 
                                 '                   "Sequence": 3  '  + 
                                 '                 },  '  + 
                                 '                 {  '  + 
                                 '                   "RiskCode": "NB",  '  + 
                                 '                   "Description": "No date-of-birth reported for the input identity",  '  + 
                                 '                   "Sequence": 4  '  + 
                                 '                 },  '  + 
                                 '                 {  '  + 
                                 '                   "RiskCode": "80",  '  + 
                                 '                   "Description": "The input phone was missing or incomplete",  '  + 
                                 '                   "Sequence": 5  '  + 
                                 '                 }  '  + 
                                 '               ]  '  + 
                                 '             }  '  + 
                                 '           },  '  + 
                                 '           "CustomComprehensiveVerification": {  '  + 
                                 '             "ComprehensiveVerificationIndex": 0  '  + 
                                 '           },  '  + 
                                 '           "InstantIDVersion": "1",  '  + 
                                 '           "EmergingId": false  '  + 
                                 '         }  '  + 
                                 '       }  '  + 
                                 '     }  '  + 
                                 '  }  ' ; 
        res.setBody(response);    
        res.setStatusCode(200);
            return res;
            
     }
}