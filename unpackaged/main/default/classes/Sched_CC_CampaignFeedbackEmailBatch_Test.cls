/*
 * Appirio, Inc
 * Name: Sched_CC_CampaignFeedbackEmailBatch_Test
 * Description: S-611297 - Covergae for the CC_CampaignFeedbackEmailBatch Batch
 * Created Date: 28 Mar, 2019
 * Created By: Nilesh Grover
 * 
 * Date Modified                Modified By                  Description of the update
 */
 

@isTest
private class Sched_CC_CampaignFeedbackEmailBatch_Test {
	
	@isTest static void testCampaignFeedbackEmailBatch() {
		List<Account> accList = CC_TestUtility.createAccount(1, true);
        system.assertEquals(1, [SELECT Id FROM Account].size());
        
        List<Contact> contactList = CC_TestUtility.createContact(1, true, 'Customer',accList[0].Id , 'Test123@gmail.com');
        system.assertEquals(1, [SELECT Id FROM Contact].size());
		
		List<Campaign> campList = CC_TestUtility.createCampaign(2,false);
		for(Campaign cmp : campList) {
			cmp.Event_POC__c = UserInfo.getUserId();
			cmp.StartDate = System.Today().adddays(-32) ;
            cmp.EndDate = System.Today().adddays(-2);
			cmp.Other_Comments__c = 'Test Other Comments';
		}
		insert campList;
		System.debug([SELECT Id, Event_POC__r.email FROM Campaign]);

		Date lastEmailDate = Date.today().addDays(-32);
		Date firstEmailDate = Date.today().addDays(-2);
		System.debug([SELECT id FROM Campaign WHERE EndDate >= :lastEmailDate AND EndDate <= :firstEmailDate]);
		System.assertEquals(2,[SELECT Id FROM Campaign].size());
		test.startTest();
    	Sched_CC_CampaignFeedbackEmailBatch sc = new Sched_CC_CampaignFeedbackEmailBatch();
    	String schedule = '0 0 23 * * ?';
    	system.schedule('Scheduled Batch', schedule, sc);
    	test.stopTest();
		//Database.executeBatch(new CC_CampaignFeedbackEmailBatch(null));
	}	
}