@isTest
private class CC_SalesPortalPageControllerTest {
    public static List<Order> orderList;
    
    
    public static List<WorkOrder> workOrderList;
   public static void  createTestData(){
        List<Account> accList = CC_TestUtility.createAccount(1,true);
        List<Contact> customerContact = CC_TestUtility.createContact(1, false, 'Customer', accList[0].id,'test@test.com');
        customerContact[0].MailingLatitude=10.0;
       customerContact[0].MailingLongitude=20.0;
       
       insert customerContact;
        //List<Contact> customerContact1 = CC_TestUtility.createContact(1, true, 'Technician', accList[0].id,'test@testContact.com');
        
         orderList = CC_TestUtility.createOrder(1, false,accList[0].id);
        List<Product2> productList = CC_TestUtility.createProduct(1, false);
        
        productList[0].Is_Default_Product__c = true;
      productList[0].Is_Portal_Product__c = true;
        insert productList;
        
        List<PricebookEntry> priceBookEntryList = CC_TestUtility.createPriceBookEntry(1,productList[0].id,true);
        orderList[0].status = 'Open';
        //insert orderList;
        
        //Technician__C techRecord = new Technician__C(First_Name__c = 'Test', Last_Name__c='Technician', Technician_Email__c = 'tech@email.com');
         workOrderList = CC_TestUtility.createWorkOrder(1,false,orderList[0].id);
        workOrderList[0].Status = 'Scheduled-Exception';
        workOrderList[0].ContactId = customerContact[0].id;
        //workOrderList[0].Technician__C = techRecord.id;
        //insert workOrderList;
    }
    @isTest
    static void validateSalesPortalController(){
        
        createTestData();
        Account acc= [SELECT id FROM Account LIMIT 1];
        System.assertNotEquals(null, acc.Id);
        
        Contact con = [SELECT Id, FirstName, LastName,MailingState, MailingPostalCode, Labor_Source__c,MailingLatitude,MailingLongitude
                       FROM Contact 
                       LIMIT 1];
        System.assertNotEquals(null, con.id);
        
        //WorkOrder woRecord = [SELECT Order__c, Technician__C,StartDate,Status,Labor_Source__c,Start_Time_Window__c FROM WorkOrder LIMIT 1];
         
        Product2 prod = [SELECT NAme FROM Product2 LIMIT 1];
        
       // Order oRecord = [SELECT AccountId, EffectiveDate, ShippingCity, ShippingState, ShippingPostalCode, Shipping_Name__c,
        //ShippingStreet, Customer_Name__c, Status, Customer_Street__c,Fulfillment__c,Contact__c FROM Order LIMIT 1];
        
        Test.setCurrentPageReference(new PageReference('Page.CC_SalesPortalPage')); 
		System.currentPageReference().getParameters().put('id', con.Id);
        
        CC_SalesPortalPageController sppc = new CC_SalesPortalPageController();
        PageReference pageRef = Page.CC_SalesPortalPage;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', con.id);
        sppc.sConfirmAddress_Suggestion = '';
        sppc.sCurrent_LabourSource = 'Barrister';
        sppc.currentRow='1';
        sppc.sConfirmAddress_Suggestion = '';
        sppc.iSuggestion_index = 0;
        //map<string, Postal_Code__c> m_Zip_PostalCodes=sppc.m_Zip_PostalCode;
        
        //sppc.validateAddress();
        sppc.newOrder = CC_TestUtility.createOrder(1,false,acc.id)[0];
        sppc.locateCustomer();
        sppc.newOrder = CC_testUtility.createOrder(1, false,acc.id)[0];
        sppc.newOrder.Customer_Email__c = 'test@test.com';
        sppc.newOrder.Customer_Phone__c = '1234567890';
        sppc.locateCustomer();
        sppc.newOrder = CC_testUtility.createOrder(1, false,acc.id)[0];
        sppc.newOrder.Customer_Email__c = '123@123.com';
        sppc.newOrder.Customer_Phone__c = '1234567';
        sppc.newOrder.contact__c = con.id;
        sppc.locateCustomer();
        sppc.getProducts();
        CC_SalesPortalPageController.bNeedBuildList = true;
        List<selectOption> selectOptionList = sppc.getProducts();
        
         sppc.m_Id_Technician.put(con.Id, con);
        
        sppc.LabourSourceChange();
        sppc.existConId = con.id;
        sppc.getExistingContact();
        pageRef.getParameters().put('rowIndex', '1');
        sppc.addRow();
        sppc.deleteRow();
        List<Lead> leadList = CC_TestUtility.createLead(1, false);
        sppc.newLead = leadList[0];
        sppc.newLead.LeadSource = null;
        
        sppc.saveLead();
        sppc.callWebService();
        //sppc.SelectSuggestion();
        sppc.password = 'Test@123';
        //sppc.SaveTransaction();
        List<String> msgList = new List<String>();
        
       CC_SalesPortalPageController.woOrderWrapper woOrderWrapperClass 
        = new CC_SalesPortalPageController.woOrderWrapper(workorderList[0], orderList[0],selectOptionList,String.valueOf(prod.id));
        List<CC_SalesPortalPageController.woOrderWrapper> woList = new List<CC_SalesPortalPageController.woOrderWrapper>();
        /*CC_SalesPortalPageController.woOrderWrapper woOrderWrapperClass1 
        = new CC_SalesPortalPageController.woOrderWrapper(woRecord, oRecord,selectOptionList,String.valueOf(prod.id));
        //List<CC_SalesPortalPageController.woOrderWrapper> woList = new List<CC_SalesPortalPageController.woOrderWrapper>();*/
        woList.add(woOrderWrapperClass);
        //woList.add(woOrderWrapperClass1);
       sppc.newWorkOrderWrapperList = woList;
        //sppc.newWorkOrderWrapperList.add(woOrderWrapperClass);
        
       /*CC_SalesPortalPageController.clResult resultClass = new CC_SalesPortalPageController.clResult();
        resultClass.Callback = '';
        resultClass.ElapsedTime = '';
        resultClass.HRef = '';
        resultClass.Messages = msgList ;
        resultClass.Server = '';
        resultClass.Status = '';
        resultClass.Version = '';*/
        sppc.SaveTransaction();
        //create an instance of wrapper class
        CC_SalesPortalPageController.clSuggestion sugClsWrapper = new CC_SalesPortalPageController.clSuggestion();
        //sugClsWrapper.AddressIsHomeOrWork = 'Work';
        sugClsWrapper.City = 'ROCKLIN';
        sugClsWrapper.State = 'CA';
        sugClsWrapper.Street = '595 MENLO DR';
        sugClsWrapper.Zip = '95765';
        sugClsWrapper.APT = '';
        CC_SalesPortalPageController.clValidateAddressResult valAddResult = new CC_SalesPortalPageController.clValidateAddressResult();
        valAddResult.Code = '';
        valAddResult.Message = '';
        valAddResult.Result = '';
        //valAddResult.Suggestions.add(sugClsWrapper);
        
        CC_SalesPortalPageController.clSuggestion_Item sugItemClsWrapper = new CC_SalesPortalPageController.clSuggestion_Item();
        sugItemClsWrapper.index = 1; 
        sugItemClsWrapper.sAddress = '';
        sugItemClsWrapper.oSuggestion = sugClsWrapper;
        
        System.debug(sugItemClsWrapper);
        sppc.lSuggestion_Item = new List<CC_SalesPortalPageController.clSuggestion_Item>();
        sppc.lSuggestion_Item.add(sugItemClsWrapper);
        System.debug(sppc.lSuggestion_Item);
        
        sppc.SelectSuggestion();
        //sppc.m_Id_Technician.put(con.Id, con);
        //Postal_Code__c postalCodeRecord = new Postal_Code__c(Name='57005', Lat__c=202.5937, Lng__c = 787.9629);
        //insert postalCodeRecord;
        //sppc.m_Zip_PostalCode.put('testString',postalCodeRecord);
        
    }
    public class woOrderWrapper{
        public WorkOrder newWorkOrderWrap {get;set;}
        public Order newOrderWrap{get;set;}
        public List<SelectOption> technicianList{get;set;}
        public String selectedProductWrap{get;set;}
        public woOrderWrapper(WorkOrder newWorkOrderWrap, Order newOrderWrap, List<SelectOption> technicianList,String selectedProductWrap){
            this.newWorkOrderWrap = newWorkOrderWrap;
            this.newOrderWrap = newOrderWrap;
            this.technicianList = technicianList;
            this.selectedProductWrap=selectedProductWrap;
        }
    }
    
    @isTest
    static void testValidateAddress()
    {
        createTestData();
        Contact con = [SELECT Id, FirstName, LastName,MailingState, MailingPostalCode, Labor_Source__c
                       FROM Contact 
                       LIMIT 1];
        
		System.currentPageReference().getParameters().put('id', con.Id);
        Test.setMock(HttpcalloutMock.class, new MockCC_SalesPortalController('1'));
        Test.startTest();
        CC_SalesPortalPageController sppc = new CC_SalesPortalPageController();
        sppc.validateAddress();
        Test.stopTest();
    } 
    
     @isTest
    static void testCreateCCWSUSer()
    {
        createTestData();
        Contact con = [SELECT Id, FirstName, LastName,MailingState, MailingPostalCode, Labor_Source__c
                       FROM Contact 
                       LIMIT 1];
        
        Test.setMock(HttpcalloutMock.class, new MockCC_SalesPortalController('2'));
        Test.startTest();
        CC_SalesPortalPageController sppc = new CC_SalesPortalPageController();
        sppc.existConId = con.id;
        sppc.getExistingContact();
        sppc.createCCWSUser();
        Test.stopTest();
    } 
}