@isTest
public class CCGlobal_Test {
    static testMethod void testCCGlobal() {
        Test.startTest();
        //assuming we have at least one RecordType defined
        system.assert(!CCGlobal.map_Id_RecordType.isEmpty(), 'We should have at least one RecordType defined in the system');
        RecordType rt=CCGlobal.map_Id_RecordType.values()[0];
        system.assert(CCGlobal.getRecordTypeIdByName(rt.Name, rt.SobjectType)==rt.Id, 'CCGlobal.getRecordTypeIdByName did not return correct id of the existing record type: '+rt.Name+' for object '+rt.SobjectType);
        system.assert(CCGlobal.getRecordTypeNameById(rt.Id)==rt.Name, 'CCGlobal.getRecordTypeNameById did not return correct RecordType Name for an existing Id: '+rt.Id);
        //system.assert(CCGlobal.idProductManager!=null, 'CCGlobal.idProductManager cannot be null, used in case assignment for un-qualified customers when Service Request is scheduled');
        //system.assert(CCGlobal.idConcierge!=null, 'CCGlobal.idConcierge cannot be null, used in automated assignments of ownership');
        //system.assert(CCGlobal.idBoomi!=null, 'CCGlobal.idBoomi cannot be null, used in customer provisioning and integration');
        system.assert(CCGlobal.IsTerritoryManager(UserInfo.getUserId())==false, 'CCGlobal.IsTerritoryManager identified running user as Territory Manager');
        system.debug(CCGlobal.VFC009_Contact_ProfileEmailEnabled); 
        system.debug(CCGlobal.VFC009_Contact_ProfileEmailTemplateId);
        system.debug(CCGlobal.VFC009_Contact_ProfileOrgEmailId);
        system.debug(CCGlobal.companyEmailRegex);
        system.debug(CCGlobal.Integration_Is_Active);
        system.debug(CCGlobal.CCDaily_CompleteFollowUp_TemplateId);
        system.debug(CCGlobal.IsProductionOrg());
        system.debug(CCGlobal.sURL_wsValidateAddress);
        system.debug(CCGlobal.sAuthorizationWebsvc);
        system.debug(CCGlobal.VFC017_CaseComment_EmailEnabled);
        system.debug(CCGlobal.m_Email_UserActive);
        system.debug(CCGlobal.IsSandbox);
        system.debug(CCGlobal.VFC021_SendProspect_AccessToken);
        system.debug(CCGlobal.VFC021_SendProspect_LogCalls);
        system.debug(CCGlobal.VFC021_SendProspect_CampaignId);
        system.assert(CCGlobal.IsValidEmailAddress('test@test.com')==true,'CCGlobal.IsValidEmailAddress should identify test@test.com as valid');
        system.assert(CCGlobal.IsValidEmailAddress('test@test,com')==false,'CCGlobal.IsValidEmailAddress should identify test@test,com as not valid');
        system.assert(CCGlobal.sPhone2Like('ababa')=='-1111111111','CCGlobal.sPhone2Like should return "-1111111111" for "ababa"');
        system.assert(CCGlobal.sPhone2Like('9998887766')=='%999%888%77%66%','CCGlobal.sPhone2Like should return "%999%888%77%66%" for "9998887766"');
        system.debug(CCGlobal.CC15Min_Enable_Integration_Monitor);
        system.debug(CCGlobal.CC15Min_Enable_Integration_Restart);
        Test.stopTest();
    }
}