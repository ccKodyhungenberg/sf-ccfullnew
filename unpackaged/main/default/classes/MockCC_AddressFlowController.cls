@isTest
global class MockCC_AddressFlowController implements HttpCalloutMock{
    String street = '';
    global MockCC_AddressFlowController(String street){
        this.street = street;
    } 
    global HTTPResponse respond(HTTPRequest req) {
        if(Street == 'BRANDON'){
            // Create a fake response
            HttpResponse res = new HttpResponse();
            System.assertEquals('POST', req.getMethod());
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            //res.setBody('data={"apt": "APT 125","city": "Grand Rapids","country": "","e911LocationID": null,"primary": false,"state": "MI","street": "100 50TH ST SW","types": [],"zip": "49548","id": null,"type": "address"}');
            res.setBody('{"code": 200,"data": {"apt": "APT 125","city": "Grand Rapids","country": "","e911LocationID": null,"primary": false,"state": "MI","street": "100 50TH ST SW","types": [],"zip": "49548","id": null,"type": "address"},"message": "","result": true}');
            res.setStatusCode(200);
            return res;
            
        }
        else{
            // Create a fake response
            HttpResponse res = new HttpResponse();
            System.assertEquals('POST', req.getMethod());
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            //res.setBody('data={"apt": "APT 125","city": "Grand Rapids","country": "","e911LocationID": null,"primary": false,"state": "MI","street": "100 50TH ST SW","types": [],"zip": "49548","id": null,"type": "address"}');
            res.setBody('{"code": 607,"data":{"suggestedAddresses": [{"street": "100 50TH ST SW","apt": "APT 125","city": "GRAND RAPIDS","state": "MI","zip": "49548"}]},"message": "","result": false}');
            res.setStatusCode(603);
            return res;
        }        
    }   
    
}