// Modified NBOCK 5.9.2019 C-00255939
global class CCWSAssetHistoryTriggerHandler {
  
  
  
    public static void tr_After_Insert(List<CCWS_Asset_History__c> triggerNew, List<CCWS_Asset_History__c> triggerOld, Map<Id,CCWS_Asset_History__c> triggerNewMap, Map<Id,CCWS_Asset_History__c> triggerOldMap) {
        Map<string, CCWS_Asset_History__c> m_SNUID_CCWSDeviceH=new Map<string, CCWS_Asset_History__c>();
        
        for(CCWS_Asset_History__c devh : triggerNew ) { 
            if(m_SNUID_CCWSDeviceH.containsKey(devh.deviceID__c+'#'+devh.userID__c)) {
                if(m_SNUID_CCWSDeviceH.get(devh.deviceID__c+'#'+devh.userID__c).lastUpdated__c < devh.lastUpdated__c) {
                  m_SNUID_CCWSDeviceH.put(devh.deviceID__c+'#'+devh.userID__c,devh);System.debug('>>if keyset>>'+m_SNUID_CCWSDeviceH.keyset());System.debug('>>if values>>'+m_SNUID_CCWSDeviceH.values());
                }
            } else m_SNUID_CCWSDeviceH.put(devh.deviceID__c+'#'+devh.userID__c,devh);System.debug('>>else keyset>>'+m_SNUID_CCWSDeviceH.keyset());System.debug('>>else values>>'+m_SNUID_CCWSDeviceH.values());
        }
        //find existing devices.        
        for(Asset dev: [SELECT id, CB_deviceID_userID__c FROM Asset WHERE CB_deviceID_userID__c IN :m_SNUID_CCWSDeviceH.keySet()]) 
        {System.debug('>>dev>>'+dev);
            if(m_SNUID_CCWSDeviceH.containsKey(dev.CB_deviceID_userID__c))
              
             // m_SNUID_CCWSDeviceH.remove(dev.CB_deviceID_userID__c);	//Commented by Prachi for case 00271746 on 14/11/2019
     
         System.debug('>>after remove keyset>>'+m_SNUID_CCWSDeviceH.keyset());
         System.debug('>>after remove values>>'+m_SNUID_CCWSDeviceH.values());
        }
        //now in m_SNUID_CCWSDeviceH we have devices that did not get created
        //find connected CCWS_Asset__c
        set<string> st_deviceID=new set<string>();
        set<string> st_userID=new set<string>();
        for(string sSNSUID: m_SNUID_CCWSDeviceH.keySet()) { System.debug('>>sSNSUID>>'+sSNSUID);
            st_deviceID.add(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c);System.debug('>>st_deviceID>>'+st_deviceID);
            st_userID.add(m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c);System.debug('>>st_userID>>'+st_userID);
        }
        Map<string, CCWS_Asset__c> m_SN_CCWSDevice=new Map<string, CCWS_Asset__c>();
        for(CCWS_Asset__c devccws: [select id, 
            active__c
            ,code__c
            ,configVersion__c
            ,countryCode__c
            ,createdOn__c
            ,deviceID__c
            ,deviceState__c
            ,deviceType__c
            ,firmwareVersion__c
            ,generated__c
            ,lastCheckIn__c
            ,lastUpdated__c
            ,number__c
            ,partnerCode__c
            ,POMStatus__c
            ,PSTNState__c
            ,pushToken__c
            ,SIPLocalState__c
            ,SIPRemoteState__c
            ,type__c
            from CCWS_Asset__c where deviceID__c in :st_deviceID]
        ) {System.debug('>>devccws>>'+devccws);
            m_SN_CCWSDevice.put(devccws.deviceID__c ,devccws);
        }
        Map<string,Contact> m_userID_Contact=new map<string,Contact>();
        for(Contact ct: [select id, CB_userID__c,AccountId from Contact where CB_userID__c in :st_userID]) {
            m_userID_Contact.put(ct.CB_userID__c,ct);
        }
        //ready to create new devices
        Map<string, Asset> m_SNUID_Device2Upsert = new Map<string, Asset>();
        
        //Added By Sumit Tanwar to Map Product with Asset on 4th Sept, 2018 Start----
        Map<String,String> mapProductNameToProductId = new Map<String,String>();
        
        for( Product2 prod : [ SELECT Id,CCWS_Device_Type_Code__c FROM Product2 WHERE CCWS_Device_Type_Code__c IN: CCWSAssetTriggerHandler.m_deviceTypeId_Type.Keyset() ]) {
          mapProductNameToProductId.put(prod.CCWS_Device_Type_Code__c, prod.Id);
        }
        system.debug('m_SNUID_CCWSDeviceH'+m_SNUID_CCWSDeviceH);
        system.debug('m_SN_CCWSDevice'+m_SN_CCWSDevice);
        for(string sSNSUID: m_SNUID_CCWSDeviceH.keySet()) { 
            if(m_SN_CCWSDevice.containsKey(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c) && m_userID_Contact.containsKey(m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c)) {
                
                
                //build new device object for upsert
                Asset dev=new Asset(
                    Name = m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c
                    ,CB_deviceID_userID__c=m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c+'#'+m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c 
                    ,CountryCode__c= CCWSAssetTriggerHandler.getCountry(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).countryCode__c)
                    ,DateCreated__c=m_SNUID_CCWSDeviceH.get(sSNSUID).createdOn__c
                    ,Deleted__c= (m_SNUID_CCWSDeviceH.get(sSNSUID).active__c=='true')?false:true
                    ,Identifier__c= m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c
                    ,DeviceID__c= null
                    ,DeviceUserID__c= null
                    ,Last_Check_in__c= m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).lastCheckIn__c
                    ,LastUpdated__c= m_SNUID_CCWSDeviceH.get(sSNSUID).lastUpdated__c
                    ,Number__c= m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).number__c
                    ,PartnerCode__c= CCWSAssetTriggerHandler.getPartnerCode(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).partnerCode__c)
                    ,POMStatus__c= (m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).POMStatus__c=='true')?true:false
                    ,Type__c= CCWSAssetTriggerHandler.m_deviceTypeId_Type.get(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).deviceType__c)
                    ,SerialNumber=m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c
                    ,Status = 'Active'
                    ,InstallDate = Date.today()
                    , Paired_Date__C = Date.today()
                );
                
                system.debug('>><<<<'+m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).deviceType__c);
                system.debug('mapProductNameToProductId'+mapProductNameToProductId);
                //Added By Sumit Tanwar to Map Product with Asset on 4th Sept, 2018 Start----
                if( mapProductNameToProductId.containsKey( m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).deviceType__c ) ) {
                    //NBOCK C-00255939: if device code is 2, map to product with code 10 
                    if(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).deviceType__c == '2'){
                        dev.Product2Id = mapProductNameToProductId.get('10');
                    }
                    else{
                        dev.Product2Id = mapProductNameToProductId.get(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).deviceType__c);
                    }
                }//-----End
                
                if( sSNSUID != null && m_SNUID_CCWSDeviceH.containsKey(sSNSUID ) && m_SNUID_CCWSDeviceH.get(sSNSUID ) != null && m_userID_Contact.containsKey( m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c)  ) {
                  if( m_userID_Contact.get(m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c).AccountId != null ) {
                    dev.ContactId = m_userID_Contact.get(m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c).Id ;
                  }
                }
                
                m_SNUID_Device2Upsert.put(sSNSUID,dev);
            }
        }   
        system.debug('m_SNUID_Device2Upsert'+m_SNUID_Device2Upsert);
        if(!m_SNUID_Device2Upsert.isEmpty()) 
            upsert m_SNUID_Device2Upsert.values() CB_deviceID_userID__c;
    }
    public static void tr_After_Update(List<CCWS_Asset_History__c> triggerNew, List<CCWS_Asset_History__c> triggerOld, Map<Id,CCWS_Asset_History__c> triggerNewMap, Map<Id,CCWS_Asset_History__c> triggerOldMap) {
        Map<string, CCWS_Asset_History__c> m_SNUID_CCWSDeviceH=new Map<string, CCWS_Asset_History__c>();
        
        for(CCWS_Asset_History__c devh: triggerNew) { 
            if(m_SNUID_CCWSDeviceH.containsKey(devh.deviceID__c+'#'+devh.userID__c)) {
                if(m_SNUID_CCWSDeviceH.get(devh.deviceID__c+'#'+devh.userID__c).lastUpdated__c<devh.lastUpdated__c) m_SNUID_CCWSDeviceH.put(devh.deviceID__c+'#'+devh.userID__c,devh);
            } else m_SNUID_CCWSDeviceH.put(devh.deviceID__c+'#'+devh.userID__c,devh);
        }
        //find existing devices.        
        for(Asset dev: [select id, CB_deviceID_userID__c from Asset where CB_deviceID_userID__c in :m_SNUID_CCWSDeviceH.keySet()]) {
            if(m_SNUID_CCWSDeviceH.containsKey(dev.CB_deviceID_userID__c)) m_SNUID_CCWSDeviceH.remove(dev.CB_deviceID_userID__c);
        }
        //now in m_SNUID_CCWSDeviceH we have devices that did not get created
        //find connected CCWS_Asset__c
        set<string> st_deviceID=new set<string>();
        set<string> st_userID=new set<string>();
        for(string sSNSUID: m_SNUID_CCWSDeviceH.keySet()) { 
            st_deviceID.add(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c);
            st_userID.add(m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c);
        }
        Map<string, CCWS_Asset__c> m_SN_CCWSDevice=new Map<string, CCWS_Asset__c>();
        for(CCWS_Asset__c devccws: [select id, 
            active__c
            ,code__c
            ,configVersion__c
            ,countryCode__c
            ,createdOn__c
            ,deviceID__c
            ,deviceState__c
            ,deviceType__c
            ,firmwareVersion__c
            ,generated__c
            ,lastCheckIn__c
            ,lastUpdated__c
            ,number__c
            ,partnerCode__c
            ,POMStatus__c
            ,PSTNState__c
            ,pushToken__c
            ,SIPLocalState__c
            ,SIPRemoteState__c
            ,type__c
            from CCWS_Asset__c where deviceID__c in :st_deviceID]
        ) {
            m_SN_CCWSDevice.put(devccws.deviceID__c ,devccws);
        }
        Map<string,Contact> m_userID_Contact=new map<string,Contact>();
        for(Contact ct: [select id, CB_userID__c from Contact where CB_userID__c in :st_userID]) {
            m_userID_Contact.put(ct.CB_userID__c,ct);
        }

        Map<String,String> mapProductNameToProductId = new Map<String,String>();
        
        for( Product2 prod : [ SELECT Id,CCWS_Device_Type_Code__c FROM Product2 WHERE CCWS_Device_Type_Code__c IN: CCWSAssetTriggerHandler.m_deviceTypeId_Type.Keyset() ]) {
          mapProductNameToProductId.put(prod.CCWS_Device_Type_Code__c, prod.Id);
        }

        //ready to create new devices
        Map<string, Asset> m_SNUID_Device2Upsert=new Map<string, Asset>();
        
        for(string sSNSUID: m_SNUID_CCWSDeviceH.keySet()) { 
            if(m_SN_CCWSDevice.containsKey(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c) && m_userID_Contact.containsKey(m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c)) {
                //build new device object for upsert
                Asset dev=new Asset(
                    Name = m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c
                    ,CB_deviceID_userID__c=m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c+'#'+m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c 
                    ,ContactId= m_userID_Contact.get(m_SNUID_CCWSDeviceH.get(sSNSUID).userID__c).Id
                    ,CountryCode__c= CCWSAssetTriggerHandler.getCountry(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).countryCode__c)
                    ,DateCreated__c=m_SNUID_CCWSDeviceH.get(sSNSUID).createdOn__c
                    ,Deleted__c= (m_SNUID_CCWSDeviceH.get(sSNSUID).active__c=='true')?false:true
                    ,Identifier__c= m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c
                    ,DeviceID__c= null
                    ,DeviceUserID__c= null
                    ,Last_Check_in__c= m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).lastCheckIn__c
                    ,LastUpdated__c= m_SNUID_CCWSDeviceH.get(sSNSUID).lastUpdated__c
                    ,Number__c= m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).number__c
                    ,PartnerCode__c= CCWSAssetTriggerHandler.getPartnerCode(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).partnerCode__c)
                    ,POMStatus__c= (m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).POMStatus__c=='true')?true:false
                    ,Type__c= CCWSAssetTriggerHandler.m_deviceTypeId_Type.get(m_SN_CCWSDevice.get(m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c).deviceType__c)
                    ,InstallDate = Date.today()
                    ,SerialNumber=m_SNUID_CCWSDeviceH.get(sSNSUID).deviceID__c
                    ,Status = 'Active'
                    ,Paired_Date__C = Date.today()
                );
                m_SNUID_Device2Upsert.put(sSNSUID,dev);
            }
        }
        if(!m_SNUID_Device2Upsert.isEmpty()) upsert m_SNUID_Device2Upsert.values() CB_deviceID_userID__c;
    }
}