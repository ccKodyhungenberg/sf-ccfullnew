/**
 * Created by shnair on 4/22/2020.
 */

public class LexisNexisResponse {

    public FlexIDResponseEx FlexIDResponseEx{get;set;}
    public class VerifiedElementSummary{
        public String DOBMatchLevel{get;set;}
        public Boolean DOB{get;set;}
    }
    public class ValidElementSummary{
        public Boolean SSNValid{get;set;}
        public Boolean SSNDeceased{get;set;}
        public Boolean PassportValid{get;set;}
        public Boolean SSNFoundForLexID{get;set;}
        public Boolean AddressCMRA{get;set;}
        public Boolean AddressPOBox{get;set;}
    }
    public class RiskIndicators{
        public list<RiskIndicator> RiskIndicator{get;set;}
    }
    public class RiskIndicator{
        public String RiskCode{get;set;}
        public Integer Sequence{get;set;}
        public String Description{get;set;}
    }
    public class Result{
        public VerifiedElementSummary VerifiedElementSummary{get;set;}
        public String VerifiedSSN{get;set;}
        public Integer NameAddressSSNSummary{get;set;}
        public ComprehensiveVerification ComprehensiveVerification{get;set;}
        public ValidElementSummary ValidElementSummary{get;set;}
        public CustomComprehensiveVerification CustomComprehensiveVerification{get;set;}
        public NameAddressPhone NameAddressPhone{get;set;}
        public String InstantIDVersion{get;set;}
        public String UniqueId{get;set;}
        public Boolean EmergingId{get;set;}
        public InputEcho InputEcho{get;set;}
    }
    public class response{
        public Result Result{get;set;}
        public Header Header{get;set;}
    }
    public class NameAddressPhone{
        public String Summary{get;set;}
        public String Type{get;set;}
    }
    public class Name{
        public String Last{get;set;}
        public String First{get;set;}
        public String Full{get;set;}
    }
    public class InputEcho{
        public DOB DOB{get;set;}
        public Integer Age{get;set;}
        public Name Name{get;set;}
        public String SSN{get;set;}
    }
    public class Header{
        public Integer Status{get;set;}
    }
    public class FlexIDResponseEx{
        public response response{get;set;}
        //public String @xmlns{get;set;}
}
    public class DOB{
        public Integer Day{get;set;}
        public Integer Month{get;set;}
        public Integer Year{get;set;}
    }
    public class CustomComprehensiveVerification{
        public Integer ComprehensiveVerificationIndex{get;set;}
    }
    public class ComprehensiveVerification{
        public Integer ComprehensiveVerificationIndex{get;set;}
    public RiskIndicators RiskIndicators{get;set;}
    }

}