public class CC_TestUtility {
    public static List<Inventory_Count_Line_Item__c> createInventoryCountLineItem(Integer count, Id InventoryCountId,Id productId, Boolean isInsert){
        List<Inventory_Count_Line_Item__c> icliList = new List<Inventory_Count_Line_Item__c>();
        for(Integer i =0; i<count;i++){
            Inventory_Count_Line_Item__c icli = new Inventory_Count_Line_Item__c();
            icli.Product__c = productId;
            icli.Serial_Number__c = String.valueOf(i);
            icli.Quantity__c = i;
            icli.Inventory_count__c = InventoryCountId;
            icli.Status__c = 'Saved';
            icliList.add(icli);
        }
        
        if(isInsert){
            insert icliList;
        }
           
        return icliList;
    }
    public static List<Inventory_count__c> createInventoryCount(Integer count, Boolean isInsert){
        List<Inventory_count__c> icList = new List<Inventory_count__c>();
        for(Integer i =0; i<count;i++){
            Inventory_count__c ic = new Inventory_count__c();
            ic.Status__c = 'Saved';
            icList.add(ic);
        }
        
        if(isInsert){
            insert icList;
        }
           
        return icList;
    }
    public static List<Product2> createProduct(Integer count, Boolean isInsert){
        List<Product2> productList = new List<product2>();
        for(Integer i =0; i<count;i++){
            Product2 prod = new Product2();
            prod.Name = 'Test Product'+i;
            prod.IsActive = true;
            productList.add(prod);
        }
        
        if(isInsert){
            insert productList;
        }
           
        return productList;
    }
    public static List<PriceBookEntry> createPriceBookEntry(Integer count, Id productId, Boolean isInsert){
        List<PriceBookEntry> pbeList = new List<PriceBookEntry>();
        for(Integer i =0; i<count;i++){
            PriceBookEntry prod = new PriceBookEntry();
            prod.product2id = productId;
            prod.unitprice=1.0;
            prod.IsActive = true;
            prod.pricebook2Id = Test.getStandardPricebookId();
            pbeList.add(prod);
        }
        
        if(isInsert){
            insert pbeList;
        }
           
        return pbeList;
    }
    
    public static List<Lead> createLead(Integer count, Boolean isInsert){
        List<Lead> leadList = new List<Lead>();
        for(Integer i = 0 ; i<count; i++){
            Lead l = new Lead();
            l.FirstName = 'vishwas';
            l.LastName = 'gupta';
            l.Street = '809 Heatherwood';
            l.City = 'Brandon';
            l.State = 'SD';
            l.LeadSource = 'Website';
            l.Company = 'team company';
            l.Email = 'testclass@mail.com';
            l.Phone = '1234567890';
            l.LeadSource = 'Other';
            l.MobilePhone = '1234567890';
            l.PostalCode = '00000';
            l.Has_Hearing_aide__c = 'Yes';
            l.Has_Hearing_loss__c = 'Yes';
            l.Has_home_phone_line__c = 'Yes';
            l.Has_internet__c = 'Yes';
            l.Has_smart_phone__c = 'No';
            leadList.add(l);
        }
        if(isInsert){
            insert leadList;
        }
        return leadList;
    }
    
    
    public static List<Account> createAccountRecords( String name, Integer count, Boolean isInsert ) {
      List<Account> listAccount = new List<Account>();
      for( Integer i = 0; i < count; i++) {
        Account acc = new Account();
        acc.Name = name + String.valueOf(i);
        listAccount.add(acc);
      }
      
      if( listAccount.size() > 0  && isInsert ) {
        insert listAccount;
      }
      return listAccount;
    }
    
    public static List<Contact> createContactRecords( String lastname, Integer count, Boolean isInsert ) {
      List<Contact> listContact = new List<Contact>();
      for( Integer i = 0; i < count; i++) {
        Contact con = new Contact();
        con.lastName = lastname + String.valueOf(i);
        listContact.add(con);
      }
      
      if( listContact.size() > 0  && isInsert ) {
        insert listContact;
      }
      return listContact;
    }
    
    public static List<Asset> createAssetRecords( String name, Integer count, Boolean isInsert,Id AccId ) {
      List<Asset> listAsset = new List<Asset>();
      for( Integer i = 0; i < count; i++) {
        Asset ass = new Asset();
        ass.Name = name + String.valueOf(i);
          ass.accountId = AccId;
        listAsset.add(ass);
      }
      
      if( listAsset.size() > 0  && isInsert ) {
        insert listAsset;
      }
      return listAsset;
    }
    
    
    public static List<Addresses__c> createAddressRecords ( String city, String state, String country, Boolean isActive, String AccountId, String contactId, String assetId, Integer count, Boolean isInsert ){
      List<Addresses__c> listAddresses = new List<Addresses__c>();
      
      for( Integer i = 0; i < count; i++ ) {
        Addresses__c add = new Addresses__c();
        add.City__c = city + String.valueOf(i);
        add.State_Province__c = state;
        add.country__c = country;
        add.Active__c = isActive;
        if( !String.isBlank(AccountId)) {
          add.Account__c = AccountId;
        }
        if( !String.isBlank(contactId)) {
          add.Contact__c = contactId;
        }
        if( !String.isBlank(assetId)) {
          add.Asset__c = assetId;
        }
        listAddresses.add(add);
      }
      
      if( listAddresses.size() > 0 && isInsert ) {
        insert listAddresses;
      }
      return listAddresses;
    }
    public static List<Order> createOrder(Integer count, Boolean isInsert, Id accountId){
        List<Order> orderList = new List<Order>();
        for(integer i = 0; i<count; i++){
            Order ord = new Order();
            ord.AccountId = accountId;
            ord.EffectiveDate = Date.today();
            ord.ShippingCity = 'Brandon';
            ord.ShippingState = 'SD';
            ord.ShippingPostalCode = '57005';
            ord.Shipping_Name__c = 'test shipping name';
            ord.ShippingStreet = '809 Heatherwood';
            ord.Customer_City__c = 'Brandon';
            ord.Customer_State__c = 'SD';
            ord.Customer_ZipCode__c = '57005';
            ord.Customer_Name__c = 'test shipping name';
            ord.Customer_Street__c = '809 Heatherwood';
            ord.Status = 'Open';
            orderList.add(ord);
        }
        if(isInsert){
            insert orderList;
        }
        return orderList;
    }
    public static List<Contact> createContact(Integer count,Boolean isInsert,String recordTypename,Id accountId, String email){
        List<Contact> contactList = new List<Contact>();
        
        for(Integer i = 0;i<count; i++){
            Contact con = new Contact();
            con.AccountId = accountId;
            con.Active__C = True;
            con.LastName = 'Test Contact'+i;
            con.Phone = '1234567890';
            con.HomePhone ='1234567890';
            con.OtherPhone = '1234567890';
            con.Email = email;
            con.MailingCity = 'Brandon';
            con.MailingCountry = 'US';
            con.MailingState ='SD';
            con.MailingStreet='809 Heatherwood';
            con.MailingPostalCode='57005';
            con.Labor_Source__c = 'Barrister';
            con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            contactList.add(con);
            
        }
        if(isInsert){
            insert ContactList;
        }
        return contactList;
    }
    
    public static List<WorkOrder> createWorkOrder(Integer count, Boolean isInsert, Id orderId){
        List<WorkOrder> workOrderList = new List<WorkOrder>();
        for(integer i = 0; i<count; i++){
            WorkOrder word = new WorkOrder();
            word.Order__c = orderId;
            word.StartDate = System.today();
            word.Start_Time_Window__c = '9:00 am - 11:00 am';
            workOrderList.add(word);
        }
        if(isInsert){
            insert workOrderList;
        }
        return workOrderList;
    }
    public static List<Account> createAccount(Integer count, Boolean isInsert){
        List<Account> accList = new List<Account>();
        for(integer i = 0; i<count; i++){
            Account acc = new Account();
            acc.Name = 'test account'+i;
            accList.add(acc);
        }
        if(isInsert){
            insert accList;
        }
        return accList;
    }
    
     public static List<Campaign> createCampaign(Integer count, Boolean isInsert){
        List<Campaign> campList = new List<Campaign>();
        for(integer i = 0; i<count; i++){
            Campaign camp = new Campaign();
            camp.Name = 'test Camp'+i;
            camp.StartDate = System.Today().adddays(-5) ;
            camp.EndDate = System.Today().adddays(5);
            campList.add(camp);
        }
        if(isInsert){
            insert campList;
        }
        return campList;
    }
    public static List<Opportunity> createOpportunity(Integer count, Boolean isInsert, String stage){
        List<Opportunity> oppList = new List<Opportunity>();
        for(integer i = 0; i<count; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test opp'+i;
            opp.StageName = stage;
            oppList.add(opp);
        }
        if(isInsert){
            insert oppList;
        }
        return oppList;
    }
    
   
}