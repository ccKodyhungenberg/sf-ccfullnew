@isTest
public class CaseTriggerHandlerTest {
    public static List<Case> caseRecords;
    public static List<contact> contactRecords;
    public static List<Customer_Document__c> docs;
    public static List<Attachment> attachmentRecords;
    @isTest
    public static  void createData(){
    caseRecords=new List<Case>();
    contactRecords=new List<Contact>();
    docs=new List<Customer_Document__c>();
    attachmentRecords=new List<Attachment>();
    insert new PCF_Settings__c(
            Name='Default',
            Fax_Remarks_Template_Id_With_MRF__c='Default',
            Fax_Remarks_Template_Id_Without_MRF__c='Default',
            Fax_Cover_Sheet_With_MRF__c='Cover_With_MRF',
            Fax_Cover_Sheet_Without_MRF__c='Cover_Without_MRF',
            oAuth_Client_ID__c = '123',
            oAuth_Client_Secret__c = 'abc',
            oAuth_Password__c = 'abc',
            oAuth_Username__c = 'abc',
            Signature_Top__c = 123,
            Signature_Left__c = 123,
            Signature_Width__c = 123,
            Signature_Height__c = 123,
            Signature_Date_Height__c = 123,
            Signature_Date_Left__c = 123,
            Signature_Date_Width__c = 123,
            Signature_Date_Top__c = 123,
            //Conga_Template_Id__c = congaTemplate.Id,
            //Instruction_Document_Id__c = intructions.Id,
            Delete_Pcf_Attachment_After_Signing__c = true
        );
    contactRecords=CC_TestUtility.createContactRecords('test', 20 ,false);
        for(Contact con:contactRecords){
            con.Health_Care_Professional__c=contactRecords[0].id;
        }
        insert contactRecords;
        for(Integer i=0;i<20;i++){
            Customer_Document__c  doc=new Customer_Document__c();
            doc.Document_Type__c='Medical Release Form';
            doc.Document_Name__c='test doc';
            doc.Contact__c=contactRecords[0].id;
            doc.Original_File_Name__c='test file';
            docs.add(doc);
        }
        insert docs;
        for(Integer i=0;i<20;i++){
            Case c=new Case();
            c.Status='New';
            c.status__c='Needs Review';
            c.ContactId=contactRecords[0].id;
            caseRecords.add(c);
        }
        insert caseRecords;
        for(Integer i=0;i<20;i++){
          Attachment atch =new Attachment();
          atch.Name='Test Attach'+i;
          atch.ParentId=caseRecords[i].id;
          atch.Body=EncodingUtil.base64Decode('test');
              
          attachmentRecords.add(atch);
        }
        insert attachmentRecords;
        for(Case cs:caseRecords){
            cs.PCF_Attachment_Id__c=attachmentRecords[0].id;
            cs.status__c='Signing Complete';
            cs.ContactId=contactRecords[1].id;
        }
        try{
        update caseRecords;
            }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('The signed PCF is missing from this approval') ? true : false;
			System.assertEquals(expectedExceptionThrown,true);
	}
        
        try{
        delete caseRecords[0];
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('This record does not have a Contact value saved') ? true : false;
			System.assertEquals(expectedExceptionThrown,true);
        }
	
        caseRecords[1].contactId=null;
        try{
        update caseRecords[1];
        }catch(Exception e){}
         try{
        delete caseRecords[1];
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('This record does not have a Contact value saved') ? true : false;
			System.assertEquals(expectedExceptionThrown,true);
        }
	}
        
        
    }