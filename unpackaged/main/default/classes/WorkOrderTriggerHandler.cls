// Appirio 2019 
//
//
// Modified - NBOCK - 7.15.19 - C00263091: set completed date for scheduled work orders that are completed 
public class WorkOrderTriggerHandler {
    
    public static final map<String,String> m_installwindow_starttime=new map<string,string> { '7:00 am - 9:00 am'=>' 7:00 am','9:00 am - 11:00 am'=>' 9:00 am','11:00 am - 1:00 pm'=>' 11:00 am', '1:00 pm - 3:00 pm'=>' 1:00 pm', '3:00 pm - 5:00 pm'=>' 3:00 pm', '5:00 pm - 7:00 pm'=>' 5:00 pm', '7:00 pm - 9:00 pm'=>' 7:00 pm'};
        public static final map<String,String> m_installwindow_endtime=new map<string,string> { '7:00 am - 9:00 am'=>' 9:00 am','9:00 am - 11:00 am'=>' 11:00 am','11:00 am - 1:00 pm'=>' 1:00 pm', '1:00 pm - 3:00 pm'=>' 3:00 pm', '3:00 pm - 5:00 pm'=>' 5:00 pm', '5:00 pm - 7:00 pm'=>' 7:00 pm', '7:00 pm - 9:00 pm'=>' 9:00 pm'};
    public static Id serviceRepairId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service and repair').getRecordTypeId(); //NBOCK 
            public static void tr_Before_Update(List<WorkOrder> triggerNew, List<WorkOrder> triggerOld, Map<Id,WorkOrder> triggerNewMap, Map<Id,WorkOrder> triggerOldMap) {
                string s_Completed_By=userinfo.getuserid();
                Map<Id,Contact> m_Id_Contact=new Map<Id,Contact>();
                List<Id> lContactID=new List<Id>();
                List<Case> lCase_Add=new List<Case>();
                Set<String> CCProfileSet = new Set<String>{'CC Regional Sales Manager', 'CC Territory Manager'};//Added by Lalit for S-637045
                Profile prof = [Select name from Profile where id = :UserInfo.getProfileId() limit 1];//Added by Lalit for S-637045
                for(WorkOrder sr_new: triggerNew) {
                    WorkOrder sr_old=triggerOldMap.get(sr_new.Id);
                    //Added by Lalit for S-637045 Start
                    if(CCProfileSet.contains(prof.name) && sr_new.Status == 'Completed' && triggerOldMap.get(sr_new.id).Status != sr_new.Status){
                            sr_new.addError('You may not change the status of this work order to Completed');
                    }
                    else if(CCProfileSet.contains(prof.name) && sr_new.Status == 'Completed'){ 
                            sr_new.addError('You may not edit a Completed Work Order');
                    }
                    //Added by Lalit for S-637045 END
                    
                    //Start--- Below Code added by Parag Bhatt for case #00255958
                    if(sr_new.Status == 'Completed' || sr_new.Status == 'Canceled' ){
                        sr_new.Last_Closed_Cancelled_date_Backend__c = System.Now();
                    }
                    //END--- Below Code added by Parag Bhatt for case #00255958
                    if(sr_new.Service_Request_Status__c =='Completed' && sr_new.Service_Request_Status__c != sr_old.Service_Request_Status__c && sr_new.Completed_Date__c==null) {
                        sr_new.Completed_Date__c=datetime.now();
                        sr_new.Completed_By__c=s_Completed_By;
                    }   
                    if((sr_new.StartDate  != sr_old.StartDate || sr_new.Start_Time_Window__c  != sr_old.Start_Time_Window__c) && sr_new.Service_Request_Status__c !='Completed' ) {
                        if(sr_new.StartDate==null || sr_new.Start_Time_Window__c==null) {
                            sr_new.Start_Time_Local__c=null;  sr_new.End_Time_Local__c=null; sr_new.StartDate=null; sr_new.Start_Time_Window__c=null; 
                        }
                        else {
                            sr_new.Start_Time_Local__c=sr_new.StartDate.format() + m_installwindow_starttime.get(sr_new.Start_Time_Window__c);
                            sr_new.End_Time_Local__c=sr_new.StartDate.format() + m_installwindow_endtime.get(sr_new.Start_Time_Window__c);
                        }
                    }
                    //NBOCK Start C00263091: if a work order is changed from scheduled to completed, set the enddate 
                    if(sr_new.Status != sr_old.Status && sr_new.Status == 'Completed' && sr_new.RecordTypeID == serviceRepairId && sr_New.EndDate == null){
                        if(sr_new.StartDate == null){
                            sr_new.StartDate = sr_New.CreatedDate;
                        }
                        if(sr_new.StartDate >= System.now()){
                            sr_new.StartDate = sr_New.CreatedDate;
                        }
                        sr_new.EndDate = System.Now();
                    }
                    //NBOCK End
                    lContactID.add(sr_new.ContactId);
                }
                if(!lContactID.isEmpty()) m_Id_Contact=new Map<Id,Contact>([SELECT id, Name, Qualified__c, RecordTypeId FROM Contact WHERE Id IN :lContactID]);
                for(WorkOrder sr_new: triggerNew) {
                    WorkOrder sr_old=triggerOldMap.get(sr_new.Id);
                    //check if need to notify customer care about scheduled service request for unqualified customer
                    if(sr_new.Service_Request_Status__c =='Scheduled' && sr_new.Service_Request_Status__c != sr_old.Service_Request_Status__c && ( !(m_Id_Contact.get(sr_new.ContactId).Qualified__c==true)) && m_Id_Contact.get(sr_new.ContactId).RecordTypeId==CC_GlobalUtility.getRecordTypeIdByName('Customer', 'Contact')) { lCase_Add.add(
                        new Case(
                            RecordTypeID=CC_GlobalUtility.getRecordTypeIdByName('Customer','Case')
                            ,ContactID=sr_new.ContactId
                            ,Category__c='Service Inquiry'
                            ,SubCategory__c='Set up'
                            ,Status='Open'
                            ,OwnerId=CC_GlobalUtility.idProductManager //Craig Roth - '005C0000004tsRA'
                            ,Origin='Phone'
                            ,Priority='High'
                            ,Subject='Service Request was scheduled for un-qualified customer'
                            ,Description='Service Request (' +  sr_new.WorkOrderNumber + ') was scheduled for un-qualified customer' + 
                            '\nLink to the service request: '+URL.getSalesforceBaseUrl().toExternalForm()+'/'+sr_new.Id
                            +'\n Resolve unqualified issue before scheduled date'
                        )
                    );
                                                                                                                                                                                                                                                                                                                                }               
                }
                if(!lCase_Add.isEmpty()) insert lCase_Add;
            }
    public static set<Id> stSRIDProcessed {
        get {
            if(stSRIDProcessed==null) stSRIDProcessed=new set<Id>();
            return stSRIDProcessed; 
        } set;
    }
    public static void tr_After_Update(List<WorkOrder> triggerNew, List<WorkOrder> triggerOld, Map<Id,WorkOrder> triggerNewMap, Map<Id,WorkOrder> triggerOldMap) {
        Map<Id,Order> m_Id_Order2Open=new Map<Id,Order>();
        //Start-- Code added by Parag Bhatt for Case #00253937
        Map<Id,WorkOrder> mapOfOrderIdWiseWorkOrder =new Map<Id,WorkOrder>();
        List<Order> lstOfOrderToUpdate =new List<Order>();
        //End-- Code added by Parag Bhatt for Case #00253937
        //Start-- Code added by Rohit Pachauri for C-00255977
        List<WorkOrder> lstOfWorkOrderToUpdate=new List<WorkOrder>();
        Map<Id,map<Id,Id>> mapOfWorkOrderIdWiseMapOfOrderIdandAssetId = new Map<Id,map<Id,Id>>();
        List<OrderItem> lstofOrderItemToUpdate=new List<OrderItem>();
        Map<Id,Id> mapOfWorkOrderIdWisAssetDeviceId=new Map<Id,Id>();
        List<WorkOrder> lstOfWorkOrderUpdated=new List<WorkOrder>(); //Rohit for C-00258370
        //End-- Code added by Rohit Pachauri for C-00255977
        Map<Id,Opportunity> m_Id_Opportunity2Update=new Map<Id,Opportunity>();
        Map<Id,Opportunity> m_SRId_Opportunity=new Map<Id,Opportunity>(); //opportunity to create when SR is completed
        set<Id> st_OrderID2Open=new set<id>();
        List<Task> lTask_Insert=new List<Task>();
        
        for(WorkOrder sr_new: triggerNew) {
            WorkOrder sr_old=triggerOldMap.get(sr_new.Id);
            //Start-- Code added by Parag Bhatt for Case #00253937
            if(sr_new.Order__c!=null){
                system.debug('Order @@@-->'+sr_new.Order__c);
                mapOfOrderIdWiseWorkOrder.put(sr_new.Order__c,sr_new);
            }
            //End-- Code added by Parag Bhatt for Case #00253937
            //Start-- Code added by Rohit Pachauri for C-00255977
            if(sr_new.AssetId!=null && sr_new.AssetId!=triggerOldMap.get(sr_new.Id).AssetId){      
                //Checking if asset is manually tied up to work order and filling maps on same basis
                mapOfWorkOrderIdWiseMapOfOrderIdandAssetId.put(sr_new.Id,new map<Id,Id>());
                mapOfWorkOrderIdWiseMapOfOrderIdandAssetId.get(sr_new.Id).put(sr_new.Order__c,sr_new.AssetId);           
                mapOfWorkOrderIdWisAssetDeviceId.put(sr_new.Id,sr_new.AssetId);
            }
            //End-- Code added by Rohit Pachauri for C-00255977
            
            if(sr_new.Service_Request_Status__c =='Scheduled' && sr_new.Service_Request_Status__c != sr_old.Service_Request_Status__c ) {
                if(sr_new.Labor_Source__c=='Field Solutions') Send2FieldSolutions(sr_new.Id);
                if(sr_new.Order__c!=null) st_OrderID2Open.add(sr_new.Order__c);
            } else if(sr_new.Service_Request_Status__c =='Scheduled-Confirmed' && sr_new.Service_Request_Status__c != sr_old.Service_Request_Status__c ) {
                //need to check associated opportunities
                for(Opportunity op2update: new List<Opportunity>([SELECT Id, StageName, CloseDate FROM Opportunity WHERE Work_Order__c=:sr_new.Id AND StageName IN ('Purchase','Qualify','Registration')])) {
                    op2update.StageName='Onsite Activated';
                    if(sr_new.StartDate!=null) {
                        Integer d = sr_new.StartDate.day();
                        Integer mo = sr_new.StartDate.month();
                        Integer yr = sr_new.StartDate.year();
                        op2update.CloseDate=Date.newInstance(yr, mo, d);
                    }
                    else op2update.CloseDate=date.today().adddays(7);
                    m_Id_Opportunity2Update.put(op2update.id, op2update); 
                }
            } else if(sr_new.Service_Request_Status__c =='Completed' && sr_new.Service_Request_Status__c != sr_old.Service_Request_Status__c && !stSRIDProcessed.contains(sr_new.Id)) {
                //need to check associated opportunities
                for(Opportunity op2update: new List<Opportunity>([SELECT Id, StageName, CloseDate, Description FROM Opportunity WHERE Work_Order__c=:sr_new.Id AND StageName IN ('Purchase', 'Qualify', 'Registration', 'Self Activated', 'Onsite Activated')])) {
                    op2update.StageName='Confirm';
                    op2update.CloseDate=date.today().adddays(7);
                    op2update.Description=(op2update.Description!=null)?(op2update.Description+'\n'):('');
                        op2update.Description+='Reach out to the customer to ensure they are happy with the Ensemble phone and ClearCaptions Service.\nAnswer any questions they might have and help them if they are struggling with anything.\nProvide Customer Care /Support phone number for future questions or concerns.';
                    m_Id_Opportunity2Update.put(op2update.id, op2update); 
                }
                id id_OwnerId;
                WorkOrder SR_Completed=[select id, WorkOrderNumber, ContactId, Completed_Date__c, Technician__c, Technician__r.Email, Labor_Source__c, Completed_By__c,Customer_AccountID__c, Service_Request_Type__c, Completed_By__r.UserName, Completed_By__r.IsActive FROM WorkOrder WHERE Id=:sr_new.Id];
                if(SR_Completed.Technician__c!=null && SR_Completed.Labor_Source__c=='ClearCaptions employee') {
                    List<User> lUserTech=new List<User>([select id, UserName from User WHERE Email=:SR_Completed.Technician__r.Email and IsActive=true]);
                    if(!lUserTech.isEmpty()) id_OwnerId=lUserTech[0].Id;
                }
                System.debug('****id_OwnerId'+id_OwnerId);
                if(id_OwnerId==null && SR_Completed.Completed_By__c!=null) if(SR_Completed.Completed_By__r.IsActive==true && !SR_Completed.Completed_By__r.UserName.contains('integration')) id_OwnerId=SR_Completed.Completed_By__c;
                if(id_OwnerId==null) id_OwnerId=CC_GlobalUtility.idConcierge;  //Information ClearCaptions
                lTask_Insert.add(new Task(
                    //RecordTypeId=CCGlobal.getRecordTypeIdByName('Sales Call Task','Task') //Sales Call Task
                    Subject='Completed Service Request follow up - '+ SR_Completed.WorkOrderNumber
                    ,Description='Reach out to the customer and ensure they are happy with the Ensemble phone and ClearCaptions Service.\n Answer any questions they might have and help them if they are struggling with anything.\nAsk for 3 referrals'
                    ,OwnerId=id_OwnerId
                    ,ActivityDate=date.today().adddays(2)
                    ,whatId=SR_Completed.Id
                    ,whoid=SR_Completed.ContactId
                    ,IsReminderSet=true
                    ,ReminderDateTime=datetime.now().addHours(12)
                )
                                );
                //create opportunities
                if(sr_new.Service_Request_Type__c =='Install and Training') { //only create opportunities for install and training
                    //add new opportunity "Acquisition – Service Request"
                    //START-- Added by parag bhatt for story S-612220
                    opportunity opp = new opportunity();
                    opp.Name='2-Day Confirmation';//This line is modified by KAmin for Case - 00250036 from 7-Day Confirmation to 2-Day Confirmation
                    opp.StageName='Confirm';
                    opp.CloseDate=date.today().addDays(30); //30 days from today
                    opp.AccountID=sr_new.Customer_AccountID__c;
                    opp.Type='Acquisition – Service Request';
                    opp.LeadSource='Service Request';
                    opp.Work_Order__c=sr_new.Id;
                    if(CC_GlobalUtility.idConcierge != '' && CC_GlobalUtility.idConcierge != null){
                         opp.OwnerId=CC_GlobalUtility.idConcierge; //Information ClearCaptions
                    }
                   
                        m_SRId_Opportunity.put(sr_new.Id, opp); 
                    //END-- Added by parag bhatt for story S-612220
                } // end of create opportunities
                stSRIDProcessed.add(sr_new.Id); //add SR id to "processed list
            }   
        } 
        //START-- Code added by Rohit Pachauri for C-00255977
        if(!mapOfOrderIdWiseWorkOrder.isEmpty() && !mapOfWorkOrderIdWisAssetDeviceId.isEmpty()){
            map<Id,Map<Id,List<OrderItem>>> mapOfWorkOrderIdWiseMapOfAssetIdwiseListOfOrderItems = new map<Id,Map<Id,List<OrderItem>>>();            
            for(Order ord: [Select id,(Select id,Serial_Number__c from orderItems),(Select id,Service_Request_Type__c
                                                                                    FROM Work_Orders__r)
                            FROM order
                            WHERE Id IN : mapOfOrderIdWiseWorkOrder.keyset()]){                                                                
                                for(WorkOrder wo: ord.Work_Orders__r){                                    
                                    if(mapOfWorkOrderIdWiseMapOfOrderIdandAssetId.containsKey(wo.id) && (wo.Service_Request_Type__c =='Install and Training' || wo.Service_Request_Type__c =='RMA Replacement' )){                                        
                                        Map<Id,Id> mapofOrderIdandAssetId = mapOfWorkOrderIdWiseMapOfOrderIdandAssetId.get(wo.id);
                                        List<Id> lstOfAssetId = mapofOrderIdandAssetId.values();
                                        mapOfWorkOrderIdWiseMapOfAssetIdwiseListOfOrderItems.put(wo.id, new map<Id,List<OrderItem>>());
                                        //Preparing a mapping of asset id to order items using corresponding work order
                                        mapOfWorkOrderIdWiseMapOfAssetIdwiseListOfOrderItems.get(wo.id).put(lstOfAssetId[0], Ord.orderItems);                                        
                                    }
                                    
                                }
                            }
            for(WorkOrder wo : [Select Id,Asset.Identifier__c
                                FROM WorkOrder
                                WHERE ID IN:mapOfWorkOrderIdWiseMapOfAssetIdwiseListOfOrderItems.keyset()]){
                                    //Fetching Device ID on Asset Id basis
                                    //for(Asset asse:[SELECT id,identifier__c from Asset where Id IN: mapOfWorkOrderIdWiseMapOfAssetIdwiseListOfOrderItems.get(woid).keyset() ]) {
                                    {
                                        Map<Id, List<OrderItem>> MapOfAssetIdwiseListOfOrderItems = mapOfWorkOrderIdWiseMapOfAssetIdwiseListOfOrderItems.get(wo.id);
                                        
                                        for(List<OrderItem> mapsordItems : MapOfAssetIdwiseListOfOrderItems.values()){
                                            for(OrderItem ordItems: mapsordItems){                                                
                                                ordItems.Serial_Number__c=wo.Asset.Identifier__c;
                                                //populating Serial number as the Device ID of asset
                                                lstofOrderItemToUpdate.add(ordItems);
                                            }
                                            
                                        }
                                    }
                                    
                                }
            
            if(lstofOrderItemToUpdate.size()>0){
                update lstofOrderItemToUpdate;
            }
            
        }
        //End-- Code added by Rohit Pachauri for C-00255977
        
        //Below code added By parag Bhatt for case #00262649
        Set<Id> setForCheckingDuplicateOrderId = new Set<Id>();
        
        //Start-- Code added by Parag Bhatt for Case #00253937
        if(!mapOfOrderIdWiseWorkOrder.isEmpty() || !mapOfWorkOrderIdWisAssetDeviceId.isEmpty()){
            
            //Start-Added by Rohit for case C-00255977
            for(WorkOrder wo:[SELECT id,Asset.Paired_Date__c,Asset.DateCreated__c,Order__r.id,Order__r.Paired_Date__c,StartDate,EndDate,Service_Request_Type__c,IsPairedDate__c   from WorkOrder where id IN:mapOfWorkOrderIdWisAssetDeviceId.keyset()])
            {               
                if(wo.order__r.id !=null){
                    Order ord=new Order();
                    ord.id=wo.Order__r.id;
                    ord.Technician__c=mapOfOrderIdWiseWorkOrder.get(ord.id).Technician__c;
                    ord.Paired_Date__c=wo.Asset.Paired_Date__c;
                    setForCheckingDuplicateOrderId.add(ord.id); //code added By parag Bhatt for case #00262649
                    lstOfOrderToUpdate.add(ord);
                }
                
                //Start-Added By Rohit Pachauri for Case C-00258370 
                if((wo.Service_Request_Type__c=='RMA Replacement' || wo.Service_Request_Type__c=='Install and Training') && wo.IsPairedDate__c==false ){
                    //Updating the start date and end date on work order on asset paired date basis
                    WorkOrder wrkOrd=new WorkOrder();
                    wrkOrd.Id=wo.Id;
                    wrkOrd.IsPairedDate__c=true;
                    wrkOrd.StartDate=wo.Asset.DateCreated__c;                    
                    //wrkOrd.StartDate=wo.Asset.Paired_Date__c;
                    wrkOrd.EndDate=null;
                    lstOfWorkOrderUpdated.add(wrkOrd);
                    
                } // End C-00258370 
                //End C-00255977
            }
            // Commented by Rohit Pachauri for Case-00255977
            //Start-- Un-Commented by Parag Bhatt for Case-00261498
            for(order ord: [SELECT Id,Technician__c, Status FROM Order WHERE Id IN :mapOfOrderIdWiseWorkOrder.keyset()] ){
                WorkOrder wrkOrd = mapOfOrderIdWiseWorkOrder.get(ord.id);
                ord.Technician__c = wrkOrd.Technician__c;
                //Start--> code added By parag Bhatt for case #00262649
                if(!setForCheckingDuplicateOrderId.contains(ord.id)){ 
                    lstOfOrderToUpdate.add(ord);
                }
                //End--> code added By parag Bhatt for case #00262649
            }
            //End-- Un-Commented by Parag Bhatt for Case-00261498
            // Commented by Rohit Pachauri for Case-00255977
            if(lstOfOrderToUpdate.size() > 0){
                update lstOfOrderToUpdate;
            }
            //Start-Added By Rohit Pachauri for Case C-00258370 
            if(lstOfWorkOrderUpdated.size()>0)
            {
                Constants.isAfterUpdateFirst=false;
                update lstOfWorkOrderUpdated;
            }
            //End Case C-00258370 
            
        }
        //End-- Code added by Parag Bhatt for Case #00253937       
        if(!st_OrderID2Open.isEmpty()) m_Id_Order2Open=new Map<Id,Order>([SELECT Id, Status FROM Order WHERE Id IN :st_OrderID2Open AND Status='On Hold']);
        if(!m_Id_Order2Open.isEmpty()) {
            for(Order ord: m_Id_Order2Open.values()) ord.Status='Open';
            update m_Id_Order2Open.values();
        }
        if(!m_Id_Opportunity2Update.isEmpty()) update m_Id_Opportunity2Update.values();
        if(!lTask_Insert.isEmpty()) insert lTask_Insert;
        if(!m_SRId_Opportunity.isEmpty()) {
            insert m_SRId_Opportunity.values();
            List<OpportunityContactRole> lAddOpportunityContactRole=new List<OpportunityContactRole>();
            for(Id Id_SR: m_SRId_Opportunity.keySet()) { lAddOpportunityContactRole.Add(new OpportunityContactRole(contactid=triggerNewMap.get(Id_SR).ContactId, opportunityid=m_SRId_Opportunity.get(Id_SR).Id, ISPRIMARY=true)); }
            insert lAddOpportunityContactRole;
        } 
  // Below Line added by Parag Bhatt for story S-612220
        sendWelcomeEmailToEachNewInstall(triggerNew, triggerOld, triggerNewMap, triggerOldMap);
    }
    //START-- Below Line added by Parag Bhatt for story S-612220
    public static void sendWelcomeEmailToEachNewInstall(List<WorkOrder> triggerNew, List<WorkOrder> triggerOld, Map<Id,WorkOrder> triggerNewMap, Map<Id,WorkOrder> triggerOldMap){
        Map<Id,Id> mapOfWorkOrderIdsWiseContactId = new Map<Id,Id>();
        Id installTrainingRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install and Training').getRecordTypeId();
        Id customerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Map<Id, String> mapOfContactWiseSendEmailStatus = new Map<Id, String>();
        
        for(WorkOrder wo : triggerNew){
            if(wo.recordtypeId == installTrainingRecordTypeId && triggerOldMap.get(wo.id).status != 'Completed' && wo.Status == 'Completed' && wo.ContactId != null){
                mapOfWorkOrderIdsWiseContactId.put(wo.Id, wo.ContactId);
                System.debug('setOfContactIds-->'+mapOfWorkOrderIdsWiseContactId);
            }
        }
        
        if(!mapOfWorkOrderIdsWiseContactId.isEmpty()){
            for(Contact con : [select id,RecordTypeId,Email,HasOptedOutOfEmail,Certified__c,(Select Id,Type__c,Product2.Name from Assets Where Product2.Name IN ('iPhone Phone VOIP','CC Blue Phone')),
                               (Select id,Status,ContactId,AssetId from workorders where Status = 'Completed') 
                               from Contact where Id IN :mapOfWorkOrderIdsWiseContactId.values() AND HasOptedOutOfEmail = FALSE AND Certified__c = TRUE]){
                                   Boolean foundAlreadyCompletedWorkOrder = FALSE;
                                   for(WorkOrder wo : con.workorders){
                                       if(!mapOfWorkOrderIdsWiseContactId.containsKey(wo.id)){
                                           foundAlreadyCompletedWorkOrder = TRUE;
                                       }
                                   }
                                   if(con.Email !=null && con.Email.indexOf('@Clearcaptions.com') == -1 && con.Email.indexOf('@ccnoemail.com') == -1 && con.RecordTypeId == customerRecordTypeId && !foundAlreadyCompletedWorkOrder){  //added by madhavi for C-00271789
                                       if(con.Assets.size() > 0){
                                           for(Asset ast : con.Assets){
                                               if(ast.Product2.Name == 'iPhone Phone VOIP'){
                                                   if(mapOfContactWiseSendEmailStatus.get(con.id) == 'CCBLUE'){
                                                       mapOfContactWiseSendEmailStatus.put(con.id, 'BOTH'); 
                                                   }else{
                                                       if(mapOfContactWiseSendEmailStatus.get(con.id) != 'BOTH'){
                                                           mapOfContactWiseSendEmailStatus.put(con.id, 'VOIP');
                                                       }
                                                   }
                                               }
                                               if(ast.Product2.Name == 'CC Blue Phone'){
                                                   if(mapOfContactWiseSendEmailStatus.get(con.id) == 'VOIP'){
                                                       mapOfContactWiseSendEmailStatus.put(con.id, 'BOTH'); 
                                                   }else{
                                                       if(mapOfContactWiseSendEmailStatus.get(con.id) != 'BOTH'){
                                                           mapOfContactWiseSendEmailStatus.put(con.id, 'CCBLUE'); 
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }
            System.debug('mapOfContactWiseSendEmailStatus-->'+mapOfContactWiseSendEmailStatus);
            if(!mapOfContactWiseSendEmailStatus.isEmpty()){
                List<Messaging.SingleEmailMessage> lstToSendEmails = new List<Messaging.SingleEmailMessage>();
                for(String contactIds : mapOfContactWiseSendEmailStatus.keyset()){
                    // get an email template
                    EmailTemplate VOIPTemplateId = [select Id FROM EmailTemplate WHERE Name = 'iPhone Phone VoIP' limit 1];
                    EmailTemplate CCBlueTemplateId = [select Id FROM EmailTemplate WHERE Name = 'CC Blue Phone' limit 1];
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    //Start - Added by Prachi for case 00271806
                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'support@clearcaptions.com' LIMIT 1];
                    //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    if ( owea.size() > 0 ) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }
                    //End - Added by Prachi for case 00271806
                    mail.setTargetObjectId(contactIds);
                    if(mapOfContactWiseSendEmailStatus.get(contactIds) == 'VOIP'){
                        mail.setTemplateId(VOIPTemplateId.Id);
                    }else if(mapOfContactWiseSendEmailStatus.get(contactIds) == 'CCBLUE'){
                        mail.setTemplateId(CCBlueTemplateId.Id);
                    }else{
                        mail.setTemplateId(VOIPTemplateId.Id);
                        Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                        mail1.setTemplateId(CCBlueTemplateId.id);
                        mail1.setTargetObjectId(contactIds);
                        lstToSendEmails.add(mail1);
                    }
                    lstToSendEmails.add(mail);
                }
                if(lstToSendEmails.size() > 0){
                    //send the email
                    System.debug('Reaced here--->');
                    List<Messaging.SendEmailResult> results = Messaging.sendEmail(lstToSendEmails, false); //added by madhavi for C-00271789
                    
                }
            }
        }
    }
    //END-- Below Line added by Parag Bhatt for story S-612220
    
    public static void tr_Before_Insert(List<WorkOrder> triggerNew, List<WorkOrder> triggerOld, Map<Id,WorkOrder> triggerNewMap, Map<Id,WorkOrder> triggerOldMap) {
        string s_Completed_By=userinfo.getuserid();         
        for(WorkOrder sr_new: triggerNew) {
            if(sr_new.Service_Request_Status__c =='Completed' && sr_new.Completed_Date__c==null) {
                sr_new.Completed_Date__c=datetime.now();
                sr_new.Completed_By__c=s_Completed_By;
            } 
            if(sr_new.StartDate!=null && sr_new.Start_Time_Window__c!=null ) {
                if(sr_new.Start_Time_Local__c==null) sr_new.Start_Time_Local__c=sr_new.StartDate.format() + m_installwindow_starttime.get(sr_new.Start_Time_Window__c);
                if(sr_new.End_Time_Local__c==null) sr_new.End_Time_Local__c=sr_new.StartDate.format() + m_installwindow_endtime.get(sr_new.Start_Time_Window__c);
            } else {
                sr_new.StartDate=null; sr_new.Start_Time_Window__c=null;
            }
        }
    }
    
    public static void tr_After_Insert(List<WorkOrder> triggerNew, List<WorkOrder> triggerOld, Map<Id,WorkOrder> triggerNewMap, Map<Id,WorkOrder> triggerOldMap) {
        Map<Id,Order> m_Id_Order2Open=new Map<Id,Order>();
        set<Id> st_OrderID2Open=new set<id>(); 
        //Start-- Code added by Parag Bhatt for Case #00253937
        Map<Id,WorkOrder> mapOfOrderIdWiseWorkOrder =new Map<Id,WorkOrder>();
        List<Order> lstOfOrderToUpdate =new List<Order>(); 
        //End-- Code added by Parag Bhatt for Case #00253937
        Map<Id,Opportunity> m_SRId_Opportunity=new Map<Id,Opportunity>();
        for(WorkOrder sr_new: triggerNew) {
            //Start-- Code added by Parag Bhatt for Case #00253937
            if(sr_new.Order__c!=null){
                mapOfOrderIdWiseWorkOrder.put(sr_new.Order__c,sr_new);
            }
            //End-- Code added by Parag Bhatt for Case #00253937
            if(sr_new.Service_Request_Status__c =='Scheduled') {
                if(sr_new.Labor_Source__c=='Field Solutions') Send2FieldSolutions(sr_new.Id);
                if(sr_new.Order__c!=null) st_OrderID2Open.add(sr_new.Order__c);
            }
        }
        //Start-- Code added by Parag Bhatt for Case #00253937
        if(!mapOfOrderIdWiseWorkOrder.isEmpty()){
            for(order ord: [SELECT Id,Technician__c, Status FROM Order WHERE Id IN :mapOfOrderIdWiseWorkOrder.keyset()] ){
                WorkOrder wrkOrd = mapOfOrderIdWiseWorkOrder.get(ord.id);
                ord.Technician__c = wrkOrd.Technician__c;
                lstOfOrderToUpdate.add(ord);
            }
            if(lstOfOrderToUpdate.size() > 0){
                update lstOfOrderToUpdate;
            }
        }
        //End-- Code added by Parag Bhatt for Case #00253937
        if(!st_OrderID2Open.isEmpty()) m_Id_Order2Open=new Map<Id,Order>([SELECT Id, Status FROM Order WHERE Id IN :st_OrderID2Open AND Status='On Hold']);
        if(!m_Id_Order2Open.isEmpty()) {
            for(Order ord: m_Id_Order2Open.values()) ord.Status='Open';
            update m_Id_Order2Open.values();
        }   
    }   
    
    //--------------------------------------------------------------------------    
    static final string FIELD_SOLUTIONS_API='https://test.fieldsolutions.com/api/rest/wos?';
    static final string FIELD_SOLUTIONS_PROJECT= '6980';
    @Future(callout=true)
    public static void Send2FieldSolutions(id id_SR) {
        List<WorkOrder> lSR=new List<WorkOrder>(
            [SELECT 
             Id
             ,WorkOrderNumber
             ,Street
             ,City
             ,State
             ,PostalCode
             ,Service_Request_Status__c
             ,ContactId
             ,Contact.Name
             ,Contact.Email
             ,Contact.Password__c
             ,Contact.Phone
             ,Service_Request_Type__c
             //,Contact.User_Name__c
             ,StartDate
             ,Start_Time_Window__c
             ,Start_Time_Local__c
             ,End_Time_Local__c
             ,Reference_Number__c
             ,Service_Notes__c
             ,Customer_Phone__c
             ,Customer_Email__c
             ,Customer_Type__c
             ,Customer_User_Name__c
             ,Customer_Password__c
             FROM WorkOrder WHERE Id=:id_SR AND Service_Request_Status__c ='Scheduled' AND Labor_Source__c='Field Solutions']);
        if(lSR.size()!=1) {
            //invalid Service Request or invalid status
            insert new Application_Log__c(Name='CCHourlyImplementation.Send2FieldSolutions',Severity_Level__c='ERROR', Message__c='CCHourlyImplementation.Send2FieldSolutions ERROR: Could not find Scheduled Service request to send to Field Solutions: '+id_SR);
            return;
        }
        WorkOrder SR2Send=lSR[0];
        if( SR2Send.StartDate==null || SR2Send.Start_Time_Window__c==null) {
            insert new Application_Log__c(Name='CCHourlyImplementation.Send2FieldSolutions',Severity_Level__c='ERROR', Message__c='CCHourlyImplementation.Send2FieldSolutions ERROR: Scheduled Service request must have both start date and start time window - before we can send it to Field Solutions: '+id_SR);
            return;        
        }
        string sUser='PurpleAPI';
        string sPass='PurpAPI20!4';
        //&publish=0
        //string sWO='<WO><WO_ID>AR20140919-01</WO_ID><Description>Do A, Do B, Do C</Description><SpecialInstructions>Instructions</SpecialInstructions><Requirements>Requirements</Requirements><StartDate>09/20/2014</StartDate><EndDate>09/20/2014</EndDate><StartTime>7:00 AM</StartTime><EndTime>8:00 AM</EndTime><Address>123 Abc St</Address><City>Henderson</City><State>MT</State><Zipcode>59866</Zipcode><PayMax>20.00</PayMax><Project_ID>6980</Project_ID></WO>';
        //string url='https://test.fieldsolutions.com/api/rest/wos?';
        //string url='https://api.fieldsolutions.com/api/rest/wos?';
        //returns: <WO><WIN_NUM>737437</WIN_NUM></WO>
        //encode 
        string url=FIELD_SOLUTIONS_API;
        //@@@DEBUG
        //url='https://api.fieldsolutions.com/api/rest/wos?';
        string sWO_ID=SR2Send.WorkOrderNumber;
        if(SR2Send.Reference_Number__c!=null) sWO_ID+='-'+SR2Send.StartDate;
        url=url+'user='+EncodingUtil.urlEncode(sUser, 'UTF-8');
        url=url+'&pass='+EncodingUtil.urlEncode(sPass, 'UTF-8');
        url=url+'&publish=1';
        string sWO=
            + '<WO><WO_ID>' + sWO_ID + '</WO_ID>'
            + '<SiteNumber>'+ SR2Send.WorkOrderNumber + '</SiteNumber>'
            + '<SiteName>'+ SR2Send.WorkOrderNumber + '</SiteName>'
            + '<SitePhone>' + ((SR2Send.Customer_Phone__c==null)?(''):(SR2Send.Customer_Phone__c))+ '</SitePhone>' 
            + '<Ship_Contact_Info>' + 'Customer Type: '+ SR2Send.Customer_Type__c +' ( Customer User Name='+SR2Send.Customer_User_Name__c+'; Customer Password='+SR2Send.Customer_Password__c+')'+ '</Ship_Contact_Info>' 
            + '<SiteEmail>' + ((SR2Send.Customer_Email__c==null)?(''):(SR2Send.Customer_Email__c)) + '</SiteEmail>' 
            + '<SpecialInstructions>' + SR2Send.Service_Notes__c + '</SpecialInstructions>'
            + '<StartDate>'+  SR2Send.StartDate+'</StartDate>'
            + '<EndDate>'+  SR2Send.StartDate+'</EndDate>'
            + '<StartTime>'+ m_installwindow_starttime.get(SR2Send.Start_Time_Window__c).trim().toupperCase()+'</StartTime>'
            + '<Site_Contact_Name>'+ SR2Send.Contact.Name+'</Site_Contact_Name>'
            + '<Address>'+ SR2Send.Street +'</Address>'
            + '<City>'+SR2Send.City+'</City>'
            + '<State>'+ SR2Send.State +'</State>'
            + '<Zipcode>'+ SR2Send.PostalCode +'</Zipcode>'
            +'<Project_ID>'+FIELD_SOLUTIONS_PROJECT+'</Project_ID></WO>'
            ;
        url=url+'&wo='+ EncodingUtil.urlEncode( sWO,'UTF-8');   
        Http h = new Http();
        HttpRequest req = new HttpRequest();            
        req.setEndpoint(url);
        system.debug('url@@@-->'+url);
        req.setMethod('POST');  
        req.setTimeout(120000);   
        if(!Test.isRunningTest()){
            HttpResponse res = h.send(req);
            system.debug('req@@@-->'+req);
            string call_result=res.getBody();
            system.debug('call_result@@@-->'+call_result);
            if(call_result!=null) {
                system.debug('@@@INFO call_result='+call_result);
                if(call_result.contains('<WIN_NUM>')) {
                    SR2Send.Reference_Number__c=call_result.substring(call_result.indexOf('<WIN_NUM>')+9, call_result.indexOf('</WIN_NUM>'));
                    SR2Send.Service_Request_Status__c='Sent to Field Solutions';
                    update SR2Send;
                }
            }
            
            insert new Application_Log__c(Name='CCHourlyImplementation.Send2FieldSolutions',Severity_Level__c='INFO', Message__c='CCHourlyImplementation.Send2FieldSolutions INFO: Received reply from Field Solutions for SR.Id='+id_SR+' reply:\n'+call_result+'\nsWO=\n'+sWO);               
        }
        return;
    }
    public static decimal geo_distance(decimal lat1, decimal lng1, decimal lat2, decimal lng2) {
        decimal dlng = (lng2 - lng1)*3.14159265359/180;
        decimal dlat = (lat2 - lat1)*3.14159265359/180;
        lat1=lat1*3.14159265359/180;
        lng1=lng1*3.14159265359/180;
        lat2=lat2*3.14159265359/180;
        lng2=lng2*3.14159265359/180;
        double a=math.sin(dlat/2.0) * math.sin(dlat/2.0)+math.cos(lat1) * math.cos(lat2) * (math.sin(dlng/2.0) * math.sin(dlng/2.0));
        return 3961.0 * 2.0 * math.atan2(math.sqrt(a),math.sqrt(1-a));  
    }
    
    
}