public class CC_UserTriggerHandler {
    public static void isAfterInsert(List<User> newList){
        createTechnician(newList,null);
    }
    public static void isAfterUpdate(List<User> newList,Map<id,User> oldMap){
        createTechnician(newList,oldMap);
    }
    private static void createTechnician(List<User> newList, Map<id,User> oldMap){
        Id TerritoryManagerRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'CC_Territory_Manager'].id;
        Id RegionalSalesManagerRoleId = [SELECT id FROM UserRole WHERE DeveloperName = 'CC_Regional_Sales_Manager'].id;
        Set<Id> userIdSet = new Set<Id>();
        Set<id> updatedUserIdSet = new Set<Id>();
        Set<Id> userNotTerritoryAndRegional = new Set<Id>();
        Set<Id> userInactive = new Set<Id>();
        Set<Id> userActive = new Set<Id>();
        Set<Id> userDetailsChanged = new Set<Id>();
        for(User u: newList){
            if(oldMap== null && u.isActive == true && (u.UserRoleId == TerritoryManagerRoleId ||u.UserRoleId == RegionalSalesManagerRoleId)){
                userIdSet.add(u.id);
            }
            if(oldMap!=null){
                if(oldMap.get(u.id).UserRoleId != u.UserRoleId && u.isActive == true && (oldMap.get(u.id).UserRoleId != TerritoryManagerRoleId || oldMap.get(u.id).UserRoleId != RegionalSalesManagerRoleId) && (u.UserRoleId == TerritoryManagerRoleId ||u.UserRoleId == RegionalSalesManagerRoleId)){
                    //system.assert(false, 'updatedUserIdSet');
                    updatedUserIdSet.add(u.id);
                }
                if(oldMap.get(u.id).UserRoleId != u.UserRoleId && u.isActive == true && (oldMap.get(u.id).UserRoleId == TerritoryManagerRoleId || oldMap.get(u.id).UserRoleId == RegionalSalesManagerRoleId) && (u.UserRoleId != TerritoryManagerRoleId ||u.UserRoleId != RegionalSalesManagerRoleId)){
                    //system.assert(false, 'updatedUserIdSet11');
                    userNotTerritoryAndRegional.add(u.id);
                }
                if(oldMap.get(u.id).isActive != u.isActive && u.isActive == false && (u.UserRoleId == TerritoryManagerRoleId ||u.UserRoleId == RegionalSalesManagerRoleId)){
                    //system.assert(false, 'updatedUserIdSet12');
                    userInactive.add(u.id);
                }
                if(oldMap.get(u.id).isActive != u.isActive && u.isActive == true && (u.UserRoleId == TerritoryManagerRoleId ||u.UserRoleId == RegionalSalesManagerRoleId)){
                    //system.assert(false, 'updatedUserIdSet12');
                    userActive.add(u.id);
                }
                if((oldMap.get(u.Id).FirstName != u.FirstName || oldMap.get(u.Id).LastName != u.LastName || oldMap.get(u.Id).Email != u.Email ||  oldMap.get(u.Id).Phone != u.Phone || oldMap.get(u.Id).MobilePhone != u.MobilePhone 
                    || oldMap.get(u.Id).City!= u.City || oldMap.get(u.Id).Country != u.Country || oldMap.get(u.Id).State != u.State || oldMap.get(u.Id).Street != u.Street || oldMap.get(u.Id).PostalCode != u.PostalCode) && u.isActive == true 
                   && (u.UserRoleId == TerritoryManagerRoleId ||u.UserRoleId == RegionalSalesManagerRoleId) ){
                	//System.assert(false, 'msg');
                       userDetailsChanged.add(u.id);    
                }
            }
            
        }
        if(userIdSet.size()>0){
            createContact(userIdSet);
        }
        if(updatedUserIdSet.size()>0){
            //system.assert(false, updatedUserIdSet);
            updateContact(updatedUserIdSet,true);
        }
        if(userNotTerritoryAndRegional.size()>0){
            updateContact(userNotTerritoryAndRegional,false );
        }
        if(userInactive.size()>0){
            updateContact(userInactive,false);
        }
        if(userActive.size()>0){
            updateContact(userActive,true);
        }
        if(userDetailsChanged.size()>0){
            updateContactDetails(userDetailsChanged);
        }
    }
    
    @future
    private static void updateContactDetails(Set<Id> userDetailsChanged){
        Map<Id,Contact> userIdToContactMap = new Map<Id,Contact>();
        List<Contact> conToInsert = new List<Contact>();
        for(Contact con: [SELECT id, Related_To_User__c,Active__c FROM Contact WHERE Related_To_User__c IN : userDetailsChanged]){
            userIdToContactMap.put(con.Related_To_User__c, con);
        }
        if(userIdToContactMap.size()>0){
            For(User u : [SELECT id, FirstName, LastName, Email,Phone, City,State,Street,Country,PostalCode,MobilePhone FROM User WHERE Id IN: userIdToContactMap.keySet()]){
                Contact techCon = new Contact();
                techCon.Id = userIdToContactMap.get(u.id).id;
                techCon.Related_To_User__c = u.Id;
                techCon.Active__c = true;
                techCon.FirstName = u.FirstName;
                techCon.LastName = u.LastName;
                techCon.Email = u.Email;
                techCon.MobilePhone = u.MobilePhone;
                techCon.Phone = u.Phone;
                techCon.MailingCity = u.City;
                techCon.MailingCountry = u.Country;
                techCon.MailingPostalCode = u.PostalCode;
                techCon.MailingState = u.State;
                techCon.MailingStreet = u.Street;
                conToInsert.add(techCon);
            }
        }
        if(conToInsert.size()>0){
            update conToInsert;
        }
    }
    @future
    private static void updateContact(Set<Id> updatedUserIdSet, Boolean roleToTM){
        List<Contact> contactToUpdate = new List<Contact>();
        List<Contact> contactToInsert = new List<Contact>();
        List<Contact> existingTechnician = [SELECT id, Related_To_User__c,Active__c FROM Contact WHERE Related_To_User__c IN : updatedUserIdSet];
        if(existingTechnician.size()>0){
            for(Contact con: existingTechnician){
                if(!con.Active__c && roleToTM){
                    Contact c = new Contact();
                    c.Id = con.id;
                    c.Active__c = true;
                    contactToUpdate.add(c);
                }
                else if(con.Active__c && !roleToTM){
                    Contact c = new Contact();
                    c.Id = con.id;
                    c.Active__c = false;
                    contactToUpdate.add(c);
                }
            }
            if(contactToUpdate.size()>0){
                update contactToUpdate;
            }
        }
        else{
            For(User u : [SELECT id, FirstName, LastName, Email,Phone, City,State,Street,Country,PostalCode,MobilePhone FROM User WHERE Id IN: updatedUserIdSet]){
                Contact techCon = new Contact();
                techCon.Related_To_User__c = u.Id;
                techCon.Active__c = true;
                techCon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
                techCon.Labor_Source__c = 'ClearCaptions Employee';
                techCon.FirstName = u.FirstName;
                techCon.LastName = u.LastName;
                techCon.Email = u.Email;
                techCon.MobilePhone = u.MobilePhone;
                techCon.Phone = u.Phone;
                techCon.MailingCity = u.City;
                techCon.MailingCountry = u.Country;
                techCon.MailingPostalCode = u.PostalCode;
                techCon.MailingState = u.State;
                techCon.MailingStreet = u.Street;
                techCon.AccountId = Label.CC_CLEARCAPTION_ACCOUNT_ID;
                contactToInsert.add(techCon);
            }
            if(contactToInsert.size()>0){
                insert contactToInsert;
            }
        }
    }
    @future
    private static void createContact(Set<Id> userIdSet){
        List<Contact> technicianContactList = new List<Contact>();
        For(User u : [SELECT id, FirstName, LastName, Email,Phone, City,State,Street,Country,PostalCode,MobilePhone FROM User WHERE Id IN: userIdSet]){
            Contact techCon = new Contact();
            techCon.Related_To_User__c = u.Id;
            techCon.Active__c = true;
            techCon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
            techCon.Labor_Source__c = 'ClearCaptions Employee';
            techCon.FirstName = u.FirstName;
            techCon.LastName = u.LastName;
            techCon.Email = u.Email;
            techCon.MobilePhone = u.MobilePhone;
            techCon.Phone = u.Phone;
            techCon.MailingCity = u.City;
            techCon.MailingCountry = u.Country;
            techCon.MailingPostalCode = u.PostalCode;
            techCon.MailingState = u.State;
            techCon.MailingStreet = u.Street;
            techCon.AccountId = Label.CC_CLEARCAPTION_ACCOUNT_ID;
            technicianContactList.add(techCon);
        }
        if(technicianContactList.size()>0){
            insert technicianContactList;
        }
    }
}