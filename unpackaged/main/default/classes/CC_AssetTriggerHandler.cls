// 1/15/19 C-00250021 NCarson - Block Duplicate Records From Inserting
// 2/19/19 C-00250021 NCarson - Refactor, moved SOQL Query out of For Loop
// 06/13/2019 C-00263652 NBOCK - added null check on triggeroldmap contact
// 7/5/19  S-628700 MKLICH - Refactored Class moving some methods around, Implemented logic for Number_Of_Active_Assets__c field

public class CC_AssetTriggerHandler {
    public static void beforeInsert(List<Asset> newList){

        // 2/19/19 C-00250021 NCarson - Create List to move SOQL query out of for loop

        List<String> percentPlusIdentifierList = New List<String>();
        for (Asset newAsset : newList) {
          percentPlusIdentifierList.add('%' + newAsset.Identifier__c);
        }

        List<Asset> similarAssetList = [SELECT Id, Name, Identifier__c FROM Asset WHERE Identifier__c LIKE :percentPlusIdentifierList];

        for(Asset newAsset : newList){
            // 1/15/19 C-00250021 NCarson - iterate through list of similar asset records
            // and add error if similar record exists 
            for (Asset similarAsset : similarAssetList) {
                
                
                if(similarAsset.Identifier__c != null && newAsset.Identifier__c != null){  // Line added by Parag Bhatt for story S-612220
                    
                    if (similarAsset.Identifier__c.indexOf(newAsset.Identifier__c) != -1 && UserInfo.getUserName() != Label.CC_Integration_User_Name) {
                        newAsset.addError('Similar record found: ' + similarAsset.Identifier__c);
                        break;
                    }
                }
                
            }
            
        }
        // 1/15/19 C-00250021 NCarson - End

        // MKLICH S-628700 Moved to afterInsert method
		updateAccountOnAsset(newList , null); //Modified By Nilesh for Case #00250394

    }

    // MKLICH S-628700 Moved functions from the beforeInsert to afterInsert due to best practices
    public static void afterInsert(List<Asset> newAssets) {
        //Map<Id,Integer> contactIdToAssetCountMap = new Map<Id,Integer>();
        Set<ID> AssetcontactIDs = new Set<ID>();
        List<Contact> contactToUpdate = new List<Contact>();
        List<Contact> contactToUpdateType = new List<Contact>();
        Set<Id> contactIdSet = new Set<id>();

        for(Asset newAsset : newAssets){
            if(newAsset.ContactId != null){
                contactIdSet.add(newAsset.ContactId);
            }
        }

        if(contactIdSet.size()>0){
            for(Contact con :[SELECT id, Type__C FROM Contact WHERE id IN: contactIdSet]){
                if(con.Type__c == 'Potential Customer'){
                    Contact c = new Contact();
                    c.id = con.id;
                    c.Type__c = 'Customer';
                    c.Certified__c = true;
                    contactToUpdateType.add(c);
                }
            }
        }
		updateWorkOrderStatus(newAssets);
        // S-628700 MKLICH Start
        AssetcontactIDs = createContactIdSet(null, newAssets);
        if(AssetcontactIDs.size() > 0) {
            contactToUpdate = updateNumberOfActiveAssetsOnContact(AssetcontactIDs);
        }

        System.debug('contactToUpdate:::::: '+contactToUpdate+'  ........ '+ AssetcontactIDs);
        if(contactToUpdate.size()>0){
            update contactToUpdate;
        }
        if(contactToUpdateType.size()>0){
            update contactToUpdateType;
        }
        // S-628700 MKLICH Stop

    }



    public static void updateWorkOrderStatus( List<Asset> triggerNew ) {
        //support of connecting SR to new Device

        Map<Id,Map<Id,WorkOrder>> map_ContactID_SRCandidate = new Map<Id,Map<Id,WorkOrder>>();
        Set<Id> st_ContactId                                = new Set<Id>();
        Map<Id,WorkOrder> map_DeviceID_SR2Update            = new Map<Id,WorkOrder>(); //map of connected Device-SR
        Map<Id, Order> m_Id_Order2Upsert                    = new Map<Id, Order>();
        Map<Id,String> mapOrderIdToAssetName                = new Map<Id,String>();
        List<OrderItem> listOrderItemToUpdate               = new List<OrderItem>();
        //added by sumit tanwar dor product code check
        Map<Id, Product2> mapOfProducts 					= new Map<Id, Product2>();
        Set<id> orderIds 								= new Set<Id>();
        Map<Id, OrderItem> orderItemWithProduct				= new Map<Id, orderItem>();

        for(Asset dev_new: triggerNew) {
            if(dev_new.ContactId!=null) {
                st_ContactId.add(dev_new.ContactId);
            }
        }
        //added by sumit tanwar dor product code check
        for(Product2 p: [Select id, Product2.name ,Product2.CCWS_Device_Type_Code__c from product2 where CCWS_Device_Type_Code__c != null]){
            mapOfProducts.put(p.id, p);
        }

        Map<Id,Contact> m_Id_Contact = new Map<Id,Contact>([SELECT Id FROM Contact WHERE Id IN :st_ContactId]);

        Map<Id,WorkOrder> m_Id_SRCandidates = new Map<Id,WorkOrder>( [ SELECT Id, AssetId, ContactId, Order__c, Order__r.Status, StartDate FROM WorkOrder
                                                                      WHERE ContactId IN : m_Id_Contact.values()
                                                                      AND AssetId = null
                                                                      AND Order__c !=null
                                                                      AND Service_Request_Type__c IN ('RMA Replacement', 'Install and Training')
                                                                      AND Status NOT IN ('Completed','Cancelled')
                                                                      ORDER BY CreatedDate DESC]
                                                                   );


        for(WorkOrder sr: m_Id_SRCandidates.values()) {
            if(!map_ContactID_SRCandidate.containsKey(sr.ContactId)) {
                map_ContactID_SRCandidate.put(sr.ContactId, new Map<Id,WorkOrder>());
            }
            map_ContactID_SRCandidate.get(sr.ContactId).put(sr.Id, sr);
            //added by sumit tanwar dor product code check
            orderIds.add(sr.Order__c);
        }
        //added by sumit tanwar dor product code check - Code added to check Product code to update workOrder and order.
        for(OrderItem oi: [select id, OrderId, Product2Id, Product2.name ,Product2.CCWS_Device_Type_Code__c from orderitem where orderid in :orderIds and Product2Id != null ]){
            orderItemWithProduct.put(oi.OrderId,oi);
        }

        //Set<ID> workOrderId = new Set<ID>();
        for(Asset dev_new : triggerNew ) {
            if( map_ContactID_SRCandidate.containsKey(dev_new.ContactId) && !map_DeviceID_SR2Update.containsKey(dev_new.Id) /*&& dev_new.Type__c == 'Clarity2'*/) { //not yet connected Clarity2 device to anything - Sumit 06/02/2019 Commented Clarity Device check to process other device type
                if(!map_ContactID_SRCandidate.get(dev_new.ContactId).isEmpty()) {
                    WorkOrder oSRCandidate = map_ContactID_SRCandidate.get(dev_new.ContactId).values()[0];
                    //Condition added by sumit tanwar dor product code check
                    if(mapOfProducts.containskey(dev_new.Product2Id) &&
                       orderItemWithProduct.containsKey(oSRCandidate.Order__c) &&
                       mapOfProducts.get(dev_new.Product2Id).CCWS_Device_Type_Code__c == mapOfProducts.get(orderItemWithProduct.get(oSRCandidate.Order__c).Product2Id).CCWS_Device_Type_Code__c) {
                    oSRCandidate.AssetId = dev_new.Id;
                    oSRCandidate.Status = 'Completed';
                    //oSRCandidate.Status = 'Completed'; *Commented by sumit @appirio 3rd Dec 2018 - I-355177*/
                    //oSRCandidate.Start_Date__c = date.today(); //set to today completed date /*Commented by sumit @appirio 3rd Dec 2018 - I-355177*/
                    //oSRCandidate.EndDate = system.now(); /*Added by sumit @appirio 3rd Dec 2018 - I-355177*/

                    /*Added By Sumit for Device Integration failure issue
                     If(oSRCandidate.StartDate == null){
                        oSRCandidate.StartDate = dev_new.Paired_Date__c;
                        //oSRCandidate.Start_Time_Window__c = '7:00 pm - 9:00 pm';
                        workOrderId.add(oSRCandidate.Id);
                    }
                    else {
                        oSRCandidate.EndDate = dev_new.Paired_Date__c;
                        if(dev_new.Paired_Date__c < oSRCandidate.StartDate){
                            oSRCandidate.StartDate = oSRCandidate.EndDate.addHours(-1);
                        }
                    }*/

                    oSRCandidate.EndDate = dev_new.Paired_Date__c; //added by yogesh @appirio 10th Dec 10th
                    if(oSRCandidate.EndDate !=null){
                        oSRCandidate.EndDate = oSRCandidate.EndDate.addhours(8);
                    }
                    If(oSRCandidate.StartDate == null || dev_new.Paired_Date__c < oSRCandidate.StartDate){
                        oSRCandidate.StartDate = oSRCandidate.EndDate.addHours(-1);
                        oSRCandidate.Start_Time_Window__c = '7:00 am - 9:00 am';
                    }

                    //system.Assert(false, 'EndDate Updated:::'+oSRCandidate.EndDate+' StartDate:::: '+oSRCandidate.StartDate);
                    map_DeviceID_SR2Update.put(dev_new.Id,oSRCandidate);
                    if(
                        oSRCandidate.Order__r.Status != 'Cancelled'
                        && oSRCandidate.Order__r.Status != 'Refund Pending'
                        && oSRCandidate.Order__r.Status != 'Refund Completed'
                    ) {
                        //need to complete connected order
                        Order order2add = new Order( Id = oSRCandidate.Order__c, Status = 'Completed', Paired_Date__c = Date.today());
                        m_Id_Order2Upsert.put(order2add.Id,order2add);
                        mapOrderIdToAssetName.put(order2add.Id,dev_new.Identifier__c);
                    }
                    map_ContactID_SRCandidate.get(dev_new.ContactId).remove(oSRCandidate.Id); //remove "used" SR
                   }
                }
            }
        }
        if( !map_DeviceID_SR2Update.isEmpty() ) {
            update map_DeviceID_SR2Update.values();
        }
        if( !m_Id_Order2Upsert.isEmpty() ) {
            upsert m_Id_Order2Upsert.values();
        }
        for(OrderItem oi : [SELECT id,OrderId FROM OrderItem WHERE OrderId IN :m_Id_Order2Upsert.keyset()]){
            if(mapOrderIdToAssetName.containsKey(oi.OrderId)){
                OrderItem ordI = new OrderItem();
                ordI.id = oi.id;
                ordI.Serial_Number__c = String.valueOf(mapOrderIdToAssetName.get(oi.OrderId));
                listOrderItemToUpdate.add(ordI);
            }
        }
        if(listOrderItemToUpdate.size()>0){
            update listOrderItemToUpdate;
        }
        /*if(!workOrderId.isEmpty()){
            updateWorkOrderEndDate(workOrderId);
        }*/
    }
    /************************************************
    * Method : beforeUpdate
    * Descreption : Call all the Before Update functionality in this method
    * Created By : Nilesh Grover for Case #00250394
    *************************************************/
    public static void beforeUpdate(List<Asset> triggerNew, List<Asset> triggerOld, Map<Id,Asset> triggerNewMap, Map<Id,Asset> triggerOldMap){
        updateAccountOnAsset(triggerNew, triggerOldMap);
    }
    /************************************************
    * Method : updateAccountOnAsset
    * Descreption : To update Account information on Asset Corresponding to associated Contact
    * Created By : Nilesh Grover for Case #00250394
    *************************************************/
    public static void updateAccountOnAsset(List<Asset> triggerNew , Map<Id,Asset> triggerOldMap){
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Contact> contactMap;
        Id contactCustomerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        if(triggerOldMap == NULL){
            for(Asset asset :  triggerNew){
                if(asset.ContactId != NULL && asset.AccountId == NULL) {
                    contactIds.add(asset.ContactId);
                }
            }
            if(contactIds.size() > 0){
                contactMap = new Map<Id, Contact>([SELECT Id, AccountId, RecordTypeId FROM Contact
                                                   WHERE Id IN: contactIds]);
            }
            for(Asset asset :  triggerNew){
                if(asset.ContactId != NULL && asset.AccountId == NULL
                   && contactMap.get(asset.ContactId).RecordTypeId == contactCustomerRecordTypeId) {
                    asset.AccountId = contactMap.get(asset.ContactId).AccountId;
                }
            }
        }else{
            for(Asset asset :  triggerNew){
                //NBOCK 00263652 added null check on triggeroldmap contact
                if(asset.ContactId != NULL && (triggerOldMap.get(asset.Id).ContactId == null || asset.ContactId != triggerOldMap.get(asset.Id).ContactId)) {
                    contactIds.add(asset.ContactId);
                }
            }
            if(contactIds.size() > 0){
                contactMap = new Map<Id, Contact>([SELECT Id, AccountId, RecordTypeId FROM Contact
                                                   WHERE Id IN: contactIds]);
            }
            for(Asset asset :  triggerNew){
                //NBOCK 00263652 added null check on triggeroldmap contact
                if(asset.ContactId != NULL && (triggerOldMap.get(asset.Id).ContactId == null || asset.ContactId != triggerOldMap.get(asset.Id).ContactId)){
                    //NBOCK 00263652 check if contactMap is null
                    if(contactMap != null && contactMap.get(asset.ContactId).RecordTypeId == contactCustomerRecordTypeId){
                        asset.AccountId = contactMap.get(asset.ContactId).AccountId;
                    }
                }
            }
        }
    }
   /* @future
    public static void updateWorkOrderEndDate(Set<ID> workOrderId) {
        List<WorkOrder> woToBeUpdated = new List<WorkOrder>();
        for(WorkOrder wo:[Select id, EndDate, StartDate from WorkOrder where id in: workOrderId]){
            wo.endDate = wo.StartDate;
            wo.StartDate = wo.StartDate.addHours(-1);
            woToBeUpdated.add(wo);
        }
        if(!woToBeUpdated.isEmpty()){
            update woToBeUpdated;
        }
    }*/

    //S-628700 MKLICH Start
    //afterDelete() calls functions that run after an Asset is Deleted
    public static void afterDelete(List<Asset> deletedAssets) {
        System.debug('In After Delete: ' + deletedAssets);
        Set<ID> AssetcontactIDs = createContactIdSet(null, deletedAssets);
		List<Contact> contactsToUpdate = updateNumberOfActiveAssetsOnContact(AssetcontactIDs);
        if(contactsToUpdate.size() > 0) {
            update contactsToUpdate;
        }
    }

    // afterUpdate() calls functions that run after an Asset is Updated
    public static void afterUpdate(List<Asset> oldAssets, List<Asset> updatedAssets) {
        System.debug('In After Update: ' + updatedAssets);
        List<Contact> contactsToUpdate = new List<Contact>();
        Set<ID> AssetcontactIDs = createContactIdSet(oldAssets, updatedAssets);
        if (AssetcontactIDs.size() > 0) {
        	contactsToUpdate = updateNumberOfActiveAssetsOnContact(AssetcontactIDs);
        }
        if(contactsToUpdate.size() > 0) {
            update contactsToUpdate;
        }
    }

    // MKLICH S-628700 Extract the ContactIDs From the Given Asset
    public static Set<Id> createContactIdSet(List<Asset> oldAssets, List<Asset> assets) {
        Set<Id> AssetcontactIDs = new Set<Id>();
        //Map<Id, Integer> contactIdToAssetCountMap = new Map<Id, Integer>();
    	for(Asset newAssets : assets) {
            if(!AssetcontactIDs.contains(newAssets.ContactId) && newAssets.ContactId != NULL){
                    AssetcontactIDs.add(newAssets.ContactId);
            }
    	}
        if (oldAssets!= Null) {
            for(Asset old : oldAssets) {
                if(!AssetcontactIDs.contains(old.ContactId) && old.ContactId != NULL){
                    AssetcontactIDs.add(old.ContactId);
                }
            }
        }
        return AssetcontactIDs;
    }

    // MKLICH S-628700 Abstract way to update the field from the related Contact.Number_of_Clarity_Devices__c to be Number_of_Active_Assets__c
    public static List<Contact> updateNumberOfActiveAssetsOnContact(Set<Id> conIds) {
        List<Contact> contactsToUpdate = new List<Contact>();
        Map<Id, AggregateResult> resultMap = new Map<Id, AggregateResult>([SELECT ContactId Id, COUNT(Id) totalActiveAssets FROM Asset WHERE contactID IN :conIds AND (Deleted__c = False AND CB_Active__c = True) Group By ContactId]);
        System.debug('Result Map' + resultMap);
        for(Id conID : conIds){
            if(conID != NULL) {
                Contact c = new Contact();
                c.id = conID;
                c.Number_of_Active_Assets__c = resultMap.containsKey(conID) ? Integer.valueOf(resultMap.get(conID).get('totalActiveAssets')) : 0;
                contactsToUpdate.add(c);
            }
        }
        return contactsToUpdate;
    }

    // MKLICH S-628700

}