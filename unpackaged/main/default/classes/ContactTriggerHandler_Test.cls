@isTest
public class ContactTriggerHandler_Test {
    
    public static testmethod void updateCombinedUsageOnAccount_Test() {
        List<Account> listAccounts = CC_TestUtility.createAccountRecords('Test Account', 5, TRUE);
        //Start Added By saurabh S-648388
        Contact newCon = new Contact();
        	newCon.lastName = 'Test new Con';
        	newCon.AccountId = listAccounts.get(0).Id;
            newCon.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            //newCon.Active__c = FALSE;
            newCon.Email='abcd21@test.com';
            //con.Title='test title';
            newCon.Type__c='Employee'; //updated by neha for C-00275392
             //newcon.Customer_Segmentation__c='Regular user';
            newCon.Active__c=true;
            newCon.DoNotCall=false;
            newCon.DateCreated__c=Date.newInstance(2018,7,2);
            newCon.Number_of_Clarity_Devices__c=10;
            newCon.Last_Check_in__c=Date.newInstance(2018,1,1);
        insert newCon;
        System.debug('My Contact is '+newCon);
        //End Added By saurabh S-648388
        List<Contact> listContacts = new List<Contact>();
        
        for( Contact con : CC_TestUtility.createContactRecords('Test Contact',100,FALSE)) {
            con.AccountId = listAccounts.get(0).Id;
            //con.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            con.Active__c = FALSE;
            con.Email='abc@test.com';
            //con.Title='test title';
            con.Type__c='Enterprise Customer';//Added By saurabh S-648388
            // con.Customer_Segmentation__c='Regular user';
            con.Active__c=true;
            con.DoNotCall=false;
            con.DateCreated__c=Date.newInstance(2018,7,2);
            con.Number_of_Clarity_Devices__c=10;
            con.Last_Check_in__c=Date.newInstance(2018,1,1);
            //con.Most_Recent_Billable_Call__c=Date.newInstance(2018,1,1);
            listContacts.add(con);
        }
        
        insert listContacts;
        
        
        List<UsageDetail__c> udList = new List<UsageDetail__c>();
        for(Integer i=0; i<100;i++){
            UsageDetail__c ud = new UsageDetail__c();
            ud.LifetimeBillableMins__c = 5;
            ud.Billable_Mins_Prior01Month__c=2000;
            ud.Contact__c = listContacts.get(0).Id;
            ud.MostRecentBillableCall__c=Date.newInstance(2018,1,1);
            udList.add(ud);
        }
        insert udList;
        
        //ud.MostRecentBillableCall__c=null;
        
        //update ud;
        
        listContacts.get(0).Last_Check_in__c=Date.newInstance(2018,2,1);
        update listContacts.get(0);
        
        listContacts.get(0).MailingState = 'Rajasthan';
        delete listContacts.get(0);
        
        
        listContacts.get(1).MailingCity = 'Jaipur';
        listContacts.get(1).CB_UserID__c = '12345';
        listContacts.get(1).email = 'xyz@test.com';
        
        update listContacts.get(1);
        
        
        
        listContacts.get(2).Active__c = TRUE;
        listContacts.get(2).CB_UserID__c = '1234554';
        
        
        update listContacts.get(2);
        
        Test.startTest();
        List<Contact> listContacts1 = new List<Contact>();
        
        for( Contact con : CC_TestUtility.createContactRecords('Test Contact',5,FALSE)) {
            //con.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            con.Type__c='Potential Customer';
            con.Email='abc1@test.com';
            con.Active__c = FALSE;
            
            listContacts1.add(con);
        }
        // System.assert(false, listContacts[0].recordtypeid);
        insert listContacts1;
        
        Test.stopTest();
        
        
    }
    public static testmethod void updateCombinedUsageOnAccountElse_Test(){
        List<Account> listAccounts = CC_TestUtility.createAccountRecords('Test Account', 5, TRUE);
        List<Contact> listContacts = new List<Contact>();
        
        /*for(Account ac:listAccounts){
listAccounts.get(0).companytoken__c='1234567891';
}*/
        // insert listAccounts;
        
        for( Contact con : CC_TestUtility.createContactRecords('Test Contact',5,FALSE)) {
            //con.AccountId = listAccounts.get(0).Id;
            con.Active__c = FALSE;
            //con.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            
            con.Email='abc1@test.com';
            con.Type__c='Enterprise Customer';
            // con.Customer_Segmentation__c='Regular user';
            con.Active__c=true;
            con.DoNotCall=false;
            con.DateCreated__c=Date.newInstance(2018,7,2);
            con.Number_of_Clarity_Devices__c=10;
            //con.Last_Check_in__c=Date.newInstance(2018,1,1);
            //con.Most_Recent_Billable_Call__c=Date.newInstance(2018,1,1);
            listContacts.add(con);
        }
        
        insert listContacts;
        
        UsageDetail__c ud = new UsageDetail__c();
        ud.LifetimeBillableMins__c = 5;
        ud.Billable_Mins_Prior01Month__c=2000;
        ud.Contact__c = listContacts.get(0).Id;
        ud.MostRecentBillableCall__c=Date.newInstance(2018,1,1);
        
        insert ud;
        ud.MostRecentBillableCall__c=Date.newInstance(2018,2,1);
        update ud;
        
        
        
        
        
        
        
        
        
        
    }
    
    public static testmethod void updateCombinedUsageOnAccountElseIf_Test(){
        List<Account> listAccounts = CC_TestUtility.createAccountRecords('Test Account', 5, TRUE);
        List<Contact> listContacts = new List<Contact>();
        
        
        for( Contact con : CC_TestUtility.createContactRecords('Test Contact',5,FALSE)) {
            con.AccountId = listAccounts.get(0).Id;
            con.Active__c = FALSE;
            con.Email='abc1@test.com';
            con.Type__c='Enterprise Customer';
            // con.Customer_Segmentation__c='Regular user';
            con.Active__c=true;
            con.DoNotCall=false;
            con.DateCreated__c=Date.newInstance(2018,7,2);
            con.Number_of_Clarity_Devices__c=10;
            con.Last_Check_in__c=Date.newInstance(2018,1,1);
            //con.Most_Recent_Billable_Call__c=Date.newInstance(2018,1,1);
            listContacts.add(con);
        }
        
        insert listContacts;
        
        UsageDetail__c ud = new UsageDetail__c();
        ud.LifetimeBillableMins__c = 5;
        ud.Billable_Mins_Prior01Month__c=2000;
        ud.Contact__c = listContacts.get(0).Id;
        
        
        insert ud;
        for(Contact con:listContacts){
            con.Last_Check_in__c=Date.newInstance(2017,7,6);
        }
        update listContacts;
        
        for(Contact con:listContacts){
            con.Latest_Retention_Detail_Date__c =Date.newInstance(2017,7,6);
            con.Retention_Case_Created_Date__c=date.newInstance(2018,5,5);
        }
        update listContacts;
        
        
        
        
        
        
        
        
        
        
        
    }
    public static testmethod void tr_Before_Insert_Test2(){
        List<Account> listAccounts = CC_TestUtility.createAccountRecords('Test Account', 5, TRUE);
        List<Contact> listContacts = new List<Contact>();
        
        
        
        for( Contact con : CC_TestUtility.createContactRecords('Test Contact',5,FALSE)) {
            //con.AccountId = listAccounts.get(0).Id;
            con.Active__c = FALSE;
            con.Email='abc1@test.com';
            con.Type__c='Enterprise Customer';
            con.Latest_Retention_Detail_Date__c=Date.newInstance(2018,9,5);
            con.Max_Check_in__c=Date.newInstance(2018,6,1);
            // con.Customer_Segmentation__c='Regular user';
            con.Active__c=true;
            con.DoNotCall=false;
            con.DateCreated__c=Date.newInstance(2018,7,2);
            con.Number_of_Clarity_Devices__c=10;
            con.Last_Check_in__c=Date.newInstance(2018,1,1);
            //con.Most_Recent_Billable_Call__c=Date.newInstance(2018,1,1);
            listContacts.add(con);
        }
        
        insert listContacts;
        
        UsageDetail__c ud = new UsageDetail__c();
        ud.LifetimeBillableMins__c = 5;
        ud.Billable_Mins_Prior01Month__c=2000;
        ud.Contact__c = listContacts.get(0).Id;
        
        
        insert ud;
        for(Contact con:listContacts){
            con.Last_Check_in__c=Date.newInstance(2017,7,6);
        }
        update listContacts;
        
        for(Contact con:listContacts){
            con.Latest_Retention_Detail_Date__c =Date.newInstance(2017,9,10);
            con.Retention_Case_Created_Date__c=date.newInstance(2018,5,5);
        }
        update listContacts;
        
        System.debug('listContacts.size(): ' + listContacts.size());
        for(Contact testContact : listContacts){
            System.debug('testContact.RecordTypeId: ' + testContact.RecordTypeId);
        }
       
    }
    
    @isTest
    static void testRetentionCase(){
        List<Account> listAccounts = CC_TestUtility.createAccountRecords('Test Account', 5, TRUE);
        List<Contact> listContacts = CC_TestUtility.createContact(3, false, 'Customer', listAccounts[0].id, 'test@test.com');
        List<Contact> updateCons = new List<Contact>();

        for(Contact c : listContacts){
            c.doNotCall = false;
            c.Active__c = true;
            c.Number_of_Clarity_Devices__c = 12;
            c.DateCreated__c = date.today().addDays(-20);
        }
        insert listContacts;

        UsageDetail__c ud = new UsageDetail__c();
        ud.Billable_Mins_Prior01Month__c = 4;
        ud.MostRecentBillableCall__c = Datetime.now();
        ud.Contact__c = listContacts[0].id;
        insert ud;

        UsageDetail__c ud2 = new UsageDetail__c();
        ud2.Billable_Mins_Prior01Month__c = 4;
        ud2.MostRecentBillableCall__c = Datetime.now();
        ud2.Contact__c = listContacts[1].id;
        insert ud2;

        UsageDetail__c ud3 = new UsageDetail__c();
        ud3.Billable_Mins_Prior01Month__c = 4;
        ud3.MostRecentBillableCall__c = Datetime.now();
        ud3.Contact__c = listContacts[2].id;
        insert ud3;

        Contact con = listContacts[0];
        con.Last_Check_in__c = Date.today();
        updateCons.add(con);

        Contact con2 = listContacts[1];
        con2.Latest_Retention_Detail_Date__c = Date.today().addDays(-15);
        con2.Retention_Case_Created_Date__c = DateTime.now();
        con2.Max_Check_in__c = DateTime.now().addDays(-5);
        updateCons.add(con2);

        Contact con3 = listContacts[2];
        con3.Latest_Retention_Detail_Date__c = Date.today().addDays(-5);
        con3.Max_Check_in__c = DateTime.now().addDays(-15);
        updateCons.add(con3);

        update updateCons;
        //listContacts[0].Billable_Mins_Prior01Month__c=1000;
    }
   
    /*********************************
    * Name: updateDeactivatedDate_Test
    * Created By: Nilesh Grover
    * Description: Case #00248952, Case #00250384 Test update of Deactivated Date on Deactivating a Contact - Jan 17, 2019  
    ***********************************************************************************************************************/
    public static testmethod void updateDeactivatedDate_Test() {
        List<Contact> conList = CC_TestUtility.createContactRecords('Test Contact',1,true);
    List<Contact> deactivatedContactList = new List<Contact>();
        List<Contact> reactivatedContactList = new List<Contact>();
        
        system.assertEquals(1, [SELECT Id FROM Contact].size());
        
        for(Contact con : [SELECT Id, Deactivated__c FROM Contact]) {
            if(con.Deactivated__c == false){
                con.Deactivated__c = true;
                con.Deactivation_Reason__c = 'Account under review';
                deactivatedContactList.add(con);
            }         
        }
        system.assertEquals(1, deactivatedContactList.size());
        update deactivatedContactList;
        
        List<Contact> testDeactivatedContact = [SELECT Id , Deactivated__c, Deactivated_Date__c FROM Contact];
        system.assertEquals(1, testDeactivatedContact.size());
        system.assertEquals(true, testDeactivatedContact[0].Deactivated__c);
        system.assertNotEquals(NULL, testDeactivatedContact[0].Deactivated_Date__c);        
        
        for(Contact con : testDeactivatedContact) {
            if(con.Deactivated__c == true){
                con.Deactivated__c = false;
                reactivatedContactList.add(con);
            }         
        }
        system.assertEquals(1, reactivatedContactList.size());
        update reactivatedContactList;
        
        List<Contact> testReactivatedContact = [SELECT Id , Deactivated__c, Deactivated_Date__c FROM Contact];
        system.assertEquals(1, testReactivatedContact.size());
        system.assertEquals(false, testReactivatedContact[0].Deactivated__c);    
        system.assertEquals(NULL, testReactivatedContact[0].Deactivated_Date__c);       
    }

    //NBOCK Start Increase code coverage
    public static testMethod void increaseCoverage(){
        //create test data
        Contact pCon = new Contact();
        pCon.LastName = 'Test Prospect';
        pCon.Email = 'testProspect456344463@example.com';
        pCon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        pCon.Certified__c = true;
        pCon.Type__c = 'Customer';
        insert pCon;

        pCon.FirstName = 'Tester';
        pCon.Password__c = 'clear123';
        update pCon;

        Contact qCon = [SELECT Id, Email FROM Contact WHERE Id = :pCon.Id LIMIT 1];


        
        Account acc = new Account();
        acc.Name = 'Test Enterprise';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-Customer').getRecordTypeId();
        acc.CompanyToken__c = '3X78RC';
        acc.Type = 'Enterprise';
        insert acc;

        Contact con = new Contact();
        con.LastName = 'Test Enterprise';
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Non-Customer').getRecordTypeId();
        con.Type__c = 'Enterprise Customer'; 
        con.AccountId = acc.Id;
        con.Title = '3X78RC';
        con.Email = 'testProspect456344463@example.com';
    
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true; 
        Database.SaveResult sr = Database.insert(con, dml); 
    }
    //NBOCK End
}