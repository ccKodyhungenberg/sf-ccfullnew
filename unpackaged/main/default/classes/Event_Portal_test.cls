@isTest
private class Event_Portal_test {
    
  static testMethod void EventPortalControllertest() {
      
       Contact conRec = new Contact();
       conRec.MAilingCity ='test';
       conRec.MAilingState ='test';
       conRec.MAilingCountry ='test';
       conRec.MailingStreet ='test';
       conRec.MAilingPostalCode ='test';
       conRec.CCWS_User_Create_Response__c ='test';
       conRec.email ='abc@gmail.com';
       conRec.password__c ='test';
       conRec.LastName ='test';
       conRec.FirstName ='test1';
       conRec.Phone= '0000000000';
      conRec.MobilePhone ='8767898767';
      conRec.HomePhone ='8767898767';
      try{
          insert conRec;
      }catch(Exception ex){
          system.debug('conRec@@@'+ex.getMessage() +ex.getLineNumber());
      }
      
      Campaign cam = new Campaign();
      cam.name = 'test';
      insert cam;
         
      
  Event_Portal_Controller.getSelectEventFromCache();
    Event_Portal_Controller.SearchEmail('bramhi.p@gmail.com');    
    Event_Portal_Controller.generatePassword(conRec);
     
      test.starttest();
     // Set mock callout class 
     Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('1'));
    Event_Portal_Controller.ValidateAddress('{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route","apt":"207","country":"US","Mphone":"8767898767"}');
      Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('2'));
    Event_Portal_Controller.ValidateAddress('{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route","apt":"207","country":"US","Mphone":"8767898767"}');
      Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('3'));
    Event_Portal_Controller.ValidateAddress('{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route","apt":"207","country":"US","Mphone":"8767898767"}');
      test.stoptest();
    Event_Portal_Controller.createUser(conRec.Id);
                 
    string ContactInfo = '{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route 207","country":"US","apt":"12b","Mphone":"8767898767","Smart_phone_type__c":"iOS"}';
     string ContactInfo1 = '{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route 207","country":"US","apt":"12b","Mphone":"8767898767","Smart_phone_type__c":"Android"}';
   	
     
      Event_Portal_Controller.createLeadAndContact(cam.id,ContactInfo,true, false);
    //Event_Portal_Controller.createLeadAndContact('1234123412','test',false, false);
    Event_Portal_Controller.productList();
    Event_Portal_Controller.populateStartTimeWindow();
   Event_Portal_Controller.createContactWithQualification(cam.Id,ContactInfo,true,'yes','yes','true','yes','iOS');
      try{
      Event_Portal_Controller.createContactWithQualification(cam.Id,ContactInfo1,false,'yes','yes','true','yes','Android');
      }catch(Exception ex){
          
      }
      Event_Portal_Controller.getTechnician();
   Event_Portal_Controller.WorkOrderWrapper objWork = new Event_Portal_Controller.WorkOrderWrapper();
     Product2 objProd = new Product2();
      objProd.Name ='tests';
      insert objProd;
      
      OrderItem objorder = new OrderItem();
       /*objorder.name='test';
       objorder.ShippingAddress='test';
       objorder.Status='Scheduled';*/
       string recordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install and Training').getRecordTypeId();
       WorkOrder objworkord = new WorkOrder();
       objworkord.Labor_Source__c='Field Nation';
      objworkord.recordTypeId = recordTypeId;
       objworkord.Status='Scheduled';
      objworkord.created_from_portal__c=true;
      objworkord.Service_Request_Type__c='Install and Training';
       objworkord.Service_Notes__c='test';
      objworkOrd.ContactId = conRec.Id;
      insert objworkord;
      
      objWork.ordrLineItem = objorder;
      objWork.technician = ConRec;
      objWork.product = objProd;
      objWork.workOrder = objworkord;
      
      list<Event_Portal_Controller.WorkOrderWrapper> lst = new List<Event_Portal_Controller.WorkOrderWrapper>();
      lst.add(objWork);
      Event_Portal_Controller.saveAppointmentWorkOrder(ConRec.Id, lst);
      OrderItem obj = new OrderItem();
      WorkOrder objworks = new WorkOrder();
      Product2 objProds = new Product2();
      
      try{
           Event_Portal_Controller.searchForCamps('test');
      }catch(Exception ex){
          
      }
     
      
      //Event_Portal_Controller.WorkOrderWrapper objWork1 = new Event_Portal_Controller.WorkOrderWrapper(obj,objworks,ConRec,objProds);

      /* Event_Portal_Controller.createContactWithQualification('1234123412','test',true,'no','yes','no','no','no');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',true,'no','yes','yes','no','iOS');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',true,'yes','no','yes','no','iOS');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',true,'no','no','yes','no','iOS');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',true,'no','no','yes','no','iOS');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',false,'no','no','yes','no','Android');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',false,'no','no','no','no','no');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',false,'no','no','yes','no','android');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',false,'yes','no','yes','no','android');
    Event_Portal_Controller.createContactWithQualification('1234123412','test',false,'yes','yes','yes','yes','android');
     Event_Portal_Controller.createContactWithQualification('1234123412','test',false,'yes','no','yes','no','android');
       Event_Portal_Controller.createContactWithQualification('1234123412','test',false,'no','no','yes','yes','android');*/
      
    // String CampId,String CustomerInfo,Boolean createContact, string hasHomePhone,string internet, string smartPhone, string hearing, string type
      //List<Campaign> camplist = Event_Portal_Controller.searchForCamps('searchText');

          }
    }