global class GlobalFunctions {

    global static GlobalFunctions instance;

    
    // These are dynamic, as System.runas can change the context; make their gets dynamic too
    global Boolean isBoomiIntegrationUser {
        get {return isBoomiIntegrationUser();}
        private set;}        
    global Boolean isAdminUser {
        get {return isAdminUser();}
        private set;}

    public static List <RecordType> oRecordType_list;

    public Id getRecordTypeId (String recordType, String objectType) {
   
/*        List <RecordType> oRecordType = [SELECT Id FROM recordtype
                                            WHERE name =: recordType
                                            AND SObjectType =: objectType];
        return (oRecordType [0].Id);        
*/
        if (GlobalFunctions.oRecordType_list==null ) {
            GlobalFunctions.oRecordType_list=new List <RecordType> ([SELECT Id,name, SObjectType FROM recordtype]);
        }
        for(RecordType rt:GlobalFunctions.oRecordType_list) {
            if(rt.Name==recordType && rt.SObjectType == objectType) {
                return rt.Id;
            }
        }
        return null;
    }

    // These are static, load them in the constructor
    global Id getDefaultContactOwnerId {get; private set;}

    global static GlobalFunctions getInstance(){
        if(instance == null){
            instance = new GlobalFunctions();
        }
        return instance;
    }

    private GlobalFunctions() {
        getDefaultContactOwnerId = getDefaultContactOwnerId();
    }

    private static Boolean isBoomiIntegrationUser(){
//        return (UserInfo.getUserId() == GlobalConfigurationVariable__c.getInstance('Boomi Integration User').Value__c);
        return (UserInfo.getUserId() == CCGlobal.idBoomi);
     }

    
    private static Id getDefaultContactOwnerId(){
    
        //List<User> list_DefaultContactOwner = [SELECT Id FROM user WHERE username =: GlobalConfigurationVariables.getInstance().defaultContactOwner LIMIT 1];
        //return list_DefaultContactOwner[0].Id;
        return GlobalConfigurationVariables.getInstance().defaultContactOwner;
        
    }

    private static boolean isAdminUser(){
    
        return (UserInfo.getProfileId() == GlobalConfigurationVariables.getInstance().adminProfile);
        
    }
    
    public Boolean isCompanyEmail(String emailAddress){
 
         String companyEmailRegex = GlobalConfigurationVariables.getInstance().companyEmailRegex ;
         //String companyEmailRegex = '.*(@purple\\.us;@goamerica\\.com).*';
        if(emailAddress=='' || emailAddress==null) {
            return false;
        }
        Pattern myPattern = Pattern.compile(companyEmailRegex);
        Matcher myMatcher = MyPattern.matcher(emailAddress);

        return MyMatcher.matches();

    }
    
    public Integer changeToFromCompanyEmail (String toAddress, String fromAddress){
    
        Boolean isCompanyEmail_FROM = isCompanyEmail(fromAddress);
        Boolean isCompanyEmail_TO = isCompanyEmail(toAddress);
    
        if (isCompanyEmail_FROM == isCompanyEmail_TO) {
            return 0; 
        } else if (isCompanyEmail_FROM) {
            return 1;
        } else {
            return 2;
        }
    
    }

    public Boolean isRecordType (String recordType, String objectType, Id recordTypeId) {
/*    
    
        return ([SELECT Id FROM recordtype 
                    WHERE Id =: recordTypeId 
                    AND name =: recordType 
                    AND SObjectType =: objectType].size() == 1);
*/
        if (GlobalFunctions.oRecordType_list==null ) {
            GlobalFunctions.oRecordType_list=new List <RecordType> ([SELECT Id,name, SObjectType FROM recordtype]);
        }
        for(RecordType rt:GlobalFunctions.oRecordType_list) {
            if(rt.Id==recordTypeId && rt.Name==recordType && rt.SObjectType == objectType) {
                return true;
            }
        }
        return false;
    }


    public void NewSystemLogEntry(String source, String description){
        SystemLog__c log = new SystemLog__c();
        log.Source__c = source;
        log.Description__c = description;
        Insert log;
    }
    
    public void Reset() {

        // Welcome Committee
        Set<String> welcomeCommitteeUsernames = new Set<String> (GlobalConfigurationVariables.getInstance().welcomeCommitteeMemberUsernames.split(';', 0));
        List <User> welcomeCommitteeMembers = [SELECT Id FROM user WHERE username IN :welcomeCommitteeUsernames];
        String Ids = '';
        for (User u : welcomeCommitteeMembers) {
            Ids +=u.Id + ';'; 
        }        
        GlobalConfigurationVariable__c oGCWelcome = GlobalConfigurationVariable__c.getInstance('Welcome Committee Members');
        oGCWelcome.Value__c = Ids;
        update oGCWelcome;

        // Boomi Integration  User
        //User uBoomi = [SELECT Id FROM user WHERE username =:GlobalConfigurationVariables.getInstance().integrationUsername_Boomi];
        GlobalConfigurationVariable__c oGCBoomi = GlobalConfigurationVariable__c.getInstance('Boomi Integration User');
        //oGCBoomi.Value__c = uBoomi.Id;
        oGCBoomi.Value__c = CCGlobal.idBoomi;
        update oGCBoomi;

        // Eloqua Integration  User
        User uEloqua = [SELECT Id FROM user WHERE username =:GlobalConfigurationVariables.getInstance().integrationUsername_Eloqua];
        GlobalConfigurationVariable__c oGCEloqua = GlobalConfigurationVariable__c.getInstance('Eloqua Integration User');
        oGCEloqua.Value__c = uEloqua.Id;
        update oGCEloqua;

        // Default Contact Owner
        User uOwner = [SELECT Id FROM user WHERE username =:GlobalConfigurationVariables.getInstance().defaultContactOwnerUsername];
        GlobalConfigurationVariable__c oGCOwner = GlobalConfigurationVariable__c.getInstance('Default Contact Owner');
        oGCOwner.Value__c = uOwner.Id;
        update oGCOwner;    

        // Admin Profile
        Profile p = [select id from profile where name='System Administrator'];         
        //Profile p = [select id from profile where name='CC System Administrator'];         
        GlobalConfigurationVariable__c oGCAdminProfile = GlobalConfigurationVariable__c.getInstance('Administrator Profile');
        oGCAdminProfile.Value__c = p.Id;
        update oGCAdminProfile;    
    
    }
    
    public string Welcome() {
        return 'hello';
    }
    
    public void Rebuild(){
        RebuildVariable('Administrator Profile','');
        RebuildVariable('Boomi Integration User','');
        RebuildVariable('Boomi Integration User (un)','alexander.romanchuck@clearcaptions.com');
        RebuildVariable('Company Email RegEx','.*(@purple\\.us|@goamerica\\.com|@prcnet\\.net|@stellarrelay\\.com|@hovrs\\.com|@handsonsvs\\.com|@signlanguage\\.com|@purplenetwork\\.net|@clearcaptions\\.com).*');
        RebuildVariable('Default Contact Owner','');
        RebuildVariable('Default Contact Owner (un)','alexander.romanchuck@clearcaptions.com');
        RebuildVariable('Eloqua Integration User','');
        RebuildVariable('Eloqua Integration User (un)','alexander.romanchuck@clearcaptions.com');
        RebuildVariable('Welcome Committee Members','');
        RebuildVariable('Welcome Committee Members (un)','alexander.romanchuck@clearcaptions.com');
    }
    
    private void RebuildVariable (String VariableName, String DefaultValue){
        GlobalConfigurationVariable__c oGCTest = GlobalConfigurationVariable__c.getInstance(VariableName);
        if (oGCTest==null) {
            GlobalConfigurationVariable__c oGC = new GlobalConfigurationVariable__c(
                Name = VariableName,
                Value__c = DefaultValue);
            insert oGC;
        }    
    }
        
}