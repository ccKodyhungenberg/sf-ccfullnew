global class SFaxAPIService {
  private static final String API_HOST = 'https://api.sfaxme.com/api/';
  private static final String CONTENT_TYPE = 'application/pdf';
  private static final String BOUNDARY = '-----------JavaMultipartPost';
  private static final String CRLF = '\r\n';

  private static String getUsername() {
    return SFaxSettings__c.getInstance('Username').Value__c;
  }

  private static String getAPIKey() {
    return SFaxSettings__c.getInstance('APIKey').Value__c;
  }

  private static String getEncryptionKey() {
    return SFaxSettings__c.getInstance('EncryptionKey').Value__c;
  }

  private static String getInitVector() {
    return SFaxSettings__c.getInstance('InitVector').Value__c;
  }

  private static String encryptToken(String token) {
    Blob key = Blob.valueOf(getEncryptionKey());
    Blob iv = Blob.valueOf(getInitVector());
    Blob tokenData = Blob.valueOf(token);

    Blob result = Crypto.encrypt('AES256', key, iv, tokenData);

    return EncodingUtil.base64Encode(result);
  }

  public static String getToken() {
    Map<String, String> sbParts = new Map<String, String>();

    Datetime dt = Datetime.now();
    String dtString = dt.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', '');

    sbParts.put('Username', getUsername());
    sbParts.put('ApiKey', getAPIKey());
    sbParts.put('GenDT', dtString);

    List<String> sbPartsList = new List<String>();
    for(String key : sbParts.keySet()) {
      sbPartsList.add(key + '=' + sbParts.get(key));
    }
    String sbPartsString = String.join(sbPartsList, '&');

    return encryptToken(sbPartsString);
  }

  public static String getQuerystring(Map<String, String> params, String glue) {
    // add the token and key
    if (glue == '&') {
      params.put('token', getToken());
      params.put('ApiKey', getAPIKey());
    }

     // build the body string
    List<String> querystring = new List<String>();
    for (String key : params.keySet()) {
      String value = params.get(key);
      if (value == null){
        value = '';
      } else
      if (key != 'OptionalParams' && key != 'BarcodeOption') {
        value = EncodingUtil.urlEncode(value, 'UTF-8');
      }
      querystring.add(key + '=' + value);
    }
    return String.join(querystring, glue);
  }

  public static String getUTC(Datetime dt) {
    return dt.format('yyyy-MM-dd', 'UTC') + 'T' + dt.format('HH:mm:ss', 'UTC') + 'Z';
  }

  public static SendFaxResponse SendFax(String recipientName, String recipientFax, Map<String, Blob> attachments, Map<String, String> params, Map<String, String> optionalParams) {
    if (params == null) {
      params = new Map<String, String>();
    }
    if (optionalParams == null) {
      optionalParams = new Map<String, String>();
    }

    optionalParams.put('filesarebase64', 'true');
    optionalParams.put('base64filetype', 'pdf');
    String optParamsString = getQuerystring(optionalParams, ';');

    // populate the body map
    params.put('RecipientName', recipientName);
    params.put('RecipientCompanyName', '');
    params.put('RecipientFax', recipientFax);
    if (optParamsString != null) {
      params.put('OptionalParams', optParamsString);
    }
    String querystring = getQuerystring(params, '&');

    String multipart = '';
    for (String name : attachments.keySet()) {
      multipart +=  CRLF + CRLF + '--' + BOUNDARY + CRLF;
      multipart += 'Content-Disposition: form-data; name="userfile"; filename="' + name + '.pdf"' + CRLF + CRLF;
      multipart += EncodingUtil.base64Encode(attachments.get(name)) + CRLF + CRLF;
      multipart += '--' + BOUNDARY;
    }
    // end it by adding two dashes at the end
    multipart += '--';

    String urlRequest = API_HOST + 'SendFax' + '?' + querystring;

    HttpRequest request = new HttpRequest();
    request.setHeader('Content-Type','multipart/form-data; boundary=' + BOUNDARY);
    request.setEndpoint(urlRequest);
    request.setMethod('POST');
    request.setBody(multipart);
      SendFaxResponse faxResponse;
      if(!Test.isRunningTest()){
    HttpResponse res = (new Http()).send(request);
    String body = res.getBody();
    System.debug(body);
    faxResponse = (SendFaxResponse) JSON.deserialize(body, SendFaxResponse.class);
      }
    return faxResponse;
  }

  public static String ReceiveFax(String method, Datetime startDatetime) {
    Map<String, String> params = new Map<String, String>();
    if (startDatetime != null) {
      params.put('StartDateUTC', getUTC(startDatetime));
      // Add 1 to the day to fix a bug in the API that might get fixed - the docs say UTC but seems to be for some offset
      params.put('EndDateUTC', getUTC(Datetime.now().addDays(2)));
    }
    String querystring = getQuerystring(params, '&');

    String urlRequest = API_HOST + method + '?' + querystring;

    HttpRequest request = new HttpRequest();
    request.setEndpoint(urlRequest);
    request.setMethod('GET');

    HttpResponse res = (new Http()).send(request);

    System.debug(res.getBody());
    return res.getBody();
  }

  public static InboundFaxes ReceiveInboundFax(Datetime startDatetime) {
    String jsonString = SFaxAPIService.ReceiveFax('ReceiveInboundFax', startDatetime);
    InboundFaxes faxes = (InboundFaxes) JSON.deserialize(jsonString, InboundFaxes.class);
    return faxes;
  }

  public static Blob DownloadInboundFaxAsPdf(String faxId) {
    Map<String, String> params = new Map<String, String>();
    params.put('FaxId', faxId);
    String querystring = getQuerystring(params, '&');

    String urlRequest = API_HOST + 'DownloadInboundFaxAsPdf' + '?' + querystring;

    HttpRequest request = new HttpRequest();
    request.setEndpoint(urlRequest);
    request.setMethod('GET');

    HttpResponse res = (new Http()).send(request);

    return res.getBodyAsBlob();
  }

  public static OutboundFaxes ReceiveOutboundFax(Datetime startDatetime) {
    String jsonString = SFaxAPIService.ReceiveFax('ReceiveOutboundFax', startDatetime);
    OutboundFaxes faxes = (OutboundFaxes) JSON.deserialize(jsonString, OutboundFaxes.class);
    return faxes;
  }

  /* RESPONSES */
  
  public class SendFaxResponse {
    public String SendFaxQueueId;
    public Boolean isSuccess;
    public String message;
  }

  public class OutboundFaxes {
    public List<Fax> OutboundFaxItems;
  }

  public class InboundFaxes {
    public List<Fax> InboundFaxItems;
  }

  public class Fax {
    public String SendFaxQueueId;
    public String OutboundFaxId;
    public String FaxId;
    public String Pages;
    public String ToFaxNumber;
    public String FromFaxNumber;
    public String FromCSID;
    public String FaxDateUtc;
    public String FaxSuccess;
    public Barcodes Barcodes;
    public String InboundFaxId;
    public String FaxPages;
    public String FaxDateIso;
    public String WatermarkId;
    public String CreateDateIso;
    public String ResultMessage;
    public String RecipientName;
    public Boolean isSuccess;
  }

  public class Barcodes {
    public String FirstBarcodePage;
    public String TotalPagesWithBarcodes;
    public List<Integer> PagesWithBarcodes;
    public List<BarcodeItem> BarcodeItems;
  }

  public class BarcodeItem {
    public Integer BarcodeSpacingXAxis;
    public Integer BarcodeSpacingYAxis;
    public Integer BarcodeType;
    public Integer BarcodeMode;
    public String BarcodeData;
    public Integer BarcodeX;
    public Integer BarcodeY;
    public Integer BarcodePage;
    public Integer BarcodeScale;
    public Integer BarcodeWidth;
    public Integer BarcodeHeight;
  }
}