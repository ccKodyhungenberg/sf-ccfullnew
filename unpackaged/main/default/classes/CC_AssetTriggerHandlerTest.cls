// C-00250021 2/19/19 NCarson - Removed assert as functionality had been removed in TriggerHandler
// S-628700 07/09/2019 MKLICH - Updated Test classes to test Number_Of_Active_Assets__c

@isTest
private class CC_AssetTriggerHandlerTest {
	@isTest
    static void testAssetTrigger(){
        List<Account> accList = CC_TestUtility.createAccount(1, true);
        List<Contact> cont = CC_TestUtility.createContact(1, true, 'Customer', accList[0].id, '');
        List<Order> orderList = CC_TestUtility.createOrder(5, true, accList[0].Id);
        List<Contact> techcont = CC_TestUtility.createContact(1, true, 'Technician', accList[0].id, '');
        List<WorkOrder> workorderList = CC_TestUtility.createWorkOrder(5, false, orderList[0].Id);
        for(WorkOrder wo : workOrderList) {
            wo.ContactId = cont[0].id;
            wo.Service_Request_Type__c = 'RMA Replacement';
            wo.Status = 'New';
            wo.Technician__c = techcont[0].id;
        }
        insert workOrderList;
        
        List<Asset> assetList = CC_TestUtility.createAssetRecords('test asset', 5, false,null);
        for(Asset newAss: assetList){
            newAss.contactId = cont[0].id;
            newAss.Type__c = 'Clarity';
        }
        assetList.get(3).Type__c = 'Clarity2';
        if(assetList.size()>0){
            insert assetList;
        }
        
		system.assertEquals(5, [SELECT id, Number_Of_Active_Assets__c FROM Contact WHERE Id = :cont[0].id].Number_Of_Active_Assets__c); // MKLICH S-628700 Update to Number_Of_Active_Assets__c
        
        // 2/19/19 C-00250021 NCarson - Commenting this out, as functionality has been removed in ccFullNew org - see line 132 of AssetTriggerHandler
        //system.assertEquals(assetList.get(3).Id, [SELECT AssetId FROM WorkOrder WHERE Id = : workorderList[0].id].AssetId);
        
        delete assetList;
        system.assertEquals(0, [SELECT id, Number_of_Clarity_Devices__c FROM Contact WHERE Id = : cont[0].id].Number_of_Clarity_Devices__c);
    }
    /************************************************
    * Method : testBeforeUpdate
    * Descreption : Test updateAccountOnAsset() Method on Before Insert/Update events
    * Created By : Nilesh Grover for Case #00250394
    *************************************************/    
    @isTest
    static void testUpdateAccountOnAsset(){
        List<Account> accList = CC_TestUtility.createAccount(2, true);
        system.assertEquals(2, [SELECT Id FROM Account].size());
        
        List<Contact> contactList = CC_TestUtility.createContact(1, true, 'Customer',accList[0].Id , 'Test123@gmail.com');
        system.assertEquals(1, [SELECT Id FROM Contact].size());
        
        CC_TestUtility.createAssetRecords('testasset', 1, true, accList[1].Id);
        
        
        system.assertEquals(1, [SELECT Id FROM Asset].size());
        system.assertEquals(accList[1].Id, [SELECT Id, AccountId FROM Asset LIMIT 1].AccountId);
        
        List<Asset> AssetList = [SELECT Id, ContactId FROM Asset];
        for(Asset asset : AssetList){
            asset.ContactId = contactList[0].Id;
        }
        Test.startTest();
        update AssetList;
        Test.stopTest();
        system.assertEquals(1, [SELECT Id FROM Asset].size());
        system.assertEquals(accList[0].Id, [SELECT Id, AccountId FROM Asset LIMIT 1].AccountId);        
    }
    
    /************************************************
    * Method : testNumberOfActiveAssets
    * Descreption : Test new Functionality added in S-628700
    * Created By : Mitch Klich
    *************************************************/  
    @isTest
    static void testNumberOfActiveAssets(){
        List<Account> accList = CC_TestUtility.createAccount(1, true);
        List<Contact> cont = CC_TestUtility.createContact(1, true, 'Customer', accList[0].id, '');
        
        Contact updateCon = new Contact();
        updateCon.Id = cont[0].Id;
        updateCon.type__c = 'Potential Customer';
        update cont;
        
        
        
        System.debug('Potential ' + [Select Id, Type__c FROM Contact]);
        
        List<Asset> assetList = CC_TestUtility.createAssetRecords('test asset', 5, false,null);
        for(Asset newAss: assetList){
            newAss.AccountId = accList[0].Id;
            newAss.contactId = cont[0].Id;
            newAss.Type__c = 'Clarity';
        }
        assetList.get(3).Type__c = 'Abstract Type';
        assetList.get(4).Type__c = 'Other';
        
        if(assetList.size()>0){
            insert assetList;
        }
        
        //System.assertEquals('Customer')
        
        
        // 3 Assets of type Clarity, 1 of 'Abstract Type', 1'Other' AND all are currently active which means CB_Active__c = True and Deleted__c = False
        // Assert that the number of 'Active' Assets is 5
		system.assertEquals(5, [SELECT id, Number_Of_Active_Assets__c FROM Contact WHERE Id = : cont[0].id].Number_Of_Active_Assets__c);
        
        // Test 'Deactivate' the Assets, Deactivate means that Deleted__c = true, Device_Deactivation_Reason__c != NULL. Then Workflow sets CB_Active__c to false
        assetList.get(0).Deleted__c = True;
        assetList.get(0).Device_Deactivation_Reason__c = 'Demo';
        assetList.get(1).Deleted__c = True;
        assetList.get(1).Device_Deactivation_Reason__c = 'Demo';
        
        update assetList;
        
        // Since 2 Assets were deactivated, only 3 should remain on the Contact as Active
        system.assertEquals(3, [SELECT id, Number_Of_Active_Assets__c FROM Contact WHERE Id = : cont[0].id].Number_Of_Active_Assets__c);
      
    }
    // MKLICH S-628700 STOP 
    
}