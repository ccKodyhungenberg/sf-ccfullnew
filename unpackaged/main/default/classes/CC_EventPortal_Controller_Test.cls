@isTest 
private class CC_EventPortal_Controller_Test {
    static void  createTestData(){
        //create Account
        List<Account> accList = CC_TestUtility.createAccount(1,true);
        for(Account acc : accList)
        {
            System.assertNotEquals(null, acc.Id);
        }
        
        //create contact
        List<Contact> customerContact = CC_TestUtility.createContact(1, true, 'Technician', accList[0].id,'test@clearcaptions.com.com'); //Modified by Tanisha Gupta
        for(Contact con : customerContact)
        {
            System.assertNotEquals(null, con.Id);
        }
        
        //create Lead
        List<Lead> leadList = CC_TestUtility.createLead(1, true);
        for(Lead lead : leadList)
        {
            System.assertNotEquals(null, lead.Id);
        }
        
        //create campaign
        Campaign camp = new Campaign();
        camp.Name = 'test camp';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today();
        camp.Channel__c = 'Direct Sales';
        camp.IsActive = true;
        camp.Event_POC__c = UserInfo.getUserId();
        camp.Secondary_Event_POC__c = UserInfo.getUserId();
        camp.RecordTypeId=Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Large Event').getRecordTypeId();
        insert camp;
        
        //create Product
        List<Product2> productList = CC_TestUtility.createProduct(2, false);
        productList[0].Is_Default_Product__c = true;
        productList[1].Is_Default_Product__c = false;
        insert productList;
        
        // create PriceBook
        Pricebook2 priceBookRecord = new Pricebook2( Name = 'Test Pricebook');
        insert priceBookRecord;
        
        //create Order
        List<Order> orderList = CC_TestUtility.createOrder(1, false,accList[0].id);
        orderList[0].status = 'Open';
        orderList[0].Campaign__c = camp.id;
        orderList[0].Pricebook2ID = priceBookRecord.Id;
        insert orderList;
        
        
        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true );
        insert pb;
        
        Id standardPB = Test.getStandardPricebookId();
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPB, Product2Id = productList[0].Id, UnitPrice = 1000, IsActive = true);
            insert standardPBE;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = priceBookRecord.Id, Product2Id = productList[0].Id, UnitPrice = 1000, IsActive = true);
            insert pbe;
        
        
        //create OrderItem
        OrderItem orderItemRecord = new OrderItem( OrderId = orderList[0].id, 
                                                  Product2ID = productList[0].id,
                                                  PriceBookEntryId = pbe.id,
                                                  Item_Code__c = 'CC0002',
                                                  Item_Name__c = 'Clarity phone Thor B',
                                                  Quantity=1,
                                                  UnitPrice = 5,
                                                  Order_Line_Item_Status__c='Open');
        insert orderItemRecord;
        
        //create WorkOrder
        List<WorkOrder> workOrderList = CC_TestUtility.createWorkOrder(1,false,orderList[0].id);
        workOrderList[0].Status = 'New';
        workOrderList[0].StartDate = System.today();
        workOrderList[0].ContactId = customerContact[0].id;
        workOrderList[0].Technician__c = customerContact[0].id;
        workOrderList[0].Service_Request_Status__c = 'Scheduled';
        workOrderList[0].Start_Time_Window__c = '7:00 am - 9:00 am';
        insert workOrderList;
    }
    
    static testMethod void test_Dummy() {
        createTestData();
        
        
        
        List<Contact> con = [SELECT Name, MailingCity, MailingState, MailingStreet, MailingPostalCode, MailingCountry,Labor_Source__c FROM Contact LIMIT 1];
        List<Product2> prod = [SELECT Is_Default_Product__c, Name, IsActive FROM Product2];
        Campaign camp = [SELECT IsActive, Name FROM Campaign LIMIT 1];
        Order orderRecord = [SELECT Name, EffectiveDate, AccountId, ShippingCity, ShippingPostalCode, ShippingState,
                             Shipping_Name__c, ShippingStreet, Customer_City__c, Status, campaign__C FROM Order LIMIT 1];
        OrderItem orderItemRecord = [SELECT Item_Code__c, Item_Name__c, Quantity, Order_Line_Item_Status__c FROM OrderItem];
        WorkOrder wo = [SELECT Status, StartDate, ContactId, Service_Request_Status__c ,Start_Time_Window__c, Technician__c FROM WorkOrder];
        
        
        Test.startTest();
        
        CC_EventPortalController ctr=new CC_EventPortalController();
        ctr.ordr = orderRecord;
        ctr.oCurrent_Campaign = camp;
        ctr.lChannel_Campaigns = null;
        ctr.SR = wo;
        
        Test.setCurrentPageReference(new PageReference('Page.CC_EventPortalController'));
        System.currentPageReference().getParameters().put('counter','0');
        List<Lead> leadList = [SELECT firstName, lastName, Street, City, State, Phone, PostalCode, Email, MobilePhone FROM Lead];
        ctr.Order_Lead  = leadList[0];
        ctr.Order_Contact = con[0];
        ctr.sRender_op01_Config = 'testConfigString';
        ctr.sRender_op02_CustomerEntry = 'testCustEntryString';
        ctr.sSetFocus = '';
        ctr.lContactCandidate = con;
        ctr.sRender_op05_CurrentSchedule = '';
        ctr.sRender_op05_ScheduleSR = 'true';
        ctr.sRender_op06_ConfirmSR = 'true';
        ctr.sRender_op02_PickAddress_NotEmpty = 'testAddress';
        ctr.sRender_op02_PickAddress_Empty = '';
        ctr.iSuggestion_index = 0;
        ctr.SR.StartDate = System.today();
        ctr.idCurrent = con[0].id;
        ctr.SR_Technician = con[0];
        ctr.password = 'Test@pass1';
        ctr.confirmPassword = 'Test@pass1';
        //ctr.password = '';
        //ctr.confirmPassword = '';
        ctr.sMessage_op07_ConfirmedSR = '';
        ctr.cbUserId = 'testcbUserId';
        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('1'));
        ctr.op02_Next();
        //ctr.savePassword();
        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('2'));
        ctr.op02_Next();
        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('3'));
        ctr.op02_Next();
        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('4'));
        ctr.op02_Next();
        CC_EventPortalController.oScheduleElement testSE=new CC_EventPortalController.oScheduleElement();
        CC_EventPortalController.oScheduleElement testSE2=new CC_EventPortalController.oScheduleElement('sStartTime_p','sElementType_p','sElementName_p','sElementAddress_p',0.0,null);
        ctr.lExistingSchedule.add(testSE2);
        Map<id,Campaign> idToCampMap = new Map<Id,Campaign>();
        idToCampMap.put(camp.id,camp);
        ctr.m_Id_CampaignEvent = null;
        system.debug(ctr.lExistingSchedule);
        system.debug(ctr.sRender_op01_Config);
        system.debug(ctr.sRender_op02_CustomerEntry);
        system.debug(ctr.sSetFocus);
        system.debug(ctr.sRender_op02_PickAddress);
        system.debug(ctr.sRender_op03_CustomerSearch);
        system.debug(ctr.sRender_op04_PickCustomer);
        system.debug(ctr.sRender_op05_ScheduleSR);
        system.debug(ctr.sRender_op05_CurrentSchedule);
        system.debug(ctr.sRender_op06_ConfirmSR);
        system.debug(ctr.sRender_op07_ConfirmedSR);
        system.debug(ctr.SR_Technician);
        system.debug(ctr.ordr);
        system.debug(ctr.OrderLI);
        system.debug(ctr.Order_Contact);
        system.debug(ctr.SR);
        system.debug(ctr.m_Id_ContactCandidate);
        system.debug(ctr.lContactCandidate);
        system.debug(ctr.idCurrent);
        system.debug(ctr.m_Id_CampaignEvent);
        system.debug(ctr.lChannel_Campaigns);
        system.debug(ctr.oCurrent_Campaign);
        system.debug(ctr.bOverwriteAddressVerification);
        system.debug(ctr.sMessage_op07_ConfirmedSR);
        System.debug(ctr.cc_passwordSection);
        System.debug(ctr.showQualificationSection);
        
        PageReference pr1=ctr.Cancel();
        PageReference pr2=ctr.op01_Next();
        ctr.op04_SelectExistingContact();
        ctr.getProducts();
        ctr.op02_Clear();
        ctr.captureLeadDetails();
        ctr.savePassword();
        ctr.op02_OverwriteAddress();
        
        ctr.op04_ReEnterEmail();
        ctr.op05_GetCurrentSchedule();
        ctr.op05_SetAppointment();
        ctr.op06_ChangeSR();
        ctr.op07_ConfirmedSR();
        ctr.op02_PickAddress();
        PageReference pr3=ctr.back_to_op01();
        
        List<String> msgList = new List<String>();
        
        ctr.addRow();
        ctr.op06_ConfirmSR();
        ctr.removeRow();
        
        
        Test.stopTest();
        
        List<SelectOption> wProductList = new List<SelectOption>();
        CC_EventPortalController.WorkOrderWrapper woWrapperList = 
            new CC_EventPortalController.WorkOrderWrapper(orderItemRecord, wo, wProductList);
        
        
        /*    CC_EventPortalController.clResult resultClass = new CC_EventPortalController.clResult();
resultClass.Callback = '';
resultClass.ElapsedTime = '';
resultClass.HRef = '';
resultClass.Messages = msgList ;
resultClass.Server = '';
resultClass.Status = '';
resultClass.Version = '';
*/
        //create an instance of wrapper class
        CC_EventPortalController.clSuggestion sugClsWrapper = new CC_EventPortalController.clSuggestion();
        sugClsWrapper.City = 'ROCKLIN';
        sugClsWrapper.State = 'CA';
        sugClsWrapper.Street = '595 MENLO DR';
        sugClsWrapper.APT = '';
        sugClsWrapper.Zip = '95765';
        
        CC_EventPortalController.suggestedAddr sugAddress = new CC_EventPortalController.suggestedAddr();
        //sugAddress.clSuggestion.add(sugClsWrapper);
        
        CC_EventPortalController.clValidateAddressResult valAddResult = new CC_EventPortalController.clValidateAddressResult();
        valAddResult.Code = 'ERROR';
        valAddResult.Message = '';
        valAddResult.Result = 'The supplied address was not found, although similar matches were found.';
        //   valAddResult.suggestedAddr = sugAddress;
        
        CC_EventPortalController.clSuggestion_Item sugItemClsWrapper = new CC_EventPortalController.clSuggestion_Item();
        sugItemClsWrapper.index = 1; 
        sugItemClsWrapper.sAddress = '';
        sugItemClsWrapper.oSuggestion = sugClsWrapper;
        
        System.debug(sugItemClsWrapper);
        ctr.lSuggestion_Item = new List<CC_EventPortalController.clSuggestion_Item>();
        ctr.lSuggestion_Item.add(sugItemClsWrapper);
        System.debug(ctr.lSuggestion_Item);
        ctr.op02_PickAddress();
        
    }
    
    static testMethod void testOverwriteAddress(){
        Contact con = new Contact();
        CC_EventPortalController ctr=new CC_EventPortalController();
        ctr.op02_OverwriteAddress();
    }
    
    static testMethod void testConfirmSR(){
        createTestData();
        List<Contact> con = [SELECT Name, MailingCity, MailingState, MailingStreet, MailingPostalCode, MailingCountry,Labor_Source__c 
                             FROM Contact 
                             LIMIT 1];
        CC_EventPortalController ctr=new CC_EventPortalController();
        ctr.Order_Contact = con[0];
        ctr.idCurrent = con[0].id;
        ctr.cbUserId = 'testcbUserId';
        ctr.op06_ConfirmSR();
    }
}