@isTest
private class Sales_Portal_Test {

    @testSetup
    static void testSetup(){
        List<Campaign> campList = CC_TestUtility.createCampaign(2,false);
        campList[0].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Large Event').getRecordTypeId();
        campList[1].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Event').getRecordTypeId();
        //campList[2].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Hearing Professional').getRecordTypeId();
        //campList[3].recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Marketing').getRecordTypeId();

        for(Campaign camp : camplist){
            camp.StartDate = Date.today().addDays(-10);
            camp.EndDate = Date.today();
            camp.Channel__c = 'Direct Sales';
            camp.IsActive = true;
            camp.Event_POC__c = UserInfo.getUserId();
            camp.Secondary_Event_POC__c = UserInfo.getUserId();
            camp.Location_State__c = 'OH';
            camp.Location_Postal_Code__c = '45601';
            camp.Location_City__c = 'TEST';
            camp.Location_Street_Address__c = 'TEST';
        }
        insert campList;

        List<Account> accList = CC_TestUtility.createAccount(1,true);
        List<Contact> customerContact1 = CC_TestUtility.createContact(1, true, 'Customer', accList[0].id,'tes111t@clearcaptions.com');
        List<Contact> customerContact = CC_TestUtility.createContact(2, true, 'Technician', accList[0].id,'test@clearcaptions.com');

        customerContact[1].LastName = 'Field Nation';
        update customerContact;

        List<Lead> leadList =  CC_TestUtility.createLead(1,false);
        leadList[0].campaign__c = campList[0].Id;
        insert leadList;
    }

    @isTest
    static void testMethod1(){
        Account acc = [Select id from account limit 1];
        List<Contact> customerContact = CC_TestUtility.createContact(1, false, 'Technician', acc.id,'test123@clearcaptions.com');
        customerContact[0].LastName = userInfo.getName();
        insert customerContact;
        Test.startTest();
        String profileName = SalesportalController.getProfileName();
        System.assertEquals(profileName,'System Administrator');
        SalesportalController.getUserRoleName();
        List<Contact> conList = SalesportalController.populateFieldNation();
        System.assert(conList.size()>0);
        SalesPortalController.getSelectEventFromCache();
        Contact tech = SalesPortalController.getTechnician();
        System.assert(tech!=null);
        conlist = SalesportalController.SearchEmail('test@clearcap12tions.com');
        System.assert(conList.size()==0);
        Test.stopTest();
    }

    @isTest
    static void SalesPortalControllerTest1(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('1'));
        SalesPortalController.exceptionWrap  wrap = SalesPortalController.ValidateAddress('{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route","apt":"207","country":"US","Mphone":"8767898767"}','createcustomer');

        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('2'));
        SalesPortalController.ValidateAddress('{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route","apt":"207","country":"US","Mphone":"8767898767"}','createcustomer');

        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('3'));
        SalesPortalController.ValidateAddress('{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route","apt":"207","country":"US","Mphone":"8767898767"}','createcustomer');

        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('1'));
        SalesPortalController.ValidateAddress('{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route","apt":"207","country":"US","Mphone":"8767898767"}','secondaryuser');

        test.stoptest();
        SalesPortalController.productList();
        SalesPortalController.populateStartTimeWindow();
        System.assertEquals('200',wrap.statusCode);
    }

    @isTest
    static void SalesPortalControllerTest2(){
        Campaign camp = [Select id from Campaign LIMIT 1];
        Contact conRec = [Select id from Contact LIMIT 1];
        string ContactInfo = '{"Email":"demo12345@gmail.com","HPhone":"1231931321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route 207","country":"US","apt":"12b","Mphone":"8767898767","Smart_phone_type__c":"iOS"}';
        string ContactInfo1 = '{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route 207","country":"US","apt":"12b","Mphone":"8767898767","Smart_phone_type__c":"Android"}';

        SalesPortalController.exceptionWrap  wrap = SalesPortalController.createLeadAndContact(camp.id,ContactInfo,true, false, null,'createcustomer');

        System.assert(wrap.msg.contains('Contact Created'));
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockCC_EventPortal('1'));
        SalesPortalController.exceptionWrap  wrap1 = SalesPortalController.createUser(conRec.Id);
        System.assert(!String.isBlank(wrap.msg));

        test.stoptest();
    }

    @isTest
    static void SalesPortalControllerTest3(){
        Campaign camp = [Select id from Campaign LIMIT 1];
        Contact conRec = [Select id,Name,Labor_Source__c from Contact  WHERE Recordtype.Name = 'Technician' LIMIT 1 ];
        conRec.Labor_Source__c = 'Field Nation';
        conRec.Type__c = 'Technician';
        update conRec;
        string ContactInfo1 = '{"Email":"demo1234@gmail.com","HPhone":"1231231321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route 207","country":"US","apt":"12b","Mphone":"8767898767","Smart_phone_type__c":"Android"}';
        Map<String,String> riskCodeMap= new Map<String,String>();
        riskCodeMap.put('8','Address Invalid');
        riskCodeMap.put('25','DOB Invalid');
        SalesPortalController.exceptionWrap wrap = SalesPortalController.createContactWithQualification(camp.Id,
                ContactInfo1,
                true,
                'true',
                'true',
                'true',
                'true',
                'Android',
                conRec.Id,
                Date.today(),
                null,
                'createcustomer',
                riskCodeMap,
                '6',false);
        System.assert(wrap.msg.contains('Contact Created'));
        SalesPortalController.createContactWithQualification(camp.Id,
                ContactInfo1,
                false,
                'true',
                'true',
                'true',
                'true',
                'Android',
                conRec.Id,
                Date.today(),
                null,
                'createlead',
                riskCodeMap,
                '6',false);

        List<String> laborList = SalesPortalController.getLabourSource();
        System.assert(laborList.size()>0);
        List<String> fullfullMentList = SalesPortalController.populateFulfillment();
        System.assert(fullfullMentList.size()>0);
        //Instantiate the Pricebook2 record with StandardPricebookId
        Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
        );

        //Execute an update DML on the Pricebook2 record, to make IsStandard to true
        Update standardPricebook;

        SalesPortalController.WorkOrderWrapper objWork = new SalesPortalController.WorkOrderWrapper();
        Product2 objProd = new Product2();
        objProd.Name ='tests';
        insert objProd;

        //Create the PricebookEntry
        PricebookEntry pbe = new PricebookEntry(
                Pricebook2Id = standardPricebook.Id,
                Product2Id = objProd.Id,
                UnitPrice = 1020,
                IsActive = true
        );
        Insert pbe;

        OrderItem objorder = new OrderItem();
        objorder.Product2Id = objProd.Id;

        string recordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install and Training').getRecordTypeId();
        WorkOrder objworkord = new WorkOrder();
        objworkord.Labor_Source__c='Field Nation';
        objworkord.recordTypeId = recordTypeId;
        objworkord.Status='Scheduled';
        objworkord.created_from_portal__c=true;
        objworkord.Service_Request_Type__c='Install and Training';
        objworkord.Service_Notes__c='test';
        objworkOrd.ContactId = conRec.Id;
        objworkOrd.startDate = System.Now().addDays(2);
        //insert objworkord;

        objWork.ordrLineItem = objorder;
        objWork.technician = ConRec;
        objWork.product = objProd;
        objWork.workOrder = objworkord;
        objWork.technician = conRec;

        list<SalesPortalController.WorkOrderWrapper> lstWorkOrderWrapper = new List<SalesPortalController.WorkOrderWrapper>();
        lstWorkOrderWrapper.add(objWork);
        Test.startTest();
        SalesPortalController.WorkOrderWrapperResponse workOrderResp = SalesPortalController.saveAppointmentWorkOrder(wrap.ContactId, lstWorkOrderWrapper,false,'createworkorder');
        Test.stopTest();
        System.assertEquals('success',workOrderResp.responseMessage);

    }

    @isTest
    static void SalesPortalControllerTest4(){
        Contact conRec = [Select id,AccountId from Contact WHERE Recordtype.Name = 'Customer' LIMIT 1];
        Lead lead = [SELECT Id from Lead limit 1];
        Lead lead1 = SalesPortalController.getLeadData(lead.Id);
        System.assert(lead1!=null);
        Contact con = SalesPortalController.getCustomerData(conRec.Id,'installworkorder');
        System.assert(con!=null);
        con = SalesPortalController.getCustomerData(conRec.AccountId,'secondaryuser');
        System.assert(con!=null);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new LexisNexisMock());
        string ContactInfo = '{"Email":"demo12345@gmail.com","HPhone":"1231931321","FirstName":"demo0987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route 207","country":"US","apt":"12b","Mphone":"8767898767","Smart_phone_type__c":"iOS"}';
        SalesPortalController.LexisNexisWrapper wrap = SalesPortalController.makeLexisNexisCallout(contactInfo,System.TOday());

        Test.stopTest();
        System.assertEquals(200, wrap.requestStatus);

    }

    @isTest
    static void SalesPortalControllerTest5(){
        Contact conRec = [Select id,AccountId from Contact WHERE Recordtype.Name = 'Customer' LIMIT 1];

        Test.startTest();
        string ContactInfo = '{"Email":"demo11112345@gmail.com","HPhone":"1232321931321","FirstName":"demo01987","LastName":"demo12344","city":"chillicoth","state":"oh","ZipCode":"45601","street":"1285 state route 207","country":"US","apt":"12b","Mphone":"8767898767","Smart_phone_type__c":"iOS"}';
        SalesPortalController.ExceptionWrap wrap = SalesPortalController.createMinorContact(contactInfo,System.TOday(),conRec.AccountId);

        Test.stopTest();
        System.assert(wrap.msg.contains('Contact Created'));

    }

}