public class CC_LeadTriggerHandler {
    public static void onAfterUpdate(List<Lead> newList, Map<Id,Lead> oldMap){
        Map<Id,Lead> leadIdToLeadMap = new Map<Id,Lead>();
        Set<Id> leadIdSet = new Set<Id>();
        List<Customer_Document__c> cdList = new List<Customer_Document__c>();
        List<Contact> updateContacts = new List<Contact>();
        //Added by Rohit for Story S-648512
        Set<String> strSet=new Set<String>();
        strSet.addAll(Label.Lead_State_Label.split(','));
		//END Rohit for Story S-648512
        for(Lead newLead: newList){
            if(newLead.isConverted == true && oldMap.get(newLead.id).isConverted == false){
                leadIdToLeadMap.put(newLead.id,newLead);
                leadIdSet.add(newLead.id);
                //START Rohit for Story S-648512
                if(strSet!=null && strSet.size()>0){                         				
                    System.debug('Here at line 86');            		                
                    if(!strSet.contains(newLead.State))
                    {                        
                        newLead.addError(System.label.error_message);
                    }
                }
				//END  S-648512 
            }
        }
        if(leadIdToLeadMap.size()>0){
            List<ContentDocumentLink> cdlToDelete = new List<ContentDocumentLink>();
            List<ContentDocumentLink> cdlToInsert = new List<ContentDocumentLink>();
            Map<id,Customer_Document__c> cdlIdToCdMap = new Map<id,Customer_Document__c>();
            Map<id,Id> cdlIdToConIdMap = new Map<id,Id>();
            Map<Id,Case> contactIdToCaseMap = new Map<id,Case>();
            List<Case> allCases = [select Id, Lead__c FROM Case WHERE lead__c IN :leadIdToLeadMap.keySet()];  
            for(Case ca: allCases){
                if ( leadIdToLeadMap.containsKey( ca.Lead__c ) ) {
                    // lookup the parent lead
                    Lead parentLead = leadIdToLeadMap.get( ca.Lead__c );
                    // update the fields on the child object
                    ca.ContactId = parentLead.ConvertedContactId;
                    ca.Status = 'Closed - Approved';
                    contactIdToCaseMap.put(parentLead.ConvertedContactId, ca);
            	}
            }
            if(allCases.size()>0){
                update allCases;
            }
            Map<Id,ContentDocumentLink> cdlList = new Map<Id,ContentDocumentLink>([SELECT Id,LinkedEntityId,ContentDocumentId,ShareType FROM ContentDocumentLink WHERE LinkedEntityId IN: leadIdSet AND LinkedEntity.Type='Lead']);
            for(ContentDocumentLink cdl: cdlList.values()){
                Customer_Document__c cd = new Customer_Document__c();
                cd.Contact__c = leadIdToLeadMap.get(cdl.LinkedEntityId).convertedContactId;
                cd.Created_from_lead_conversion__c = true;
                cd.Document_Type__c = 'PCF Form';
                if(contactIdToCaseMap.containsKey(cd.Contact__c)){
                	cd.Case__c = contactIdToCaseMap.get(cd.Contact__c).id;
                }
                
                cdList.add(cd);
                cdlIdToConIdMap.put(cdl.id,leadIdToLeadMap.get(cdl.LinkedEntityId).convertedContactId);
                cdlIdToCdMap.put(cdl.id, cd);
            }
            
            
            if(cdList.size()>0){
                insert cdList;
            }
            // Lokesh korani case no.00253932 31/1/2019
            if(contactIdToCaseMap != null && cdlIdToConIdMap != null) {
            for(Id cdlId :cdlIdToCdMap.keySet()){
                if(cdlId != Null){
                    // Lokesh korani case no.00253932 31/1/2019
                Contact conToUpdate = new Contact(Id = cdlIdToConIdMap.get(cdlId), PCF_Form__c = cdlIdToCdMap.get(cdlId).id,PCF_Approved__c = true, Latest_Pcf_Approval__c = (contactIdToCaseMap.get(cdlIdToConIdMap.get(cdlId)) != NULL ? contactIdToCaseMap.get(cdlIdToConIdMap.get(cdlId)).id : null) );
                updateContacts.add(conToUpdate);
                ContentDocumentLink newclnk = cdlList.get(cdlId).clone();
  				newclnk.linkedentityid = cdlIdToCdMap.get(cdlId).id; //This ID is the New customer document Id.
                cdlToInsert.add(newclnk);
                cdlToDelete.add(cdlList.get(cdlId));
            }
            }
        }
            if(updateContacts.size()>0){
                update updateContacts;
            }
            if(cdlList.size()>0){
                insert cdlToInsert;
            }
            if(cdlToDelete.size()>0){
                delete cdlToDelete;
            }
        }
    }
    /*START Rohit for Story S-648512
public static void onBeforeUpdate(List<Lead> newList, Map<Id,Lead> oldMap){    

Set<String> strSet=new Set<String>();
strSet.addAll(Label.Lead_State_Label.split(','));
if(strSet!=null && strSet.size()>0){                         
for(Lead lead: newList){
System.debug('Here at line 86');
if(lead.IsConverted && !oldMap.get(lead.id).IsConverted){

if(!strSet.contains(lead.State))
{
lead.addError('Address must be populated with a valid two-letter State abbreviation');
}
}
}

}
} */
}