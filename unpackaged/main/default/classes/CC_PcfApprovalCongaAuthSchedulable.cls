global class CC_PcfApprovalCongaAuthSchedulable implements Schedulable {
    global void execute(SchedulableContext sc) {
        CC_PcfApprovalCongaAuthSchedulable.process();
    }
    
    @future(callout=true)
    static void process() {
        PCF_Settings__c settings = PCF_Settings__c.getInstance('Default');
        
        String body = 'grant_type=password&client_id=' + EncodingUtil.urlEncode(settings.oAuth_Client_ID__c, 'utf-8') +
            '&client_secret=' + EncodingUtil.urlEncode(settings.oAuth_Client_Secret__c, 'utf-8') + 
            '&username=' + EncodingUtil.urlEncode(settings.oAuth_Username__c, 'utf-8') + '&password=' + EncodingUtil.urlEncode(settings.oAuth_Password__c, 'utf-8');
        
        System.debug(body);
        
        // Login via oAuth
        HttpRequest req = new HttpRequest();
        req.setEndpoint(settings.oAuth_URL__c);
        req.setMethod('POST');
        req.setBody(body);
        
        HttpResponse res = (new Http()).send(req);
        CC_PcfApprovalCongaAuthSchedulable.AuthResponse authInfo = (CC_PcfApprovalCongaAuthSchedulable.AuthResponse) JSON.deserialize(res.getBody(), CC_PcfApprovalCongaAuthSchedulable.AuthResponse.class);
        System.debug(authInfo+'-----------------------'+res);
        
        // set the session id to be the access token from the oAuth login
        settings.Session_ID__c = authInfo.access_token;
        update settings;
    }
    
    public class AuthResponse {
        public String access_token;
        public String instance_url;
        public String id;
        public String token_type;
        public String issued_at;
        public String signature;
    }
}