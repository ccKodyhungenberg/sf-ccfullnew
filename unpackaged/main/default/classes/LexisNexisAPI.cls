public class LexisNexisAPI {

   public static String makeCallouttoLxNx(string firstname,
            string lastname,
            string ssn ,
            string mobile,
            string birthdate,
            string mailingstreet,
            string mailingstate,
            string mailingcity,
            string zipcode) {
            
        LxNx.RootObject rootObj = new LxNx.RootObject();
        LxNx.FlexIDRequest flexObj = new LxNx.FlexIDRequest();
        LxNx.SearchBy srchObj = new LxNx.SearchBy();
        LxNx.Name nmObj = new LxNx.Name();
        LxNx.User  usrObj = new LxNx.User();
        LxNx.EndUser endUsrObj = new LxNx.EndUser();
        LxNx.Options  optObj = new LxNx.Options  ();
        LxNx.DOB  dob = new LxNx.DOB() ;
        if(birthdate!= null && birthdate != ''){
            string[] date1 = birthdate.split('/');
            dob.Year =integer.valueOf(date1[2]);
            dob.Month =integer.valueOf(date1[0]);
            dob.Day =integer.valueOf(date1[1]);
        }
        LxNx.Address  add = new LxNx.Address();
        
        if(zipcode != null && zipcode != ''){
            add.Zip5 = zipcode ;
        }
        if(mailingstreet != null && mailingstreet != ''){
            add.StreetAddress1 = mailingstreet;
        }
        if(mailingcity!= null && mailingcity!= ''){
            add.City = mailingcity;
        }
        if(mailingstate!= null && mailingstate!= ''){
            add.State = mailingstate;
        }


        LxNx.Passport pass = new LxNx.Passport();
        pass.Number1 = null;
        LxNx.ExpirationDate expPass = new LxNx.ExpirationDate();
        expPass.Year = 0;
        expPass.Month=0;
        expPass.Day=0;
        pass.ExpirationDate = expPass;

        srchObj.DOB = dob;
        srchObj.Address = add;
        srchObj.Passport =pass;
        if(ssn  != null && ssn  != ''){
            system.debug('ssnis-----' + ssn );
            if(ssn.length() > 4){
                srchObj.SSN = ssn ;
            }else{
                srchObj.SSNLast4 =ssn ;
            }
        }
        
        flexObj.SearchBy = srchObj;


        nmObj.Full=firstname +''+lastname;
        if(firstname != null && firstname != ''){
            nmObj.First=firstname;
        }
        
        nmObj.Middle=null;
        if(lastname != null && lastname != ''){
            nmObj.Last =lastname;
        }
        
        nmObj.Suffix =null;
        nmObj.Prefix=null;

        srchObj.Name = nmObj;
        srchObj.Age = 0;
       
        if(mobile != null && mobile != ''){
            system.debug('originalPhoneno--' + mobile);
            system.debug('FormatedPhone No--' + mobile.replaceAll('[^0-9.]','' ));
            srchObj.HomePhone = mobile.replaceAll('[^0-9.]','' );
        }

        usrObj.GLBPurpose =1;
        usrObj.DLPurpose =0;

        endUsrObj.CompanyName='null';
        endUsrObj.StreetAddress1='null';
        endUsrObj.City='null';
        endUsrObj.State='null';
        endUsrObj.Zip5=null;
        endUsrObj.Phone=null;
        usrObj.EndUser  = endUsrObj;
        usrObj.MaxWaitSeconds = 0;
        flexObj.User = usrObj;

        LxNx.CVICalculationOptions cv = new LxNx.CVICalculationOptions();
        cv.IncludeDOB = true;
        cv.IncludeDriverLicense = false;
        cv.DisableCustomerNetworkOption = false;
        optObj.CVICalculationOptions =cv;
        optObj.UseDOBFilter = false;
        optObj.IncludeMSOverride  = false;
        optObj.IncludeSSNVerification = false;
        optObj.IncludeVerifiedElementSummary = false;
        optObj.PoBoxCompliance= false;
        optObj.DOBRadius = 0 ;
        optObj.GlobalWatchlistThreshold = 0;
        optObj.IncludeAllRiskIndicators= true;
        optObj.IncludeDLVerification= false;
        optObj.IncludeMIOverride= false;
        optObj.NameInputOrder = 'Unknown';

        LxNx.DOBMatch dobmatch = new LxNx.DOBMatch();
        dobmatch.MatchType = 'FuzzyCCYYMMDD';
        dobmatch.MatchYearRadius = 0;
        optObj.DOBMatch = dobmatch;

        LxNx.IncludeModels inc = new LxNx.IncludeModels();

        LxNx.FraudPointModel fraoud = new LxNx.FraudPointModel();
        fraoud.ModelName = null;
        fraoud.IncludeRiskIndices =true;
        inc.FraudPointModel = fraoud;

        List<LxNx.ModelOption> lstModOptions = new List<LxNx.ModelOption>();
        LxNx.ModelOption modOpt = new LxNx.ModelOption();
        modOpt.OptionName = null;
        modOpt.OptionValue = null;
        lstModOptions.add(modOpt);

        LxNx.ModelOptions modoptoons = new LxNx.ModelOptions();
        modoptoons.ModelOption = lstModOptions;

        List<LxNx.ModelRequest> lstModReq =new List<LxNx.ModelRequest>();
        LxNx.ModelRequest nodrequest = new LxNx.ModelRequest();
        nodrequest.ModelName = null;
        nodrequest.ModelOptions =modoptoons;
        lstModReq.add(nodrequest);

        LxNx.ModelRequests model = new LxNx.ModelRequests();
        model.ModelRequest = lstModReq;

        inc.ModelRequests = model;
        optObj.IncludeModels = inc;

        LxNx.RequireExactMatch reqExactMatch = new  LxNx.RequireExactMatch();
        reqExactMatch.LastName = false;
        reqExactMatch.FirstName = false;
        reqExactMatch.FirstNameAllowNickname= false;
        reqExactMatch.Address= false;
        reqExactMatch.HomePhone = false;
        reqExactMatch.SSN = false;
        reqExactMatch.DriverLicense= false;
        optObj.RequireExactMatch = reqExactMatch;

        List<object> WatchList = new List<object>();
        WatchList.add(null);
        LxNx.WatchLists watch = new LxNx.WatchLists();
        
        watch.WatchList = WatchList;

        optObj.WatchLists= watch;

        flexObj.Options = optObj;
        rootObj.FlexIDRequest = flexObj;

        system.debug('reqbody--'+rootObj);
        string reqbody = JSON.serialize(rootObj,true);
        reqbody = reqbody.replace('Number1','Number');
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(System.Label.LxNxApi);
        request.setMethod('GET');
        Blob headerValue = Blob.valueOf(System.Label.LxNxUserName+ ':' + System.Label.LxNxPassword);

        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);

        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        
        system.debug('requestbodyis--'+ reqbody);
        request.setBody(reqbody);

        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
           
            system.debug('ResponsefromLexisNexis'+response.getStatusCode() +'000'+ response.getBody());
            Map <String,Object> resultsMap = (Map<String,Object>)JSON.deserializeUntyped(response.getBody());
            system.debug('resultsMap-->' + resultsMap);
            
            return response.getBody();


        }
        return null;
    }

}