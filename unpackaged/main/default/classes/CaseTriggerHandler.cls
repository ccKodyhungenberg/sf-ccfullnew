// C-00249602 1/11/19 NCarson - PCF Forms should only be associated with contacts on case approval
// C-00249521 2/13/19 NCarson - Check for Value after assignment to eliminate null pointer exception
// C-00273851 2/10/20 ASchilling - Move update outside of loop to prevent Too many SOQL queries
public class CaseTriggerHandler {
  
  PCF_Settings__c settings;
  
  public CaseTriggerHandler() {
    settings = PCF_Settings__c.getInstance('Default');
  }
  
  public void onBeforeInsert( List<Case> newList ) {
    System.debug('** isBefore isInsert ***');
    Map<Id, Case> hcpByContactId = new Map<Id, Case>();
    
    for (Case objPcfApproval : newList ) {
      if (objPcfApproval.ContactId != null) {
        hcpByContactId.put(objPcfApproval.ContactId, objPcfApproval);
      }      
      if(objPcfApproval.Status == 'Closed - Approved' && objPcfApproval.RecordType.Name == 'PCF Verification'){       
            objPcfApproval.Signature_Status__c = 'Verified';        
      }
      
    }
    
    
    
    // get the HCP for the contact
    system.debug('hcpByContactId' +hcpByContactId);
    if (!hcpByContactId.isEmpty()) {
      for (Contact c : [SELECT Id, Health_Care_Professional__c FROM Contact WHERE Id IN :hcpByContactId.keySet()]) {
        Case a = hcpByContactId.get(c.Id);
        a.Health_Care_Professional__c = c.Health_Care_Professional__c;
        System.debug('Set the Case.Health_Care_Professional__c ' + a + ' ' + c.Health_Care_Professional__c);
      }
    }
    System.debug('** End isBefore isInsert **');
  }
  
  
  public void onBeforeUpdate( List<Case> newList, Map<Id,Case> oldMap ) {
    
    System.debug('** isBefore isUpdate **');
    Map<Id, Case> approvalsById = new Map<Id, Case>();
    Map<Id, Case> approvalsByContactId = new Map<Id, Case>();
    Map<Id, Case> hcpByContactId = new Map<Id, Case>();
    // Begin C-00249602 1/11/19 NCarson, keep soql query outside of for loop
    List<Id> contactIdList = New List<Id>(); 
    List<Contact> contactsToUpdate = New List<Contact>();
    for (Case objPcfApproval : newList) {
        contactIdList.add(objPcfApproval.ContactId); // get the associated conact IDs for SOQL query
    }
    Map<Id, Contact> contactsMap = new Map<Id, Contact>([SELECT Id, Name FROM Contact WHERE Id IN :contactIdList]);
    // End C-00249602 1/11/19 NCarson

    // Check to see if there is an update we need to process
    for (Case objPcfApproval : newList) {
        
      Case objOldPcfApproval = oldMap.get(objPcfApproval.Id);
       if(objPcfApproval.Status == 'Closed - Approved' && objPcfApproval.RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get('PCF Verification').getRecordTypeId()){
          
            objPcfApproval.Signature_Status__c = 'Verified';
        }
      // The contact is changing
      if (objOldPcfApproval.ContactId != objPcfApproval.ContactId) {
        // Update the PCF name
        objPcfApproval.Title__c = objPcfApproval.Patient_Name__c + ' sent to ' + objPcfApproval.Contact.Name;

        system.debug('objPcfApproval.ContactId' +objPcfApproval.ContactId);
          if (objPcfApproval.ContactId != null) {
          // double check the HCP if it didnt change
          if (objPcfApproval.Health_Care_Professional__c == objOldPcfApproval.Health_Care_Professional__c) {
              system.debug('test1');
            hcpByContactId.put(objPcfApproval.ContactId, objPcfApproval);
          }
        } else {
          // remove the HCP
          objPcfApproval.Health_Care_Professional__c = null;
        }
      }
        
      // Anything that used to be REVIEW or UNASSIGNED and is now SIGNED we want to process
      if (objOldPcfApproval.Status__c == PcfApprovalConstants.REVIEW || objOldPcfApproval.Status__c == PcfApprovalConstants.UNASSIGNED) {
        if (objPcfApproval.Status__c == PcfApprovalConstants.SIGNED) {
          if (objPcfApproval.ContactId != null) {
            approvalsById.put(objPcfApproval.Id, objPcfApproval);
            approvalsByContactId.put(objPcfApproval.ContactId, objPcfApproval);
          } else {
            String message = 'This record does not have a Contact value saved.';
            System.debug(message + ' ' + objPcfApproval);
            objPcfApproval.addError(message);
          }
        }
      }
      
      // C-00249602 1/9/19 NCarson - 
      // update the PCF_Form field on the related contact
      // if the case has been approved 
      Id PCFCollectionsId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PCF Collection').getRecordTypeId();
      
      if (objPcfApproval.Status == PcfApprovalConstants.CLOSED_APPROVED && 
            objOldPcfApproval.Status != PcfApprovalConstants.CLOSED_APPROVED && 
            objPcfApproval.RecordTypeId == PCFCollectionsId) {
        Contact contact = contactsMap.get(objPcfApproval.ContactId);
        if (contact != null) {
            contact.PCF_Form__c = objPcfApproval.Customer_Document_Id__c;
            contactsToUpdate.add(contact);
        }
      }   // end C-00249602 1/9/19 NCarson
    }
    
    //Start - Added by Kiran for Case 00260971 on 10/5/2019
    Id WelcomeCallId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Welcome Call').getRecordTypeId();
    Set<Id> contactIds = new Set<Id>();
    //add contactid from case to set
    for(Case c : newList){
      contactIds.add(c.ContactId);
    }
    
    /*
    List<Contact> caseContacts = [SELECT Id, Do_not_Create_Retention_Case__c FROM Contact WHERE Id IN :contactIds];
    List<Contact> conts = new List<Contact>();

      for(Case cs: newlist){
          if(cs.RecordTypeid == WelcomeCallId && cs.Status == 'Closed'){
              for(Contact cnt: caseContacts){
                if(cnt.Id == cs.ContactId){
                  cnt.Do_not_Create_Retention_Case__c = True;
                  conts.add(cnt);
                }
              }
          }
          else{
              for(Contact cnt: caseContacts){
                if(cnt.Id == cs.ContactId && cnt.Do_not_Create_Retention_Case__c == true){
                  cnt.Do_not_Create_Retention_Case__c = False;
                  conts.add(cnt);
                }
              }
          }
        } 
        // ALS - moved from above closing bracket to after closing bracket for case 00273851 on 02/10/2020
        if(!conts.isEmpty()){
          update conts;
        }*/
        //End - Added by Kiran for Case 00260971 on 10/5/2019
        
    if (contactsToUpdate.size() > 0) {
      update contactsToUpdate;
    }
   
    //update contactsToUpdate.values();

    // get the HCP for the contact
    system.debug('hcpByContactId' +hcpByContactId);
    if (!hcpByContactId.isEmpty()) {
      System.debug('Getting the Health_Care_Professional__c for ' + hcpByContactId.keySet());
      for (Contact c : [SELECT Id, Health_Care_Professional__c FROM Contact WHERE Id IN :hcpByContactId.keySet()]) {
        Case a = hcpByContactId.get(c.Id);
        a.Health_Care_Professional__c = c.Health_Care_Professional__c;
        System.debug('Set the Case.Health_Care_Professional__c ' + a + ' ' + c.Health_Care_Professional__c);
      }
    }

    // We need to check for the signed attachment
    if (!approvalsById.isEmpty()) {
      Map<Id, Attachment> attachmentsByApprovalId = new Map<Id, Attachment>();
      Set<Id> contactsToCheckForDocs = new Set<Id>();
      Map<Id, Case> contactsToPcfApprovals = new Map<Id, Case>();
      List<Customer_Document__c> newApprovalDocuments = new List<Customer_Document__c>();
        
      // Look for attachments on the PCF Approval
      for (Attachment attachment : [SELECT Id, ParentId, Name, Body FROM Attachment WHERE ParentId IN :approvalsById.keySet()]) {
        Case objPcfApproval = approvalsById.get(attachment.ParentId);
        // NCarson 2/12/19 - Add Outer If to Eliminate Null Pointer Exception
        if (objPcfApproval != null) {
          if (attachment.Id == objPcfApproval.PCF_Attachment_Id__c || attachment.Id == objPcfApproval.PCF_Signed_Attachment_Id__c) {
            contactsToCheckForDocs.add(objPcfApproval.ContactId);
            contactsToPcfApprovals.put(objPcfApproval.ContactId, objPcfApproval);
            attachmentsByApprovalId.put(attachment.ParentId, attachment);

            // We found one!
            approvalsById.remove(objPcfApproval.Id);
          }
        } 
      }

      // if there are any remaining then they don't have an attachment, show an error
      if (!approvalsById.isEmpty()) {
        for (Case objPcfApproval : approvalsById.values()) {
          String message = 'The signed PCF is missing from this approval.';
          System.debug(objPcfApproval + message);
          objPcfApproval.addError(message);
        }
      }

      // Ok, all should be valid now, get the customer documents that exist
      // C-00249602 1/11/19 NCarson
      // Added OR to catch both current and legacy document types
      List<Customer_Document__c> docs = [
        SELECT Id, Document_Type__c, Document_Name__c, Contact__c, Original_File_Name__c 
        FROM Customer_Document__c 
        WHERE Contact__c IN :contactsToCheckForDocs AND 
        (Document_Type__c = :settings.PCF_Signed_Document_Type__c
        OR Document_Type__c = :settings.PCF_Signed_Document_Type_Legacy__c)
      ];

      // if we didn't get a document for every attachment we need to create some
      if (docs.size() != attachmentsByApprovalId.size()) {
        // Check to see which contacts are missing a customer document
        for (Customer_Document__c doc : docs) {
          // Remove the contacts that have a a customer document already
          contactsToCheckForDocs.remove(doc.Contact__c);
        }

        // create a customer document for the remaining contacts
        List<Customer_Document__c> newDocs = new List<Customer_Document__c>();
        for (Id contactId : contactsToCheckForDocs) {
          newDocs.add(new Customer_Document__c(
            Document_Type__c = settings.PCF_Signed_Document_Type__c,
            Document_Name__c = settings.PCF_Signed_Document_Name__c,
            Contact__c = contactId
          ));
        }
        System.debug('Creating newDocs ' + newDocs);
        insert newDocs;

        // add the new docs to the existing ones
        docs.addAll(newDocs);
      }

      // now we need to put the attachment on to the customer document
      if (!docs.isEmpty()) {
        List<Attachment> attachments = new List<Attachment>();
        Map<Id, Customer_Document__c> customerDocumentsById = new Map<Id, Customer_Document__c>();
        for (Customer_Document__c doc : docs) {
          customerDocumentsById.put(doc.Id, doc);
          // Get the approval for this contact
          Case objPcfApproval = contactsToPcfApprovals.get(doc.Contact__c);
          Attachment attachment = attachmentsByApprovalId.get(objPcfApproval.Id);
          Attachment clone = attachment.clone();
          clone.ParentId = doc.Id;
          attachments.add(clone);
        }
        System.debug('Creating new attachments ' + attachments);
        // Insert the new/moved attachments
        insert attachments;

        // Delete the old attachments, maybepcf 
        if (settings.Delete_Pcf_Attachment_After_Signing__c) {
          delete attachmentsByApprovalId.values();
        }

        // Update the contact
        List<Contact> contacts = new List<Contact>();
        for (Attachment attachment : attachments) {
          Customer_Document__c doc = customerDocumentsById.get(attachment.ParentId);
          Case approval = approvalsByContactId.get(doc.Contact__c);
          Contact con = new Contact(
            Id = doc.Contact__c,
            // Make the signed PCF the latest one
            Latest_PCF_Approval__c = approval.Id,
            PCF_Form__c = doc.Id
          );
          if( String.isBlank(con.PCF_Form__c) ) {
            con.PCF_Form__c = doc.Id;
          }
          contacts.add(con);
        }
        if (!contacts.isEmpty()) {
          update contacts;
        }
        system.debug('contacts updated - '+contacts);
      }
    }

    System.debug('** End isBefore isUpdate **');
  
  }
  
  public void onBeforeDelete( List<Case> oldList ) {
    
    System.debug('** isBefore isDelete **');
    // We don't want to allow signed PCF Approvals to be deleted
    if (!settings.Allow_Signed_PCF_Approval_Deletion__c) {
      for (Case objPcfApproval : oldList ) {
        if (objPcfApproval.Status__c == PcfApprovalConstants.SIGNED) {
          String message = 'This record is locked because it has been signed.';
          System.debug(message + ' ' + objPcfApproval);
          objPcfApproval.addError(message);
        }
      }
    }
    System.debug('** End isBefore isDelete **');
  
  }
  public static void isAfterInsert(List<Case> newList){
      List<Customer_Document__c> cdList = new List<Customer_Document__c>();
      for(Case c: newList){
          if(c.Customer_Document_Id__c != null){
              Customer_Document__c cd = new Customer_Document__c();
              cd.Id = c.Customer_Document_Id__c;
              cd.Case__c = c.id;
              cdList.add(cd);
          }
      }
      if(cdList.size()>0){
          update cdList;
      }
    }
}