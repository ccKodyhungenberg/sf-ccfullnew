@isTest
private class CC_AutoRedirectionToFlowControllerTest {
    @isTest
    static void testAutoRedirect1(){
        List<Account> accList = CC_TestUtility.createAccount(1, true);
        List<Contact> conList = CC_TestUtility.createContact(1, true, 'Customer', accList[0].id,'');
        List<Asset> assList = CC_TestUtility.createAssetRecords('test asset', 1, true,accList[0].id);
		List<Addresses__c> addList = CC_TestUtility.createAddressRecords('jaipur', 'SD', 'US', True, accList[0].id, null, null, 1, true);
        List<Addresses__c> addList1 = CC_TestUtility.createAddressRecords('jaipur', 'SD', 'US', True, null, conList[0].id, null, 1, true);
        List<Addresses__c> addList2 = CC_TestUtility.createAddressRecords('jaipur', 'SD', 'US', True, null, null, assList[0].id, 1, true);
        PageReference pageRef = Page.CC_AutoRedirectionToFlow;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(addList[0]);
        CC_AutoRedirectionToFlowController autoRedirectionflowCont = new CC_AutoRedirectionToFlowController(sc);
        autoRedirectionflowCont.redirectToFlow();
        CC_AutoRedirectionToFlowController.getAddressRecord(assList[0].id);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(addList1[0]);
        CC_AutoRedirectionToFlowController autoRedirectionflowCont1 = new CC_AutoRedirectionToFlowController(sc1);
        autoRedirectionflowCont1.redirectToFlow();
        ApexPages.StandardController sc2 = new ApexPages.StandardController(addList2[0]);
        CC_AutoRedirectionToFlowController autoRedirectionflowCont2 = new CC_AutoRedirectionToFlowController(sc2);
        autoRedirectionflowCont2.redirectToFlow();
    }
}