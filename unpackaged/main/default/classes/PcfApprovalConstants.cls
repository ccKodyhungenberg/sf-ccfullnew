// C00249602 NCarson 1/9/19 - Added CLOSED_REJECTED and CLOSED_APPROVED Strings

public abstract class PcfApprovalConstants {

  public static final String INCOMING = 'Incoming';
  public static final String UNASSIGNED = 'Unassigned';
  public static final String PENDING = 'Pending';
  public static final String EMAILED = 'Emailed';
  public static final String FAXED = 'Faxed';
  public static final String EMAILED_FAXED = 'Emailed & Faxed';
  public static final String NEW_CUSTOMER = 'New Customer';
  public static final String REVIEW = 'Needs Review';
  public static final String SIGNED = 'Signing Complete';
  public static final String REJECTED = 'Rejected';
  public static final String OPTOUT = 'Opted Out';
  public static final String NEWCASE = 'New';
  public static final String CLOSED_REJECTED = 'Closed - Rejected';
  public static final String CLOSED_APPROVED = 'Closed - Approved';
  

  public static final String MATCH_NONE = 'None';
  public static final String MATCH_EMAIL = 'Email';
  public static final String MATCH_PHONE = 'Phone';
  public static final String MATCH_FAX = 'Fax';
  public static final String MATCH_ADDRESS = 'Address';
  public static final String MATCH_NAME = 'Name';

  public static final String KEY_INSTRUCTIONS = 'instructions';
  public static final String KEY_PCF = 'pcf';
  public static final String KEY_MRF = 'mrf';

}