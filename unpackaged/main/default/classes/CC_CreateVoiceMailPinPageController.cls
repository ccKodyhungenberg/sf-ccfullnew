public class CC_CreateVoiceMailPinPageController {
    public String voiceMailNum{get;set;}
    public Contact con{get;set;}
    public CC_CreateVoiceMailPinPageController(){
        String conId = Apexpages.currentPage().getParameters().get('Id');
        con = [SELECT Id,CC_Voice_Number__c,CB_UserId__c,Email,MailingPostalCode FROM Contact WHERE Id =: conId];
    }
    public PageReference savePin(){
        if(con.CB_userID__c != null){
            if(voiceMailNum.length() != 4){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Voicemail Pin must be of 4 digits!'));
            }
            else{
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                String sURL=Label.CC_CCWS_ENDPOINT+'/user/update';
                req.setEndpoint(sURL);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
                String requestBody = 'data={"userID":"'+con.CB_userID__c+'","voicemailPassword": "'+voiceMailNum+'"}';
                system.debug('requestBody'+requestBody);
                req.setBody(requestBody);
                String result;
                // if( !Test.isRunningTest() ) {
                HttpResponse res = h.send(req);
                string call_result=res.getBody();
                //String cc_voiceNumber;
                String message;
                String resultCCws;
                if (res.getStatusCode() != 200) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,res.getBody() + ' '+
                                                               res.getStatusCode() + ' ' + res.getStatus()));
                    return null;
                } 
                else{
                    system.debug('false,'+res.getBody());
                    String addressResponse=res.getBody();
                    JSONParser parser = JSON.createParser(res.getBody());
                    while (parser.nextToken() != null) {
                        if(parser.getText() == 'message'){
                            parser.nextToken();
                            message = parser.getText();
                        }
                        if(parser.getText() == 'result'){
                            parser.nextToken();
                            resultCCws = parser.getText();
                        }
                        
                    }
                    
                    if(resultCCws == 'true'){
                        Contact c = new Contact();
                        c.Id = Apexpages.currentPage().getParameters().get('Id');
                        c.Voicemail_PIN__c = voiceMailNum;
                        update c;
                        if(con.Email != null){
                            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
                            String[] sendingTo = new String[]{con.Email}; 
                                semail.setToAddresses(sendingTo); 
                            semail.setSubject('Voicemail pin updated'); 
                            semail.setPlainTextBody('Your Voicemail Pin was changed recently if you have not made this changes please contact support.'); 
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail}); 
                        }
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,message));
                    }
                    else{
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message));
                    }
                }
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'User ID Not Available'));
        }
        return null;
    }
    
    
    
}