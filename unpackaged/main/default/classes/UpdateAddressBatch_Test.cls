@isTest
public class UpdateAddressBatch_Test {
    
    public static testmethod void testmethod1() {
        
        List<Account> listAccounts = CC_TestUtility.createAccountRecords('Test Account', 5, TRUE);
        System.assertEquals(5, listAccounts.size());
        List<Contact> listContacts = new List<Contact>();
        for( Contact con : CC_TestUtility.createContactRecords('Test Contact',5,FALSE)) 
        {
            con.AccountId = listAccounts.get(0).Id;
            con.Active__c = FALSE;
            listContacts.add(con);
        }
        insert listContacts;
        
        for(Contact con : listContacts)
        {
            System.assertNotEquals(null, con.Id);
        }
        
        UsageDetail__c ud = new UsageDetail__c();
        ud.LifetimeBillableMins__c = 5;
        ud.Contact__c = listContacts.get(0).Id;
        
        insert ud;
        System.assertNotEquals(null, ud.Id);
        
        listContacts.get(0).MailingCity = 'Jaipur';
        listContacts.get(0).CB_UserID__c = '12345';
        
        update listContacts.get(0);
        
        System.assertEquals('Jaipur', listContacts.get(0).MailingCity);
        System.assertEquals('12345', listContacts.get(0).CB_UserID__c);
        
        Test.setMock(HttpCalloutMock.class, new MockUpdateAddressBatch());

        Test.startTest();
        
        
        
       UpdateAddressBatch uab = new UpdateAddressBatch();
       Database.executeBatch(uab,10);
        /*Test.setMock(HttpCalloutMock.class, new MockUpdateAddressBatch());
        Database.BatchableContext     bc;
        UpdateAddressBatch uab = new UpdateAddressBatch();
        HttpResponse response = UpdateAddressBatch.execute(uab, listContacts);
        */
        UpdateAddressBatch mtn = new UpdateAddressBatch();
        for(Integer i = 0; i < 60;i+=15) {
            String jobID1 = system.schedule('Test Activate/Deactivate User '+i, '0 '+i+' * * * ?', mtn);
        }
        
        /* String contentType = res.getHeader('Content-Type');
System.assert(contentType == 'application/x-www-form-urlencoded;charset=UTF-8');
String actualValue = res.getBody();
String expectedValue = 'data={"id": "'+''+'","userID": "'+''+'",'+''+'}';
System.assertEquals(actualValue, expectedValue);
System.assertEquals(200, res.getStatusCode());
*/
        Test.stopTest();
        
        /*listContacts.get(2).Active__c = TRUE;
listContacts.get(2).CB_UserID__c = '1234554';

update listContacts.get(2);*/
        
        
    }
}