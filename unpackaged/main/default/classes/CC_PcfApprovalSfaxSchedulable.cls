public with sharing class CC_PcfApprovalSfaxSchedulable implements Schedulable {

  public void execute(SchedulableContext context) {
    // incoming faxes
    CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
    if (Test.isRunningTest()) {
      // we get DML exceptions unless we do it this way :(
      b.execute(null);
    } else {
      System.enqueueJob(b);
    }
  
}
}