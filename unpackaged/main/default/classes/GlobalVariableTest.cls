/* ============================================================
 * This code is part of Richard Vanhook's submission to the 
 * Cloudspokes Geolocation Toolkit challenge.
 *
 * This software is provided "AS IS," and you, its user, 
 * assume all risks when using it. 
 * ============================================================
 */
@IsTest
private class GlobalVariableTest {

    private static testmethod void testGlobalVariablesExist(){
        final Map<String,String> theVariables = new Map<String,String>{
            // ------FORCEXPERTS-------------
            // Removed traces of Simple Geo Service from here
            // ------FORCEXPERTS-------------
            GlobalVariable.KEY_USE_GOOGLE_GEOCODING_API   => 'false'
        };
        for(String key : theVariables.keySet()){
            GlobalVariableTestUtils.ensureExists(new GlobalVariable__c(name=key,Value__c=theVariables.get(key)));
        }
        // ------FORCEXPERTS-------------
        // Removed traces of Simple Geo Service from here
        // and modified the code w.r.t. GoogleGeoService
        // ------FORCEXPERTS-------------
        System.assertEquals(false, GlobalVariable.getInstance().useGoogleGeocodingAPI);
    }

    private static testmethod void testGlobalVariablesDoNotExist(){
        final Map<String,String> theVariables = new Map<String,String>{
            // ------FORCEXPERTS-------------
            // Removed traces of Simple Geo Service from here
            // and modified the code a bit for GoogleGeoService
            // ------FORCEXPERTS-------------
            GlobalVariable.KEY_USE_GOOGLE_GEOCODING_API   => null
        };
        for(String key : theVariables.keySet()){
            GlobalVariableTestUtils.ensureExists(new GlobalVariable__c(name=key,Value__c=theVariables.get(key)));
        }
        // ------FORCEXPERTS-------------
        // Removed traces of Simple Geo Service from here
        // and modified the code a bit for GoogleGeoService
        // ------FORCEXPERTS-------------
        System.assertEquals(false, GlobalVariable.getInstance().useGoogleGeocodingAPI);
    }

}