/* ============================================================
 * This code is part of Richard Vanhook's submission to the 
 * Cloudspokes Geolocation Toolkit challenge.
 *
 * This software is provided "AS IS," and you, its user, 
 * assume all risks when using it. 
 * ============================================================
 */
global class GlobalVariable {
    
    //==================================================
    // PROPERTIES
    //==================================================
    //--------------FORCEXPERTS-------------------------
    // Removed traces of Simple Geo Service from here---
    global static final String KEY_USE_GOOGLE_GEOCODING_API = 'UseGoogleGeocodingAPI';
    //--------------FORCEXPERTS-------------------------
    
    global static GlobalVariable instance;

    //--------------FORCEXPERTS-------------------------
    // Removed traces of Simple Geo Service from here---
    global boolean useGoogleGeocodingAPI {get; private set;}
    //--------------FORCEXPERTS-------------------------

    //==================================================
    // CONSTRUCTOR  
    //==================================================
    private GlobalVariable(){
        //--------------FORCEXPERTS-------------------------
        // Removed traces of Simple Geo Service from here---
        //--------------FORCEXPERTS-------------------------
        useGoogleGeocodingAPI = retrieveBoolean(KEY_USE_GOOGLE_GEOCODING_API);
    }

    global static GlobalVariable getInstance(){
        if(instance == null){
            instance = new GlobalVariable();
        }
        return instance;
    }

    //==================================================
    // HELPERS
    //==================================================
    private String retrieveString(String key){
        String returnValue = null;
        final Map<String,GlobalVariable__c> all = GlobalVariable__c.getAll();
        if(    key != null 
            && key.trim() != null 
            && key.trim().length() > 0 
            && all != null 
            && all.get(key) != null
        ){
            returnValue = all.get(key).value__c;
        }
        return returnValue;
    }
    private boolean retrieveBoolean(String key){
        return al.BooleanUtils.strToBoolean(retrieveString(key));
    }
}