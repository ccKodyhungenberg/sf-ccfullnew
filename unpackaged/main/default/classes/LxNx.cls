public class LxNx{


public class EndUser
{
    public object CompanyName { get; set; }
    public object StreetAddress1 { get; set; }
    public object City { get; set; }
    public object State { get; set; }
    public object Zip5 { get; set; }
    public object Phone { get; set; }
}

public class User
{
    public object ReferenceCode { get; set; }
    public object BillingCode { get; set; }
    public object QueryId { get; set; }
    public object GLBPurpose { get; set; }
    public object DLPurpose { get; set; }
    public EndUser EndUser { get; set; }
    public integer MaxWaitSeconds { get; set; }
    public object AccountNumber { get; set; }
    public object OutputType { get; set; }
}

public class WatchLists
{
    public List<object> WatchList { get; set; }
}

public class RequireExactMatch
{
    public boolean LastName { get; set; }
    public boolean FirstName { get; set; }
    public boolean FirstNameAllowNickname { get; set; }
    public boolean Address { get; set; }
    public boolean HomePhone { get; set; }
    public boolean SSN { get; set; }
    public boolean DriverLicense { get; set; }
}

public class DOBMatch
{
    public string MatchType { get; set; }
    public integer MatchYearRadius { get; set; }
}

public class FraudPointModel
{
    public object ModelName { get; set; }
    public boolean IncludeRiskIndices { get; set; }
}

public class ModelOption
{
    public object OptionName { get; set; }
    public object OptionValue { get; set; }
}

public class ModelOptions
{
    public List<ModelOption> ModelOption { get; set; }
}

public class ModelRequest
{
    public object ModelName { get; set; }
    public ModelOptions ModelOptions { get; set; }
}

public class ModelRequests
{
    public List<ModelRequest> ModelRequest { get; set; }
}

public class IncludeModels
{
    public FraudPointModel FraudPointModel { get; set; }
    public ModelRequests ModelRequests { get; set; }
}

public class CVICalculationOptions
{
    public boolean IncludeDOB { get; set; }
    public boolean IncludeDriverLicense { get; set; }
    public boolean DisableCustomerNetworkOption { get; set; }
}

public class Options
{
    public WatchLists WatchLists { get; set; }
    public boolean UseDOBFilter { get; set; }
    public integer DOBRadius { get; set; }
    public boolean IncludeMSOverride { get; set; }
    public boolean PoBoxCompliance { get; set; }
    public RequireExactMatch RequireExactMatch { get; set; }
    public boolean IncludeAllRiskIndicators { get; set; }
    public boolean IncludeVerifiedElementSummary { get; set; }
    public boolean IncludeDLVerification { get; set; }
    public DOBMatch DOBMatch { get; set; }
    public IncludeModels IncludeModels { get; set; }
    public object CustomCVIModelName { get; set; }
    public object LastSeenThreshold { get; set; }
    public boolean IncludeMIOverride { get; set; }
    public boolean IncludeSSNVerification { get; set; }
    public CVICalculationOptions CVICalculationOptions { get; set; }
    public object InstantIDVersion { get; set; }
    public string NameInputOrder { get; set; }
    public integer GlobalWatchlistThreshold { get; set; }
}

public class Name
{
    public object Full { get; set; }
    public object First { get; set; }
    public object Middle { get; set; }
    public object Last { get; set; }
    public object Suffix { get; set; }
    public object Prefix { get; set; }
}

public class Address
{
    public object StreetNumber { get; set; }
    public object StreetPreDirection { get; set; }
    public object StreetName { get; set; }
    public object StreetSuffix { get; set; }
    public object StreetPostDirection { get; set; }
    public object UnitDesignation { get; set; }
    public object UnitNumber { get; set; }
    public object StreetAddress1 { get; set; }
    public object StreetAddress2 { get; set; }
    public object City { get; set; }
    public object State { get; set; }
    public object Zip5 { get; set; }
    public object Zip4 { get; set; }
    public object County { get; set; }
    public object PostalCode { get; set; }
    public object StateCityZip { get; set; }
    public object Latitude { get; set; }
    public object Longitude { get; set; }
}

public class DOB
{
    public integer Year { get; set; }
    public integer Month { get; set; }
    public integer Day { get; set; }
}

public class ExpirationDate
{
    public integer Year { get; set; }
    public integer Month { get; set; }
    public integer Day { get; set; }
}

public class Passport
{
    public object Number1 { get; set; }
    public ExpirationDate ExpirationDate { get; set; }
    public object Country { get; set; }
    public object MachineReadableLine1 { get; set; }
    public object MachineReadableLine2 { get; set; }
}

public class SearchBy
{
    public Name Name { get; set; }
    public Address Address { get; set; }
    public DOB DOB { get; set; }
    public integer Age { get; set; }
    public object SSN { get; set; }
    public object SSNLast4 { get; set; }
    public object DriverLicenseNumber { get; set; }
    public object DriverLicenseState { get; set; }
    public object IPAddress { get; set; }
    public object HomePhone { get; set; }
    public object WorkPhone { get; set; }
    public Passport Passport { get; set; }
    public object Gender { get; set; }
    public object Email { get; set; }
    public object Channel { get; set; }
    public object Income { get; set; }
    public object OwnOrRent { get; set; }
    public object LocationIdentifier { get; set; }
    public object OtherApplicationIdentifier1 { get; set; }
    public object OtherApplicationIdentifier2 { get; set; }
    public object OtherApplicationIdentifier3 { get; set; }
}

public class FlexIDRequest
{
    public User User { get; set; }
    public Options Options { get; set; }
    public SearchBy SearchBy { get; set; }
}

public class RootObject
{
    public FlexIDRequest FlexIDRequest { get; set; }
}
}