public class CC_PcfApprovalSilanisScheduler implements Schedulable {
    public void execute(SchedulableContext context) {
        // incoming email
        CC_PcfApprovalSilanisBatch b = new CC_PcfApprovalSilanisBatch();
        if (Test.isRunningTest()) {
          // we get DML exceptions unless we do it this way :(
          b.execute(null);
        } else {
          System.enqueueJob(b);
        }
      }
}