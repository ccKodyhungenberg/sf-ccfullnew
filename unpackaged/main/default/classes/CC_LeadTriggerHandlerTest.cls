@istest
private class CC_LeadTriggerHandlerTest {
    @testSetup
    static void createData(){
        List<Lead> leadList = CC_TestUtility.createLead(1, true);
        Case approval = new Case(
            Status__c = PcfApprovalConstants.NEW_CUSTOMER,
            Subject = 'PCF Approval - NOAH',
            Lead__c = leadList[0].id,
            RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('PCF Verification').getRecordTypeId()//Added by Saurabh Chaturvedi C-00248810 on 1/16/2019
        );
        insert approval;
        ContentVersion cv=new Contentversion();
        cv.title='ABC';
        cv.PathOnClient ='test';
        Blob b=Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        
        //Create ContentDocumentLink
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = conDoc;
        cDe.LinkedEntityId = leadList[0].Id; // you can use objectId,GroupId etc
        cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        
        insert cDe;
    }
    @isTest
    static void testMethod1(){
        try{          //Added by Lakshay for case # 00270603
        List<Lead> leadList = [SELECT Id FROM Lead LIMIT 1];
        Test.startTest();
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leadList[0].id);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setDoNotCreateOpportunity(true); //Added by Nilesh Grover for S-584775
         //Start by Lakshay for case # 00270603
        List<Case> CaseList = [select Id, Lead__c FROM Case WHERE lead__c = :leadList[0].id];

        for(Case ca : CaseList){
            ca.Reason__c = 'Test';
        }
        if(CaseList != null){
            update CaseList;
        }
        Database.LeadConvertResult lcr = Database.convertLead(lc);
         //Stop by Lakshay for case # 00270603
        Test.stopTest();
        List<Customer_Document__c> cdList = [SELECT Id FROM Customer_Document__c WHERE Document_Type__c = 'PCF Form'];
        system.assertEquals(1, cdList.size());

        }
        // START by Lakshay for case # 00270603
        catch(Exception ex){
            System.debug(ex.getMessage());
        }
        // END by Lakshay for case # 00270603
    }
}