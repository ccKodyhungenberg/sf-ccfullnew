@isTest
public class CC_UploadDocumentContactControllerTest {
    public static List<Contact> conList;
    public static List<Customer_Document__c> docs;
    public static List<Attachment> attachmentRecords;
    public static List<Case> caseRecords;
    public static void createData(){
        conList=new List<Contact>();
        docs=new List<Customer_Document__c>();
        caseRecords=new List<Case>();
        attachmentRecords=new List<Attachment>();
        Id customerRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        for(Integer i=0;i<20;i++){
            Contact con=new Contact();
            con.LastName='test'+i;
            con.cc_voice_number__c='(614) 864-5864'+i;
            con.CB_userID__c='test user id'+i;
            con.MailingPostalCode='302022'+i;
            con.Email='abc.xyz@test.com';
            con.RecordTypeId=customerRecordTypeId;
            conList.add(con);
        }
        insert conList;
        for(Integer i=0;i<20;i++){
            Customer_Document__c  doc=new Customer_Document__c();
            doc.Document_Type__c='Medical Release Form';
            doc.Document_Name__c='test doc';
            doc.Contact__c=conList[i].id;
            doc.Original_File_Name__c='test file';
            
            docs.add(doc);
        }
       // insert docs;
        for(Integer i=0;i<20;i++){
            Case c=new Case();
            c.Status='New';
            c.status__c='Needs Review';
            c.ContactId=conList[0].id;
            caseRecords.add(c);
        }
        insert caseRecords;
        for(Integer i=0;i<20;i++){
          Attachment atch =new Attachment();
          atch.Name='Test Attach'+i;
          atch.ParentId=caseRecords[i].id;
          atch.Body=EncodingUtil.base64Decode('test');
              
          attachmentRecords.add(atch);
        }
        //insert attachmentRecords;
        
       for(Contact con:conList){
            con.Medical_Release_Form__c=docs[0].id;
         }
        try{
        update conList;
        }
        catch(Exception e){
            System.assert(true);
        }
    }
    @isTest
    public static void testMethods(){
        createData();
         ApexPages.currentPage().getParameters().put('cid',conList[0].id);
        CC_UploadDocumentContactPageController upload=new CC_UploadDocumentContactPageController();
        upload.objDocFile=attachmentRecords[0];
        upload.objDoc=docs[0];
        List<SelectOption> ltype=upload.lType_option;
        Customer_Document__c cdoc=upload.objDoc;
        String srender=upload.sRendered_Upload;
        upload.getobjDocFile();
        try{
        upload.SaveDoc();
        }catch(Exception e){
            System.assert(true);
        }
        docs[0].Document_Type__c='PCF Form';
        upload.objDoc=docs[0];
        try{
        upload.SaveDoc();
        }catch(Exception e){
         System.assert(true);   
        }
        upload.Cancel();
        
    }
    @isTest
    public  static void testSaveDocWithPCF(){
        createData();
        docs[1].Document_Type__c='PCF Form';
        ApexPages.currentPage().getParameters().put('cid',conList[0].id);
        CC_UploadDocumentContactPageController upload=new CC_UploadDocumentContactPageController();
        upload.objDocFile=attachmentRecords[0];
        upload.objDoc=docs[1];
        try{
        upload.SaveDoc();
        }catch(Exception e){
         System.assert(true);   
        }
    }

}