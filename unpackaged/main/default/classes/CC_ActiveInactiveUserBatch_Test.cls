@isTest
public class CC_ActiveInactiveUserBatch_Test {
    
    @isTest
    public static void testmethod1() {
        
        List<Account> listAccounts = CC_TestUtility.createAccountRecords('Test Account', 5, TRUE);
        List<Contact> listContacts = new List<Contact>();
        
        
        for( Contact con : CC_TestUtility.createContactRecords('Test Contact',5,FALSE)) {
            con.AccountId = listAccounts.get(0).Id;
            con.Active__c = FALSE;
            con.Deactivated__c = true;
            con.Deactivation_Reason__c = 'Account under review';
            con.Update_To_CCWS__c = true;
            listContacts.add(con);
        }
        
        insert listContacts;
        
        UsageDetail__c ud = new UsageDetail__c();
        ud.LifetimeBillableMins__c = 5;
        ud.Contact__c = listContacts.get(0).Id;
        
        insert ud;
        
        
        listContacts.get(0).MailingCity = 'Jaipur';
        listContacts.get(0).CB_UserID__c = '12345';
        
        update listContacts.get(0);
        
        listContacts.get(2).Active__c = TRUE;
        listContacts.get(2).CB_UserID__c = '1234554';
        
        update listContacts.get(2);
        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock_CC_ActiveInactiveUserBatch_Test());
        
        CC_ActiveInactiveUserBatch uab = new CC_ActiveInactiveUserBatch();
        Database.executeBatch(uab,10);
        
        
        CC_ActiveInactiveUserBatch mtn = new CC_ActiveInactiveUserBatch();
        for(Integer i = 0; i < 60;i+=15) {
            String jobID1 = system.schedule('Test Address Update '+i, '0 '+i+' * * * ?', mtn);
        }
        
        
        Test.stopTest();
        
        Contact conUpdated = [SELECT Id, Deactivated__c FROM Contact WHERE Id = : listContacts.get(0).Id];
        //System.assertEquals(false, conUpdated.Deactivated__c);
        
    }
}