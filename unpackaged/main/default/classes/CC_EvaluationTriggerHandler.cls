public without sharing class CC_EvaluationTriggerHandler {
    public static void beforeInsert(List<Evaluation__c> lstOfNewEvaluation){
        populateEvaluationScriptAndTotalWords(lstOfNewEvaluation, null);
        //Added below 1 line by NSINGH - S-584772 [12/26/2018] 
        populateCallType(lstOfNewEvaluation);
    }
    
    public static void beforeUpdate(List<Evaluation__c> lstOfNewEvaluation, Map<Id,Evaluation__c > oldMapOfEvaluation){
        populateEvaluationScriptAndTotalWords(lstOfNewEvaluation, oldMapOfEvaluation);
        //Added below 1 line by NSINGH - S-584772 [12/26/2018]         
        populateCallType(lstOfNewEvaluation);
    }
    
    //START by NSINGH - S-584772 [12/26/2018] - populate the call type on evaluation based on evaluation script's call type
    private static void populateCallType(List<Evaluation__c> lstOfNewEvaluation){
        Map<Id,String> evalScriptMap = new Map<Id,String>();
        Set<Id> evalScriptIds = new Set<Id>();
        for(Evaluation__c eval: lstOfNewEvaluation){
            if(eval.EvaluationScript__c!=NULL){
                evalScriptIds.add(eval.EvaluationScript__c);
            }
        }
        for(EvaluationScript__c evalScript: [SELECT Id, callType__c FROM EvaluationScript__c where ID IN:evalScriptIds]){
            if(evalScript.callType__c !=  NULL){
                evalScriptMap.put(evalScript.Id, evalScript.callType__c);
            }
        }
        for(Evaluation__c eval: lstOfNewEvaluation){
            if(eval.EvaluationScript__c!=NULL && (eval.callType__c == NULL || eval.callType__c == '')){
                if(evalScriptMap.containsKey(eval.EvaluationScript__c)){
                    eval.callType__c = evalScriptMap.get(eval.EvaluationScript__c);
                }
            }
        }
        
    }
    //END by NSINGH - S-584772 [12/26/2018]

    
    
    private static void populateEvaluationScriptAndTotalWords(List<Evaluation__c> lstOfNewEvaluation, Map<Id,Evaluation__c > oldMapOfEvaluation){
        List<GroupMember> loggedInGroupMemberData = [SELECT Id FROM GroupMember WHERE UserOrGroupId =: UserInfo.getUserId() AND Group.DeveloperName='EvaluationTeam' AND Group.Type='Regular'];
        Set<String> setOfEvaluationScript = new Set<String>();
        Set<String> setOfRemoteScriptNumber = new Set<String>();
        List<Evaluation__c> lstOfEvaluationToBeProcessed = new List<Evaluation__c>();
        Boolean isEvaluationAdded = false;
        Map<String,String> mapOfEvaluationScriptNameToId = new Map<String,String>();
        Map<String,Decimal> mapOfEvaluationScriptIdToTotalWords = new Map<String,Decimal>();
        
        for(Evaluation__c evaluation : lstOfNewEvaluation){
            if(loggedInGroupMemberData == null || loggedInGroupMemberData.isEmpty()){
                evaluation.addError('You must be member of the "Evaluation Team" group to create Evaluations!');
            }
            else{
                isEvaluationAdded = false;
                if(String.isNotBlank(evaluation.Remote_Script_Number__c) && 
                    (
                        oldMapOfEvaluation == null || 
                        (evaluation.Remote_Script_Number__c <> oldMapOfEvaluation.get(evaluation.Id).Remote_Script_Number__c || String.isBlank(evaluation.EvaluationScript__c)) 
                    ) 
                  ){    
                    lstOfEvaluationToBeProcessed.add(evaluation);
                    isEvaluationAdded = true;
                    setOfRemoteScriptNumber.add(evaluation.Remote_Script_Number__c);
                }    
                if(String.isNotBlank(evaluation.EvaluationScript__c) && 
                    (
                        oldMapOfEvaluation == null ||
                        evaluation.EvaluationScript__c <> oldMapOfEvaluation.get(evaluation.Id).EvaluationScript__c
                    )
                  ){
                    setOfEvaluationScript.add(evaluation.EvaluationScript__c);  
                    if(!isEvaluationAdded){
                        lstOfEvaluationToBeProcessed.add(evaluation);
                    }
                }
            }
        }
        
        if(!setOfRemoteScriptNumber.isEmpty() || !lstOfEvaluationToBeProcessed.isEmpty()){
            for(EvaluationScript__c evaluationScript : [SELECT Id, Name, TotalWords__c FROM EvaluationScript__c WHERE Id IN : setOfEvaluationScript OR Name IN : setOfRemoteScriptNumber]){
                mapOfEvaluationScriptNameToId.put(evaluationScript.Name, evaluationScript.Id);
                mapOfEvaluationScriptIdToTotalWords.put(evaluationScript.Id, evaluationScript.TotalWords__c);
            }
            
            if(!lstOfEvaluationToBeProcessed.isEmpty()){
                for(Evaluation__c evaluation : lstOfEvaluationToBeProcessed){
                    if(mapOfEvaluationScriptNameToId.containsKey(evaluation.Remote_Script_Number__c)){
                        evaluation.EvaluationScript__c = mapOfEvaluationScriptNameToId.get(evaluation.Remote_Script_Number__c);
                    }
                    if(mapOfEvaluationScriptIdToTotalWords.containsKey(evaluation.EvaluationScript__c)){
                        evaluation.TotalWords__c = mapOfEvaluationScriptIdToTotalWords.get(evaluation.EvaluationScript__c);
                    }
                }
            }
        }
    }
}