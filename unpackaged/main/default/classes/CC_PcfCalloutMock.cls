@isTest
public class CC_PcfCalloutMock implements HttpCalloutMock {

    protected Integer code = 200;
    protected String status = 'status';
    protected String body = 'body';
    protected Map<String, String> responseHeaders = new Map<String, String>();

    public CC_PcfCalloutMock() {
    }
    public CC_PcfCalloutMock(String body) {
        this.body = body;
    }

    public CC_PcfCalloutMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }

}