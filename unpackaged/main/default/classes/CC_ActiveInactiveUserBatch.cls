public class CC_ActiveInactiveUserBatch implements Database.Batchable<sObject>,Schedulable, Database.AllowsCallouts {
    
    
    public void execute( SchedulableContext sc ) {
      CC_ActiveInactiveUserBatch uab = new CC_ActiveInactiveUserBatch();
      Database.executeBatch(uab,10);
    }
    
    public Database.QueryLocator start( Database.BatchableContext bc ) {
      return Database.getQueryLocator('SELECT Id,Active__c,Deactivated__c, CB_userID__c FROM Contact WHERE Update_To_CCWS__c = TRUE');
      
    }
    public void execute(Database.BatchableContext bc, List<Contact> scope) {
        List<Contact> conToUpdate = new List<Contact>();
      for( Contact con : scope ) {
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(Label.CC_CCWS_ENDPOINT+'/user/update');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        // Set the body as a JSON object
        //String addressBodyString = '"addresses": [{"city":"'+ con.MailingCity+'","state":"'+con.MailingState+'","street":"'+con.MailingStreet+'","country":"'+con.MailingCountry+'","zip":"'+con.MailingPostalCode+'"}]';
        request.setBody('data={"id": "'+con.CB_userID__c+'","userID": "'+con.CB_userID__c+'","active":'+!con.Deactivated__c+'}');
        
        //if( !Test.isRunningTest() ) {
          HttpResponse response = http.send(request);
          // Parse the JSON response
          if (response.getStatusCode() != 200) {
              con.addError('The status code returned was: ' +
                           response.getStatusCode() + ' ' + response.getStatus());
          } 
          else {
            contact c = new Contact();
              c.id = con.id;
             c.Update_To_CCWS__c = false;
              conToUpdate.add(c);
            //con.Deactivated__c = False;
          }
        //}
      }
      
      //update scope;
        if(conToUpdate.size()>0){
            update conToUpdate;
        }
    }
    
    public void finish( Database.BatchableContext bc ) {
      
    }
    
}