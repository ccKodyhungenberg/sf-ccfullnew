/**********************************************
* Name :updateAccountOnAssetsBatch
* Description : To update the Account on Assets with Intall Date Prior to Feb 21, 2019 (Story S-609398) 
* Author : Nilesh Grover
**********************************************/

global class updateAccountOnAssetsBatch implements Database.Batchable<sObject> {	
	
	global String query;
	global updateAccountOnAssetsBatch() {
			query = 'SELECT Id , AccountId, ContactId, Contact.AccountId, InstallDate ' + 
								'FROM Asset ' + 
								'Where AccountId = null ' +
									'AND ContactId != null';
	}	
	global updateAccountOnAssetsBatch(String dynamicQuery) {
			query = dynamicQuery;
	}	
	global Database.QueryLocator start(Database.BatchableContext BC) {		
			return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC, List<Asset> scope) {
			System.debug('Scope : ' + scope);
			if(scope.size() > 0) {
				for(Asset asset : scope) {
					if(asset.Contact.AccountId != null) {
							asset.AccountId = asset.Contact.AccountId;
					}
				}
				Database.update(scope, false);
			}
	}	
	global void finish(Database.BatchableContext BC) {
			
	}	
}

// Execute Script
//Database.executeBatch(new updateAccountOnAssetsBatch());