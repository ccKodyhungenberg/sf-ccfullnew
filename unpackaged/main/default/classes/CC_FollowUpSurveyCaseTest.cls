@istest
private class CC_FollowUpSurveyCaseTest {
    @isTest
    static void testFollowUpSurvey_HM(){
        Account newAcc = new Account(Name='Test Account');
        insert newAcc;
        //Order List
        	Order ord = new Order();
            ord.AccountId = newAcc.Id;
            ord.EffectiveDate = Date.today();
            ord.ShippingCity = 'Brandon';
            ord.ShippingState = 'SD';
            ord.ShippingPostalCode = '57005';
            ord.Shipping_Name__c = 'test shipping name';
            ord.ShippingStreet = '809 Heatherwood';
            ord.Customer_City__c = 'Brandon';
            ord.Customer_State__c = 'SD';
            ord.Customer_ZipCode__c = '57005';
            ord.Customer_Name__c = 'test shipping name';
            ord.Customer_Street__c = '809 Heatherwood';
            ord.Status = 'Open';
        //Order List
        insert ord;
        Contact conNew=new Contact(LastName='Test Contact',Email='abc@gmail.com.test',AccountId=newAcc.Id);
        insert conNew;
        //Work order
        WorkOrder word = new WorkOrder();
            word.Order__c = ord.Id;
            word.Labor_Source__c = 'Field Solutions';
            Id devRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Service and repair').getRecordTypeId();
            word.RecordTypeId = devRecordTypeId;
            word.Status = 'On Hold';
        //Work order
        insert word;
        Case c = new Case();
        c.AccountId = newAcc.Id;
        c.RecordTypeId = '012f4000001MamS';
        c.Work_Order__c = word.id;
        C.Subject = '7-Day Confirmation';
        c.Subject = 'Follow-up';
        c.Follow_up_survey_completed__c = true;
        insert c;
        CC_FollowUpSurveyCase.getCase(c.id);
    }
    /*
    static void testFollowUpSurvey(){
        List<Account> accList = CC_TestUtility.createAccount(1, true);
        List<Order> orderList = CC_TestUtility.createOrder(1, true, accList[0].id);
        List<Contact> contactList = CC_TestUtility.createContactRecords('Amazing Test',1,true); // Edited by Himanshu Matharu C-00263124 
        List<WorkOrder> workOrderList = CC_TestUtility.createWorkOrder(1, false, orderList[0].id);
        // START by Himanshu Matharu C-00263124 
        contactList[0].Do_not_Create_Retention_Case__c = false;
        update contactList[0];
        workOrderList[0].AccountId = accList[0].Id;
        workOrderList[0].Contact = contactList[0];
        Database.insert(workOrderList, false);
        // END by Himanshu Matharu C-00263124
        Case c = new Case();
        c.AccountId = workOrderList[0].AccountId;
        c.RecordTypeId = '012f4000001MamS';
        c.Work_Order__c = workOrderList[0].id;
        C.Subject = '7-Day Confirmation';
        c.Subject = 'Follow-up';
        c.Follow_up_survey_completed__c = true;
        insert c;
        CC_FollowUpSurveyCase.getCase(c.id);
    }*/
}