@isTest
public class AddRemoveDidNumberPageControllerTest {
    private class Mock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            
            res.setBody('{"Result": "true","message":"test message","data":["2675447251"]}');
            
            res.setStatusCode(200);
            return res;
        }
    }
    
    public static List<Contact> conList;
    public static void createData(){
        conList=new List<Contact>();
        for(Integer i=0;i<20;i++){
            Contact con=new Contact();
            con.LastName='test'+i;
            con.cc_voice_number__c='(614) 864-5864'+i;
            con.CB_userID__c='test user id'+i;
            con.MailingStreet =' 123 Main St'+i;
            con.MailingPostalCode='302022'+i;
            conList.add(con);
        }
        insert conList;
    }
    @isTest   
    public static void testAddNumber(){
        createData();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock());
        
        ApexPages.currentPage().getParameters().put('id',conList[0].id);
        AddRemoveDidNumberPageController addDid=new AddRemoveDidNumberPageController();
        addDid.addNumber();
        Test.stopTest();
    }
    
    @isTest
    public static void testRemoveNumber(){
        createData();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock());
        
        ApexPages.currentPage().getParameters().put('id',conList[0].id);
        AddRemoveDidNumberPageController removeDid=new AddRemoveDidNumberPageController();
        try{
            removeDid.removeNumber();
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('User ID Not Available') ? true : false;
			System.assertEquals(expectedExceptionThrown,true);
        }
        Test.stopTest();
        
    }
}