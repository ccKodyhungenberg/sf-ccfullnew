/*
SendProspect
https://qa-clearcaptions.cs93.force.com/api/services/apexrest/SendProspect
Post
Header paramenters:
token= (static 'password', token is allocated for every endpoint - check list below)
FirstName
LastName
Email
Phone
utm_source
utm_medium
utm_campaign
utm_content
utm_term
Marketing_LeadId
RefNumber - unique campaign identifier (to place prospect/lead into a Campaign in Salesforce, may be hidden form field)

Message (professional only)
BusinessName (professional only)

Required parameters (for all endpoints):
FirstName
LastName
Email
Phone

cc.com web form - retail (first/last/phone email)
1D89AD1E-5338-4F4E-8C69-85154A7B5C4A

cc.com web form – professional (first/last/phone email/business name/message)
FD76C95C-7F0A-42A5-B219-ABB06998E843

Mobile app - (first/last/phone email)
6F5956B0-6361-4F8C-A6C9-1131BC43415F

New Web site(sandbox): 
CB306368-4FC0-4738-BE9D-7AB8C3D6F371
default campaign (sandbox): 7014F00000050mO

Returns JSON in body
class clMessage
{ 
public String Code; //"1" - successful operation "0" - error 
public String Message; 
}
HTTP status is 200 (if call to custom ws was successful, the rest of http codes are returned by platform). 
if Code=1 prospect submission was successful
if Code=0 - there was an error.

Deployment Note: in production set real access token

Production URL: 
https://clearcaptions.secure.force.com/api/services/apexrest/SendProspect
Production token: 
EB8149C4-957D-43E9-88D3-7A2C83249F39
Production RefNumber: 
R6018
*/

@RestResource(urlMapping='/SendProspect')
global without sharing class CC_SendProspect {
    @HttpPost
    global static void SendProspectWS() {
        RestRequest req=RestContext.request;
        RestContext.response.addHeader('Content-Type', 'application/json');
        //Added 3 new parameters City,State,PostalCode in SendProspect Method by parag Bhatt for Case #00255962
       	//Added TimeZone Parameter in SendProspect  method By Parag Bhatt for case #00260412
        clMessage objReply=SendProspect(
            req.headers.get('token')
            ,req.headers.get('FirstName')
            ,req.headers.get('LastName')
            ,req.headers.get('Email')
            ,req.headers.get('Phone')
            //Start-added by Prachi Sharma for the case 00266795
            ,req.headers.get('MobilePhone')
            //End-added by Prachi Sharma for the case 00266795
            ,req.headers.get('City')
            ,req.headers.get('State')
            ,req.headers.get('Street')
            ,req.headers.get('PostalCode')
            ,req.headers.get('TimeZone')
            ,req.headers.get('Message')
            ,req.headers.get('BusinessName')            
            ,req.headers.get('utm_source')
            ,req.headers.get('utm_medium')
            ,req.headers.get('utm_campaign')
            ,req.headers.get('utm_content')
            //Start-edited by Gursehaj Pal Singh Aneja for the story S-606009
            ,req.headers.get('utm_term')
            //End-edited by Gursehaj Pal Singh Aneja for the story S-606009
            ,req.headers.get('Marketing_LeadId')
            ,req.headers.get('RefNumber')
            //added by KMH 3.24
            // ,req.headers.get('IR Click Id')
            // ,req.headers.get('IR CustomerId')
            // ,req.headers.get('IR EventDate') 
            // ,req.headers.get('IR Oid')
            // //,req.headers.get('IR Update') 
            // ,req.headers.get('IR ActionTrackId')         
        );
        //always return success if(objReply.Code=='1') RestContext.response.statusCode=200; else RestContext.response.statusCode=400; 
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objReply));
        return;
    }
    
    global class clMessage {
        public String Code; //"1" - successful operation "0" - error
        public String Message;
        public clMessage(string scode, string mess) {
            this.Code=scode;
            this.Message=mess;
        }
    }
    public static map<string,CC_SendProspectEndpoint__mdt> m_Token_SPE {
        get {
            if(m_Token_SPE==null) {
                m_Token_SPE=new map<string,CC_SendProspectEndpoint__mdt>();
                for(CC_SendProspectEndpoint__mdt spe: [SELECT MasterLabel,CampaignID__c,Token__c FROM CC_SendProspectEndpoint__mdt ]) m_Token_SPE.put(spe.Token__c, spe); 
            }
            return m_Token_SPE;
        } set; 
    }

    public static boolean bIsValidToken(string sToken) {
        return m_Token_SPE.containsKey(sToken);
    }
    
    //Added 3 new parameters sCity,sState,sPostalCode in SendProspect Method by parag Bhatt for Case #00255962
    global static clMessage SendProspect(
        string sAccessToken
        ,string sFirstName
        ,string sLastName
        ,string sEmail
        ,string sPhone
        ,string sMobilePhone //Added by Prachi Sharma for the case 00266795
        ,string sCity
        ,string sState
        ,string sStreet
        ,string sPostalCode
        ,string sTimeZone
        ,string sMessage
        ,string sBusinessName
        ,string sutm_source
        ,string sutm_medium
        ,string sutm_campaign
        ,string sutm_content
        //Start-edited by Gursehaj Pal Singh Aneja for the story S-606009
        ,string sutm_term
        //End-edited by Gursehaj Pal Singh Aneja for the story S-606009
        ,string sMarketing_LeadId
        ,string sRefNumber
        //added by KMH 3.24
        // ,string sIrClickId
        // ,string sIrCustomerId
        // ,string sIrEventDate
        // ,string sIrOid
        // //,boolean sIrUpdate
        // ,string sIrActiontrackId     
        ) {
          
          
        //check parameters
        if(sAccessToken==null) {
            insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='ERROR', Message__c='Invalid (null) Token provided. Site url='+URL.getSalesforceBaseUrl().toExternalForm());
            return new clMessage('0','Invalid Token (null) provided. Site url='+URL.getSalesforceBaseUrl().toExternalForm());
        }        
        if(!CC_SendProspect.m_Token_SPE.containsKey(sAccessToken)) {
            insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='ERROR', Message__c='Invalid Token provided ('+sAccessToken+'). Site url='+URL.getSalesforceBaseUrl().toExternalForm());

                return new clMessage('0','Invalid Token provided ('+sAccessToken+'). Site url='+URL.getSalesforceBaseUrl().toExternalForm());
            
        }
        if(sFirstName==null) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='FirstName is required');
            return new clMessage('0','FirstName is required');
        } else {
             sFirstName=sFirstName.trim();
             if(sFirstName=='') {
                if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='FirstName is required (emty after trim)');
                return new clMessage('0','FirstName is required');               
             }
        }
        if(sLastName==null) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='LastName is required');
            return new clMessage('0','LastName is required');
        } else {
             sLastName=sLastName.trim();
             if(sLastName=='') {
                if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='LastName is required (emty after trim)');
                return new clMessage('0','LastName is required');                
             }
        }
        if(sEmail==null) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Email is required');
            return new clMessage('0','Email is required');
        } else {
             sEmail=sEmail.trim().toLowerCase();
             if(sEmail=='') {
                if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Email is required (emty after trim)');
                return new clMessage('0','Email is required');               
             }
        }
        //validate email format
        if(!CC_GlobalUtility.IsValidEmailAddress(sEmail)) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Invalid Email address: '+sEmail);
            return new clMessage('0','Invalid Email address: '+sEmail);              
        }

        /*    Commented by Prachi for Case 00266795
        if(sPhone==null || sPhone=='') {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Phone is required');
            return new clMessage('0','Phone is required');
        */  //Commented by Prachi for Case 00266795
         
         //Start - Added by Prachi for Case 00266795 
       if((sPhone==null || sPhone=='') && (sMobilePhone==null || sMobilePhone=='')) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') 
                insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Phone is required');
                return new clMessage('0','Either Phone or Mobile Number is required');
        } 
          //End - Added by Prachi for Case 00266795  
          
        if (sPhone!=null){  //Added by Prachi for Case 00266795
             sPhone=sPhone.trim();
             sPhone=sPhone.replaceAll('[^0-9]', '');
             /* //Commented by Prachi for Case 00266795
             if(sPhone=='') {
                if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Phone is required (emty after trim)');
                return new clMessage('0','Phone is required');              
             }
             */ //Commented by Prachi for Case 00266795
             
            if(sPhone.length()==11 && sPhone.startsWith('1')) sPhone=sPhone.substring(1); //remove leading 1 when needed
        if(sPhone.length()!=10) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Invalid Phone Number (have to be 10 digits): '+sPhone);
            return new clMessage('0','Invalid Phone Number (have to be 10 digits): '+sPhone);                
        }
        }
        
       //Start - Added by Prachi for Case 00266795
      if (sMobilePhone!=null){
             sMobilePhone=sMobilePhone.trim();
             sMobilePhone=sMobilePhone.replaceAll('[^0-9]', '');
             
          if(sMobilePhone.length()==11 && sMobilePhone.startsWith('1')) sMobilePhone=sMobilePhone.substring(1); //remove leading 1 when needed
        if(sMobilePhone.length()!=10) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Invalid Mobile Phone Number (have to be 10 digits): '+sMobilePhone);
            return new clMessage('0','Invalid Mobile Phone Number (have to be 10 digits): '+sMobilePhone);                
        }
        } 
        //End - Added by Prachi for Case 00266795 
            
        //validate phone format
        
        /*  Comment - Added by Prachi for Case 00266795
         if(sPhone.length()==11 && sPhone.startsWith('1')) sPhone=sPhone.substring(1); //remove leading 1 when needed
        if(sPhone.length()!=10) {
            if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='Invalid Phone Number (have to be 10 digits): '+sPhone);
            return new clMessage('0','Invalid Phone Number (have to be 10 digits): '+sPhone);                
        }
        Comment - Added by Prachi for Case 00266795  */
                    
        //ready to search by email
        List<Contact> lContactExisting=new List<Contact>([select id, accountid, Campaign__c, email, phone, homephone, otherphone, recordtypeid, recordtype.name from Contact where email=:sEmail]);
        if(lContactExisting.isEmpty()) {//did not find duplicate by email
            //search by phone
            string sPatternLike=CC_GlobalUtility.sPhone2Like(sPhone); 
            lContactExisting=new List<Contact>([select id, AccountId, Campaign__c, email, phone, homephone, otherphone, recordtypeid, recordtype.name from Contact where (phone!=null and phone like :sPatternLike) OR (homephone!=null and homephone like :sPatternLike) OR (otherphone!=null and otherphone like :sPatternLike)]);
        }
            Id idCampaign2Add =  m_Token_SPE.get(sAccessToken).CampaignID__c;
        
        //ifRefNumber provided - find campaign by RefNumber
        if(sRefNumber!=null) {
            list<Campaign>lCampaign=new List<Campaign>([select id, RefNumber__c from campaign where RefNumber__c=:sRefNumber]);
            if(!lCampaign.isEmpty()) idCampaign2Add=lCampaign[0].Id;
        } 
        
        
         Savepoint sp = Database.setSavepoint();
        try {
            Lead ld = new Lead();
            ld.FirstName = sFirstName;
            ld.LastName = sLastName;
            ld.Company = sLastName;
            ld.Email = sEmail;
            //START--> Below Code added by parag Bhatt for Case #00255962
            ld.Phone = sPhone;
            ld.MobilePhone = sMobilePhone; //Added by Prachi Sharma for the case 00266795
            ld.City = sCity;
            ld.State = sState;
            ld.Street = sStreet;
            ld.Country = 'United States';
            ld.PostalCode = sPostalCode;
            ld.Time_Zone__c = sTimeZone;
            //END--> Below Code added by parag Bhatt for Case #00255962
            ld.utm_source__c = sutm_source;
            ld.utm_medium__c = sutm_medium;
            ld.utm_content__c = sutm_content;
            ld.utm_campaign__c = sutm_campaign;
            //Start-edited by Gursehaj Pal Singh Aneja for the story S-606009
            ld.utm_term__c = sutm_term;
            //End-edited by Gursehaj Pal Singh Aneja for the story S-606009
            ld.Status = 'New';
            //ld.Campaign = idCampaign2Add;
            ld.Campaign__c = idCampaign2Add;
            // ld.IR_ClickId__c = sIrClickId;
            // ld.IR_CustomerID__c = sIrCustomerId;
            // ld.IR_EventDate__c = sIrEventDate;
            // ld.IR_Oid__c = sIrOid;
            // //ld.IR_Update__c = sIrUpdate;
            // ld.IR_ActionTrackID__c = sIrActiontrackId;
            
            insert ld;
            
            insert new CampaignMember(CampaignId=idCampaign2Add, LeadId = ld.Id,  Status='Responded');
            
            
        } catch(System.DMLException e) {
            Database.rollback(sp);
            insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='ERROR', Message__c='End Point:' + m_Token_SPE.get(sAccessToken).MasterLabel + ' System error. Could not save prospect data:\n' + e);   
            return new clMessage('0','System error. Could not save prospect data' + e);              
        }             
            
        //select id, homephone from contact where homephone like '%425%961%46%80%' limit 1
        //Start-edited by Gursehaj Pal Singh Aneja for the story S-606009 
        //Edited below line by Prachi Sharma for case 00266795
        if(Label.CC_SEND_PROSPECT_LOGCALLS=='true') insert new Application_Log__c( Name='CC_SendProspect.SendProspect', Severity_Level__c='INFO', Message__c='End Point:' + m_Token_SPE.get(sAccessToken).MasterLabel + ' Was called with prospect info: \nFirstName='+sFirstName +'\nLastName='+sLastName+'\nEmail='+sEmail+'\nPhone='+sPhone+'\nMobilePhone='+sMobilePhone+'\nBusinessName='+sBusinessName+'\nMessage='+sMessage+'\nutm_source='+sutm_source +'\nutm_medium='+sutm_medium +'\nutm_campaign='+sutm_campaign +'\nutm_content='+sutm_content+'\nutm_term='+sutm_term+'\nMarketing_LeadId='+sMarketing_LeadId+'\nRefNumber='+sRefNumber);
        clMessage WS_Result=new clMessage('1','Prospect information was received');
        return WS_Result;
        //End-edited by Gursehaj Pal Singh Aneja for the story S-606009    
    }    
}