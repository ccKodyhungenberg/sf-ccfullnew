@IsTest
public class Welcome_Call_Case_Test {

    @IsTest
    public static void Should_Create_Welcome_Call_When_WO_Closes(){
    //assemble

    Date orderWODate = Date.newInstance(2020, 5, 1);

    Account customerAcct = new Account();
    customerAcct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Household').getRecordTypeId();
    customerAcct.Name = 'Stacy Household';
    customerAcct.Type = 'Customer';

    insert customerAcct;

    Contact customer = new Contact();
    customer.RecordTypeId = Contact.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
    customer.AccountId = customerAcct.Id;
    customer.lastName = 'Stacy';
    customer.Type__c = 'Customer';
    customer.CC_Voice_Number__c = '555-555-5555';
    customer.Open_Welcome_Call_Case__c = false;

    insert customer;

    Account techAccount = new Account();
    techAccount.Name = 'Johnson';
    techAccount.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Non_Customer').getRecordTypeId();

    insert techAccount;

    Contact technician = new Contact();
    technician.RecordTypeId = Contact.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Technician').getRecordTypeId();
    technician.lastName = 'Johnson';
    technician.AccountId = techAccount.Id;
    technician.Active__c = true;
    technician.Type__c = 'Technician';

    insert technician;

    Asset installedAsset = new Asset();
    installedAsset.RecordTypeId = Asset.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Phone').getRecordTypeId();
    installedAsset.AccountId = customerAcct.Id;
    installedAsset.ContactId = customer.Id;
    installedAsset.Name = 'Test phone';
    installedAsset.Deleted__c = false;
    
    insert installedAsset;

    Order blueOrder = new Order();
      blueOrder.AccountId = customerAcct.Id;
      blueOrder.Shipping_Name__c = customer.lastName;
      blueOrder.Type = 'Customer';
      blueOrder.Status = 'Approved';
      blueOrder.Created_From_Portal__c = true;
      blueOrder.EffectiveDate = orderWODate;
      blueOrder.ShippingStreet = '123 Main';
      blueOrder.ShippingCity = 'Roseville';
      blueOrder.ShippingState = 'CA';
      blueOrder.ShippingPostalCode = '95661';
      blueOrder.ShippingCountry = 'USA';
      blueOrder.RecordTypeId = Order.SObjectType
        .getDescribe()
        .getRecordTypeInfosByDeveloperName()
        .get('Product_Order')
        .getRecordTypeId();

    insert blueOrder;
    
    WorkOrder installWO = new WorkOrder();
    installWO.Order__c = blueOrder.Id;
    installWO.Last_Closed_Cancelled_date_Backend__c = DateTime.newInstance(2020, 5, 5, 10, 5, 0);
    installWO.RecordTypeId = WorkOrder.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Install_and_training').getRecordTypeId();
    installWO.ContactId = customer.Id;
    installWO.AccountId = customerAcct.Id;
    installWO.Status = 'Scheduled';
    //installWO.Technician__c = technician.Id;
    installWO.AssetID = installedAsset.Id;
    installWO.Service_Request_Type__c = 'Install and Training';

    insert installWO;
        
    //System.assertEquals(technician.id, installWO.Technician__c); SF-2059

    //act
    // Need to set work order to completed to trigger PB

    installWO.Status = 'Completed';
    update installWO;
    

    //assert
    //Need to assert that a case was created as a welcome call case and all the fields are populated
    Case welcomeCase = new Case();
    welcomeCase = [SELECT Id,RecordType.Name FROM Case WHERE ContactId =: customer.Id];
    customer = [SELECT Id, Open_Welcome_Call_Case__c FROM Contact WHERE Id =: customer.Id];

    System.assertEquals(true, customer.Open_Welcome_Call_Case__c);
    System.assertEquals('Welcome Call', welcomeCase.RecordType.Name);


 
    }
}