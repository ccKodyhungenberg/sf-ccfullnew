/*
 * Appirio, Inc
 * Name: Sched_CC_CampaignFeedbackEmailBatch
 * Description: S-611297
 * Created Date: 28 Mar, 2019
 * Created By: Nilesh Grover
 * 
 * Date Modified                Modified By                  Description of the update
 */
 
global class Sched_CC_CampaignFeedbackEmailBatch implements Schedulable {
    
    //execute method to execute the logic of batch processing 
    global void execute(SchedulableContext sc) {
        CC_CampaignFeedbackEmailBatch BC = new CC_CampaignFeedbackEmailBatch(null);
        Database.executeBatch(BC, 100);
    }
}


/* Sample Query for testing Purpose
Date lastEmailDate = Date.today().addDays(-32);
Date firstEmailDate = Date.today().addDays(-2);
String query = 'SELECT id, Event_POC__c, Event_POC__r.email, Gross_Sales__c, Event_Well_Attended__c, Good_Booth_Location__c, Was_Event_Attended_by_Our_Demographic__c, Should_We_Attend_Next_Year__c, Number_of_Attendees__c, Number_of_People_over_age_65__c, Average_Age__c, Location__c, Weather__c, Demographic__c, Placement__c, Setup__c, Other_Comments__c ' + 
					'FROM Campaign ' +
						'WHERE ' +
							'EndDate >= :lastEmailDate AND ' +
							'EndDate <= :firstEmailDate' +
							' AND Id = \'7011k000000fqQf\'';
Database.executeBatch(new CC_CampaignFeedbackEmailBatch(query));
*/