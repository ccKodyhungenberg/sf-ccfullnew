global class CC_GlobalUtility {
	public static boolean IsTerritoryManager(id idUser) {
        boolean bReply=false;
        if(m_Id_UserActive.containsKey(idUser)) if(m_Id_UserActive.get(idUser).UserRole.Name=='CC Territory Manager') bReply=true;
        return bReply;
    }
    public static Map<Id,User> m_Id_UserActive {
        get {
            if(m_Id_UserActive==null) m_Id_UserActive=new Map<Id,User>([SELECT Id,Name, username, UserRoleId, UserRole.Name, FIRSTNAME,LASTNAME, Email, Phone, Street, City, State, PostalCode FROM User WHERE IsActive=true]); 
            return m_Id_UserActive;
        } set;
    }
    
    public static id idProductManager { //Craigh Roth
        get {
            if(idProductManager==null) for(User us: m_Id_UserActive.values()) { if(us.FIRSTNAME=='Craig' && us.LASTNAME=='Roth') { idProductManager=us.Id; break; }}
            return idProductManager;
        } set; 
    }
    
    public static id idBoomi { //Dell Boomi Appliance
        get {
            if(idBoomi==null) for(User us: m_Id_UserActive.values()) { if(us.UserName.startsWithIgnoreCase('integration@clearcaptions.com')) { idBoomi=us.Id; break;}}
            return idBoomi;
        } set; 
    }
    
    public static decimal geo_distance(decimal lat1, decimal lng1, decimal lat2, decimal lng2) {
        decimal dlng = (lng2 - lng1)*3.14159265359/180;
        decimal dlat = (lat2 - lat1)*3.14159265359/180;
        lat1=lat1*3.14159265359/180;
        lng1=lng1*3.14159265359/180;
        lat2=lat2*3.14159265359/180;
        lng2=lng2*3.14159265359/180;
        double a=math.sin(dlat/2.0) * math.sin(dlat/2.0)+math.cos(lat1) * math.cos(lat2) * (math.sin(dlng/2.0) * math.sin(dlng/2.0));
        return 3961.0 * 2.0 * math.atan2(math.sqrt(a),math.sqrt(1-a));  
    }
    
    
    //RecordType
    public static map<Id, RecordType> map_Id_RecordType {
        get {
            if(map_Id_RecordType==null) {
                map_Id_RecordType=new map <Id, RecordType>([select id, Name, SobjectType from recordtype where Name!=null and SobjectType!=null]);
            }
            return map_Id_RecordType; 
        } set;
    }
    
    public static map<string, recordtype> map_RTNameObject_recordtype {
        get {
            if(map_RTNameObject_recordtype==null) {
                map_RTNameObject_recordtype=new map <string, recordtype>();
                for(recordtype rt: map_Id_RecordType.values()) { map_RTNameObject_recordtype.put(rt.Name + '_' + rt.SobjectType, rt);}
            }
            return map_RTNameObject_recordtype; 
        } set;
    }
    
    public static id getRecordTypeIdByName(string rt_name, string rt_sobjecttype) {
        system.assert(map_RTNameObject_recordtype.containskey(rt_name+'_' + rt_sobjecttype), 'Invalid record type name '+rt_name+' for the object: '+ rt_sobjecttype);
        return map_RTNameObject_recordtype.get(rt_name+'_' + rt_sobjecttype).Id;
    }
    
    public static boolean IsValidEmailAddress(string sEmail) {
        if(sEmail==null || sEmail=='') return false; 
        Pattern oEmailPattern = Pattern.compile('^[a-z0-9._%+-/!#$%&\'*=?^_`{|}~]+@[a-z0-9.-]+\\.[a-z]{2,4}$');
        return oEmailPattern.matcher(sEmail.trim().toLowerCase()).matches();
    }
    
    public static string sPhone2Like(string sPhone) {
        if(sPhone==null || sPhone=='') return '-1111111111';
        if(sPhone.length()!=10) return '-1111111111';
        return '%'+sPhone.substring(0,3)+'%'+sPhone.substring(3,6)+'%'+sPhone.substring(6,8)+'%'+sPhone.substring(8,10)+'%';
    }
    
    public static id idConcierge { //Shared inside sales
        get {
            if(idConcierge==null) for(User us: m_Id_UserActive.values()) { if(us.FIRSTNAME=='Information' && us.LASTNAME=='ClearCaptions') {idConcierge=us.Id; break; }}
            return idConcierge;
        } set; 
    }
    
    public static boolean IsSandbox {
        get {
            if(IsSandbox==null) {
                Organization objCurrentOrg=[select Id, IsSandbox from Organization limit 1];
                IsSandbox=objCurrentOrg.IsSandbox;
            }
            return IsSandbox;
        } set;
    }
    
    public static string CC_SendProspect_AccessToken = 'DEV-EB8149C4-957D-43E9-88D3-7A2C83249F39';
    
    public static Boolean isCompanyEmail(String emailAddress){
 
         String companyEmailRegex = Label.Company_Email_RegEx ;
         //String companyEmailRegex = '.*(@purple\\.us;@goamerica\\.com).*';
        if(emailAddress=='' || emailAddress==null) {
            return false;
        }
        Pattern myPattern = Pattern.compile(companyEmailRegex);
        Matcher myMatcher = MyPattern.matcher(emailAddress);

        return MyMatcher.matches();

    }
    
    
    global static Boolean isAdminUser {
        get {return isAdminUser();}
        private set;}
    
    private static boolean isAdminUser(){
    
        return (UserInfo.getProfileId().contains(Label.Administrator_Profile));
        
    }
    
    public static Integer changeToFromCompanyEmail (String toAddress, String fromAddress){
    
        Boolean isCompanyEmail_FROM = isCompanyEmail(fromAddress);
        Boolean isCompanyEmail_TO = isCompanyEmail(toAddress);
    
        if (isCompanyEmail_FROM == isCompanyEmail_TO) {
            return 0; 
        } else if (isCompanyEmail_FROM) {
            return 1;
        } else {
            return 2;
        }
    
    }
    
    public static string VFC009_Contact_ProfileEmailEnabled = 'true';
    
    
    public static Boolean IsTriggerRunning(Id userId){
        Boolean isTrigger = true;
        if(ByPass_TriggerSetting__c.getInstance(userId).id!=null){
            isTrigger = false;
        }        
         return isTrigger;
    }
}