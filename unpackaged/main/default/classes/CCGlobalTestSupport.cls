public class CCGlobalTestSupport {
    public static Contact[] CreateCustomer(integer iHowMany) {
        if(iHowMany==null || iHowMany<=0) throw new ClearCaptionsApplicationException('CCGlobalTestSupport.CreateCustomer: number of customer contacts to create have to be more than 0');
        List<Contact> lct=new List<Contact>();
        
        for(integer i=0; i<iHowMany; ++i) {
                lct.add(new Contact(
                ContactID__c = -99999+i,
                CB_userID__c = string.valueof(-99999+i),
                DateCreated__c = datetime.now(),
                Email = 'homer'+String.valueof(i)+'@simpsons.com',
                HasOptedOutOfEmail = True,
                FBID__c = 999+i,
                FirstName = 'Homer'+String.valueof(i),
                LanguagePreference__c = 'English',
                LastName = 'Simpson'+String.valueof(i),
                LastUpdated__c = datetime.now(),
                MailingCity = 'Springfield',
                MailingCountry = 'United States',
                MailingState = 'CA',
                MailingStreet = '55 Evergreen Terr',
                MailingPostalCode = '93410',
                Password__c = 'DOH'+String.valueof(i),
                RelayType__c = 'Setec',
                RecordTypeId = CCGlobal.getRecordTypeIdByName('Customer','Contact'),
                CPNI_Opt_In__c=true
                )
                );
        }
        return lct;     
    }
    public static Opportunity[] CreateOpportunity(integer iHowMany) {
        if(iHowMany==null || iHowMany<=0) throw new ClearCaptionsApplicationException('CCGlobalTestSupport.CreateOpportunity: number of Opportunities to create have to be more than 0');
        List<Opportunity> lOp=new List<Opportunity>();
        //no accounts
        date dtToday=date.today();
        for(integer i=0; i<iHowMany; ++i) {
                lOp.add(new Opportunity(
                    StageName='Qualify'
                    ,CloseDate=dtToday.addDays(i)            
                    ,Name='Test Opportunity #'+i
                    ));
        }
        return lOp;     
    }
    
    public class ClearCaptionsApplicationException extends Exception {} 
}