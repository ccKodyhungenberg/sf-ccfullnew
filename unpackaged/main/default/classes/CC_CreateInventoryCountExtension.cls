public class CC_CreateInventoryCountExtension {
    public Inventory_Count_Line_Item__c inventoryCount;
    public Inventory_Count_Line_Item__c deletedIc;
    public List<Inventory_Count_Line_Item__c> iclIToInsert ;
    public List<Inventory_Count_Line_Item__c> addIcList {get;set;}
    public List<Inventory_Count_Line_Item__c> delIcList {get;set;}
    public List<Inventory_Count_Line_Item__c> ICList {get;set;}
    public Integer totalCount {get;set;}
    public Integer rowIndex {get;set;}
    // MKLICH S-618699 start
    public Map <String, Decimal> trueUpProducts = new map<String, Decimal>();
    public Map <Id, String> productMap = new map<Id, String>();
    // MKLICH S-618699 stop
    Boolean isSubmission ;
    public List<Inventory_Count_Line_Item__c> delAttendees {get; set;} 
    public CC_CreateInventoryCountExtension(ApexPages.StandardController controller){
        ICList = new List<Inventory_Count_Line_Item__c>();
        iclIToInsert = new List<Inventory_Count_Line_Item__c>();
        // MKLICH S-618699 start setup TrueUP products to validate
        for (True_Up_Products__mdt prod : [SELECT MasterLabel, length__c From True_Up_Products__mdt]) {
            trueUpProducts.put(prod.MasterLabel, prod.length__c);
        }
        System.debug('trueUp Products :' + trueUpProducts);
        for(Product2 products : [SELECT Id, Name FROM Product2 WHERE name = :trueUpProducts.keyset()]) {
            productMap.put(products.Id, products.Name);
        }
        // MKLICH S-618699 stop
        ICList = [SELECT id,Product__c,Serial_Number__c,Quantity__c,Inventory_Count__c , Status__c
                  FROM Inventory_Count_Line_Item__c 
                  WHERE CreatedById = : UserInfo.getUserId() 
                  AND Status__c = 'Saved'];
        if(ICList.size()==0){
            isSubmission = false;
            for(Integer i =0;i<5;i++){
                ICList.add(new Inventory_Count_Line_Item__c(Quantity__c = 1));
                totalCount = 5;
            }
        }
        else{
            isSubmission = true;
        }
        addIcList = new List<Inventory_Count_Line_Item__c>();
        delIcList = new List<Inventory_Count_Line_Item__c>();
    }
    public void addRow(){
        addIcList = new List<Inventory_Count_Line_Item__c>();
        ICList.add(new Inventory_Count_Line_Item__c(Quantity__c = 1));
        
    }
    public void deleteRow(){
        
        rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
        System.debug('rowbe deleted ' + rowIndex );
        System.debug('row to be deleted '+ICList[rowIndex]);
        if(ICList[rowIndex].id != null){
            delete ICList[rowIndex];
        }
        deletedIc = ICList.remove(rowIndex);
        delIcList.add(deletedIc);
        System.debug('delIcList::::::::::::::::::'+delIcList);
        
    }

    // MKLICH S-618699 start
    // Method that checks the Custom MetaData Type: True Up Product for the correct Serial Number length
    public Boolean validateSerialNumberLength(Inventory_Count_Line_Item__c lineItem) {
        Boolean answer = false;
        if (lineItem.Product__c != null && lineItem.serial_Number__c != null) {
            if(productMap.keyset().contains(lineItem.Product__c)) {
                String ProductName = productMap.get(lineItem.Product__c);
                Decimal lengthOfSerialNum = trueUpProducts.get(ProductName);
                if (lineItem.Serial_Number__c.length() == lengthOfSerialNum) {
                    answer = true;
                }
            }
            else {
                System.debug('Reference to a True Up that is not in the MetaDataType "True Up Product"');
            }
        }
        return answer;
    }
    // MKLICH S-618699 stop

    public PageReference save(){
        if(ICList.size()>0){
            if(!isSubmission){
                Integer i = 0;
                List<String> icSerialList = new List<String>();
                for(Inventory_Count_Line_Item__c ic:ICList){
                    if(ic.Serial_Number__c!=null){
                        icSerialList.add(ic.Serial_Number__c);
                    }
                }
                System.debug('icSerialList:::::::::::::'+icSerialList);
                Set<string>icSerialSet = new set<string>(icSerialList);
                if(icSerialList.size()>icSerialSet.size()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate serial numbers are not allowed. Please try with unique values.'));
                    return null;
                }
                Inventory_count__c inventoryC = new Inventory_count__c();
                inventoryC.Status__c = 'Saved';
                insert inventoryC;
                //system.assert(false, ICList);
                // MKLICH S-618699 Start
                Integer count = 1;
                for(Inventory_Count_Line_Item__c ic:ICList){
                    if(ic.Product__c != null) {
                        if(ic.Serial_Number__c != null && validateSerialNumberLength(ic)) {
                            if(ic.id == null) {
                                ic.Inventory_count__c = inventoryC.id;
                                ic.Status__c = 'Saved';
                                iclIToInsert.add(ic);
                            }
                            else {
                                String productName = productMap.get(ic.Product__c);
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' ' + productName + ' should have a Serial Number length of ' + trueUpProducts.get(productName) + ' at Row ' + count));
                                return null;
                            }
                        }
                        else {
                            String productName = productMap.get(ic.Product__c);
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' ' + productName + ' should have a Serial Number length of ' + trueUpProducts.get(productName) + ' at Row ' + count));
                            return null;
                        }
                    }
                    // else {
                    //     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' Product needs a value at Row ' + count));
                    //     return null;
                    // }
                    count++;
                }
                //system.assert(false, iclIToInsert);
                if (iclIToInsert.size() >= 1) {
                    upsert iclIToInsert;

                }
                //MKLICH S-618699 STOP
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Saved Successfully'));
                isSubmission = true;
            }
            else{
                List<String> icSerialListUpdate = new List<String>();
                List<Inventory_Count_Line_Item__c> newICLI = new List<Inventory_Count_Line_Item__c>();
                List<Inventory_Count_Line_Item__c> oldICLI = new List<Inventory_Count_Line_Item__c>();
                // MKLICH S-618699 Start
                Integer count = 1;
                for(Inventory_Count_Line_Item__c ic:ICList){
                    if(ic.Serial_Number__c!=null ){
                        icSerialListUpdate.add(ic.Serial_Number__c);
                    }
                    if(ic.Product__c != null) {
                        if (ic.Serial_Number__c != null && validateSerialNumberLength(ic)) {
                            if (ic.id == null) {
                                ic.Inventory_count__c = ICList[0].Inventory_count__c;
                                ic.Status__c = 'Saved';
                                newICLI.add(ic);
                            }
                            else {
                                oldICLI.add(ic);
                            }
                        }
                        else {
                            String productName = productMap.get(ic.Product__c);
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' ' + productName + ' should have a Serial Number length of ' + trueUpProducts.get(productName) + ' at Row ' + count));
                            return null;
                        }
                    }
                    // else {
                    //     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' Product needs a value at Row ' + count));
                    //     return null;
                    // }
                    count++;
                }
                // MKLICH S-618699 STOP
                Set<string>icSerialSet = new set<string>(icSerialListUpdate);
                if(icSerialListUpdate.size()>icSerialSet.size()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate serial numbers are not allowed. Please try with unique values.'));
                    return null;
                }
                if(newICLI.size()>0){
                    insert newICLI;
                }
                if(oldICLI.size()>0){
                	update oldICLI;
                }
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Saved Successfully'));
            	isSubmission = true;
            }
            
            if(delIcList.size()>0 && delIcList[0].id!= null){
                //delete delIcList; // Commented by Himanshu Matharu C-00261356 [15 May 2019]
                delIcList = new List<Inventory_Count_Line_Item__c>();
            }
            
        } 
        return null;
    }
    public PageReference Submit(){
        User u = new User();
        u.id = UserInfo.getUserId();
        u.Last_True_Up_Date__c = DateTime.now();
        update u;
        
        if(ICList.size()>0){
            //system.assert(false, ICList +''+ isSubmission);
            if(!isSubmission){
                Integer i = 1; // MKLICH S-618699 counter for Row #
                List<String> icSerialList = new List<String>();
                for(Inventory_Count_Line_Item__c ic:ICList){
                    if(ic.Serial_Number__c!=null){
                        icSerialList.add(ic.Serial_Number__c);
                    }
                }
                System.debug('icSerialList:::::::::::::'+icSerialList);
                Set<string>icSerialSet = new set<string>(icSerialList);
                if(icSerialList.size()>icSerialSet.size()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate serial numbers are not allowed. Please try with unique values.'));
                    return null;
                }
                Inventory_count__c inventoryC = new Inventory_count__c();
                inventoryC.Status__c = 'Submitted';
                inventoryC.Submitter__c = UserInfo.getUserId();
                insert inventoryC;
                //system.assert(false, ICList);
                // MKLICH S-618699 Start
                Integer count = 1;
                for(Inventory_Count_Line_Item__c ic:ICList){
                    if(ic.Product__c != null) {
                        if (ic.Serial_Number__c != null && validateSerialNumberLength(ic)) {
                            if(ic.id == null) {
                                ic.Inventory_count__c = inventoryC.id;
                                ic.Status__c = 'Submitted';
                                iclIToInsert.add(ic);
                            }
                            else {
                                String productName = productMap.get(ic.Product__c);
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' ' + productName + ' should have a Serial Number length of ' + trueUpProducts.get(productName) + ' at Row ' + count));
                                return null;
                            }
                        }
                        else {
                            String productName = productMap.get(ic.Product__c);
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' ' + productName + ' should have a Serial Number length of ' + trueUpProducts.get(productName) + ' at Row ' + count));
                            return null;
                        }
                    }
                    // else {
                    //     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' Product needs a value at Row ' + count));
                    //     return null;
                    // }
                    count++;
                }
                //system.assert(false, iclIToInsert);
                if (iclIToInsert.size() >= 1) {
                    upsert iclIToInsert;

                }
                //MKLICH S-618699 STOP
                isSubmission = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully'));
                return null;
            }
            else{
                List<String> icSerialListUpdate = new List<String>();
                List<Inventory_Count_Line_Item__c> newICLI = new List<Inventory_Count_Line_Item__c>();
                List<Inventory_Count_Line_Item__c> oldICLI = new List<Inventory_Count_Line_Item__c>();
                // MKLICH S-618699 Start
                Integer count = 1;
                for(Inventory_Count_Line_Item__c ic:ICList){
                    if(ic.Serial_Number__c!=null ){
                        icSerialListUpdate.add(ic.Serial_Number__c);
                    }
                    if(ic.Product__c != null) {
                        if (ic.Serial_Number__c != null && validateSerialNumberLength(ic)) {
                            if (ic.id == null) {
                                ic.Inventory_count__c = ICList[0].Inventory_count__c;
                                ic.Status__c = 'Submitted';
                                newICLI.add(ic);
                            }
                            else {
                                ic.Status__c = 'Submitted';
                                oldICLI.add(ic);
                            }
                        }
                        else {
                            String productName = productMap.get(ic.Product__c);
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' ' + productName + ' should have a Serial Number length of ' + trueUpProducts.get(productName) + ' at Row ' + count));
                            return null;
                        }
                    }
                    // else {
                    //     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, + ' Product needs a value at Row ' + count));
                    //     return null;
                    // }
                    count++;
                }
                // MKLICH S-618699 STOP
                Set<string>icSerialSet = new set<string>(icSerialListUpdate);
                if(icSerialListUpdate.size()>icSerialSet.size()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate serial numbers are not allowed. Please try with unique values.'));
                    return null;
                }
                Inventory_count__c icount = new Inventory_count__c();
                icount.Id = ICList[0].Inventory_count__c;
                icount.Status__c = 'Submitted';
                icount.Submitter__c = UserInfo.getUserId();
                update icount;
                //system.assert(false, newICLI);
                
                if(newICLI.size()>0){
                    insert newICLI;
                }
                if(oldICLI.size()>0){
                	update oldICLI;
                }
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully'));
            	
                isSubmission = true;
                return null;
            }
            
           /* if(delIcList.size()>0 && delIcList[0].id!= null){
                delete delIcList;
                delIcList = new List<Inventory_Count_Line_Item__c>();
            }*/
            
        }
        return null;
        
        
        
        /*if(isSubmission){
            Inventory_Count__c icToSubmit = new Inventory_Count__c();
            icToSubmit.id = ICList[0].Inventory_Count__c;
            icToSubmit.Status__c = 'Submitted';
            icToSubmit.Submitter__c = UserInfo.getUserId();
            update icToSubmit;
            List<Inventory_Count_Line_Item__c> icliUpdate = new List<Inventory_Count_Line_Item__c>();
            for(Inventory_Count_Line_Item__c icli: ICList){
                icli.status__C = 'Submitted';
                icliUpdate.add(icli);
            }
            update icliUpdate;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully'));
            return null;
        }
        if(iclIToInsert.size()>0){
            Inventory_Count__c icToSubmit = new Inventory_Count__c();
            icToSubmit.id = iclIToInsert[0].Inventory_Count__c;
            icToSubmit.Status__c = 'Submitted';
            icToSubmit.Submitter__c = UserInfo.getUserId();
            update icToSubmit;
            List<Inventory_Count_Line_Item__c> icliUpdate = new List<Inventory_Count_Line_Item__c>();
            //system.assert(false,iclIToInsert);
            for(Inventory_Count_Line_Item__c icli: iclIToInsert){
                /*if(icli.Status__c != 'Saved' && icli.Status__c !='Submitted'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please Save the Inventory Count Line Item Before Submitting'));
                }
                else{*/
               /* icli.status__C = 'Submitted';
                icliUpdate.add(icli);
                
            }
            update icliUpdate;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully'));
            return null;
        }
        else{
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please Save the Inventory Count Line Item Before Submitting'));
            return null;
        }
        /*if(delIcList.size()>0){
        delete delIcList;
        }*/
        
    }
    
}