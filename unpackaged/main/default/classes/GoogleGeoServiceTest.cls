@IsTest
private class GoogleGeoServiceTest {

    private static testmethod void test_ILLEGAL_ARGUMENT(){
        setupGlobalVariables();
        Boolean exceptionCaught = false;
        try{
             GoogleGeoService.lookup(null);
        }catch(GoogleGeoServiceException e){
            exceptionCaught = true;
        }
        System.assert(exceptionCaught == true,'GoogleGeoServiceException not thrown');
    }

    private static testmethod void test_PARSE_ERROR_INVALID_RESPONSE(){
        setupGlobalVariables();
        al.HttpUtils.pushTest('abc 123');
        Boolean exceptionCaught = false;
        try{
             GoogleGeoService.lookup(''); //query param just can't be blank
        }catch(GoogleGeoServiceException e){
            exceptionCaught = true;
        }
        System.assert(exceptionCaught == true,'GoogleGeoServiceException not thrown');
    }

    private static testmethod void test_PARSE_ERROR_NO_COORDINATES(){
        setupGlobalVariables();
        al.HttpUtils.pushTest(buildBasicResponseWithCoordinates('',''));
        Boolean exceptionCaught = false;
        try{
             GoogleGeoService.lookup('x'); //query param just can't be blank
        }catch(GoogleGeoServiceException e){
            exceptionCaught = true;
        }
        System.assert(exceptionCaught == true,'GoogleGeoServiceException not thrown');
    }

    private static testmethod void test_PARSE_ERROR_INAVLID_COORDINATES(){
        al.HttpUtils.pushTest(buildBasicResponseWithCoordinates('a','b'));
        Boolean exceptionCaught = false;
        try{
             GoogleGeoService.lookup('x'); //query param just can't be blank
        }catch(GoogleGeoServiceException e){
            exceptionCaught = true;
        }
        System.assert(exceptionCaught == true,'GoogleGeoServiceException not thrown');
    }

    private static testmethod void testLookup_WithNoResults(){
        final HttpResponse testResponse = new HttpResponse();
        testResponse.setBody('<?xml version="1.0" encoding="UTF-8"?><GeocodeResponse><status>ZERO_RESULTS</status></GeocodeResponse>');
        testResponse.setStatusCode(404);
        al.HttpUtils.pushTest(testResponse);
        Boolean exceptionCaught = false;
        try{
             GoogleGeoService.lookup('x'); //query param just can't be blank
        }catch(GoogleGeoServiceException e){
            exceptionCaught = true;
        }
        System.assert(exceptionCaught == true,'GoogleGeoServiceException not thrown');
    }

    private static testmethod void testLookup_WithOneResults(){
        final GeoPoint expected = new GeoPoint(23.456789d, 12.345678d);
        al.HttpUtils.pushTest(buildBasicResponseWithCoordinates(''+expected.latitude,''+expected.longitude));
        final GeoPoint actual = GoogleGeoService.lookup('x'); //query param just can't be blank
        System.assertNotEquals(null, actual);
        System.assert(actual.equals(expected),'expected: ' + expected.toStr() + ', actual: ' + actual.toStr());
    }

    private static testmethod void test_joinMap(){
        Map<string,string> mapToJoin = new Map<String,String>();
        mapToJoin.put('k1','v1');
        mapToJoin.put('k2','v2');        
        system.assertEquals(GoogleGeoService.joinMap(mapToJoin,':','|'),'k1:v1|k2:v2');
    }
    private static String buildBasicResponseWithCoordinates(String lat, String lng){
        return   '<?xml version="1.0" encoding="UTF-8"?>' 
            +'<GeocodeResponse>' 
            +'<status>OK</status>'
            +'<result>' 
            +'<geometry>' 
            +'<location>' 
            +'<lat>' + lat + '</lat>' 
            +'<lng>' + lng + '</lng>' 
            +'</location>' 
            +'<location_type>ROOFTOP</location_type>' 
            +'</geometry>'
            +'</result>'
            +'</GeocodeResponse>';
    }
    
    private static void setupGlobalVariables(){
        final Map<String,String> theVariables = new Map<String,String>{
            // ------FORCEXPERTS-------------
            // Removed traces of Simple Geo Service from here
            // ------FORCEXPERTS-------------
            GlobalVariable.KEY_USE_GOOGLE_GEOCODING_API   => 'true'
        };
        for(String key : theVariables.keySet()){
            GlobalVariableTestUtils.ensureExists(new GlobalVariable__c(name=key,Value__c=theVariables.get(key)));
        }
    }
    
    //'http://maps.googleapis.com/maps/api/geocode/xml?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false';
    private static final String TEST_RESPONSE_200 = 
           '<?xml version="1.0" encoding="UTF-8"?>' 
            +'<GeocodeResponse>' 
            +'<status>OK</status>'
            +'<result>' 
            +'<geometry>' 
            +'<location>' 
            +'<lat>37.4220881</lat>' 
            +'<lng>-122.0828878</lng>' 
            +'</location>' 
            +'<location_type>ROOFTOP</location_type>' 
            +'</geometry>'
            +'</result>'
            +'</GeocodeResponse>';
         
}