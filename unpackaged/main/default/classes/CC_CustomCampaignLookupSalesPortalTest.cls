@isTest
private class CC_CustomCampaignLookupSalesPortalTest {
	@isTest
    static void testCustomLookupController(){
        Campaign camp = new Campaign();
        camp.Name = 'Test Campaign';
        camp.Channel__c = 'Direct Sales';
        camp.Sub_Channel__c = 'Event';
        camp.Event_POC__c = UserInfo.getUserId();
        insert camp;
        
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.CC_CustomCampaignLookup')); 
		System.currentPageReference().getParameters().put('channel', 'Marketing');
        System.currentPageReference().getParameters().put('subchannel', 'Digital');
        System.currentPageReference().getParameters().put('lksrch', 'Test Campaign');
        System.currentPageReference().getParameters().put('salesRep', UserInfo.getName());
        CC_CustomCampaignLookupSalesPortal cclS = new CC_CustomCampaignLookupSalesPortal();
        cclS.search();
        Test.stopTest();
    }
}