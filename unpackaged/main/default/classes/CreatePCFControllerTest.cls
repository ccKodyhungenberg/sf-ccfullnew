@isTest
public class CreatePCFControllerTest {
	public static Contact customer ;
    public static PCF_Settings__c settings;
    public static  ContentVersion conVer;
    static void testSetup() {
 CC_TestPcfDataFactory.createSettings();
        //Contact con = [SELECT Id FROM Contact LIMIT 1];
        customer = CC_TestPcfDataFactory.createCustomer(true, true);
        Case approval = CC_testPcfDataFactory.createApproval(customer);
        settings = PCF_Settings__c.getInstance('Default');
        conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = settings.PCF_Signed_Document_Type__c + '.pdf'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = settings.PCF_Signed_Document_Type__c; // Display name of the files
        	conVer.VersionData=EncodingUtil.base64Decode(EncodingUtil.base64Encode(Blob.valueOf(approval.id)));
            insert conVer;
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        ContentDocumentLink cdlink  = new ContentDocumentLink();
        cdlink.ContentDocumentId = conDoc;
        cdlink.LinkedEntityId=customer.Medical_Release_Form__c;
        cdlink.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cdlink.Visibility = 'AllUsers';
        insert cdlink;
}
    
    public class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://example.com/example/test', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        return res;
    }
}
    
    @isTest
    static void testConstructor() {
        testSetup();
        /* CC_TestPcfDataFactory.createSettings();
        //Contact con = [SELECT Id FROM Contact LIMIT 1];
        Contact customer = CC_TestPcfDataFactory.createCustomer(true, true);
        Case approval = CC_testPcfDataFactory.createApproval(customer);
        PCF_Settings__c settings = PCF_Settings__c.getInstance('Default');
        ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = settings.PCF_Signed_Document_Type__c + '.pdf'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = settings.PCF_Signed_Document_Type__c; // Display name of the files
        	conVer.VersionData=EncodingUtil.base64Decode(EncodingUtil.base64Encode(Blob.valueOf(approval.id)));
            insert conVer;
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        ContentDocumentLink cdlink  = new ContentDocumentLink();
        cdlink.ContentDocumentId = conDoc;
        cdlink.LinkedEntityId=customer.Medical_Release_Form__c;
        cdlink.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cdlink.Visibility = 'AllUsers';
        insert cdlink;*/
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setCurrentPage(Page.CreatePCF);
        ApexPages.currentPage().getParameters().put('contactId', customer.Id);
        CreatePCFController o = new CreatePCFController();
        o.objContact=customer;
        
        
        o.backToListView();
        o.backToContactDetail();
        o.updateContactMetadata();
        o.allowRegeneratePcfApproval();
        o.regeneratePcfApproval();
        o.saveMergedPcfAttachment();
        o.createPcfApproval();
        o.setIsPrepared();
        o.sendFax();
        o.cancelFax();
        o.goToEmailForm();
        //o.createEslPackage();
        //o.mergePcfDocument();
        Test.stopTest();
        
        
        
    }
    @isTest
    public static void testmergePcfDocument(){
        testSetup();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setCurrentPage(Page.CreatePCF);
        ApexPages.currentPage().getParameters().put('contactId', customer.Id);
        CreatePCFController o = new CreatePCFController();
        o.objContact=customer;
        o.mergePcfDocument();
        Test.stopTest();
    }
    
    @isTest
    public static void testcreateEslPackage(){
        testSetup();
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setCurrentPage(Page.CreatePCF);
        ApexPages.currentPage().getParameters().put('contactId', customer.Id);
        CreatePCFController o = new CreatePCFController();
        
       // o.objContact=customer;
        o.createEslPackage();
        Test.stopTest();
    }
    
    @isTest
    public static void testcreateEslPackageWithOtherContentType(){
        CC_TestPcfDataFactory.createSettings();
        //Contact con = [SELECT Id FROM Contact LIMIT 1];
        customer = CC_TestPcfDataFactory.createCustomer(true, true);
        Case approval = CC_testPcfDataFactory.createApproval(customer);
        settings = PCF_Settings__c.getInstance('Default');
        conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = settings.PCF_Signed_Document_Type__c + '.jpg'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = settings.PCF_Signed_Document_Type__c; // Display name of the files
        	conVer.VersionData=EncodingUtil.base64Decode(EncodingUtil.base64Encode(Blob.valueOf(approval.id)));
            insert conVer;
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        ContentDocumentLink cdlink  = new ContentDocumentLink();
        cdlink.ContentDocumentId = conDoc;
        cdlink.LinkedEntityId=customer.Medical_Release_Form__c;
        cdlink.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cdlink.Visibility = 'AllUsers';
        insert cdlink;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setCurrentPage(Page.CreatePCF);
        ApexPages.currentPage().getParameters().put('contactId', customer.Id);
        CreatePCFController o = new CreatePCFController();
        
       // o.objContact=customer;
        o.createEslPackage();
        Test.stopTest();
    }
    
    @isTest
    public static void testupdateContactMetadata(){
        testSetup();
        customer.PCF_Unsigned_Form__c=null;
        update customer;
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.setCurrentPage(Page.CreatePCF);
        ApexPages.currentPage().getParameters().put('contactId', customer.Id);
        CreatePCFController o = new CreatePCFController();
        o.updateContactMetadata();
        Test.stopTest();
        
    }
    
}