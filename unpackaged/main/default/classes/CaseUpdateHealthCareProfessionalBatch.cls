global class CaseUpdateHealthCareProfessionalBatch implements Schedulable, Database.Batchable<sObject> {
    
    
    
     global Database.QueryLocator start(Database.BatchableContext BC) {
        String recordTypeId = [SELECT Id,Name from Recordtype WHERE Name = 'PCF Collection' LIMIT 1].Id;
        //String recordTypeId = '0121k000000CjMqAAK';
        String query = 'SELECT Id,Health_Care_Professional__c,Contact.Health_Care_Professional__c FROM Case WHERE Health_Care_Professional__c = NULL AND recordtypeid =  \''+recordTypeId+'\' ';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<Case> caseList) {
        List <Case> updateList = new List <Case>();
        for(Case cs: caseList){
            if((cs.Health_Care_Professional__c == null || cs.Health_Care_Professional__c == '') && cs.Contact.Health_Care_Professional__c != null ){
                cs.Health_Care_Professional__c = cs.Contact.Health_Care_Professional__c;
                updateList.add(cs);
            }
        }
        
        if(updateList.size()>0){
            update updateList;
        }
    }   
     
    global void finish(Database.BatchableContext BC) {
        
  }
    
    global void execute (SchedulableContext SC) {
        Database.executeBatch(new CaseUpdateHealthCareProfessionalBatch());
        //System.schedule('Hourly', '0 0 * * * ?', new CaseUpdateHealthCareProfessionalBatch() );
    }
}