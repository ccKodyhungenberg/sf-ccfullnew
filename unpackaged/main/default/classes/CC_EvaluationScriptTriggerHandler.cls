public without sharing class CC_EvaluationScriptTriggerHandler {
    
    public static void beforeInsert(List<EvaluationScript__c > lstOfNewEvaluationScript){
        createAndUpdateOnlyForEvaluationTeamGroup(lstOfNewEvaluationScript);
    }
    
    public static void beforeUpdate(List<EvaluationScript__c > lstOfNewEvaluationScript){
        createAndUpdateOnlyForEvaluationTeamGroup(lstOfNewEvaluationScript);
    }
    
    private static void createAndUpdateOnlyForEvaluationTeamGroup(List<EvaluationScript__c > lstOfNewEvaluationScript){
        List<GroupMember> loggedInGroupMemberData = [SELECT Id FROM GroupMember WHERE UserOrGroupId =: UserInfo.getUserId() AND Group.DeveloperName='EvaluationTeam' AND Group.Type='Regular'];
        for(EvaluationScript__c evaluationScript : lstOfNewEvaluationScript){
            if(loggedInGroupMemberData == null || loggedInGroupMemberData.isEmpty()){
                evaluationScript.addError('You must be member of the "Evaluation Team" group to create/change Evaluation Scripts!');
            }
        }
    }
}