/**
 * Please consider this class a template.  You will need to customize
 * this class according to the global variables you might require in
 * your application.  Also, you might consider creating multiple
 * classes similar to this broken down by global configuration variables that make
 * sense to be packaged together.
 *
 * First (1), define your global configuration variables as member variables of this class.
 * For example, if you're defining a twitter username as a global variable, then
 * the variable might be:
 *
 * public String twitterUsername {get; private set;}
 *
 * Next (2), define the key for your global variable.  This is the value which
 * must be entered as the name of the "Global Variable" custom settings record
 * defined at:
 *
 * Setup | App Setup | Develop | Custom Settings | Global Variable | Manage
 * (NOTE: if you haven't deployed that Custom Settings object yet, then obviously
 * the above won't be available.)
 *
 * The key can be the same value as member variable but that isn't required.
 * Using same example as above, the variable key might be:
 *
 * public static final String KEY_TWITTER_USERNAME = 'twitter.username';
 *
 * Third (3), define how the variable will be loaded.  Included are two
 * methods which read Strings and Integers.  If you have a different data type,
 * definie a different "retrieve" method.
 *
 * And last (4), consider putting in some sanity checking.  For example, if
 * you have a timeout variable defined as integer, perhaps it's a good idea
 * to make sure it's > 0 but less than 60000 milliseconds.
 *
 * Once your configuration variables are defined, you should access the configuration variables in your
 * code via the following:
 *
 * GlobalConfigurationVariables.getInstance().twitterUsername
 *
 * And I suggest you always call getInstance and you never store a reference of
 * the GlobalConfigurationVariables object.  That way, you can be assured that all transactions
 * will get the latest copy of the configuration variables AND only do it once.
 *
 * @author Richard Vanhook
 */
global class GlobalConfigurationVariables {

    //==================================================
    // (1) Define your configuration variables
    //==================================================
    //global String exampleString {get; private set;}
    //global Integer exampleInteger {get; private set;}
    global String welcomeCommitteeMembers {get; private set;}
    global String welcomeCommitteeMemberUsernames {get; private set;}
    global String integrationUser_Eloqua {get; private set;}
    global String integrationUsername_Eloqua {get; private set;}
    global String integrationUser_Boomi {get; private set;}
    global String integrationUsername_Boomi {get; private set;}
    global String integrationUsers {get; private set;}
    global String companyEmailRegex {get; private set;}
    global String defaultContactOwner {get; private set;}
    global String defaultContactOwnerUsername {get; private set;}
    global String adminProfile {get; private set;}

    //==================================================
    // (2) Define variable keys
    //==================================================
    //global static final String KEY_STRING_EXAMPLE = 'ExampleString';
    //global static final String KEY_INTEGER_EXAMPLE = 'ExampleInteger';
    global static final String KEY_WELCOMECOMMITTEE_MEMBERS = 'Welcome Committee Members';
    global static final String KEY_WELCOMECOMMITTEE_MEMBERS_UN = 'Welcome Committee Members (un)';
    global static final String KEY_ELOQUA_INTEGRATION_USER = 'Eloqua Integration User';
    global static final String KEY_ELOQUA_INTEGRATION_USER_UN = 'Eloqua Integration User (un)';
    global static final String KEY_BOOMI_INTEGRATION_USER = 'Boomi Integration User';
    global static final String KEY_BOOMI_INTEGRATION_USER_UN = 'Boomi Integration User (un)';
    global static final String KEY_COMPANY_EMAIL_REGEX = 'Company Email RegEx'; 
    global static final String KEY_DEFAULT_CONTACT_OWNER = 'Default Contact Owner'; 
    global static final String KEY_DEFAULT_CONTACT_OWNER_UN = 'Default Contact Owner (un)'; 
    global static final String KEY_ADMIN_PROFILE = 'Administrator Profile'; 
    
    private GlobalConfigurationVariables(){
        final Map<String,GlobalConfigurationVariable__c> all = GlobalConfigurationVariable__c.getAll();
        //==================================================
        // (3) Load configuration variables
        //==================================================
        //exampleString = retrieveString(KEY_STRING_EXAMPLE, all);
        //exampleInteger = retrieveInteger(KEY_INTEGER_EXAMPLE, all);
        welcomeCommitteeMembers = retrieveString(KEY_WELCOMECOMMITTEE_MEMBERS , all);
        welcomeCommitteeMemberUsernames = retrieveString(KEY_WELCOMECOMMITTEE_MEMBERS_UN , all);
        integrationUser_Eloqua = retrieveString(KEY_ELOQUA_INTEGRATION_USER, all);
        integrationUsername_Eloqua = retrieveString(KEY_ELOQUA_INTEGRATION_USER_UN, all);
        integrationUser_Boomi = retrieveString(KEY_BOOMI_INTEGRATION_USER , all);
        integrationUsername_Boomi = retrieveString(KEY_BOOMI_INTEGRATION_USER_UN , all);
        integrationUsers  = retrieveString(KEY_ELOQUA_INTEGRATION_USER, all) + ',' + retrieveString(KEY_BOOMI_INTEGRATION_USER , all);
        companyEmailRegex = retrieveString(KEY_COMPANY_EMAIL_REGEX , all);
        defaultContactOwner= retrieveString(KEY_DEFAULT_CONTACT_OWNER, all);
        defaultContactOwnerUsername= retrieveString(KEY_DEFAULT_CONTACT_OWNER_UN, all);
        adminProfile = retrieveString(KEY_ADMIN_PROFILE, all);
                        
        //==================================================
        // (4) Do some sanity checking
        //==================================================
        //if(exampleInteger == null || exampleInteger < 0 || exampleInteger > 60000){
        //    exampleInteger = 1000;
        //}
    }

    //==================================================
    // HELPER METHODS
    //==================================================
    global static GlobalConfigurationVariables instance;

    global static GlobalConfigurationVariables getInstance(){
        if(instance == null){
            instance = new GlobalConfigurationVariables();
        }
        return instance;
    }

    private static Integer retrieveInteger(String key, Map<String,GlobalConfigurationVariable__c> all){
        Integer returnValue = null;
        if(all != null && !isBlank(key) && all.get(key) != null){
            try{
                if(all.get(key).value__c != null){
                    returnValue = Integer.valueOf(all.get(key).value__c);
                }
            }catch(System.TypeException e){}
        }
        return returnValue;
    }

    private static String retrieveString(String key, Map<String,GlobalConfigurationVariable__c> all){
        String returnValue = null;
        if(all != null && !isBlank(key) && all.get(key) != null){
            returnValue = all.get(key).value__c;
        }
        return returnValue;
    }

    private static boolean isBlank(String str) {
        return str == null || str.trim() == null || str.trim().length() == 0;
    }

}