@isTest
public class CC_EvaluationScriptTriggerHandlerTest {
    

    static testMethod void testAddEvaluationScript() {

        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        String userName = String.valueOf(Datetime.Now().getTime());

        User u = new User(
            FirstName         = 'Test',
            LastName          = 'User',
            Email             = 'test@user.com',
            Alias             = 'tuser',
            Username          = userName + 'test@user.com',
            LocaleSidKey      = 'en_US',
            TimeZoneSidKey    = 'GMT',
            LanguageLocaleKey = 'en_US',
            EmailEncodingKey  = 'UTF-8',
            ProfileId         = p.Id,
            city              = 'BRANDON',
            state             = 'SD',
            Street            = '809 HEATHERWOOD DR',
            country           = 'USA',
            postalCode        = '57005'
        );
        insert u;
        Group g = [SELECT id  FROM Group WHERE DeveloperName = 'EvaluationTeam'];
        

        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = u.Id
        );
        insert gm;
    
        System.runAs(u) {
        //Modified below 1 line by NSINGH - S-584772
        EvaluationScript__c evas= new EvaluationScript__c(
                Script__c='test script text',
                Title__c='test script title', 
                CallType__c='Answering Machine',
                TotalWords__c=3
        );
        insert evas;
        evas.Script__c = 'hellp';
        update evas;
        }
    }

}