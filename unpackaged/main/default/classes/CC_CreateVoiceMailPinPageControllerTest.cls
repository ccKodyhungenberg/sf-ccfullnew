@isTest
public class CC_CreateVoiceMailPinPageControllerTest {
    private class Mock implements HttpCalloutMock {
         private Integer statusCode = 200;
        public HTTPResponse respond(HTTPRequest req) {
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if(statusCode != 400)
            res.setBody('{"Result": "true","message":"test message","data":"test data"}');
            
            res.setStatusCode(statusCode);
            return res;
        }
        public void setStatusCode(Integer statusCode) {
             this.statusCode = statusCode;
         }
    }
    public static List<Contact> conList;
    public static void createData(){
        conList=new List<Contact>();
        for(Integer i=0;i<20;i++){
            Contact con=new Contact();
            con.LastName='test'+i;
            con.cc_voice_number__c='864'+i;
            con.CB_userID__c='test user id'+i;
            con.MailingPostalCode='302022'+i;
            con.Email='abc.xyz@test.com';
            conList.add(con);
        }
        insert conList;
    }
    @isTest   
    public static void testSavePin(){
        createData();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock());
        //ApexPages.currentPage().getParameters().put('voiceMailNum','1234');
        ApexPages.currentPage().getParameters().put('id',conList[0].id);
        CC_CreateVoiceMailPinPageController obj_savepin=new CC_CreateVoiceMailPinPageController();
        obj_savepin.voiceMailNum='1234';
        obj_savepin.savePin();
        Test.stopTest();
        
    }
    
   @isTest
    public static void testSavePinException(){
        createData();
      Test.startTest();
        Mock mock = new Mock();
        mock.setStatusCode(400);
        Test.setMock(HttpCalloutMock.class, mock);
        try {
            ApexPages.currentPage().getParameters().put('id',conList[0].id);
           
        	 CC_CreateVoiceMailPinPageController obj_savepinEx=new CC_CreateVoiceMailPinPageController();
            obj_savepinEx.voiceMailNum='1234';
        	 obj_savepinEx.savePin();
        } catch(AuraHandledException e) {
            System.assert(true);
        }  
    }
    
	@isTest   
    public static void testSaveWrongPin(){
        createData();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock());
        //ApexPages.currentPage().getParameters().put('voiceMailNum','1234');
        ApexPages.currentPage().getParameters().put('id',conList[0].id);
        CC_CreateVoiceMailPinPageController obj_savepin=new CC_CreateVoiceMailPinPageController();
        obj_savepin.voiceMailNum='124';
        obj_savepin.savePin();
        Test.stopTest();
        
    }

}