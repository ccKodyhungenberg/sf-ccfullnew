@isTest
public class CC_AddressFlowControllerTest {
    public static List<Contact> conList=new List<Contact>();
    static void createTestData()
    {
        
        List<Account> accList = CC_TestUtility.createAccountRecords('testAccount', 1, true);
        Addresses__c addRecord = new Addresses__c(Account__c = accList[0].id, Street__c='Brandon', City__c='SA',IsValidated__c=true);
        insert addRecord;
        conList = CC_TestUtility.createContactRecords('lastname', 1, true);
        
    }
    
    @isTest
    static void testAllMethods() {
        createTestData();
        Account acc = [SELECT Name FROM Account LIMIT 1];
        Addresses__c add = [SELECT Name, Street__c, country__c FROM Addresses__c LIMIT 1];
        System.debug('address:'+add);
        Integer flag = 0;
        Test.setCurrentPageReference(new PageReference('Page.CC_AddressFlow'));
        
        if(flag==0)
        {
            System.currentPageReference().getParameters().put('addressId', '');
            System.currentPageReference().getParameters().put('retURL', '');
            //System.currentPageReference().getParameters().put('accountId', acc.Id);
            System.currentPageReference().getParameters().put('iSuggestion_index', '0');
            System.currentPageReference().getParameters().put('radioValue', 'Shipping');
            System.currentPageReference().getParameters().put('fromComponent', 'testComponent');
            System.currentPageReference().getParameters().put('is911Address', 'true');
            
            ApexPages.StandardController sc = new ApexPages.StandardController(add);
            CC_AddressFlowController addFlowCont = new CC_AddressFlowController(sc);
            addFlowCont.fillAddressToUpdate();
        }
        
        List<Asset> assetList = CC_TestUtility.createAssetRecords('test asset', 1, false,acc.id);
        assetList[0].identifier__c = '123455';
        //assetList[0].accountId = acc.id;
        insert assetList;
        
        System.currentPageReference().getParameters().put('accountId', acc.id);
        System.currentPageReference().getParameters().put('addressId', add.id);
        //System.currentPageReference().getParameters().put('retURL', '/'+acc.id);
        System.currentPageReference().getParameters().put('retURL', assetList[0].id);
        System.currentPageReference().getParameters().put('is911Address', 'true');
        System.currentPageReference().getParameters().put('radioValue', 'Billing');
        
        //create an instance of wrapper class
        CC_EventPortalController.clSuggestion sugClsWrapper = new CC_EventPortalController.clSuggestion();
        //sugClsWrapper.AddressIsHomeOrWork = 'Work';
        sugClsWrapper.City = 'ROCKLIN';
        sugClsWrapper.State = 'CA';
        sugClsWrapper.Street = '595 MENLO DR';
        sugClsWrapper.Zip = '95765';
        sugClsWrapper.APT = '';
        
        System.debug(sugClsWrapper);
        
        CC_EventPortalController.clSuggestion_Item sugItemClsWrapper = new CC_EventPortalController.clSuggestion_Item();
        sugItemClsWrapper.index = 1; 
        sugItemClsWrapper.sAddress = 'BRANDON';
        sugItemClsWrapper.oSuggestion = sugClsWrapper;
        
        
        Test.startTest();
        ApexPages.StandardController sc1 = new ApexPages.StandardController(add);
        CC_AddressFlowController addFlowCont = new CC_AddressFlowController(sc1);
        
        
        addFlowCont.lSuggestion_Item = new List<CC_EventPortalController.clSuggestion_Item>();
        addFlowCont.lSuggestion_Item.add(sugItemClsWrapper);
        
        List<SelectOption> options = addFlowCont.getItems();
        System.assertEquals(2, options.size());
        CC_AddressFlowController.addressWrapper addWrapperCls = new CC_AddressFlowController.addressWrapper(add, true);
        
       
        addFlowCont.radioResponse = '';
        addFlowCont.fillAddressOnPage();
        addFlowCont.pickAddress();
          
		Test.setMock(HttpCalloutMock.class, new MockCC_AddressFlowController(add.Street__c));
        addFlowCont.validateAddress(); 
        Test.setMock(HttpCalloutMock.class, new MockCC_AddressFlowController('Brandon123'));
        addFlowCont.validateAddress();        

        addFlowCont.validateRequired();
        addFlowCont.fillAddressToUpdate();
        System.currentPageReference().getParameters().put('is911Address', 'true');
        addFlowCont.is911Address = true;
        //addFlowCont.updateAddress();
       
        
        Test.stopTest();
    }
    
    
    @isTest
    static void testAllMethods2() {
        createTestData();
        Account acc = [SELECT Name FROM Account LIMIT 1];
        Addresses__c add = [SELECT Name, Street__c, country__c FROM Addresses__c LIMIT 1];
        System.debug('address:'+add);
        Integer flag = 0;
        Test.setCurrentPageReference(new PageReference('Page.CC_AddressFlowController'));
        
        if(flag==0)
        {
            System.currentPageReference().getParameters().put('addressId', '');
            System.currentPageReference().getParameters().put('retURL', '');
            System.currentPageReference().getParameters().put('accountId', acc.Id);
            System.currentPageReference().getParameters().put('iSuggestion_index', '0');
            System.currentPageReference().getParameters().put('radioValue', 'Shipping');
            System.currentPageReference().getParameters().put('fromComponent', 'testComponent');
            ApexPages.StandardController sc = new ApexPages.StandardController(add);
            CC_AddressFlowController addFlowCont = new CC_AddressFlowController(sc);
            addFlowCont.fillAddressToUpdate();
        }
        
        System.currentPageReference().getParameters().put('accountId', acc.id);
        System.currentPageReference().getParameters().put('addressId', add.id);
        System.currentPageReference().getParameters().put('retURL', '/'+acc.id);
        System.currentPageReference().getParameters().put('radioValue', 'Billing');
        
        //create an instance of wrapper class
        CC_EventPortalController.clSuggestion sugClsWrapper = new CC_EventPortalController.clSuggestion();
        //sugClsWrapper.AddressIsHomeOrWork = 'Work';
        sugClsWrapper.City = 'ROCKLIN';
        sugClsWrapper.State = 'CA';
        sugClsWrapper.Street = '595 MENLO DR';
        sugClsWrapper.Zip = '95765';
        sugClsWrapper.APT = '';
        
        System.debug(sugClsWrapper);
        
        CC_EventPortalController.clSuggestion_Item sugItemClsWrapper = new CC_EventPortalController.clSuggestion_Item();
        sugItemClsWrapper.index = 1; 
        sugItemClsWrapper.sAddress = 'BRANDON';
        sugItemClsWrapper.oSuggestion = sugClsWrapper;
        
        
        Test.startTest();
        ApexPages.StandardController sc1 = new ApexPages.StandardController(add);
        CC_AddressFlowController addFlowCont = new CC_AddressFlowController(sc1);
        
        
        addFlowCont.lSuggestion_Item = new List<CC_EventPortalController.clSuggestion_Item>();
        addFlowCont.lSuggestion_Item.add(sugItemClsWrapper);
        
        List<SelectOption> options = addFlowCont.getItems();
        System.assertEquals(2, options.size());
        CC_AddressFlowController.addressWrapper addWrapperCls = new CC_AddressFlowController.addressWrapper(add, true);
        
       
        addFlowCont.radioResponse = '';
        addFlowCont.fillAddressOnPage();
        addFlowCont.pickAddress();
          
        Test.setMock(HttpCalloutMock.class, new MockCC_AddressFlowController('Brandon123'));
        addFlowCont.validateAddress();        

        addFlowCont.validateRequired();
        addFlowCont.fillAddressToUpdate();
        addFlowCont.updateAddress();
         addFlowCont.overrideRequest();
        Test.stopTest();
    }
    
    
    @isTest
    static void testAllMethods3() {
        List<Account> accList = CC_TestUtility.createAccountRecords('testAccount', 1, true);
        Addresses__c addRecord = new Addresses__c( Street__c='Brandon', City__c='SA',IsValidated__c=true);
        insert addRecord;
        conList = CC_TestUtility.createContactRecords('lastname', 1, false);
        conlist[0].CB_userID__c = 'tdsdszz';
        insert conList;
        Account acc = [SELECT Name FROM Account LIMIT 1];
        Addresses__c add = [SELECT Name, Street__c, country__c FROM Addresses__c LIMIT 1];
        System.debug('address:'+add);
        Integer flag = 0;
        Test.setCurrentPageReference(new PageReference('Page.CC_AddressFlowController'));
        
        if(flag==0)
        {
            System.currentPageReference().getParameters().put('addressId', '');
            System.currentPageReference().getParameters().put('retURL', '');
            System.currentPageReference().getParameters().put('accountId', acc.Id);
            System.currentPageReference().getParameters().put('iSuggestion_index', '0');
            System.currentPageReference().getParameters().put('radioValue', 'Shipping');
            System.currentPageReference().getParameters().put('fromComponent', 'testComponent');
            ApexPages.StandardController sc = new ApexPages.StandardController(add);
            CC_AddressFlowController addFlowCont = new CC_AddressFlowController(sc);
            addFlowCont.fillAddressToUpdate();
        }
        
        System.currentPageReference().getParameters().put('accountId', acc.id);
        System.currentPageReference().getParameters().put('addressId', add.id);
        System.currentPageReference().getParameters().put('retURL', conList.get(0).id);
        System.currentPageReference().getParameters().put('radioValue', 'Billing');
        
        //create an instance of wrapper class
        CC_EventPortalController.clSuggestion sugClsWrapper = new CC_EventPortalController.clSuggestion();
        //sugClsWrapper.AddressIsHomeOrWork = 'Work';
        sugClsWrapper.City = 'ROCKLIN';
        sugClsWrapper.State = 'CA';
        sugClsWrapper.Street = '595 MENLO DR';
        sugClsWrapper.Zip = '95765';
        sugClsWrapper.APT = '';
        
        System.debug(sugClsWrapper);
        
        CC_EventPortalController.clSuggestion_Item sugItemClsWrapper = new CC_EventPortalController.clSuggestion_Item();
        sugItemClsWrapper.index = 1; 
        sugItemClsWrapper.sAddress = 'BRANDON';
        sugItemClsWrapper.oSuggestion = sugClsWrapper;
        
        
        Test.startTest();
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(add);
        CC_AddressFlowController addFlowCont = new CC_AddressFlowController(sc1);
        
        
        addFlowCont.lSuggestion_Item = new List<CC_EventPortalController.clSuggestion_Item>();
        addFlowCont.lSuggestion_Item.add(sugItemClsWrapper);
        
        List<SelectOption> options = addFlowCont.getItems();
        System.assertEquals(2, options.size());
        CC_AddressFlowController.addressWrapper addWrapperCls = new CC_AddressFlowController.addressWrapper(add, true);
        
       
        addFlowCont.radioResponse = '';
        addFlowCont.fillAddressOnPage();
        addFlowCont.pickAddress();
          
        Test.setMock(HttpCalloutMock.class, new MockCC_AddressFlowController('Brandon123'));
        addFlowCont.validateAddress();        

        addFlowCont.validateRequired();
        addFlowCont.fillAddressToUpdate();
        addFlowCont.updateAddress();
         addFlowCont.overrideRequest();
        Test.stopTest();
    }
    
    
    
    
}