@isTest
public class CC_AdminPcfPageController_Test {
    @isTest
    public static void testAllMethods(){
        
        AsyncApexJob asyncjob = new AsyncApexJob();
        //insert asyncjob;
       // ApexPages.currentPage().getParameters().put('id',conList[0].id);
       CC_AdminPcfPageController.PcfStatus pcf = new CC_AdminPcfPageController.PcfStatus();
        insert new PCF_Settings__c(
            Name='Default',
            oAuth_Client_ID__c = '123',
            oAuth_Client_Secret__c = 'abc',
            oAuth_Password__c = 'abc',
            oAuth_Username__c = 'abc'
            
        );
        pcf.jobId=asyncjob.id;
        pcf.jobInfo=asyncjob;
        Test.startTest();
        CC_AdminPcfPageController adminpg = new CC_AdminPcfPageController();
        adminpg.getStatus();
        adminpg.runSilanisJob();
        adminpg.runSfaxJob();
        adminpg.runCongaAuthJob();
        Test.stopTest();
        
    }

}