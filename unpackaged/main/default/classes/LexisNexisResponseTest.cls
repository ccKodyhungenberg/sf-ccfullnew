@istest
public class LexisNexisResponseTest {
    
    @isTest
    static void testWrapper(){
        
        
        LexisNexisResponse.VerifiedElementSummary verSummary = new LexisNexisResponse.VerifiedElementSummary();
        verSummary.DOBMatchLevel = '8';
        verSummary.DOB = true;
        
        LexisNexisResponse.ValidElementSummary elemSummary = new LexisNexisResponse.ValidElementSummary();
        elemSummary.SSNValid = true;
        elemSummary.SSNDeceased = false;
        elemSummary.PassportValid=true;
        elemSummary.SSNFoundForLexID=true;
        elemSummary.AddressCMRA=true;
        elemSummary.AddressPOBox=true;
        
        
        LexisNexisResponse.RiskIndicator rsInicator = new LexisNexisResponse.RiskIndicator();
        rsInicator.RiskCode='25';
        rsInicator.Sequence=25;
        rsInicator.Description='Not Valid';
        
        LexisNexisResponse.RiskIndicators rsInticators = new LexisNexisResponse.RiskIndicators();
        rsInticators.RiskIndicator = new List<LexisNexisResponse.RiskIndicator>{rsInicator};
            
        LexisNexisResponse.NameAddressPhone namePhone = new LexisNexisResponse.NameAddressPhone();
        namePhone.Summary='Str';
        namePhone.Type='Str';
        
        LexisNexisResponse.Name name= new LexisNexisResponse.Name();
        name.Last='test';
        name.First='test';
        name.Full='test';
        LexisNexisResponse.DOB dob = new LexisNexisResponse.DOB();
        dob.Day=12;
        dob.Month=12;
        dob.Year=1993;
        LexisNexisResponse.InputEcho inputEcho = new LexisNexisResponse.InputEcho();
        inputEcho.Age=12;
        inputEcho.SSN='123';
        inputEcho.Name = name;
        inputEcho.DOB = dob;
        LexisNexisResponse.CustomComprehensiveVerification custCompVerification = new LexisNexisResponse.CustomComprehensiveVerification();
        custCompVerification.ComprehensiveVerificationIndex=1;
        LexisNexisResponse.ComprehensiveVerification compVerification = new LexisNexisResponse.ComprehensiveVerification();
        compVerification.ComprehensiveVerificationIndex = 2;
        compVerification.RiskIndicators = rsInticators;
        
        LexisNexisResponse.Result result = new LexisNexisResponse.Result();
        result.VerifiedSSN='123';
        result.NameAddressSSNSummary=34;
        result.InstantIDVersion='123';
        result.UniqueId='123';
        result.EmergingId=false;
        result.VerifiedElementSummary = verSummary;
        result.ComprehensiveVerification =compVerification;
        result.ValidElementSummary =elemSummary;
        result.CustomComprehensiveVerification =custCompVerification;
        result.NameAddressPhone =namePhone;
        result.InputEcho =InputEcho;
       
        LexisNexisResponse.response response = new LexisNexisResponse.response();
        LexisNexisResponse.Header header= new LexisNexisResponse.Header();
        header.Status = 200;
        response.Result = result;
        response.Header = header;
        
        LexisNexisResponse.FlexIDResponseEx flexRensp = new LexisNexisResponse.FlexIDResponseEx();
        flexRensp.response = response;
        
        LexisNexisResponse resp = new LexisNexisResponse();
        resp.FlexIDResponseEx= flexRensp;
    }

}