@isTest
global class MockCC_SalesPortalController implements HttpCalloutMock{
    String counter = '';
    global MockCC_SalesPortalController(String counter){
        this.counter = counter;
    } 
    global HTTPResponse respond(HTTPRequest req) {
        if(counter == '1'){
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            res.setBody('{"code": 200,"data":{"apt": "APT 125","city": "Grand Rapids","country": "","e911LocationID": null,"primary": false,"state": "MI","street": "100 50TH ST SW","types": [],"zip": "49548","id": null,"type": "address"},"message": "","result": true}');
            res.setStatusCode(200);
            return res;
            
        }
        else{
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
            res.setBody('{"code": 200,"data": {"id": "7715b6aa360b68b5"},"message": "Profile created","result": true}');
            res.setStatusCode(200);
            return res;
        }    
    }
}