@isTest
private class CalendarAnythingVFUpdatedControllerTest {
  @isTest
    static void testCAUpdatedController(){ 
        
        

        Profile oP = [SELECT ID from Profile limit 1];  
        User oU = new User(firstname = 'user1', lastname='def', profileid = oP.id,
            Username = 'aaa@bbb.com.zxcvbnfddsasdsfdfdfdfffd', 
            Email = 'test@test.com', 
            Alias = 'fdfdfd', 
            CommunityNickname = 'aaa',
            TimeZoneSidKey = 'Pacific/Kiritimati',
            LocaleSidKey = 'en_AU', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US'
            

        );
        insert oU;
        
        Account oA = new Account (name = 'Acc');
        insert oA;
        Contact oC = new Contact(
            FirstName = 'abc', 
            lastName = 'def', 
            accountid= oA.id, 
            related_to_user__c = oU.id
            
        ); 
        insert oC;
        CalendarAnythingVFUpdatedController oController = new CalendarAnythingVFUpdatedController();
        system.assert(oController.sContactID == '', 'Wrong Contact');
        system.assert(oController.sUserId == '', 'Wrong User');
        
        ApexPages.CurrentPage().getParameters().put('pv1', '' + oC.id);
        oController = new CalendarAnythingVFUpdatedController();
        system.assert(oController.sContactID == oC.id, 'Wrong Contact');
        system.assert(oController.sUserId == oU.id, 'Wrong User');
    }
}