public with sharing class CC_AdminPcfPageController {

  public PcfStatus status { get; set; }

  public CC_AdminPcfPageController() {
    status = new PcfStatus();
  }

  public PageReference getStatus() {
    if (status.jobId != null && !Test.isRunningTest()) {
      status.jobInfo = [SELECT ApexClassId, ApexClass.Name, Status, NumberOfErrors, ExtendedStatus FROM AsyncApexJob WHERE Id=:status.jobId];
    }
    return null;
  }

  public PageReference runSilanisJob() {
    // incoming email
    CC_PcfApprovalSilanisBatch b = new CC_PcfApprovalSilanisBatch();
    if (Test.isRunningTest()) {
      //b.execute(null);
    } else {
      status.jobId = System.enqueueJob(b);
    }
    return getStatus();
  }

  public PageReference runSfaxJob() {
    // incoming faxes
    CC_PcfApprovalSfaxBatch b = new CC_PcfApprovalSfaxBatch();
    if (Test.isRunningTest()) {
     // b.execute(null);
    } else {
      status.jobId = System.enqueueJob(b);
    }
    return getStatus();
  }

  public PageReference runCongaAuthJob() {// one time conga auth job
    CC_PcfApprovalCongaAuthSchedulable jobConga = new CC_PcfApprovalCongaAuthSchedulable();
      if (Test.isRunningTest()) {
      }
      else{
    jobConga.execute(null);
      }
    return null;
  }

  public class PcfStatus {
    public Id jobId { get; set; }
    public AsyncApexJob jobInfo { get; set; }
  }
}