public class CC_FollowUpSurveyCase {
	@AuraEnabled
    public static Case getCase(String CaseId) {
        return [
       	    SELECT Id, CaseNumber,IsClosed,Service_Request_Status__c,Subject,Follow_up_survey_completed__c,SR_Dossier_ContactId__c,Work_Order__c,SR_Id__c,SR_Completed_By__c,SR_Start_Date__c
            FROM Case
            WHERE Id = :CaseId
        ];
    }
}