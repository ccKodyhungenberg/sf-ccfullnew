// MKLICH S-628700 added update to Number_of_Active_Assets__c
public class ContactTriggerHandler {

    public static void updateCombinedUsageOnAccount( List<Contact> oldList ) {
        
        Set<Id> accountIds = new Set<Id>();
        Map<Id,List<Contact>> mapAccountIdToListContacts = new Map<Id,List<Contact>>();
        List<Account> listAccountToUpdate = new List<Account>();
        Decimal sumTotalLifetimeBillabeMins = 0 ;
        
        for( Contact con : oldList ) {
            if( con.AccountId != null ) {
                accountIds.add(con.AccountId);
            }
        }
        
        for( Contact con : [SELECT Id, Total_Lifetime_Billable_Mins__c, AccountId FROM Contact WHERE AccountId IN :accountIds ]) {
            
            if( !mapAccountIdToListContacts.containsKey( con.AccountId )) {
                mapAccountIdToListContacts.put(con.AccountId, new List<Contact>());
            } 
            mapAccountIdToListContacts.get(con.AccountId).add(con);
        }
        
        for( Account acc : [ SELECT Id,Combined_usage_for_household__c FROM Account WHERE Id IN :accountIds ]) {
            sumTotalLifetimeBillabeMins = 0;
            if( mapAccountIdToListContacts.containsKey(acc.Id) && mapAccountIdToListContacts.get(acc.Id).size() > 0 ) {
                for( Contact con : mapAccountIdToListContacts.get(acc.Id) ) {
                    if( con.Total_Lifetime_Billable_Mins__c != null ) {
                        sumTotalLifetimeBillabeMins += con.Total_Lifetime_Billable_Mins__c; 
                    }
                }
                acc.Combined_usage_for_household__c = sumTotalLifetimeBillabeMins;
                listAccountToUpdate.add(acc);
            }
        }
        if( listAccountToUpdate.size() > 0 ) {
            //system.assert(false,listAccountToUpdate);
            update listAccountToUpdate;
        }
        
    }
    
    public static void updateAddressOnCCWS( List<Contact> newList, Map<Id,Contact> oldMap ) {
        
        for( Contact con : newList ) {
            if( !String.isBlank( con.CB_userID__c ) ) {
                if( (!String.isBlank(con.MailingStreet) && (oldMap.get(con.Id).MailingStreet != con.MailingStreet)) ||
                   (!String.isBlank(con.MailingCity) && (oldMap.get(con.Id).MailingCity != con.MailingCity)) ||
                   (!String.isBlank(con.MailingPostalCode) && (oldMap.get(con.Id).MailingPostalCode != con.MailingPostalCode)) ||
                   (!String.isBlank(con.MailingState) && (oldMap.get(con.Id).MailingState != con.MailingState)) ||
                   (!String.isBlank(con.MailingCountry) && (oldMap.get(con.Id).MailingCountry != con.MailingCountry))
                   
                  ) {
                      con.validForAPI__c = TRUE;
                  }     
            }

        
        }
    }
    public static void activeInactiveOnCCWS( List<Contact> newList, Map<Id,Contact> oldMap ) {
        
        for( Contact con : newList ) {
            if( !String.isBlank( con.CB_userID__c ) ) {
                if( ( (oldMap.get(con.Id).Deactivated__c != con.Deactivated__c)) ) {
                    //con.Deactivated__c = TRUE;
                    con.Update_To_CCWS__c = true;
                }
            }
        }
    }
    
    
    
    //MOVED FROM PROD START--------------------
    public static void tr_Before_Insert(List<Contact> triggerNew, List<Contact> triggerOld, Map<Id,Contact> triggerNewMap, Map<Id,Contact> triggerOldMap) {
        Id TMProfileID = [SELECT id FROM profile WHERE name = 'CC Territory Manager'].id;
        ID RMProfileID = [SELECT id FROM profile WHERE name = 'CC Regional Sales Manager'].id;
        Boolean isTMRMUser = false;
        if(UserInfo.getProfileId() ==RMProfileID || UserInfo.getProfileId() ==TMProfileID){
            isTMRMUser = true;
        }
        Map<Id,Account> mapContactIdToAccount = new Map<Id,Account>();
        Integer i = 0;
        Id customerContactId= Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        for( Contact con : triggerNew ) {
            
            if(isTMRMUser && !con.Is_Lead_Converted__c && !con.Created_From_Portal__c && !String.isBlank( con.RecordTypeId ) && con.RecordTypeId.equals(customerContactId)){
                con.addError('TM/RM Should not be able create customer contact record type, allowed from portal only.');
            }
            else{
                if( String.isBlank( con.RecordTypeId ) ) {
                con.RecordTypeId = customerContactId;
            }
            string key = '';
            if(i >=0 && i < 10 ) {
                key = '000'+i;
            }
            else if(i >= 10 && i < 100 ) {
                key = '00'+i;
            }
            else if(i >= 100 && i < 1000 ) {
                key = '0'+i;
            }
            con.Id = '0031I00000M'+key;
            i++;
            if( String.isBlank(con.AccountId) ) {
                Account oNewAccount = New Account(name = con.LastName + ' Household', OwnerID = con.OwnerID);
                mapContactIdToAccount.put(con.id,oNewAccount);
            }
        }
    }
        if( mapContactIdToAccount.values().size() > 0 ) {
            try{
                insert mapContactIdToAccount.values();
            }
            catch(Exception e){
                
            }
        }
            
        for( Contact con : triggerNew ) {
            if( String.isBlank(con.AccountId) ) {
                if( mapContactIdToAccount.containsKey(con.id) && mapContactIdToAccount.get(con.id).Id != null ) {
                    con.AccountId = mapContactIdToAccount.get(con.id).Id;
                    
                }
            }
            con.id = null;
        }
        
        
        
        //if ( UserInfo.getUserId() == CC_GlobalUtility.idBoomi || UserInfo.getProfileId().contains(Label.Administrator_Profile) ) {
        // Assemble a list of existing Potential Customer contacts based on email            
        Set <String> emailAddresses = New Set<String>();
        Set <String> setCompanyToken= new Set<String>();
        
        for (Contact oNewContact : triggerNew) {
            if(oNewContact.MailingCountry==null){
                oNewContact.MailingCountry='USA';//default all to USA
            }
            
            if(oNewContact.email!=null && oNewContact.email!=''){
                emailAddresses.add(oNewContact.email); 
            }
            
            //Enterprise Customer Support
            if(oNewContact.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId()&& oNewContact.Type__c=='Enterprise Customer') {
                
                if(oNewContact.Title!=null && oNewContact.Title!='') {
                    setCompanyToken.add( (String)oNewContact.Title );
                }
            }
        }
        Map <Id,Contact> existingContacts = New Map<Id,Contact>([
            SELECT Id, Email, RecordTypeId, FirstName, LastName, Description, Type__c, ContactID__c, CB_userID__c FROM contact
            //       WHERE RecordTypeId =: GlobalFunctions.getInstance().GetRecordTypeId('Potential Customer','Contact')
            WHERE RecordTypeId =: Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId() 
            AND Email IN : emailAddresses
            // Added Type__c to query looking for Potential Customer value
            AND Type__c = 'Potential Customer']);
        
        Map <String,Id> existingEmails = New Map<String,Id>();
        for (Id idExisting : existingContacts.keySet()){ 
            existingEmails.put(existingContacts.get(idExisting).email, idExisting);
        }
        
        Map <String, Id> m_CompanyToken_AccountID = new Map  <String, Id> ();
        List<Account> lEnterpriseAccount=new List<Account>();
        //system.assert(false,setCompanyToken);
        if(setCompanyToken.size()>0) {
            //  id id_Enterprise_Account_RecordTypeId=GlobalFunctions.getInstance().GetRecordTypeId('Enterprise','Account');
            Id id_Enterprise_Account_RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-Customer').getRecordTypeId() ;
            list<String> lCompanyToken = new List<String>(setCompanyToken);
            //  lEnterpriseAccount=new List<Account>([SELECT id, CompanyToken__c, FROM Account WHERE RecordTypeID= :id_Enterprise_Account_RecordTypeId
            lEnterpriseAccount=new List<Account>([SELECT id, CompanyToken__c, Type FROM Account WHERE RecordTypeID= :id_Enterprise_Account_RecordTypeId
                                                  AND CompanyToken__c IN :lCompanyToken
                                                  AND Type='Enterprise']);
        }
        if(lEnterpriseAccount.size()>0) {
            for(Account ea: lEnterpriseAccount) { 
                m_CompanyToken_AccountID.put(ea.CompanyToken__c,ea.Id);
            }
        }
        
        for (Contact oNewContact : triggerNew) {
            //     if(oNewContact.RecordTypeId == GlobalFunctions.getInstance().GetRecordTypeId('Enterprise Customer','Contact')) {
            if(oNewContact.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId()  && oNewContact.Type__c=='Enterprise Customer') { 
                if(m_CompanyToken_AccountID.containsKey(oNewContact.Title)) {
                    oNewContact.AccountId=m_CompanyToken_AccountID.get(oNewContact.Title);
                }
                else {
                    oNewContact.addError('Can not locate CompanyToken to connect enterprise contact to an account: '+oNewContact.Title);
                }
            }
        }
        
        Map<Id,Account> mapContactIdToAccountLocal = new Map<Id,Account>();
        List<Contact> contactToUpdate = new List<Contact>();
        for (Contact oNewContact : triggerNew) {
            //System.debug('customer id'+Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId());
            /* Moved from line 230 to here, registration type should be updated */
            if (CC_GlobalUtility.IsCompanyEmail(oNewContact.email)) {
                oNewContact.RegistrationType__c = 'Employee';
                oNewContact.Type__c = 'Employee';
            } else {
                //Added By Ankit Kungwani for case 00249338 -Start
                if(oNewContact.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Non-Customer').getRecordTypeId()) {
                oNewContact.RegistrationType__c = '--None--';
                oNewContact.Type__c = '--None--';
                oNewContact.RelayType__c = '--None--';
                }
                else{
                //Added By Ankit Kungwani for case 00249338 -end
                    oNewContact.RegistrationType__c = 'Customer';
                    oNewContact.Type__c = 'Customer';
                }    //Added By Ankit Kungwani for case 00249338 
            } 
            System.debug(oNewContact.RecordTypeId+'   '+oNewContact.Type__c);
            /*if(oNewContact.RecordTypeId != Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId()  && oNewContact.Type__c=='Enterprise Customer') {
                if (existingEmails.containsKey(oNewContact.email)) {
                    System.Debug('FOUND A MATCH');
                    Contact oExistingContact = existingContacts.get(existingEmails.get(oNewContact.email));
                    // Set Record Type to Customer
                    oExistingContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId() ;
                    // Set Dossier values
                    oExistingContact.FirstName = oNewContact.FirstName;
                    oExistingContact.LastName = oNewContact.LastName;
                    oExistingContact.ContactID__c=oNewContact.ContactID__c;
                    oExistingContact.CB_userID__c=oNewContact.CB_userID__c;
                    oExistingContact.LastUpdated__c=datetime.now();
                    oExistingContact.AddressValidated__c = oNewContact.AddressValidated__c;
                    oExistingContact.DateCreated__c = oNewContact.DateCreated__c;
                    oExistingContact.Deactivated__c = oNewContact.Deactivated__c;
                    oExistingContact.FBID__c = oNewContact.FBID__c;
                    oExistingContact.HasOptedOutOfEmail = oNewContact.HasOptedOutOfEmail;
                    oExistingContact.HomePhone = oNewContact.HomePhone;
                    oExistingContact.LanguagePreference__c = oNewContact.LanguagePreference__c;
                    oExistingContact.MailingCity = oNewContact.MailingCity;
                    oExistingContact.MailingCountry = oNewContact.MailingCountry;
                    oExistingContact.MailingPostalCode = oNewContact.MailingPostalCode;
                    oExistingContact.MailingState = oNewContact.MailingState;
                    oExistingContact.MailingStreet = oNewContact.MailingStreet;
                    oExistingContact.OtherPhone = oNewContact.OtherPhone;
                    oExistingContact.Password__c = oNewContact.Password__c;
                    oExistingContact.Qualified__c = oNewContact.Qualified__c;
                    oExistingContact.RegistrationType__c = oNewContact.RegistrationType__c;
                    oExistingContact.Verification_Score__c = oNewContact.Verification_Score__c;
                    oExistingContact.Description = (oExistingContact.Description==null)?('Updated Potential Customer'):(oExistingContact.Description + '\nUpdated Potential Customer');
                        contactToUpdate.add(oExistingContact);
                    oNewContact.email = oNewContact.email + '.DELETEME';
                    oNewContact.LastName = '##DELETE##'; 
                    oNewContact.ContactID__c=null;
                    oNewContact.CB_userID__c=null;
                } else {
                    // Set Record Type to Customer
                    oNewContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId() ;
                    oNewContact.OwnerID = Label.Information_ClearCaptions_UserId; //Information ClearCaptions 
                    if( oNewContact.AccountId == null ) {
                        system.debug('IN 2nd');
                        Account oNewAccount = New Account(name = oNewContact.LastName + ' Household', OwnerID = oNewContact.OwnerID);
                        mapContactIdToAccountLocal.put(oNewContact.Id, oNewAccount);
                    }
                    
                }
            }*/ 
        }
        if(contactToUpdate.size()>0){
            update contactToUpdate;
        }
        if( mapContactIdToAccountLocal.values().size() > 0 ) {
            insert mapContactIdToAccountLocal.values();
            for (Contact oNewContact : triggerNew) {
                if( mapContactIdToAccountLocal.containsKey( oNewContact.Id) && mapContactIdToAccountLocal.get(oNewContact.Id).Id != null ) {
                    oNewContact.AccountID = mapContactIdToAccountLocal.get(oNewContact.Id).Id ;
                }
            }
        }
        
    }
    
    public static void tr_After_Insert(List<Contact> triggerNew, List<Contact> triggerOld, Map<Id,Contact> triggerNewMap, Map<Id,Contact> triggerOldMap) {
        // Because SFDC doesn't handle triggers like normal databases do... 
        Set<Id> st_Id2Delete=new Set<Id>();
        for (Contact oContactNew : TriggerNew) { if(oContactNew.LastName == '##DELETE##') st_Id2Delete.add(oContactNew.Id); }
        if(!st_Id2Delete.isEmpty()) {
            List<Contact> oToDelete = New List<Contact>([select id from Contact where id in :st_Id2Delete]); 
            if(!oToDelete.isEmpty()) delete oToDelete; 
        }
    }
    public static void tr_Before_Update(List<Contact> triggerNew, List<Contact> triggerOld, Map<Id,Contact> triggerNewMap, Map<Id,Contact> triggerOldMap) {
        for (Contact oContactNew : TriggerNew) {
            if (!(UserInfo.getUserId() == CC_GlobalUtility.idBoomi)) {oContactNew.LastUpdated__c = datetime.now(); } // set last updated for the integration
            Contact oContactOld=triggerOldMap.get(oContactNew.Id);
            
            if (oContactNew.RecordTypeId != oContactOld.RecordTypeId ) {
                // Only admins and the Boomi integration user can change Record Types from/to Customer Contact type
                if ((!CC_GlobalUtility.IsAdminUser) && ((UserInfo.getUserId() != CC_GlobalUtility.idBoomi)) && 
                    (oContactNew.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId()
                     || oContactOld.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId())
                   ) {
                       oContactNew.RecordTypeId.addError('You cannot change a Customer Contact Record Type.');
                   }
                // If the Record Type is being changed, set the Registration Type
                if (oContactNew.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId()) {
                    if (CC_GlobalUtility.IsCompanyEmail(oContactNew.email)) {
                        oContactNew.RegistrationType__c = 'Employee';
                    } else {
                        oContactNew.RegistrationType__c = 'Customer';
                    }   
                } else {
                    oContactNew.RegistrationType__c = 'None';
                }
            }     
            if (oContactNew.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId()) {
                // Anybody can change email now: Only the Boomi integration can change Email
                //if (!GlobalFunctions.getInstance().IsBoomiIntegrationUser && (oContactNew.email != oContactOld.email)) {  oContactNew.RecordTypeId.addError('You cannot change a Contact Email.'); }
                if(oContactNew.email != oContactOld.email && (oContactNew.email==null || oContactNew.email=='') ) {  oContactNew.Email.addError('Customer email is required.'); }
                // Make sure Email is not being changed to/from a company email
                if(oContactNew.email != oContactOld.email) {
                    Integer iEmailChange = CC_GlobalUtility.changeToFromCompanyEmail(oContactNew.email, oContactOld.email);
                    if (iEmailChange == 1) {
                        oContactNew.email.addError('You cannot change from a company email to a non-company email');
                    } else if (iEmailChange == 2) {
                        oContactNew.email.addError('You cannot change from a non-company email to a company email');        
                    }
                }
                //handle changed field list
                oContactNew.Changed_Field_List__c=sGetChangedFieldList(oContactNew, oContactOld);
            }
            // Start - Added by Nilesh for Case #00248952
            // Modified By Nilesh Grover for Case #00250384
            if(oContactNew.Deactivated__c != triggerOldMap.get(oContactNew.Id).Deactivated__c) {
                if(oContactNew.Deactivated__c == true){
                    oContactNew.Deactivated_Date__c = DateTime.now();
                }else {
                    oContactNew.Deactivated_Date__c = NULL;                    
                }                                    
            } 
            //Modified By Nilesh Grover for Case #00250384
            // End - Added by Nilesh for Case #00248952
        }
    }
    public static void tr_After_Update(List<Contact> triggerNew, List<Contact> triggerOld, Map<Id,Contact> triggerNewMap, Map<Id,Contact> triggerOldMap) {
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> ();
        List<Application_Log__c> lError_Log=new List<Application_Log__c>();
        Set<string> st_Email=new Set<string>();
        for (Contact oContactNew : TriggerNew) {
            Contact oContactOld=triggerOldMap.get(oContactNew.Id);
            if(oContactNew.Email != oContactOld.Email && oContactNew.Email !=null && oContactNew.Email!='') { st_Email.add(oContactNew.Email); }
        }       
        
        Map<string,Contact>m_Email_ContactExisting=new Map<string,Contact>(); 
        for(Contact oContact: [SELECT Id, Email, RecordType.Name, Type__c FROM contact WHERE Email IN :st_Email AND RecordType.Name IN ('Customer')]) {
            m_Email_ContactExisting.put(oContact.Email.toLowerCase(), oContact); 
        }
        
        for (Contact oContactNew : TriggerNew) {
            Contact oContactOld=triggerOldMap.get(oContactNew.Id);
            if((UserInfo.getUserId() != CC_GlobalUtility.idBoomi) && oContactNew.Email != oContactOld.Email && oContactNew.Email !=null && oContactNew.Email!='') { 
                if(m_Email_ContactExisting.containsKey(oContactNew.Email.toLowerCase())) {
                    if(m_Email_ContactExisting.get(oContactNew.Email.toLowerCase()).Id!=oContactNew.Id) {
                        oContactNew.Email.addError('Duplicate email already used by another "'+m_Email_ContactExisting.get(oContactNew.Email.toLowerCase()).RecordType.Name+'"');
                    }
                }
            }
            
            if(CC_GlobalUtility.VFC009_Contact_ProfileEmailEnabled=='true' && oContactNew.Changed_Field_List__c!=null && oContactNew.Email !=null && oContactNew.RecordTypeId == Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId() ) { //need to send email about profile change
                if(!oContactNew.Email.Contains('noemail.com')) { //only send email to the contacts with valid email addresses
                    if(messages.size()>10) oContactNew.AddError('You can only initiate send of profile update email to up to 10 customers in a single transaction');
                    try {
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.setTemplateID(Label.ContactTriggerHandler_ProfileEmailTemplateId); //Account info Change Notification
                        message.setOrgWideEmailAddressId(Label.ContactTriggerHandler_ProfileOrgEmailId); //support@clearcaptions.com
                        //         message.setReplyTo('support@clearcaptions.com');
                        message.setReplyTo('support@clearcaptionstest.com');
                        //START-->Added by parag bhatt for story S- 637733
                        message.setTargetObjectId(oContactNew.Id);
                        string[] to = new string[] {oContactOld.Email};
                        message.settoaddresses(to);
                        message.setTreatTargetObjectAsRecipient(false); 
                        //END-->Added by parag bhatt for story S- 637733
                        if(oContactNew.Changed_Field_List__c.Contains('Email') && oContactOld.Email !=null && !oContactOld.Email.Contains('noemail.com')) {
                            string[] cc = new string[] {oContactOld.Email};
                                message.setCcAddresses(cc);
                        }       
                        messages.add(message);
                        if(!Test.isRunningTest()) {
                            Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{message});
                            if(!results[0].isSuccess()) {
                                //could not send email
                                lError_Log.add(new Application_Log__c(Name='ContactTriggerHandler.tr_After_Update', Severity_Level__c='ERROR', Message__c='Email delivery error message for profile change notification '+ oContactNew.Email+' : '+ results[0].getErrors()[0].getMessage()));  
                            }
                        }
                    } catch(Exception e) {
                        lError_Log.add(new Application_Log__c(Name='ContactTriggerHandler.tr_After_Update', Severity_Level__c='ERROR', Message__c='Email delivery error message for profile change notification '+ oContactNew.Email+' : '+ e.getMessage()));  
                    }      
                }
            }
        }
        if(!lError_Log.isEmpty()) insert lError_Log;         
    }
    
    public static string sGetChangedFieldList(Contact ct_new, Contact ct_old) {
        string sReturn=null;
        if(ct_new==null || ct_old==null) return sReturn;
        if(ct_new.Deactivated__c==true) return sReturn; //no need to notify deactivated customers
        if(ct_new.RecordTypeId!=ct_old.RecordTypeId) return sReturn; //no need to notify customers during conversion
        if(ct_new.Certified__c==false) return sReturn; //Do not send notification emails to uncertified customers -KMH 06032019
        if(ct_new.Type__c == 'Potential Customer') return sReturn;
        /* process changes in monitored fields
Password
Username (for VRS only)
Adddress (Street 1, Street 2, City, State, Zip)
Email Address
Phone
ClearCaptions Voice Number (Other Phone)
Assisted Number (Home Phone)
First or Last Name
CPNI*/
        
        if(ct_new.Password__c != ct_old.Password__c) {sReturn=(sReturn==null)?('Password'):(sReturn+', '+'Password');}
        if(
            !bSameString(ct_new.MailingStreet,ct_old.MailingStreet)
            || !bSameString(ct_new.MailingCity,ct_old.MailingCity)
            || !bSameString(ct_new.MailingState,ct_old.MailingState)
            || !bSameString(ct_new.MailingPostalCode,ct_old.MailingPostalCode)
        ) {sReturn=(sReturn==null)?('Address'):(sReturn+', '+'Address');}
        if(!bSameString(ct_new.Email,ct_old.Email)) {sReturn=(sReturn==null)?('Email'):(sReturn+', '+'Email');}
        if(!bSameString(ct_new.FirstName,ct_old.FirstName)) {sReturn=(sReturn==null)?('First Name'):(sReturn+', '+'First Name');}
        if(!bSameString(ct_new.LastName,ct_old.LastName)) {sReturn=(sReturn==null)?('Last Name'):(sReturn+', '+'Last Name');}
        if(ct_new.CPNI_Opt_In__c != ct_old.CPNI_Opt_In__c) {sReturn=(sReturn==null)?('CPNI Opt In'):(sReturn+', '+'CPNI Opt In');}
        if(!bSameString(ct_new.Phone,ct_old.Phone)) {sReturn=(sReturn==null)?('Phone'):(sReturn+', '+'Phone');}
        if(!bSameString(ct_new.OtherPhone,ct_old.OtherPhone)) {sReturn=(sReturn==null)?('Phone'):(sReturn+', '+'Phone');}
        if(!bSameString(ct_new.HomePhone,ct_old.HomePhone)) {sReturn=(sReturn==null)?('Phone'):(sReturn+', '+'Phone');}
        if(!bSameString(ct_new.MobilePhone,ct_old.MobilePhone)) {sReturn=(sReturn==null)?('Phone'):(sReturn+', '+'Phone');}
        return sReturn; 
    }
    public static boolean bSameString(string s1, string s2) {
        if(s1==null) {
            if(s2==null) return true; 
            else return false; 
        } else {
            if(s2==null) return false; 
            else return s1.replaceall('\\s','').equalsIgnoreCase(s2.replaceall('\\s',''));
        }
    }
    //END----------------------
    
    
    /*
    public static void createRetentionCaseDate(List<Contact> triggerNew, List<Contact> triggerOld, Map<Id,Contact> triggerNewMap, Map<Id,Contact> triggerOldMap){
        
        String customerRecId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        List<Contact> contactsToUpdate = new List<Contact>();
        
        for(Contact ct: triggerNew){
            System.debug('@@@--->'+ct.RecordTypeId+customerRecId+ct.DoNotCall+ct.Active__c+ct.Customer_Segmentation__c+ct.Billable_Mins_Prior01Month__c+ct.DateCreated__c+system.today().addDays(-15));
        
              if(ct.RecordTypeId == customerRecId && !ct.DoNotCall && ct.Active__c && !ct.Want_to_create_case__c
               && (ct.Customer_Segmentation__c == 'Super User' || ct.Customer_Segmentation__c == 'Power User' || ct.Customer_Segmentation__c == 'Heavy User' ||
                   ct.Customer_Segmentation__c == 'Regular User' || ct.Customer_Segmentation__c == 'Light User') && ct.Number_of_Active_Assets__c > 0 && ct.Certified_On__c !=null && ct.Certified_On__c < system.today().addDays(-15)){//If condition modified - DateCreated__c to Certified_On__c by Kiran for Case - 00263833 on 17/6/2019 
                            
                       system.debug('ct.Latest_Retention_Detail_Date__c: '+ct.Latest_Retention_Detail_Date__c);
                       system.debug('ct.Retention_Case_Created_Date__c: '+ct.Retention_Case_Created_Date__c);
                       system.debug('ct.Most_Recent_Billable_Call__c: '+ct.Most_Recent_Billable_Call__c);
                       system.debug('ct.Last_Check_in__c: '+ct.Last_Check_in__c);
                       system.debug('ct.Max_Check_in__c: '+ct.Max_Check_in__c);
                       If(ct.Latest_Retention_Detail_Date__c == null && ct.Retention_Case_Created_Date__c == null){
                           if(ct.Most_Recent_Billable_Call__c != null 
                              && ct.Last_Check_in__c !=null 
                              && (ct.Most_Recent_Billable_Call__c != triggerOldMap.get(ct.Id).Most_Recent_Billable_Call__c || ct.Last_Check_in__c != triggerOldMap.get(ct.Id).Last_Check_in__c)
                              && (ct.Max_Check_in__c == null || ct.Most_Recent_Billable_Call__c.addHours(72) > ct.Max_Check_in__c || ct.Last_Check_in__c.addHours(72) > ct.Max_Check_in__c)
                             ){
                                 Contact c = getConToBeUpdated(ct);
                                 c.Max_Check_in__c = ct.Most_Recent_Billable_Call__c > ct.Last_Check_in__c ? ct.Most_Recent_Billable_Call__c.addHours(72) : ct.Last_Check_in__c.addHours(72);
                                 c.Want_to_create_case__c = true;
                                 //ct.Max_Check_in__c = ct.Most_Recent_Billable_Call__c > ct.Last_Check_in__c ? ct.Most_Recent_Billable_Call__c.addHours(72) : ct.Last_Check_in__c.addHours(72);
                                 contactsToUpdate.add(c);
                             }
                           else if(ct.Most_Recent_Billable_Call__c == null && ct.Last_Check_in__c !=null && ct.Last_Check_in__c != triggerOldMap.get(ct.Id).Last_Check_in__c){
                               Contact c = getConToBeUpdated(ct);
                               c.Max_Check_in__c = ct.Last_Check_in__c.addHours(71);
                               c.Want_to_create_case__c = true;
                               contactsToUpdate.add(c);                        
                           }
                           else if(ct.Most_Recent_Billable_Call__c != null && ct.Last_Check_in__c ==null && ct.Most_Recent_Billable_Call__c != triggerOldMap.get(ct.Id).Most_Recent_Billable_Call__c ){
                               Contact c = getConToBeUpdated(ct);
                               c.Max_Check_in__c = ct.Most_Recent_Billable_Call__c.addHours(71);
                               c.Want_to_create_case__c = true;
                               contactsToUpdate.add(c); 
                           }
                       }
                       else if(ct.Latest_Retention_Detail_Date__c != null && ct.Retention_Case_Created_Date__c > ct.Latest_Retention_Detail_Date__c
                               && ct.Retention_Case_Created_Date__c != null 
                               && ct.Retention_Case_Created_Date__c != triggerOldMap.get(ct.Id).Retention_Case_Created_Date__c 
                               && ct.Retention_Case_Created_Date__c > ct.Max_Check_in__c ){
                                   Contact c = getConToBeUpdated(ct);
                                   //ct.Max_Check_in__c = ct.Latest_Retention_Detail_Date__c;
                                   c.Max_Check_in__c = ct.Retention_Case_Created_Date__c.addDays(30);
                                   c.Want_to_create_case__c = true;
                                   contactsToUpdate.add(c);
                               }
                       else if(ct.Latest_Retention_Detail_Date__c != null && ct.Latest_Retention_Detail_Date__c != triggerOldMap.get(ct.Id).Latest_Retention_Detail_Date__c && ct.Latest_Retention_Detail_Date__c > ct.Max_Check_in__c){
                           Contact c = getConToBeUpdated(ct);
                           //ct.Max_Check_in__c = ct.Latest_Retention_Detail_Date__c;
                           c.Max_Check_in__c = ct.Latest_Retention_Detail_Date__c;
                           c.Want_to_create_case__c = true;
                           contactsToUpdate.add(c);
                       }
                   }
            else if(ct.DateCreated__c !=null && ct.DateCreated__c > system.today().addDays(-15) && ct.DateCreated__c != triggerOldMap.get(ct.Id).DateCreated__c){
                Contact c = getConToBeUpdated(ct);
                c.Max_Check_in__c = ct.DateCreated__c.addDays(15);
                c.Want_to_create_case__c = true;
                contactsToUpdate.add(c);
            }
            
            
        }
        
        if(!contactsToUpdate.isEmpty()){
            update contactsToUpdate;
        }
        
    }
    */
    public static Contact getConToBeUpdated(Contact ct){
        Contact c = new Contact();
        c.id = ct.id;
        return c;
    }
    
    public static void updateCombinedUsageOnAccount(List<Contact> newList, Map<Id,Contact> oldMap){
        Set<Id> accountIdSet = new Set<Id>();
        List<Account> accToUpdate = new List<Account>();
        Map<Id, Decimal> mapAccountIdToTotalHours = new Map<ID,Decimal>();
        for(Contact con: newList){
            if(con.AccountId != null && con.Total_Lifetime_Billable_Mins__c != null && (Oldmap == null || Oldmap.get(con.id).Total_Lifetime_Billable_Mins__c != con.Total_Lifetime_Billable_Mins__c)){
                accountIdSet.add(con.accountId);
            }
        }
        //system.debug('accountIdSet'+accountIdSet);
        if(accountIdSet.size()>0){
            For(Contact c: [SELECT Id,Total_Lifetime_Billable_Mins__c,accountId FROM Contact WHERE AccountId =: accountIdSet]){
                if(c.Total_Lifetime_Billable_Mins__c != null){
                    if(!mapAccountIdToTotalHours.containsKey(c.AccountId)){
                        mapAccountIdToTotalHours.put(c.AccountId,0.00);
                    }
                    mapAccountIdToTotalHours.put(c.AccountId,mapaccountIdToTotalHours.get(c.accountId)+ c.Total_Lifetime_Billable_Mins__c); 
                }
            }
        }
        //system.debug('mapAccountIdToTotalHours'+mapAccountIdToTotalHours);
        if(mapAccountIdToTotalHours.size()>0){
            for(Id accId: mapAccountIdToTotalHours.keySet()){
                Account a = new Account();
                a.id = accId;
                a.Combined_usage_for_household__c = mapAccountIdToTotalHours.get(accId);
                accToUpdate.add(a);
            }
        }
        //system.debug('accToUpdate'+accToUpdate);
        if(accToUpdate.size()>0){
            update accToUpdate;
        }
    }    
    
    // c-00249602 1/23/19 NCarson - remove legacy form on file status when form is deleted
    public static void updateLegacyPCFStatus(List<Contact> triggerNew, Map<Id,Contact> triggerOldMap) {
        for (Contact contact : triggerNew) {
            Contact contactBeforeUpdate = triggerOldMap.get(contact.Id);
            if (String.isBlank( contact.PCF_Form__c) && !String.isBlank( contactBeforeUpdate.PCF_Form__c)) {
                contact.Legacy_Has_Certification_Form_on_File__c = false;
            }
        }
    }
    // end c-00249602 1/23/19 NCarson
    
    // MKLICH S-628700 Start
    public static void updateNumberOfActiveAssets(List<Contact> triggerNew, Map<Id, Contact> triggerNewMap) {
        System.debug('LOOK HERE ' + triggerNewMap);
        if (triggerNewMap != null) {
            Map<Id, AggregateResult> resultMap = new Map<Id, AggregateResult>([SELECT ContactId Id, COUNT(Id) totalActiveAssets FROM Asset WHERE contactID IN :triggerNewMap.keyset() AND (Deleted__c = False AND CB_Active__c = True) Group By ContactId]);
            for(Contact con : triggerNew){
                con.Number_of_Active_Assets__c = resultMap.containsKey(con.ID) ? Integer.valueOf(resultMap.get(con.Id).get('totalActiveAssets')) : 0;
            }
        }
        else { 
            for(Contact con : triggerNew){
                con.Number_of_Active_Assets__c = 0;
            } 
        }     
    }
    // MKLICH S-628700 Stop  

    
}