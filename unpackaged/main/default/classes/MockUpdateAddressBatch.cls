@isTest
global class MockUpdateAddressBatch implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest req) {
        System.assertEquals('http://ccws.test.clearcaptions.com/user/update', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
		
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        res.setBody('data={"id": "'+''+'","userID": "'+''+'",'+''+'}');
        res.setStatusCode(200);
        return res;
        
    }

}